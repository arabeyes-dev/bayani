#include <qapplication.h>
#include <qmainwindow.h>
#include <qtable.h>
#include <qtabdialog.h>
#include <qpushbutton.h>
#include <qvbox.h>

using namespace std;

class GUI: public QMainWindow
{
  Q_OBJECT
public:
  GUI();
  ~GUI();

public slots:
void ShowTabDialog();

protected:
  QTable * MyTable;
  QTabDialog * MyTabDialog;
  QPushButton * MyPushButton;
  QVBox * MyQVBox;
};
