QTDIR = /usr/lib/qt3/

moc_%.cc: %.h
	$(QTDIR)/bin/moc $< -o $@

CC = g++
LINK = g++
CFLAGS = -c

LINKFLAGS = -Wl,-rpath,$(QTDIR)/lib/

INCLUDEPATHS = -I$(QTDIR)/include/

LIBSPATHS = -L$(QTDIR)/lib/ -L/usr/X11R6/lib/
LIBS = -lqt-mt -lm -lX11 -lXext -lGL -lfreetype -lXmu -lICE -lSM -lXrender -lXft -lXt

OBJS =  qt-bug.o moc_qt-bug.o

PROGS = qt-bug

all: $(PROGS)

qt-bug.o: qt-bug.cc
	$(CC) $(CFLAGS) $(INCLUDEPATHS) $< -o $@

moc_qt-bug.o: moc_qt-bug.cc
	$(CC) $(CFLAGS) $(INCLUDEPATHS) $< -o $@

qt-bug: $(OBJS)
	$(LINK) $(LINKFLAGS) $^ $(LIBSPATHS$) $(LIBS) -o $@
