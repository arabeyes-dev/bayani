#include "qt-bug.h"

GUI::GUI()
  :QMainWindow()
{
  MyPushButton = new QPushButton("Show QTabDialog", this);
  MyPushButton->setFixedSize(MyPushButton->sizeHint());
  
  MyQVBox = new QVBox(this);
  MyQVBox->setSpacing(5);
  MyQVBox->setMargin(5);
  
  MyTable = new QTable(5, 2, MyQVBox);
  MyTable->horizontalHeader()->setLabel(0, "Column 0");
  MyTable->horizontalHeader()->setLabel(1, "Column 1");

  MyTabDialog = new QTabDialog(this);
  MyTabDialog->setOKButton();
  MyTabDialog->setApplyButton();
  MyTabDialog->setCancelButton();
  MyTabDialog->addTab(MyQVBox, "");
  MyTabDialog->setFixedSize(MyTabDialog->sizeHint());
  
  connect(MyPushButton, SIGNAL(clicked()), this, SLOT(ShowTabDialog()));
}

GUI::~GUI()
{
}

void GUI::ShowTabDialog()
{
  MyTabDialog->show();
}

int main(int argc, char ** argv)
{
  QApplication MyApp(argc, argv);
  GUI MyGUI;
  
  //MyApp.setReverseLayout(false);
  MyApp.setReverseLayout(true);
  
  MyGUI.setGeometry(0, 0, 150, 50);

  MyApp.setMainWidget(&MyGUI);
  
  MyGUI.show();

  return MyApp.exec();
}
