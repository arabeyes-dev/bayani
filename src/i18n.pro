#--
# Bayani 'pro' file (the file that generates 'ts' (translation) files).
# $Id$
#--

HEADERS         = bayani-core/bexpression.h
SOURCES         = bayani-core/fit.cc bayani-core/bexpression.cc \
                  bayani-qt/frame.cc bayani-qt/fitscontainer.cc bayani-qt/qtexteditbayani.cc bayani-qt/qfontselector.cc bayani-qt/qfileselector.cc bayani-qt/btextbrowser.cc bayani-qt/bxmltoqlvhandler.cc \
                  bayani-qtgui/qtabdialogframezone.cc bayani-qtgui/qtabdialogframesetactiveplot.cc bayani-qtgui/qtabdialogtablefillfromfile.cc bayani-qtgui/qtabdialogtablewritetofile.cc bayani-qtgui/qtabdialogtableswap.cc bayani-qtgui/qtabdialogtablemanage.cc bayani-qtgui/qtabdialogtableempty.cc bayani-qtgui/qtabdialoggraphfunction.cc bayani-qtgui/qtabdialoggraphhistogram.cc bayani-qtgui/qtabdialoggraphgraph2d.cc bayani-qtgui/qtabdialoggraphfits.cc bayani-qtgui/qtabdialoggraphbounds.cc bayani-qtgui/qtabdialoggraphaxes.cc bayani-qtgui/qtabdialoggraphfit.cc bayani-qtgui/qtabdialogcomputationvariables.cc bayani-qtgui/qtabdialogcomputationcompute.cc bayani-qtgui/qtabdialogtextsframe.cc bayani-qtgui/qtabdialogtextsgraph.cc bayani-qtgui/qtabdialogsettingsglobal.cc bayani-qtgui/qtabdialogsettingsframe.cc bayani-qtgui/qtabdialogsettingsgraph.cc bayani-qtgui/qtabdialogsettingsdata.cc bayani-qtgui/bhelpbrowser.cc bayani-qtgui/qtabdialoghelpabout.cc bayani-qtgui/gui.cc \
	          bayani.cc
TRANSLATIONS    = ts/ar.ts ts/fr.ts
