/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTextEditBayani Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtexteditbayani.h>

QTextEditBayani::QTextEditBayani(QWidget * Parent, const char * Name)
  : QTextEdit(Parent, Name)
{
  SetTextColor        (QColor("#CCCCCC"));
  SetInformationColor (QColor("#55AA7F"));
  SetWarningColor     (QColor("#FFAA00"));
  SetCriticalColor    (QColor("#FF0000"));
  SetStatusColor      (QColor("#55AAFF"));
  SetBGColor          (QColor("#000000"));
  SetTextFont         (font());
  
  HistoryIndex   = 0;
  Para           = 0;
  Index          = 0;
  PromptString   = trUtf8("Bayani> ");
  PromptEndIndex = PromptString.length()-1;
  
  //setWrapPolicy(QTextEdit::Anywhere);
  setWrapPolicy(QTextEdit::AtWordOrDocumentBoundary);
  
  connect(this, SIGNAL(clicked(int, int)), this, SLOT(CheckMouseSlot(int, int)));
  
  PrintPrompt();
  
  getCursorPosition(&Para, &Index);
}

QTextEditBayani::~QTextEditBayani()
{
}

void QTextEditBayani::PrintInformation(QString Message)
{
  append(QString("<font color=%1>%2</font>") .arg(InformationColor.name()) .arg(Message));
}

int QTextEditBayani::PrintWarning(QString Message)
{
  append(QString("<font color=%1>%2<br />%3</font>") .arg(WarningColor.name()) .arg(Message) .arg(trUtf8("[Y/N]")));
  
  return QMessageBox::No;
}

void QTextEditBayani::PrintCritical(QString Message)
{
  append(QString("<font color=%1>%2</font>") .arg(CriticalColor.name()) .arg(Message));
}

void QTextEditBayani::PrintStatus(QString Message)
{
  append(QString("<font color=%1>%2</font>") .arg(StatusColor.name()) .arg(Message));
}

void QTextEditBayani::SetHistory(vector <QString> NewCommandsHistory)
{
  CommandsHistory = NewCommandsHistory;
  HistoryIndex    = CommandsHistory.size();
}

vector <QString> QTextEditBayani::GetHistory()
{
  return CommandsHistory;
}

void QTextEditBayani::SetTextColor(QColor NewTextColor)
{
  TextColor = NewTextColor;
  setPaletteForegroundColor(NewTextColor);
}

void QTextEditBayani::SetInformationColor(QColor NewInformationColor)
{
  InformationColor = NewInformationColor;
}

void QTextEditBayani::SetWarningColor(QColor NewWarningColor)
{
  WarningColor = NewWarningColor;
}

void QTextEditBayani::SetCriticalColor(QColor NewCriticalColor)
{
  CriticalColor = NewCriticalColor;
}

void QTextEditBayani::SetStatusColor(QColor NewStatusColor)
{
  StatusColor = NewStatusColor;
}

void QTextEditBayani::SetBGColor(QColor NewBGColor)
{
  QBrush CurrentPaper = paper();
  
  CurrentPaper.setColor(NewBGColor);
  
  setPaper(CurrentPaper);
}

void QTextEditBayani::SetTextFont(QFont NewTextFont)
{
  setFont(NewTextFont);
}

void QTextEditBayani::CheckMouseSlot(int NewPara, int NewIndex)
{
  if(NewIndex <= PromptEndIndex || NewPara != Para)
    setCursorPosition(Para, Index);
  else
    getCursorPosition(&Para, &Index);  
}

void QTextEditBayani::PrintPrompt()
{
  append(QString("<font color=%1>%2</font>") .arg(TextColor.name()) .arg(PromptString));
  moveCursor(QTextEdit::MoveEnd, false);  
}

void QTextEditBayani::keyPressEvent(QKeyEvent * Event)
{
  int NewPara = 1;
  int NewIndex = 1;
  
  getCursorPosition(&NewPara, &NewIndex);
  
  if(Event->key() ==  Qt::Key_Return || Event->key() == Qt::Key_Enter)
    {
      QString Command = text(Para);
      
      Command = Command.remove(0, PromptString.length());
      
      if(Command != "" && Command.simplifyWhiteSpace() != "")
	{
	  Command = Command.remove(Command.length()-1, 1);/* Needed because a whitespace is added a the end of the command?! */
	  CommandsHistory.push_back(Command);
	  HistoryIndex = CommandsHistory.size();
	  
	  emit CommandEntered(Command);
	}
      
      PrintPrompt();
    }
  else if(Event->key() == Qt::Key_Backspace)
    {
      if(NewIndex > PromptEndIndex+1)
	doKeyboardAction(QTextEdit::ActionBackspace);
    }
  else if(Event->key() == Qt::Key_Delete)
    {
      doKeyboardAction(QTextEdit::ActionDelete);
    }
  else if(Event->key() == Qt::Key_Left)
    {
      if(NewIndex > PromptEndIndex+1)
	moveCursor(QTextEdit::MoveBackward, false);
    }
  else if(Event->key() == Qt::Key_Right)
    {
      moveCursor(QTextEdit::MoveForward, false);
    }
  else if(Event->key() == Qt::Key_Up)
    {
      if(HistoryIndex > 0)
	{
	  HistoryIndex--;
	  
	  setCursorPosition(Para, PromptEndIndex+1);
	  doKeyboardAction(QTextEdit::ActionKill);
	  
	  insert(CommandsHistory[HistoryIndex]);
	  moveCursor(QTextEdit::MoveEnd, false);
	}
    }
  else if(Event->key() == Qt::Key_Down)
    {
      if(!CommandsHistory.empty())
	{
	  if(HistoryIndex < CommandsHistory.size())
	    {
	      HistoryIndex++;
	      
	      setCursorPosition(Para, PromptEndIndex+1);
	      doKeyboardAction(QTextEdit::ActionKill);
	      
	      if(HistoryIndex < CommandsHistory.size())
		{
		  insert(CommandsHistory[HistoryIndex]);
		  moveCursor(QTextEdit::MoveEnd, false);
		}
	    }
	}
    }
  else
    {
      insert(Event->text());
    }
  
  getCursorPosition(&Para, &Index);
}

void QTextEditBayani::showEvent(QShowEvent *)
{
  emit Shown();
}

void QTextEditBayani::closeEvent(QCloseEvent * Event)
{
  Event->accept();
  
  emit Closed();
}
