/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QPushButtonColorSelect Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QPUSHBUTTONCOLORSELECT_H
#define BAYANI_QPUSHBUTTONCOLORSELECT_H

/* QT headers */
#include <qobject.h>
#include <qwidget.h>
#include <qpushbutton.h>
#include <qpixmap.h>
#include <qcolor.h>
#include <qcolordialog.h>

/*!
  \file qpushbuttoncolorselect.h
  \brief Header file for the QPushButtonColorSelect class.
  
  The QPushButtonColorSelect class is defined in this file.
*/

//! A color selector.
/*!
  This class inherits QPushButton. It gives the user the ability to select a specific color.
*/
class QPushButtonColorSelect: public QPushButton
{
  Q_OBJECT
  
 protected:
  QColor  * Color;
  QPixmap * Pixmap;
  
  void ChangeColor();
  
 public:
                 QPushButtonColorSelect(QWidget * Parent = 0, const char * Name = 0);
  virtual       ~QPushButtonColorSelect();
          void   SetColor              (QColor);
	  QColor GetColor              ();
  
 protected slots:
  void OpenDialog();
};

#endif
