/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QFileSelector Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QFILESELECTOR_H
#define BAYANI_QFILESELECTOR_H

/* QT headers */
#include <qnamespace.h>
#include <qobject.h>
#include <qwidget.h>
#include <qstring.h>
#include <qhbox.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qfiledialog.h>

/*!
  \file qfileselector.h
  \brief Header file for the QFileSelector class.
  
  The QFileSelector class is defined in this file.
*/

//! A filename selector.
/*!
  This class inherits QHBox. It contains a QLineEdit and a QPushButton and gives the user the ability to type or select a filename.
*/
class QFileSelector: public QHBox
{
  Q_OBJECT
  
 protected:
  bool          OpenTypeFlag;
  QString       FileTypes;
  QLineEdit   * LineEdit;
  QPushButton * PushButton;
  
 public:
                  QFileSelector(QWidget * Parent = 0, const char * Name = 0);
  virtual        ~QFileSelector();
          void    SetFileName  (QString);
	  QString GetFileName  ();
	  void    AddFileType  (QString);
	  void    SetOpenType  (bool);
	  void    SetR2L       (bool);
  
 protected slots:
  void OpenDialog();
};

#endif
