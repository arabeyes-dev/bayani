/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QPushButtonColorSelect Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qpushbuttoncolorselect.h>

QPushButtonColorSelect::QPushButtonColorSelect(QWidget * Parent, const char * Name)
  : QPushButton(Parent, Name)
{
  Pixmap = new QPixmap(50, 16);
  Color  = new QColor(0, 0, 0);
  
  ChangeColor();
  
  setFixedSize(sizeHint());
  
  connect(this, SIGNAL(clicked()), this, SLOT(OpenDialog()));
}

QPushButtonColorSelect::~QPushButtonColorSelect()
{
}

void QPushButtonColorSelect::SetColor(QColor NewColor)
{
  *Color = NewColor;
  
  ChangeColor();
}

QColor QPushButtonColorSelect::GetColor()
{
  return *Color;
}

void QPushButtonColorSelect::ChangeColor()
{
  Pixmap->fill(*Color);
  
  setPixmap(*Pixmap);
}

void QPushButtonColorSelect::OpenDialog()
{
  QColor TestColor = QColorDialog::getColor(*Color, this);

  if(TestColor.isValid())
    {
      *Color = TestColor;

      ChangeColor();
    }
}
