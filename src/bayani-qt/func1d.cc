/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the Func1D Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <func1d.h>

Func1D::Func1D()
  : BBDO()
{
  LineWidth = 1;
  LineStyle = Qt::SolidLine;
  LineColor.setNamedColor("#000000");
  
  InternalExpression = NULL;
}

Func1D::~Func1D()
{
  if(InternalExpression)
    delete InternalExpression;
}

void Func1D::SetLineWidth(int NewLineWidth)
{
  LineWidth = NewLineWidth;
}

void Func1D::SetLineColor(QColor NewLineColor)
{
  LineColor = NewLineColor;
}

void Func1D::SetLineStyle(Qt::PenStyle NewLineStyle)
{
  LineStyle = NewLineStyle;
}

void Func1D::SetData(BExpression * NewInternalExpression)
{
  if(InternalExpression)
    delete InternalExpression;
  
  InternalExpression = NewInternalExpression;
}

void Func1D::SetXMinXMax(double NewXMin, double NewXMax)
{
  XMin = NewXMin;
  XMax = NewXMax;
}

void Func1D::ComputeMinMax()
{
  double Step = fabs((XMax-XMin)/100.);/* We compute in 100+1 points only */
  
  for(double i = XMin; i <= XMax; i += Step)
    {
      InternalExpression->setVariable(QObject::trUtf8("x", "Variable"), i);
      InternalExpression->evaluate();
      
      if(i == XMin)
	{
	  YMin = InternalExpression->value();
	  YMax = InternalExpression->value();
	}
      else
	{
	  if(InternalExpression->value() < YMin)
	    YMin = InternalExpression->value();
	  
	  if(InternalExpression->value() > YMax)
	    YMax = InternalExpression->value();
	}
    }
}

bool Func1D::DrawSelf(Graph * ParentGraph)
{
  int InfValue = ParentGraph->XValue2Pixel(XMin);
  int SupValue = ParentGraph->XValue2Pixel(XMax);
  
  if(ParentGraph->GetXSup() < XMax)
    SupValue = ParentGraph->XValue2Pixel(ParentGraph->GetXSup());
  
  if(ParentGraph->GetXInf() > XMin)
    InfValue = ParentGraph->XValue2Pixel(ParentGraph->GetXInf());
  
  QPointArray NewPointArray;
  
  /* In case axes are reversed */
  
  if(InfValue > SupValue)
    {
      int TmpValue = InfValue;
      
      InfValue = SupValue;
      SupValue = TmpValue;
    }
  
  int Counter = 0;
  
  bool ErrorFlag = false;
  
  for(int i = InfValue; i <= SupValue; i++)
    {
      double XValue = ParentGraph->XPixel2Value(i);
      
      /*
	Protection (bad precision when using transformations back and forth...) 
	So, this test is needed when plotting a function defined in a domain only.
	Nevertheless, this doesn't mean that function is not drawn precisely.
	We simply avoid issues that may occur at the borders (if XMin or XMax are close or equal to
	the definition domain limits). Otherwise, the function value for a given pixel is correct.
      */
      if(XValue < XMin || XValue > XMax)
	continue;
      
      InternalExpression->setVariable(QObject::trUtf8("x", "Variable"), XValue);
      
      if(!InternalExpression->evaluate() && !ErrorFlag)
	{
	  ErrorFlag = true;
	  
	  setError(InternalExpression->error());
	}
      
      NewPointArray.putPoints(Counter, 1, i, ParentGraph->YValue2Pixel(InternalExpression->value()));
      
      Counter++;
    }
  
  ParentGraph->AddPolyline(NewPointArray, LineWidth, LineColor, LineStyle);
  
  return !ErrorFlag;
}
