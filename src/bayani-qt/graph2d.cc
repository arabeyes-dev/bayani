
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the Graph2D Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <graph2d.h>

Graph2D::Graph2D()
  : BBDO()
{
  DataRatio = 0.01;
  DataType  = FILCIRCLE;
  DataColor.setNamedColor("#000000");
  
  ErrorsLineWidth = 1;
  ErrorsLineStyle = Qt::SolidLine;
  ErrorsLineColor.setNamedColor("#000000");
}

Graph2D::~Graph2D()
{
}

void Graph2D::SetDataRatio(double NewDataRatio)
{
  DataRatio = NewDataRatio;
}

void Graph2D::SetDataType(int NewDataType)
{
  DataType = NewDataType;
}

void Graph2D::SetDataColor(QColor NewDataColor)
{
  DataColor = NewDataColor;
}

void Graph2D::SetErrorsLineWidth(int NewErrorsLineWidth)
{
  ErrorsLineWidth = NewErrorsLineWidth;
}

void Graph2D::SetErrorsLineStyle(Qt::PenStyle NewErrorsLineStyle)
{
  ErrorsLineStyle = NewErrorsLineStyle;
}

void Graph2D::SetErrorsLineColor(QColor NewErrorsLineColor)
{
  ErrorsLineColor = NewErrorsLineColor;
}

void Graph2D::SetData(DataArray NewInternalDataArray)
{
  InternalDataArray = NewInternalDataArray;
}

int Graph2D::GetEntries()
{
  return InternalDataArray.GetSize();
}

double Graph2D::GetMean()
{
  return InternalDataArray.GetMeanY();
}

double Graph2D::GetSigma()
{
  return InternalDataArray.GetSigmaY();
}

DataArray Graph2D::GetData()
{
  return InternalDataArray;
}

void Graph2D::ComputeMinMax()
{
  InternalDataArray.Compute();
  
  XMin = InternalDataArray.GetXValuePlusErrorMin();
  XMax = InternalDataArray.GetXValuePlusErrorMax();
  YMin = InternalDataArray.GetYValuePlusErrorMin();
  YMax = InternalDataArray.GetYValuePlusErrorMax();
}

bool Graph2D::DrawSelf(Graph * ParentGraph)
{
  bool XErrorDownFlag = false;
  bool YErrorDownFlag = false;
  bool XErrorUpFlag   = false;
  bool YErrorUpFlag   = false;
  
  double XErrorDownValue = -1.;
  double YErrorDownValue = -1.;
  double XErrorUpValue   = -1.;
  double YErrorUpValue   = -1.;
  
  int Size = int(DataRatio*sqrt(double(ParentGraph->width()*ParentGraph->height())));
  
  int XAxisDirection = 1;
  int YAxisDirection = -1;
  
  if(ParentGraph->XValue2Pixel(2.) < ParentGraph->XValue2Pixel(1.))
    XAxisDirection = -1;
  if(ParentGraph->YValue2Pixel(2.) > ParentGraph->YValue2Pixel(1.))
    YAxisDirection = 1;
  
  for(int i = 0; i < InternalDataArray.GetSize(); i++)
    {
      ParentGraph->AddShape(ParentGraph->XValue2Pixel(InternalDataArray.GetX(i)), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i)), Size, DataColor, DataType);
      
      if(InternalDataArray.GetXErrorDown(i) > 0.)
	{
	  XErrorDownFlag  = true;
	  XErrorDownValue = InternalDataArray.GetX(i)-InternalDataArray.GetXErrorDown(i);
	}
      else
	XErrorDownFlag = false;
      
      if(InternalDataArray.GetYErrorDown(i) > 0.)
	{
	  YErrorDownFlag  = true;
	  YErrorDownValue = InternalDataArray.GetY(i)-InternalDataArray.GetYErrorDown(i);
	}
      else
	YErrorDownFlag = false;
      
      if(InternalDataArray.GetXErrorUp(i) > 0.)
	{
	  XErrorUpFlag  = true;
	  XErrorUpValue = InternalDataArray.GetX(i)+InternalDataArray.GetXErrorUp(i);
	}
      else
	XErrorUpFlag = false;
      
      if(InternalDataArray.GetYErrorUp(i) > 0.)
	{
	  YErrorUpFlag  = true;
	  YErrorUpValue = InternalDataArray.GetY(i)+InternalDataArray.GetYErrorUp(i);
	}
      else
	YErrorUpFlag = false;
      
      if(XErrorDownFlag)
	{
	  ParentGraph->AddLine(ParentGraph->XValue2Pixel(XErrorDownValue), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i)), ParentGraph->XValue2Pixel(InternalDataArray.GetX(i))-int(XAxisDirection*0.5*Size), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i)), ErrorsLineWidth, ErrorsLineColor, ErrorsLineStyle, true);
	  
	  ParentGraph->AddLine(ParentGraph->XValue2Pixel(XErrorDownValue), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i))-int(0.5*Size), ParentGraph->XValue2Pixel(XErrorDownValue), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i))+int(0.5*Size), ErrorsLineWidth, ErrorsLineColor, ErrorsLineStyle, true);
	}
      
      if(YErrorDownFlag)
	{
	  ParentGraph->AddLine(ParentGraph->XValue2Pixel(InternalDataArray.GetX(i)), ParentGraph->YValue2Pixel(YErrorDownValue), ParentGraph->XValue2Pixel(InternalDataArray.GetX(i)), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i))-int(YAxisDirection*0.5*Size), ErrorsLineWidth, ErrorsLineColor, ErrorsLineStyle, true);
	  
	  ParentGraph->AddLine(ParentGraph->XValue2Pixel(InternalDataArray.GetX(i))-int(0.5*Size), ParentGraph->YValue2Pixel(YErrorDownValue), ParentGraph->XValue2Pixel(InternalDataArray.GetX(i))+int(0.5*Size), ParentGraph->YValue2Pixel(YErrorDownValue), ErrorsLineWidth, ErrorsLineColor, ErrorsLineStyle, true);
	}
      
      if(XErrorUpFlag)
	{
	  ParentGraph->AddLine(ParentGraph->XValue2Pixel(InternalDataArray.GetX(i))+int(XAxisDirection*0.5*Size), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i)), ParentGraph->XValue2Pixel(XErrorUpValue), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i)), ErrorsLineWidth, ErrorsLineColor, ErrorsLineStyle, true);
	  
	  ParentGraph->AddLine(ParentGraph->XValue2Pixel(XErrorUpValue), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i))-int(0.5*Size), ParentGraph->XValue2Pixel(XErrorUpValue), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i))+int(0.5*Size), ErrorsLineWidth, ErrorsLineColor, ErrorsLineStyle, true);
	}
      
      if(YErrorUpFlag)
	{
	  ParentGraph->AddLine(ParentGraph->XValue2Pixel(InternalDataArray.GetX(i)), ParentGraph->YValue2Pixel(InternalDataArray.GetY(i))+int(YAxisDirection*0.5*Size), ParentGraph->XValue2Pixel(InternalDataArray.GetX(i)), ParentGraph->YValue2Pixel(YErrorUpValue), ErrorsLineWidth, ErrorsLineColor, ErrorsLineStyle, true);
	  
	  ParentGraph->AddLine(ParentGraph->XValue2Pixel(InternalDataArray.GetX(i))-int(0.5*Size), ParentGraph->YValue2Pixel(YErrorUpValue), ParentGraph->XValue2Pixel(InternalDataArray.GetX(i))+int(0.5*Size), ParentGraph->YValue2Pixel(YErrorUpValue), ErrorsLineWidth, ErrorsLineColor, ErrorsLineStyle, true);
	}
    }
  
  return true;
}
