/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTableBayani Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABLEBAYANI_H
#define BAYANI_QTABLEBAYANI_H

using namespace std;

/* QT headers */
#include <qevent.h>
#include <qobject.h>
#include <qwidget.h>
#include <qtable.h>

/*!
  \file qtablebayani.h
  \brief Header file for the QTableBayani class.
  
  The QTableBayani class is defined in this file.
*/

//! An advanced QTable.
/*!
  This class inherits QTable. One of its advantages is that it's possible to know if the table has been modified (since last save for example).
*/
class QTableBayani: public QTable
{
  Q_OBJECT
  
 protected:
  bool ModifiedFlag;
  
  virtual void showEvent (QShowEvent *);
  virtual void closeEvent(QCloseEvent *);
  
 public:
               QTableBayani(QWidget * Parent = 0, const char * Name = 0);
  virtual     ~QTableBayani();
          void SetR2L      (bool);
	  void SetModified (bool);
	  bool IsModified  ();
  
 public slots:
  void ValueChangedSlot(int, int);
  
 signals:
  void Shown ();
  void Closed();
};

#endif
