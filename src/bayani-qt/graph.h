/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the Graph Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_GRAPH_H
#define BAYANI_GRAPH_H

using namespace std;

/* "Usual" headers */
#include <math.h>
#include <vector>
/* QT headers */
#include <qnamespace.h>
#include <qstring.h>
#include <qfont.h>
#include <qcolor.h>
#include <qwidget.h>
/* Bayani headers */
#include <fitsimage.h>
#include <bbgo.h>
#include <bbdo.h>
#include <histo1d.h>
#include <graph2d.h>
#include <fitscontainer.h>

/*!
  \file graph.h
  \brief Header file for the Graph class.
  
  The Graph class is defined in this file.
*/

//! Class representing graph objects.
/*!
  This class inherits BBGO. It is specially designed to be generic and to display all sorts of data. It is easy to encapsulate many Graph objects in a 2D grid, using a Frame object.
  \sa BBGO and Frame.
*/
class Graph: public BBGO
{  
  friend class Func1D;
  friend class Histo1D;
  friend class Graph2D;
  friend class FITSContainer;
  
  Q_OBJECT
  
 protected:
  bool      LogarithmicXAxisFlag;
  bool      LogarithmicYAxisFlag;
  bool      ReverseXAxisFlag;
  bool      ReverseYAxisFlag;
  bool      XTicksLabelsFlag;
  bool      YTicksLabelsFlag;
  bool      StatsBoxFlag;
  bool      XInfBoundFlag;
  bool      YInfBoundFlag;
  bool      XSupBoundFlag;
  bool      YSupBoundFlag;
  bool      ComputeBoundsFlag;
  bool      ComputeTransfoFlag;
  int       XGridLineWidth;
  int       YGridLineWidth;
  int       BigXTicksLineWidth;
  int       BigYTicksLineWidth;
  int       SmallXTicksLineWidth;
  int       SmallYTicksLineWidth;
  int       StatsBoxLineWidth;
  double    XAxis2AxisLabelRatio;
  double    YAxis2AxisLabelRatio;
  double    XAxis2TicksLabelsRatio;
  double    YAxis2TicksLabelsRatio;
  double    BigXTicksRatio;
  double    BigYTicksRatio;
  double    SmallXTicksRatio;
  double    SmallYTicksRatio;
  double    StatsBoxWidthRatio;
  double    StatsBoxHeightRatio;
  double    XAxis2StatsBoxRatio;
  double    YAxis2StatsBoxRatio;
  double    StatsBox2StatsRatio;
  double    AlphaX;
  double    AlphaY;
  double    BetaX;
  double    BetaY;
  double    XInf;
  double    YInf;
  double    XSup;
  double    YSup;
  double    XInfBound;
  double    YInfBound;
  double    XSupBound;
  double    YSupBound;
  vector    <double> BigXTicksPositions;
  vector    <double> BigYTicksPositions;
  vector    <double> SmallXTicksPositions;
  vector    <double> SmallYTicksPositions;
  vector    <BBDO *> AllDatas;
  QColor    XGridColor;
  QColor    YGridColor;
  QColor    BigXTicksColor;
  QColor    BigYTicksColor;
  QColor    SmallXTicksColor;
  QColor    SmallYTicksColor;
  QColor    XAxisLabelColor;
  QColor    YAxisLabelColor;
  QColor    XTicksLabelsColor;
  QColor    YTicksLabelsColor;
  QColor    StatsBoxColor;
  QColor    StatsBoxTextColor;
  QFont     XAxisLabelFont;
  QFont     YAxisLabelFont;
  QFont     XTicksLabelsFont;
  QFont     YTicksLabelsFont;
  QFont     StatsBoxFont;
  QString   XAxisLabel;
  QString   YAxisLabel;
  PenStyle  XGridLineStyle;
  PenStyle  YGridLineStyle;
  PenStyle  BigXTicksLineStyle;
  PenStyle  BigYTicksLineStyle;
  PenStyle  SmallXTicksLineStyle;
  PenStyle  SmallYTicksLineStyle;
  PenStyle  StatsBoxLineStyle;
  
          void    ComputeBounds             ();
  virtual void    AddMargin2InfSup          ();
          void    ComputeTransfo            ();
	  void    ComputeTicksPositions     (double, double, vector <double> &, vector <double> &);
	  void    ComputeTicksLogPositions  (double, double, vector <double> &, vector <double> &);
  virtual void    ComputePositionsComplement();
  virtual bool    paintEventComplement      ();
          void    mousePressEvent           (QMouseEvent *);
          void    mouseMoveEvent            (QMouseEvent *);
          void    mouseReleaseEvent         (QMouseEvent *);
  virtual void    DrawPixmapComplement      ();
	  double  XPixel2Value              (int);
	  double  YPixel2Value              (int);
	  int     XValue2Pixel              (double);
	  int     YValue2Pixel              (double);
	  
 public:
                  Graph                    (QWidget * Parent = 0, const char * Name = 0);
  virtual        ~Graph                    ();
          void    SetLogarithmicXAxis      (bool);
	  void    SetLogarithmicYAxis      (bool);
	  void    SetReverseXAxis          (bool);
	  void    SetReverseYAxis          (bool);
	  void    SetXTicksLabels          (bool);
	  void    SetYTicksLabels          (bool);
	  void    SetStatsBox              (bool);
	  void    SetXGridLineWidth        (int);
	  void    SetYGridLineWidth        (int);
	  void    SetBigXTicksLineWidth    (int);
	  void    SetBigYTicksLineWidth    (int);
	  void    SetSmallXTicksLineWidth  (int);
	  void    SetSmallYTicksLineWidth  (int);
	  void    SetStatsBoxLineWidth     (int);
	  void    SetXAxis2AxisLabelRatio  (double);
	  void    SetYAxis2AxisLabelRatio  (double);
	  void    SetXAxis2TicksLabelsRatio(double);
	  void    SetYAxis2TicksLabelsRatio(double);
	  void    SetBigXTicksRatio        (double);
	  void    SetBigYTicksRatio        (double);
	  void    SetSmallXTicksRatio      (double);
	  void    SetSmallYTicksRatio      (double);
	  void    SetStatsBoxWidthRatio    (double);
	  void    SetStatsBoxHeightRatio   (double);
	  void    SetXAxis2StatsBoxRatio   (double);
	  void    SetYAxis2StatsBoxRatio   (double);
	  void    SetStatsBox2StatsRatio   (double);
	  double  GetXInf                  ();
	  double  GetXSup                  ();
	  double  GetYInf                  ();
	  double  GetYSup                  ();
	  double  GetXInfLimit             ();
	  double  GetXSupLimit             ();
	  double  GetYInfLimit             ();
	  double  GetYSupLimit             ();
	  void    SetXInfBound             (double);
	  void    SetYInfBound             (double);
	  void    SetXSupBound             (double);
	  void    SetYSupBound             (double);
	  void    UnSetXInfBound           ();
	  void    UnSetYInfBound           ();
	  void    UnSetXSupBound           ();
	  void    UnSetYSupBound           ();
	  void    SetXGridColor            (QColor);
	  void    SetYGridColor            (QColor);
	  void    SetBigXTicksColor        (QColor);
	  void    SetBigYTicksColor        (QColor);
	  void    SetSmallXTicksColor      (QColor);
	  void    SetSmallYTicksColor      (QColor);
	  void    SetXAxisLabelColor       (QColor);
	  void    SetYAxisLabelColor       (QColor);
	  void    SetXTicksLabelsColor     (QColor);
	  void    SetYTicksLabelsColor     (QColor);
	  void    SetStatsBoxColor         (QColor);
	  void    SetStatsBoxTextColor     (QColor);
	  void    SetXAxisLabelFont        (QFont);
	  void    SetYAxisLabelFont        (QFont);
	  void    SetXTicksLabelsFont      (QFont);
	  void    SetYTicksLabelsFont      (QFont);
	  void    SetStatsBoxFont          (QFont);
	  void    SetXAxisLabel            (QString);
	  void    SetYAxisLabel            (QString);
	  void    SetXGridLineStyle        (PenStyle);
	  void    SetYGridLineStyle        (PenStyle);
	  void    SetBigXTicksLineStyle    (PenStyle);
	  void    SetBigYTicksLineStyle    (PenStyle);
	  void    SetSmallXTicksLineStyle  (PenStyle);
	  void    SetSmallYTicksLineStyle  (PenStyle);
	  void    SetStatsBoxLineStyle     (PenStyle);
	  void    AddData                  (BBDO *);
	  BBDO  * GetData                  (int);
	  vector  <BBDO *> GetData         ();
	  
 signals:
  void DrawingProblem         (QString);
  void SendCoordinates        (double, double);
  void SendCoordinatesAndValue(double, double, double);
  void UnSendCoordinates      ();
};

#endif
