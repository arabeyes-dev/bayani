/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the BListView Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_BLISTVIEW_H
#define BAYANI_BLISTVIEW_H

/* QT headers */
#include <qstring.h>
#include <qlistview.h>

/*!
  \file blistviewitem.h
  \brief Header file for the BListViewItem class.
  
  The BListViewItem class is defined in this file.
*/

//! An advanced QListViewItem.
/*!
  This class inherits QListViewItem. It is destined to easily fit in a help browser that provides a list of contents by giving a quick access to that content.
*/
class BListViewItem: public QListViewItem
{
 protected:
  QString ID;
  QString Anchor;
  
 public:
                  BListViewItem(QListView     *, QString        , QString, QString);
		  BListViewItem(QListViewItem *, QListViewItem *, QString, QString, QString);
  virtual        ~BListViewItem();
          void    SetID        (QString);
          QString GetID        ();
          void    SetAnchor    (QString);
          QString GetAnchor    ();
};

#endif
