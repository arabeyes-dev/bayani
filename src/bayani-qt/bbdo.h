/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the BBDO Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_BBDO_H
#define BAYANI_BBDO_H

/* QT headers */
#include <qstring.h>

class Graph;

/*!
  \file bbdo.h
  \brief Header file for the BBDO class.
  
  The BBDO class is defined in this file.
*/

//! General class for data objects.
/*!
  BBDO stands for <b>Basic Bayani Data Object</b>. It is a general abstracted class that summarizes all the common methods used by the different Bayani data classes. It is directly subclassed into Func1D, Histo1D, Graph2D and FITSContainer. These classes are the basic data needed by the BBGO class (and its subclasses), in order to be drawn.
  \sa Func1D, Histo1D, Graph2D, FITSContainer and BBGO.
*/
class BBDO
{
 protected:
  double  XMin;
  double  XMax;
  double  YMin;
  double  YMax;
  QString ErrorMessage;
  
  void setError(QString);
  
 public:
                  BBDO         ();
  virtual        ~BBDO         ();
  virtual int     GetEntries   ();
          double  GetXMin      ();
	  double  GetXMax      ();
	  double  GetYMin      ();
	  double  GetYMax      ();
  virtual double  GetMean      ();
  virtual double  GetSigma     ();
  virtual void    ComputeMinMax();
  virtual bool    DrawSelf     (Graph *);
          QString error        ();
};

#endif
