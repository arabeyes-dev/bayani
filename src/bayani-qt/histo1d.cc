/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the Histo1D Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <histo1d.h>

Histo1D::Histo1D()
  : BBDO()
{
  LineWidth = 1;
  LineStyle = Qt::SolidLine;
  LineColor.setNamedColor("#000000");
  
  FillStyle = Qt::NoBrush;
  FillColor.setNamedColor("#000000");
}

Histo1D::~Histo1D()
{
}

void Histo1D::SetLineWidth(int NewLineWidth)
{
  LineWidth = NewLineWidth;
}

void Histo1D::SetLineColor(QColor NewLineColor)
{
  LineColor = NewLineColor;
}

void Histo1D::SetLineStyle(Qt::PenStyle NewLineStyle)
{
  LineStyle = NewLineStyle;
}

void Histo1D::SetFillColor(QColor NewFillColor)
{
  FillColor = NewFillColor;
}

void Histo1D::SetFillStyle(Qt::BrushStyle NewFillStyle)
{
  FillStyle = NewFillStyle;
}

void Histo1D::SetData(Histogram NewInternalHistogram)
{
  InternalHistogram = NewInternalHistogram;
}

int Histo1D::GetEntries()
{
  return InternalHistogram.GetData().size();
}

double Histo1D::GetMean()
{
  return InternalHistogram.GetMean();
}

double Histo1D::GetSigma()
{
  return InternalHistogram.GetSigma();
}

Histogram Histo1D::GetData()
{
  return InternalHistogram;
}

void Histo1D::ComputeMinMax()
{
  XMin = InternalHistogram.GetXMin();
  XMax = InternalHistogram.GetXMax();
  YMin = InternalHistogram.GetMin();
  YMax = InternalHistogram.GetMax();
}

bool Histo1D::DrawSelf(Graph * ParentGraph)
{
  QPointArray NewPointArray;
  
  int Counter = 0;
  
  for(int i = 0; i < InternalHistogram.GetBinsNumber(); i++)
    {
      if(i == 0)
	{
	  NewPointArray.putPoints(Counter, 1, ParentGraph->XValue2Pixel(InternalHistogram.GetXMin()), ParentGraph->YValue2Pixel(double(i)));
	  Counter++;
	  
	  NewPointArray.putPoints(Counter, 1, ParentGraph->XValue2Pixel(InternalHistogram.GetXMin()), ParentGraph->YValue2Pixel(double(InternalHistogram.GetBinEntries(i))));
	  Counter++;
	}
      
      NewPointArray.putPoints(Counter, 1, int(ParentGraph->XValue2Pixel(InternalHistogram.GetXMin())+i*ParentGraph->AlphaX*InternalHistogram.GetBinWidth()), ParentGraph->YValue2Pixel(double(InternalHistogram.GetBinEntries(i))));
      Counter++;
      
      NewPointArray.putPoints(Counter, 1, int(ParentGraph->XValue2Pixel(InternalHistogram.GetXMin())+(i+1)*ParentGraph->AlphaX*InternalHistogram.GetBinWidth()), ParentGraph->YValue2Pixel(double(InternalHistogram.GetBinEntries(i))));
      Counter++;
      
      if(i < InternalHistogram.GetBinsNumber()-1)
	{
	  NewPointArray.putPoints(Counter, 1, int(ParentGraph->XValue2Pixel(InternalHistogram.GetXMin())+(i+1)*ParentGraph->AlphaX*InternalHistogram.GetBinWidth()), ParentGraph->YValue2Pixel(double(InternalHistogram.GetBinEntries(i))));
	  Counter++;
	  
	  NewPointArray.putPoints(Counter, 1, int(ParentGraph->XValue2Pixel(InternalHistogram.GetXMin())+(i+1)*ParentGraph->AlphaX*InternalHistogram.GetBinWidth()), ParentGraph->YValue2Pixel(double(InternalHistogram.GetBinEntries(i+1))));
	  Counter++;
	}
      else
	{
	  NewPointArray.putPoints(Counter, 1, int(ParentGraph->XValue2Pixel(InternalHistogram.GetXMin())+(i+1)*ParentGraph->AlphaX*InternalHistogram.GetBinWidth()), ParentGraph->YValue2Pixel(double(InternalHistogram.GetBinEntries(i))));
	  Counter++;
	  
	  NewPointArray.putPoints(Counter, 1, int(ParentGraph->XValue2Pixel(InternalHistogram.GetXMin())+(i+1)*ParentGraph->AlphaX*InternalHistogram.GetBinWidth()), ParentGraph->YValue2Pixel(0.));
	  Counter++;
	}
    }
  
  ParentGraph->AddPolyline(NewPointArray, LineWidth, LineColor, LineStyle);
  ParentGraph->AddPolygon(NewPointArray, FillColor, FillStyle);
  
  return true;
}
