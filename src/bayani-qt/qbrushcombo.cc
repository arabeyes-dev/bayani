/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QBrushCombo Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qbrushcombo.h>

Qt::BrushStyle BrushStyles[BRUSH_STYLES_NUMBER] = {Qt::NoBrush, Qt::SolidPattern, Qt::Dense1Pattern, Qt::Dense2Pattern, Qt::Dense3Pattern, Qt::Dense4Pattern, Qt::Dense5Pattern, Qt::Dense6Pattern, Qt::Dense7Pattern, Qt::HorPattern , Qt::VerPattern, Qt::CrossPattern, Qt::BDiagPattern, Qt::FDiagPattern, Qt::DiagCrossPattern};

QBrushCombo::QBrushCombo(QWidget * Parent, const char * Name)
  : QComboBox(Parent, Name)
{
  setEditable(false);
  
  for(int i = 0; i < BRUSH_STYLES_NUMBER; i++)
    {
      QPixmap Pixmap(50, 16);
      Pixmap.fill(QColor(255, 255, 255));
      
      QPainter Painter(&Pixmap);
      QBrush   Brush;
      
      Painter.drawRect(0, 0, Pixmap.width(), Pixmap.height());
      
      Brush.setStyle(BrushStyles[i]);
      
      Painter.fillRect(2, 2, Pixmap.width()-4, Pixmap.height()-4, Brush);
      
      insertItem(Pixmap);
    }
  
  setCurrentItem(0);
  
  setFixedSize(sizeHint());
}

QBrushCombo::~QBrushCombo()
{
}

void QBrushCombo::SetCurrentStyle(Qt::BrushStyle NewStyle)
{
  for(int i = 0; i < BRUSH_STYLES_NUMBER; i++)
    if(BrushStyles[i] == NewStyle)
      {
	if(i != currentItem())
	  setCurrentItem(i);
	
	return;
      }
}

Qt::BrushStyle QBrushCombo::GetCurrentStyle()
{
  return BrushStyles[currentItem()];
}
