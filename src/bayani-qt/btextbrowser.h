/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the BTextBrowser Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_BTEXTBROWSER_H
#define BAYANI_BTEXTBROWSER_H

/* QT headers */
#include <qwidget.h>
#include <qstring.h>
#include <qfile.h>
#include <qtextbrowser.h>
#include <qmessagebox.h>

/*!
  \file btextbrowser.h
  \brief Header file for the BTextBrowser class.
  
  The BTextBrowser class is defined in this file.
*/

//! An advanced QTextBrowser.
/*!
  This class inherits QTextBrowser. It provides more interactive behaviour when browsing rich text content.
*/
class BTextBrowser: public QTextBrowser
{
  Q_OBJECT
  
 public:
           BTextBrowser(QWidget * Parent = 0, const char * Name = 0);
  virtual ~BTextBrowser();
  
 public slots:
  virtual void setSource(const QString &); 
};

#endif
