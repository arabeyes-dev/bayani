/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the FITSContainer Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <fitscontainer.h>

FITSContainer::FITSContainer()
  : BBDO()
{
  Palette = PALETTE_MC;
  
  SetMinDyn(false);
  SetMaxDyn(false);
}

FITSContainer::~FITSContainer()
{
}

void FITSContainer::SetPalette(int NewPalette)
{
  Palette = NewPalette;
}

void FITSContainer::SetData(FITSImage NewInternalFITSImage)
{
  InternalFITSImage = NewInternalFITSImage;
}

FITSImage FITSContainer::FITSContainer::GetData()
{
  return InternalFITSImage;
}

void FITSContainer::SetMinDyn(bool NewMinDynFlag, double NewMinDyn)
{
  MinDynFlag = NewMinDynFlag;
  MinDyn     = NewMinDyn;
}

void FITSContainer::SetMaxDyn(bool NewMaxDynFlag, double NewMaxDyn)
{
  MaxDynFlag = NewMaxDynFlag;
  MaxDyn     = NewMaxDyn;
}

void FITSContainer::ComputeMinMax()
{
  XMin = 0.;
  XMax = double(InternalFITSImage.GetXSize()-1);
  YMin = 0.;
  YMax = double(InternalFITSImage.GetYSize()-1);
}

bool FITSContainer::DrawSelf(Graph * ParentGraph)
{
  if(ParentGraph->LogarithmicXAxisFlag || ParentGraph->LogarithmicYAxisFlag)
    {
      setError(QObject::trUtf8("I cannot draw a FITS image in logarithmic scale"));
      
      return false;
    }
  
  double Min    = (MinDynFlag) ? MinDyn : InternalFITSImage.GetMinDyn();
  double Max    = (MaxDynFlag) ? MaxDyn : InternalFITSImage.GetMaxDyn();
  
  if(Min >= Max)
    {
      if(MinDynFlag)
	Max = Min + 0.5;
      else if(MaxDynFlag)
	Min = Max - 0.5;
      else
	{
	  Min -= 0.5;
	  Max += 0.5;
	}
    }
  
  double MinMax = Max-Min;
  
  int X1 = (int(ParentGraph->GetXInfLimit()) > 0                           ) ? int(ParentGraph->GetXInfLimit()): 0;
  int Y1 = (int(ParentGraph->GetYInfLimit()) > 0                           ) ? int(ParentGraph->GetYInfLimit()): 0;
  int X2 = (int(ParentGraph->GetXSupLimit()) < InternalFITSImage.GetXSize()) ? int(ParentGraph->GetXSupLimit()): InternalFITSImage.GetXSize();
  int Y2 = (int(ParentGraph->GetYSupLimit()) < InternalFITSImage.GetYSize()) ? int(ParentGraph->GetYSupLimit()): InternalFITSImage.GetYSize();
  
  X2++;
  Y2++;
  
  if(X2 > InternalFITSImage.GetXSize())
    X2--;
  
  if(Y2 > InternalFITSImage.GetYSize())
    Y2--;
  
  int XSize = X2-X1;
  int YSize = Y2-Y1;
  
  if(XSize < 1 || YSize < 1)
    {
      setError(QObject::trUtf8("The FITS image is empty"));
      
      return false;
    }
  
  if(!InternalQImage.create(XSize, YSize, 32))
    {
      setError(QObject::trUtf8("I could not allocate the FITS image"));
      
      return false;
    }
  
  int Red   = 0;
  int Green = 0;
  int Blue  = 0;
  
  for(int i = X1; i < X2; i++)
    for(int j = Y1; j < Y2; j++)
      {
	if(Palette == PALETTE_MC)
	  MCPalette(double(InternalFITSImage.GetPixel(i,j)), Min, Max, MinMax, Red, Green, Blue);
	else if(Palette == PALETTE_BRYW)
	  BRYWPalette(double(InternalFITSImage.GetPixel(i,j)), Min, Max, MinMax, Red, Green, Blue);
	else
	  BBGRYWPalette(double(InternalFITSImage.GetPixel(i,j)), Min, Max, MinMax, Red, Green, Blue);
	
	InternalQImage.setPixel(i-X1, j-Y1, qRgb(Red, Green, Blue));
      }
  
  int TLX = ParentGraph->XValue2Pixel(double(X1));
  int TLY = ParentGraph->YValue2Pixel(double(Y1));
  int BRX = ParentGraph->XValue2Pixel(double(X2));
  int BRY = ParentGraph->YValue2Pixel(double(Y2));
  
  if(ParentGraph->ReverseXAxisFlag)
    {
      TLX = ParentGraph->XValue2Pixel(double(X2));
      BRX = ParentGraph->XValue2Pixel(double(X1));
      
      InternalQImage = InternalQImage.mirror(true, false);
    }
  
  if(!ParentGraph->ReverseYAxisFlag)
    {
      TLY = ParentGraph->YValue2Pixel(double(Y2));
      BRY = ParentGraph->YValue2Pixel(double(Y1));
      
      InternalQImage = InternalQImage.mirror(false, true);
    }
  
  ParentGraph->AddImage(InternalQImage.scale(BRX-TLX, BRY-TLY), TLX, TLY);
  
  return true;
}

void FITSContainer::MCPalette(double Value, double Min, double Max, double MinMax, int & Red, int & Green, int & Blue)
{
  if(Value <= Min)
    {
      Red   = 0;
      Green = 0;
      Blue  = 0;
    }
  else if (Value >= Max)
    {
      Red   = 255;
      Green = 255;
      Blue  = 255;
    }
  else
    {
      int Scale = int(255*(Value-Min)/MinMax);
      
      Red   = Scale;
      Green = Scale;
      Blue  = Scale;
    }
}

void FITSContainer::BRYWPalette(double Value, double Min, double Max, double MinMax, int & Red, int & Green, int & Blue)
{
  if(Value <= Min)
    {
      Red   = 0;
      Green = 0;
      Blue  = 0;
    }
  else if (Value >= Max)
    {
      Red   = 255;
      Green = 255;
      Blue  = 255;
    }
  else
    {
      if(Value <= Min+0.33*MinMax)
	{
	  int Scale = int(255*(Value-Min)/(0.33*MinMax));
	  
	  Red   = Scale;
	  Green = 0;
	  Blue  = 0;
	  
	  return;
	}
      
      if(Value <= Min+0.66*MinMax)
	{
	  int Scale = int(255*(Value-(Min+0.33*MinMax))/(0.33*MinMax));
	  
	  Red   = 255;
	  Green = Scale;
	  Blue  = 0;
	  
	  return;
	}
      
      if(1)
	{
	  int Scale = int(255*(Value-(Min+0.66*MinMax))/(0.34*MinMax));
	  
	  Red   = 255;
	  Green = 255;
	  Blue  = Scale;
	  
	  return;
	}
    }
}

void FITSContainer::BBGRYWPalette(double Value, double Min, double Max, double MinMax, int & Red, int & Green, int & Blue)
{
  if(Value <= Min)
    {
      Red   = 0;
      Green = 0;
      Blue  = 0;
    }
  else if (Value >= Max)
    {
      Red   = 255;
      Green = 255;
      Blue  = 255;
    }
  else
    {
      if(Value <= Min+0.20*MinMax)
	{
	  int Scale = int(255*(Value-Min)/(0.20*MinMax));
	  
	  Red   = 0;
	  Green = 0;
	  Blue  = Scale;
	  
	  return;
	}
      
      if(Value <= Min+0.40*MinMax)
	{
	  int Scale = int(255*(Value-(Min+0.20*MinMax))/(0.20*MinMax));
	  
	  Red   = 0;
	  Green = Scale;
	  Blue  = 255-Scale;
	  
	  return;
	}
      
      if(Value <= Min+0.60*MinMax)
	{
	  int Scale = int(255*(Value-(Min+0.40*MinMax))/(0.20*MinMax));
	  
	  Red   = Scale;
	  Green = 255-Scale;
	  Blue  = 0;
	  
	  return;
	}
      
      if(Value <= Min+0.80*MinMax)
	{
	  int Scale = int(255*(Value-(Min+0.60*MinMax))/(0.20*MinMax));
	  
	  Red   = 255;
	  Green = Scale;
	  Blue  = 0;
	  
	  return;
	}
      
      if(1)
	{
	  int Scale = int(255*(Value-(Min+0.80*MinMax))/(0.20*MinMax));
	  
	  Red   = 255;
	  Green = 255;
	  Blue  = Scale;
	  
	  return;
	}
    }
}
