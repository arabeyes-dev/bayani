/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QFontSelector Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QFONTSELECTOR_H
#define BAYANI_QFONTSELECTOR_H

/* QT headers */
#include <qnamespace.h>
#include <qobject.h>
#include <qwidget.h>
#include <qstring.h>
#include <qhbox.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qfont.h>
#include <qfontdialog.h>
#include <qframe.h>

/*!
  \file qfontselector.h
  \brief Header file for the QFontSelector class.
  
  The QFontSelector class is defined in this file.
*/

//! A font selector.
/*!
  This class inherits QHBox. It contains a QLabel and a QPushButton and gives the user the ability to select a specific font.
*/
class QFontSelector: public QHBox
{
  Q_OBJECT
  
 protected:
  QFont       * Font;
  QLabel      * Label;
  QPushButton * PushButton;
  
  void ChangeFont();
  
 public:
                QFontSelector(QWidget * Parent = 0, const char * Name = 0);
  virtual      ~QFontSelector();
          void  SetFont      (QFont);
	  QFont GetFont      ();
  
 protected slots:
  void OpenDialog();
};

#endif
