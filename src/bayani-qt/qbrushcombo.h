/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QBrushCombo Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QBRUSHCOMBO_H
#define BAYANI_QBRUSHCOMBO_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qcombobox.h>
#include <qpixmap.h>
#include <qcolor.h>
#include <qpainter.h>
#include <qbrush.h>

/* Number of QT brush styles */
#define BRUSH_STYLES_NUMBER 15

/*!
  \file qbrushcombo.h
  \brief Header file for the QBrushCombo class.
  
  The QBrushCombo class is defined in this file.
*/

//! A brush style selector.
/*!
  This class inherits QComboBox. It gives the user the ability to select a specific brush style.
*/
class QBrushCombo: public QComboBox
{
 public:
                         QBrushCombo    (QWidget * Parent = 0, const char * Name = 0);
  virtual               ~QBrushCombo    ();
          void           SetCurrentStyle(Qt::BrushStyle);
	  Qt::BrushStyle GetCurrentStyle();
};

#endif
