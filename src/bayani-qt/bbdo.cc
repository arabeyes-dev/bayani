/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation the of BBDO Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <bbdo.h>

BBDO::BBDO()
{
  XMin = -1.e99;
  XMax =  1.e99;
  YMin = -1.e99;
  YMax =  1.e99;
  
  ErrorMessage = "";
}

BBDO::~BBDO()
{
}

void BBDO::ComputeMinMax()
{
}

bool BBDO::DrawSelf(Graph *)
{
}

int BBDO::GetEntries()
{
  return 0;
}

double BBDO::GetXMin()
{
  return XMin;
}

double BBDO::GetXMax()
{
  return XMax;
}

double BBDO::GetYMin()
{
  return YMin;
}

double BBDO::GetYMax()
{
  return YMax;
}

double BBDO::GetMean()
{
  return 0.;
}

double BBDO::GetSigma()
{
  return 0.;
}

void BBDO::setError(QString NewErrorMessage)
{
  ErrorMessage = NewErrorMessage;
}

QString BBDO::error()
{
  return ErrorMessage;
}
