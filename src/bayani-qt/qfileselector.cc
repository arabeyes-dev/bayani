/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QFileSelector Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qfileselector.h>

QFileSelector::QFileSelector(QWidget * Parent, const char * Name)
  : QHBox(Parent, Name)
{
  OpenTypeFlag = true;
  
  FileTypes = "";
  
  LineEdit   = new QLineEdit(this);
  PushButton = new QPushButton(trUtf8("&Browse..."), this);
  
  PushButton->setFixedSize(PushButton->sizeHint());
  
  setSpacing(5);
  
  setFocusProxy(LineEdit);
  
  connect(PushButton, SIGNAL(clicked()), this, SLOT(OpenDialog()));
}

QFileSelector::~QFileSelector()
{
}

void QFileSelector::SetFileName(QString NewFileName)
{
  LineEdit->setText(NewFileName);
}

QString QFileSelector::GetFileName()
{
  return LineEdit->text();
}

void QFileSelector::AddFileType(QString NewFileType)
{
  FileTypes += NewFileType + ";;";
}

void QFileSelector::SetOpenType(bool NewOpenTypeFlag)
{
  OpenTypeFlag = NewOpenTypeFlag;
}

void QFileSelector::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    LineEdit->setAlignment(Qt::AlignRight);
  else
    LineEdit->setAlignment(Qt::AlignLeft);
}

void QFileSelector::OpenDialog()
{
  QString FileName;
  
  if(OpenTypeFlag)
    FileName = QFileDialog::getOpenFileName(LineEdit->text(), trUtf8("%1All files (*)") .arg(FileTypes), parentWidget());
  else
    FileName = QFileDialog::getSaveFileName(LineEdit->text(), trUtf8("%1All files (*)") .arg(FileTypes), parentWidget());
  
  if(FileName)
    LineEdit->setText(FileName);
}
