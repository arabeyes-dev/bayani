/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTextEditBayani Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTEXTEDITBAYANI_H
#define BAYANI_QTEXTEDITBAYANI_H

using namespace std;

/* "Usual" headers */
#include <vector>
/* QT headers */
#include <qnamespace.h>
#include <qobject.h>
#include <qwidget.h>
#include <qstring.h>
#include <qevent.h>
#include <qtextedit.h>
#include <qcolor.h>
#include <qfont.h>
#include <qbrush.h>
#include <qmessagebox.h>

/*!
  \file qtexteditbayani.h
  \brief Header file for the QTextEditBayani class.
  
  The QTextEditBayani class is defined in this file.
*/

//! A basic terminal.
/*!
  This class inherits QTextEdit. It implements a basic terminal that may be used to command an application.
*/
class QTextEditBayani: public QTextEdit
{
  Q_OBJECT
  
 protected:
  int               HistoryIndex;
  int               PromptEndIndex;
  int               Para;
  int               Index;
  QString           PromptString;
  vector  <QString> CommandsHistory;
  QColor            TextColor;
  QColor            InformationColor;
  QColor            WarningColor;
  QColor            CriticalColor;
  QColor            StatusColor;
  
          void PrintPrompt  ();
  virtual void keyPressEvent(QKeyEvent   *);
  virtual void showEvent    (QShowEvent  *);
  virtual void closeEvent   (QCloseEvent *);
  
 public:
                 QTextEditBayani     (QWidget * Parent = 0, const char * Name = 0);
  virtual       ~QTextEditBayani     ();
          void   PrintInformation    (QString);
	  int    PrintWarning        (QString);
	  void   PrintCritical       (QString);
	  void   PrintStatus         (QString);
	  void   SetHistory          (vector <QString>);
	  vector <QString> GetHistory();
	  void   SetTextColor        (QColor);
	  void   SetInformationColor (QColor);
	  void   SetWarningColor     (QColor);
	  void   SetCriticalColor    (QColor);
	  void   SetStatusColor      (QColor);
	  void   SetBGColor          (QColor);
	  void   SetTextFont         (QFont);
	  
 public slots:
  void CheckMouseSlot(int, int);
  
 signals:
  void CommandEntered(QString);
  void Shown         ();
  void Closed        ();
};

#endif
