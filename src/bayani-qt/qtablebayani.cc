/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTableBayani Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtablebayani.h>

QTableBayani::QTableBayani(QWidget * Parent, const char * Name)
  : QTable(Parent, Name)
{
  SetModified(false);
  
  connect(this, SIGNAL(valueChanged(int, int)), this, SLOT(ValueChangedSlot(int, int)));
}

QTableBayani::~QTableBayani()
{
}

void QTableBayani::SetR2L(bool NewR2LFlag)
{
  /*
    g2dAddDataTable->setAlignment(R2LAlignement);
    ((QLineEdit *)g2dAddDataTable->cellWidget(1,1))->setAlignment(R2LAlignement);
    g2dAddDataTable->setFixedSize(g2dAddDataTable->sizeHint());
  */
}

void QTableBayani::SetModified(bool NewModifiedFlag)
{
  ModifiedFlag = NewModifiedFlag;
}

bool QTableBayani::IsModified()
{
  return ModifiedFlag;
}

void QTableBayani::ValueChangedSlot(int, int)
{
  ModifiedFlag = true;
}

void QTableBayani::showEvent(QShowEvent *)
{
  emit Shown();
}

void QTableBayani::closeEvent(QCloseEvent * Event)
{
  Event->accept();
  
  emit Closed();
}
