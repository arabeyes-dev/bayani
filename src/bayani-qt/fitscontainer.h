/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the FITSContainer Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_FITSCONTAINER_H
#define BAYANI_FITSCONTAINER_H

/* QT headers */
#include <qobject.h>
#include <qcolor.h>
#include <qimage.h>
/* Bayani headers */
#include <fitsimage.h>
#include <graph.h>
#include <bbdo.h>

/* Palettes */
#define PALETTE_MC     0
#define PALETTE_BRYW   1
#define PALETTE_BBGRYW 2

/*!
  \file fitscontainer.h
  \brief Header file for the FITSContainer class.
  
  The FITSContainer class is defined in this file.
*/

//! Class to display FITS images.
/*!
  This class inherits BBDO. It is specially designed to display FITS images.
  \sa BBDO, Func1D, Histo1D and Graph2D.
*/
class FITSContainer: public BBDO
{
 protected:
  bool      MinDynFlag;
  bool      MaxDynFlag;
  int       Palette;
  double    MinDyn;
  double    MaxDyn;
  QImage    InternalQImage;
  FITSImage InternalFITSImage;
  
  virtual void ComputeMinMax();
  virtual bool DrawSelf     (Graph *);
          void MCPalette    (double, double, double, double, int &, int &, int &);
	  void BRYWPalette  (double, double, double, double, int &, int &, int &);
	  void BBGRYWPalette(double, double, double, double, int &, int &, int &);
  
 public:
                    FITSContainer();
  virtual          ~FITSContainer();
          void      SetPalette   (int);
	  void      SetData      (FITSImage);
	  FITSImage GetData      ();
	  void      SetMinDyn    (bool, double NewMinDyn = -1.);
	  void      SetMaxDyn    (bool, double NewMaxDyn = -1.);
};

#endif
