/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation the of Graph Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <graph.h>

Graph::Graph(QWidget * Parent, const char * Name)
  : BBGO(Parent, Name)
{
  
  SetXTicksLabels          (true);
  SetYTicksLabels          (true);
  SetStatsBox              (true);
  SetXGridLineWidth        (1);
  SetYGridLineWidth        (1);
  SetBigXTicksLineWidth    (1);
  SetBigYTicksLineWidth    (1);
  SetSmallXTicksLineWidth  (1);
  SetSmallYTicksLineWidth  (1);
  SetStatsBoxLineWidth     (1);
  SetXAxis2AxisLabelRatio  (0.035);
  SetYAxis2AxisLabelRatio  (0.02);
  SetXAxis2TicksLabelsRatio(0.04);
  SetYAxis2TicksLabelsRatio(0.02);
  SetBigXTicksRatio        (0.02);
  SetBigYTicksRatio        (0.02);
  SetSmallXTicksRatio      (0.01);
  SetSmallYTicksRatio      (0.01);
  SetStatsBoxWidthRatio    (0.2);
  SetStatsBoxHeightRatio   (0.1);
  SetXAxis2StatsBoxRatio   (0.02);
  SetYAxis2StatsBoxRatio   (0.02);
  SetStatsBox2StatsRatio   (0.02);
  SetXGridColor            (QColor("#000000"));
  SetYGridColor            (QColor("#000000"));
  SetBigXTicksColor        (QColor("#000000"));
  SetBigYTicksColor        (QColor("#000000"));
  SetSmallXTicksColor      (QColor("#000000"));
  SetSmallYTicksColor      (QColor("#000000"));
  SetXAxisLabelColor       (QColor("#000000"));
  SetYAxisLabelColor       (QColor("#000000"));
  SetXTicksLabelsColor     (QColor("#000000"));
  SetYTicksLabelsColor     (QColor("#000000"));
  SetStatsBoxColor         (QColor("#000000"));
  SetStatsBoxTextColor     (QColor("#000000"));
  SetXAxisLabelFont        (font());
  SetYAxisLabelFont        (font());
  SetXTicksLabelsFont      (font());
  SetYTicksLabelsFont      (font());
  SetStatsBoxFont          (font());
  SetXAxisLabel            ("");
  SetYAxisLabel            ("");
  SetXGridLineStyle        (Qt::DotLine);
  SetYGridLineStyle        (Qt::DotLine);
  SetBigXTicksLineStyle    (Qt::SolidLine);
  SetBigYTicksLineStyle    (Qt::SolidLine);
  SetSmallXTicksLineStyle  (Qt::SolidLine);
  SetSmallYTicksLineStyle  (Qt::SolidLine);
  SetStatsBoxLineStyle     (Qt::SolidLine);
  
  ComputeTransfoFlag = false;
  ForceRepaintFlag   = false;
  
  ComputeBoundsFlag = false;
  UnSetXInfBound();
  UnSetXSupBound();
  UnSetYInfBound();
  UnSetYSupBound();
  
  SetLogarithmicXAxis(false);
  SetLogarithmicYAxis(false);
  SetReverseXAxis(false);
  SetReverseYAxis(false);
  
  ComputePositions();
  ComputeBounds   ();
  ComputeTransfo  ();
}

Graph::~Graph()
{
}

void Graph::SetLogarithmicXAxis(bool NewLogarithmicXAxisFlag)
{
  LogarithmicXAxisFlag = NewLogarithmicXAxisFlag;
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::SetLogarithmicYAxis(bool NewLogarithmicYAxisFlag)
{
  LogarithmicYAxisFlag = NewLogarithmicYAxisFlag;
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::SetReverseXAxis(bool NewReverseXAxisFlag)
{
  ReverseXAxisFlag = NewReverseXAxisFlag;
  
  if(!ComputeTransfoFlag)
    ComputeTransfoFlag = true;
}

void Graph::SetReverseYAxis(bool NewReverseYAxisFlag)
{
  ReverseYAxisFlag = NewReverseYAxisFlag;
  
  if(!ComputeTransfoFlag)
    ComputeTransfoFlag = true;
}

void Graph::SetXTicksLabels(bool NewXTicksLabelsFlag)
{
  XTicksLabelsFlag = NewXTicksLabelsFlag;
}

void Graph::SetYTicksLabels(bool NewYTicksLabelsFlag)
{
  YTicksLabelsFlag = NewYTicksLabelsFlag;
}

void Graph::SetStatsBox(bool NewStatsBoxFlag)
{
  StatsBoxFlag = NewStatsBoxFlag;
}

void Graph::SetXGridLineWidth(int NewXGridLineWidth)
{
  XGridLineWidth = NewXGridLineWidth;
}

void Graph::SetYGridLineWidth(int NewYGridLineWidth)
{
  YGridLineWidth = NewYGridLineWidth;
}

void Graph::SetBigXTicksLineWidth(int NewBigXTicksLineWidth)
{
  BigXTicksLineWidth = NewBigXTicksLineWidth;
}

void Graph::SetBigYTicksLineWidth(int NewBigYTicksLineWidth)
{
  BigYTicksLineWidth = NewBigYTicksLineWidth;
}

void Graph::SetSmallXTicksLineWidth(int NewSmallXTicksLineWidth)
{
  SmallXTicksLineWidth = NewSmallXTicksLineWidth;
}

void Graph::SetSmallYTicksLineWidth(int NewSmallYTicksLineWidth)
{
  SmallYTicksLineWidth = NewSmallYTicksLineWidth;
}

void Graph::SetStatsBoxLineWidth(int NewStatsBoxLineWidth)
{
  StatsBoxLineWidth = NewStatsBoxLineWidth;
}

void Graph::SetXAxis2AxisLabelRatio(double NewXAxis2AxisLabelRatio)
{
  XAxis2AxisLabelRatio = NewXAxis2AxisLabelRatio;
}

void Graph::SetYAxis2AxisLabelRatio(double NewYAxis2AxisLabelRatio)
{
  YAxis2AxisLabelRatio = NewYAxis2AxisLabelRatio;
}

void Graph::SetXAxis2TicksLabelsRatio(double NewXAxis2TicksLabelsRatio)
{
  XAxis2TicksLabelsRatio = NewXAxis2TicksLabelsRatio;
}
void Graph::SetYAxis2TicksLabelsRatio(double NewYAxis2TicksLabelsRatio)
{
  YAxis2TicksLabelsRatio = NewYAxis2TicksLabelsRatio;
}

void Graph::SetBigXTicksRatio(double NewBigXTicksRatio)
{
  BigXTicksRatio = NewBigXTicksRatio;
}

void Graph::SetBigYTicksRatio(double NewBigYTicksRatio)
{
  BigYTicksRatio = NewBigYTicksRatio;
}

void Graph::SetSmallXTicksRatio(double NewSmallXTicksRatio)
{
  SmallXTicksRatio = NewSmallXTicksRatio;
}

void Graph::SetSmallYTicksRatio(double NewSmallYTicksRatio)
{
  SmallYTicksRatio = NewSmallYTicksRatio;
}

void Graph::SetStatsBoxWidthRatio(double NewStatsBoxWidthRatio)
{
  StatsBoxWidthRatio = NewStatsBoxWidthRatio;
}

void Graph::SetStatsBoxHeightRatio(double NewStatsBoxHeightRatio)
{
  StatsBoxHeightRatio = NewStatsBoxHeightRatio;
}

void Graph::SetXAxis2StatsBoxRatio(double NewXAxis2StatsBoxRatio)
{
  XAxis2StatsBoxRatio = NewXAxis2StatsBoxRatio;
}

void Graph::SetYAxis2StatsBoxRatio(double NewYAxis2StatsBoxRatio)
{
  YAxis2StatsBoxRatio = NewYAxis2StatsBoxRatio;
}

void Graph::SetStatsBox2StatsRatio(double NewStatsBox2StatsRatio)
{
  StatsBox2StatsRatio = NewStatsBox2StatsRatio;
}

double Graph::GetXInf()
{
  return XInf;
}

double Graph::GetXSup()
{
  return XSup;
}

double Graph::GetYInf()
{
  return YInf;
}

double Graph::GetYSup()
{
  return YSup;
}

double Graph::GetXInfLimit()
{
  if(XInfBoundFlag)
    return XInfBound;
  
  return XInf;
}

double Graph::GetXSupLimit()
{
  if(XSupBoundFlag)
    return XSupBound;
  
  return XSup;
}

double Graph::GetYInfLimit()
{
  if(YInfBoundFlag)
    return YInfBound;
  
  return YInf;
}

double Graph::GetYSupLimit()
{
  if(YSupBoundFlag)
    return YSupBound;
  
  return YSup;
}

void Graph::SetXInfBound(double NewXInfBound)
{
  XInfBound = NewXInfBound;
  XInfBoundFlag = true;

  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::SetYInfBound(double NewYInfBound)
{
  YInfBound = NewYInfBound;
  YInfBoundFlag = true;
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::SetXSupBound(double NewXSupBound)
{
  XSupBound = NewXSupBound;
  XSupBoundFlag = true;
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::SetYSupBound(double NewYSupBound)
{
  YSupBound = NewYSupBound;
  YSupBoundFlag = true;
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::UnSetXInfBound()
{
  XInfBound = 0.;
  XInfBoundFlag = false;
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::UnSetYInfBound()
{
  YInfBound = 0.;
  YInfBoundFlag = false;
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::UnSetXSupBound()
{
  XSupBound = 0.;
  XSupBoundFlag = false;
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::UnSetYSupBound()
{
  YSupBound = 0.;
  YSupBoundFlag = false;
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

void Graph::SetXGridColor(QColor NewXGridColor)
{
  XGridColor = NewXGridColor;
}

void Graph::SetYGridColor(QColor NewYGridColor)
{
  YGridColor = NewYGridColor;
}

void Graph::SetBigXTicksColor(QColor NewBigXTicksColor)
{
  BigXTicksColor = NewBigXTicksColor;
}

void Graph::SetBigYTicksColor(QColor NewBigYTicksColor)
{
  BigYTicksColor = NewBigYTicksColor;
}

void Graph::SetSmallXTicksColor(QColor NewSmallXTicksColor)
{
  SmallXTicksColor = NewSmallXTicksColor;
}

void Graph::SetSmallYTicksColor(QColor NewSmallYTicksColor)
{
  SmallYTicksColor = NewSmallYTicksColor;
}

void Graph::SetXAxisLabelColor(QColor NewXAxisLabelColor)
{
  XAxisLabelColor = NewXAxisLabelColor;
}

void Graph::SetYAxisLabelColor(QColor NewYAxisLabelColor)
{
  YAxisLabelColor = NewYAxisLabelColor;
}

void Graph::SetXTicksLabelsColor(QColor NewXTicksLabelsColor)
{
  XTicksLabelsColor = NewXTicksLabelsColor;
}

void Graph::SetYTicksLabelsColor(QColor NewYTicksLabelsColor)
{
  YTicksLabelsColor = NewYTicksLabelsColor;
}

void Graph::SetStatsBoxColor(QColor NewStatsBoxColor)
{
  StatsBoxColor = NewStatsBoxColor;
}

void Graph::SetStatsBoxTextColor(QColor NewStatsBoxTextColor)
{
  StatsBoxTextColor = NewStatsBoxTextColor;
}

void Graph::SetXAxisLabelFont(QFont NewXAxisLabelFont)
{
  XAxisLabelFont = NewXAxisLabelFont;
}

void Graph::SetYAxisLabelFont(QFont NewYAxisLabelFont)
{
  YAxisLabelFont = NewYAxisLabelFont;
}

void Graph::SetXTicksLabelsFont(QFont NewXTicksLabelsFont)
{
  XTicksLabelsFont = NewXTicksLabelsFont;
}

void Graph::SetYTicksLabelsFont(QFont NewYTicksLabelsFont)
{
  YTicksLabelsFont = NewYTicksLabelsFont;
}

void Graph::SetStatsBoxFont(QFont NewStatsBoxFont)
{
  StatsBoxFont = NewStatsBoxFont;
}

void Graph::SetXAxisLabel(QString NewXAxisLabel)
{
  XAxisLabel = NewXAxisLabel;
}

void Graph::SetYAxisLabel(QString NewYAxisLabel)
{
  YAxisLabel = NewYAxisLabel;
}

void Graph::SetXGridLineStyle(PenStyle NewXGridLineStyle)
{
  XGridLineStyle = NewXGridLineStyle;
}

void Graph::SetYGridLineStyle(PenStyle NewYGridLineStyle)
{
  YGridLineStyle = NewYGridLineStyle;
}

void Graph::SetBigXTicksLineStyle(PenStyle NewBigXTicksLineStyle)
{
  BigXTicksLineStyle = NewBigXTicksLineStyle;
}

void Graph::SetBigYTicksLineStyle(PenStyle NewBigYTicksLineStyle)
{
  BigYTicksLineStyle = NewBigYTicksLineStyle;
}

void Graph::SetSmallXTicksLineStyle(PenStyle NewSmallXTicksLineStyle)
{
  SmallXTicksLineStyle = NewSmallXTicksLineStyle;
}

void Graph::SetSmallYTicksLineStyle(PenStyle NewSmallYTicksLineStyle)
{
  SmallYTicksLineStyle = NewSmallYTicksLineStyle;
}

void Graph::SetStatsBoxLineStyle(PenStyle NewStatsBoxLineStyle)
{
  StatsBoxLineStyle = NewStatsBoxLineStyle;
}

void Graph::AddData(BBDO * NewData)
{
  AllDatas.push_back(NewData);
  
  if(!ComputeBoundsFlag)
    ComputeBoundsFlag = true;
}

BBDO * Graph::GetData(int I)
{
  if(I < 0 || I >= AllDatas.size())
    {
      BBDO * NewBBDO = new BBDO;
      return NewBBDO;
    }
  
  return AllDatas[I];
}

vector <BBDO *> Graph::GetData()
{
  return AllDatas;
}

//! Computing the data's bounds.
/*!
  This method loops over the data and computes the inferior and superior data bounds. It calls the virtual methods ComputeInfSup() and AddMargin2InfSup(), so it is possible to process many data types (in the subclasses).
*/
void Graph::ComputeBounds()
{
  XInf = -1.e99;
  XSup =  1.e99;
  YInf = -1.e99;
  YSup =  1.e99;
  
  if(!AllDatas.empty())
    {
      AllDatas[0]->ComputeMinMax();
      
      XInf = AllDatas[0]->GetXMin();
      XSup = AllDatas[0]->GetXMax();
      YInf = AllDatas[0]->GetYMin();
      YSup = AllDatas[0]->GetYMax();
    }
  
  if(XInfBoundFlag)
    XInf = XInfBound;
  if(YInfBoundFlag)
    YInf = YInfBound;
  if(XSupBoundFlag)
    XSup = XSupBound;
  if(YSupBoundFlag)
    YSup = YSupBound;
  
  /*
    We assume that if XInfBoundFlag == XSupBoundFlag == true  then XInf <  XSup.
    And       that if XInfBoundFlag == XSupBoundFlag == false then XInf == XSup.
  */
  
  if(XInf >= XSup)
    {
      if(XInfBoundFlag)
	XSup = XInf + 1.;
      else if(XSupBoundFlag)
	XInf = XSup - 1.;
      else
	{
	  XInf -= 0.5;
	  XSup += 0.5;
	}
    }
  
  if(YInf >= YSup)
    {
      if(YInfBoundFlag)
	YSup = YInf + 1.;
      else if(YSupBoundFlag)
	YInf = YSup - 1.;
      else
	{
	  YInf -= 0.5;
	  YSup += 0.5;
	}
    }
  
  AddMargin2InfSup();
  
  if(!LogarithmicXAxisFlag
     ||
     (LogarithmicXAxisFlag && (XInf <= 0. || XSup <= 0.)))
    ComputeTicksPositions(XInf, XSup, BigXTicksPositions, SmallXTicksPositions);
  else
    ComputeTicksLogPositions(XInf, XSup, BigXTicksPositions, SmallXTicksPositions);
  
  if(!LogarithmicYAxisFlag
     ||
     (LogarithmicYAxisFlag && (YInf <= 0. || YSup <= 0.)))
    ComputeTicksPositions(YInf, YSup, BigYTicksPositions, SmallYTicksPositions);
  else
    ComputeTicksLogPositions(YInf, YSup, BigYTicksPositions, SmallYTicksPositions);
  
  ComputeBoundsFlag = false;
  
  if(!ComputeTransfoFlag)
    ComputeTransfoFlag = true;
}

//! Adds margins.
/*!
  In this method, we augment the computed bounds by 10% in order to take account of the (reasonable) finite size of the drawn points. This method is virtual, so it is possible to re-implement it in the subclasses so it acts differently (in FITSContainer, we don't want margins for example).
*/
void Graph::AddMargin2InfSup()
{
  if(!AllDatas.empty())
    {
      FITSContainer * OldFITSContainer = dynamic_cast<FITSContainer *>(AllDatas[0]);
      
      if(OldFITSContainer)
	return;
    }
  
  if(!LogarithmicXAxisFlag
     ||
     (LogarithmicXAxisFlag && (XInf <= 0. || XSup <= 0.)))
    {
      double XInfXSup = XSup-XInf;
      
      XInf -= 0.05*XInfXSup;
      XSup += 0.05*XInfXSup;
    }
  else
    {
      double XInfXSup = log10(XSup/XInf);
      
      XInf /= pow(10, 0.05*XInfXSup);
      XSup *= pow(10, 0.05*XInfXSup);
    }
  
  if(!LogarithmicYAxisFlag
     ||
     (LogarithmicYAxisFlag && (YInf <= 0. || YSup <= 0.)))
    {
      double YInfYSup = YSup-YInf;
      
      YInf -= 0.05*YInfYSup;
      YSup += 0.05*YInfYSup;
    }
  else
    {
      double YInfYSup = log10(YSup/YInf);
      
      YInf /= pow(10, 0.05*YInfYSup);
      YSup *= pow(10, 0.05*YInfYSup);
    }
}

void Graph::ComputeTransfo()
{
  if(!LogarithmicXAxisFlag
     ||
     (LogarithmicXAxisFlag && (XInf <= 0. || XSup <= 0.)))
    {
      AlphaX = pow(-1., int(ReverseXAxisFlag))*(Width)/(XSup-XInf);
      BetaX  = 0.5*(2.*X+Width-AlphaX*(XSup+XInf));
    }
  else
    {
      AlphaX = pow(-1., int(ReverseXAxisFlag))*(Width)/(log10(XSup/XInf));
      BetaX  = 0.5*(2.*X+Width-AlphaX*log10(XSup*XInf));
    }
  
  if(!LogarithmicYAxisFlag
     ||
     (LogarithmicYAxisFlag && (YInf <= 0. || YSup <= 0.)))
    {
      AlphaY = pow(-1., int(ReverseYAxisFlag))*(Height)/(YSup-YInf);
      BetaY  = 0.5*(2.*Y+Height-AlphaY*(YSup+YInf));
    }
  else
    {
      AlphaY = pow(-1., int(ReverseYAxisFlag))*(Height)/(log10(YSup/YInf));
      BetaY  = 0.5*(2.*Y+Height-AlphaY*log10(YSup*YInf));
    }
  
  ComputeTransfoFlag = false;
}

/*
  The values that need to be flexible (settable) are marked ###.
  There should be 3 levels of flexibility:
  -# The algorithm does everything alone (the number of small ticks are fixed by the user though - or is possible to do otherwise ???).
  -# The user fixes the number of big and/or small ticks.
  -# The user fixes the number of big ticks and a starting value and/or small ones.
  In any case, we need to test thouroughly the values given by the user: a, b, ticks number...
*/
void Graph::ComputeTicksPositions(double a, double b, vector <double> & BigTicksPositions, vector <double> & SmallTicksPositions)
{
  if(b <= a)
    return;
  
  BigTicksPositions.clear();
  SmallTicksPositions.clear();
  
  int    TicksNumberLimit = 10;//###
  double PaceLimit        = (b-a)/TicksNumberLimit;
  double Pace             = pow(10., int(log10(b-a))-1.);
  double InitialValue     = a;
  
  /* Computing the pace */
  
  bool Index = false;
  double Factor[2] = {2., 2.5};//###
  
  while(Pace < PaceLimit)
    {
      Pace *= Factor[int(Index)];
      Index = !Index;
    }
  
  /* Computing the initial value */
  
  if(a != 0.)
    {
      InitialValue = (fabs(a)/a)*Pace;
      
      while(InitialValue >= (a+Pace))
	InitialValue -= Pace;
      
      while(InitialValue < a)
	InitialValue += Pace;
    }
  
  /* Computing the big ticks */
  
  for(double i = InitialValue; i <= b; i += Pace)
    BigTicksPositions.push_back(i);
  
  /* Computing the small ticks number */
  
  int SmallTicksNumber = 1+int(TicksNumberLimit/BigTicksPositions.size());
  
  if(SmallTicksNumber > TicksNumberLimit)
    SmallTicksNumber = TicksNumberLimit;
  
  while(SmallTicksNumber == 1 || 10%SmallTicksNumber != 0)//###
    SmallTicksNumber++;
  
  /* Computing the small pace */
  
  double SmallPace = Pace/SmallTicksNumber;
  
  /* Computing the small ticks */
  
  for(double i = InitialValue-Pace; i < InitialValue; i += SmallPace)
    if(i >= a)
      SmallTicksPositions.push_back(i);
  
  for(int i = 0; i < BigTicksPositions.size()-1; i++)
    for(int j = 1; j <= SmallTicksNumber; j++)
      SmallTicksPositions.push_back(BigTicksPositions[i]+j*SmallPace);
  
  for(double i = BigTicksPositions[BigTicksPositions.size()-1]+SmallPace; i <= b; i += SmallPace)
    SmallTicksPositions.push_back(i);
}

/*
  See Graph::ComputeTicksLogPositions() todos (for flexibility stuff for example).
  Some precision issues (marked @@@).
  Test if (BigTicksPositions.size() < 2) (marked (1)). This should never happen however ?
*/
void Graph::ComputeTicksLogPositions(double a, double b, vector <double> & BigTicksPositions, vector <double> & SmallTicksPositions)
{
  if(b <= a)
    return;
  
  if(b <= 100.*a)//###
    {
      ComputeTicksPositions(a, b, BigTicksPositions, SmallTicksPositions);
      
      return;
    }
  
  BigTicksPositions.clear();
  SmallTicksPositions.clear();
  
  int    TicksNumberLimit = 10;//###
  double PaceLimit        = log10(b/a)/TicksNumberLimit;
  double Pace             = 1.;
  double InitialValue     = 1.;
  
  /* Computing the pace */
  
  bool Index = false;
  double Factor[2] = {2., 2.5};//###
  
  while(Pace < PaceLimit)
    {
      Pace *= Factor[int(Index)];
      Index = !Index;
    }
  
  Pace = pow(10., Pace);
  
  /* Computing the initial value */
  
  while(InitialValue > a)
    InitialValue /= Pace;
  
  while(InitialValue*Pace < a)
    InitialValue *= Pace;
  
  /* Computing the big ticks */
  
  for(double i = InitialValue; i <= b; i *= Pace)
    if(i >= a)
      BigTicksPositions.push_back(i);
  
  //(1)
  
  /* Computing the small pace, ticks number, ticks */
  
  if(log10(BigTicksPositions[1]/BigTicksPositions[0]) < 2)//###
    {
      int    TicksNumberMidLimit = 5;//###
      double SmallPace           = BigTicksPositions[0]/10.;
      double SmallInitialValue   = BigTicksPositions[0];
      
      while(SmallInitialValue > a)
	SmallInitialValue -= SmallPace;
      
      for(double i = SmallInitialValue; i < BigTicksPositions[0]; i += SmallPace)
	{
	  if(i < a)
	    continue;
	  
	  if(
	     BigTicksPositions.size() <= TicksNumberMidLimit
	     ||
	     (BigTicksPositions.size() > TicksNumberMidLimit && fabs(i/BigTicksPositions[0]-0.5) < 0.01)//@@@
	     )
	    SmallTicksPositions.push_back(i);
	}
      
      for(int i = 0; i < BigTicksPositions.size()-1; i++)
	{
	  SmallPace = BigTicksPositions[i];
	  
	  double j  = BigTicksPositions[i]+SmallPace;
	  
	  while(j < BigTicksPositions[i+1])
	    {
	      if(
		 BigTicksPositions.size() <= TicksNumberMidLimit
		 ||
		 (BigTicksPositions.size() > TicksNumberMidLimit && fabs(j/BigTicksPositions[i]-5.) < 0.1)//@@@
		 )
		SmallTicksPositions.push_back(j);
	      
	      j += SmallPace;
	    }
	}
      
      SmallPace = BigTicksPositions[BigTicksPositions.size()-1];
      
      for(double i = BigTicksPositions[BigTicksPositions.size()-1]+SmallPace; i <= b; i += SmallPace)
	if(
	   BigTicksPositions.size() <= TicksNumberMidLimit
	   ||
	   (BigTicksPositions.size() > TicksNumberMidLimit && fabs(i/BigTicksPositions[BigTicksPositions.size()-1]-5.) < 0.1)//@@@
	   )
	  SmallTicksPositions.push_back(i);
    }
  else
    {
      int SmallTicksNumberLimit = 10;//###
      int LogSmallPace          = 1;
      int IntLogPace            = int(log10(Pace));
      
      while(LogSmallPace < IntLogPace/SmallTicksNumberLimit || IntLogPace%LogSmallPace != 0)
	LogSmallPace++;
      
      double SmallPace         = pow(10., LogSmallPace);      
      double SmallInitialValue = BigTicksPositions[0];
      
      while(SmallInitialValue > a)
	SmallInitialValue /= SmallPace;
      
      for(double i = SmallInitialValue; i < BigTicksPositions[0]; i *= SmallPace)
	{
	  if(i < a)
	    continue;
	  
	  SmallTicksPositions.push_back(i);
	}
      
      for(int i = 0; i < BigTicksPositions.size()-1; i++)
	{
	  double j  = BigTicksPositions[i]*SmallPace;
	  
	  while(j < BigTicksPositions[i+1])
	    {
	      SmallTicksPositions.push_back(j);
	      
	      j *= SmallPace;
	    }
	}
      
      for(double i = BigTicksPositions[BigTicksPositions.size()-1]*SmallPace; i <= b; i *= SmallPace)
	SmallTicksPositions.push_back(i);
    }
}

void Graph::ComputePositionsComplement()
{
  if(!ComputeTransfoFlag)
    ComputeTransfoFlag = true;
}

bool Graph::paintEventComplement()
{
  bool LocalFlag = false;
  
  if(ComputeBoundsFlag)
    {
      ComputeBounds();
      LocalFlag = true;
    }
  
  if(ComputeTransfoFlag)
    {
      ComputeTransfo();
      LocalFlag = true;
    }
  
  return LocalFlag;
}

void Graph::mousePressEvent(QMouseEvent * Event)
{
  if(Event->x() >= X && Event->x() <= X+Width && Event->y() >= Y && Event->y() <= Y+Height)
    {
      if(!AllDatas.empty())
	{
	  FITSContainer * OldFITSContainer = dynamic_cast<FITSContainer *>(AllDatas[0]);
	  
	  if(!OldFITSContainer)
	    {
	      emit SendCoordinates(XPixel2Value(Event->x()), YPixel2Value(Event->y()));
	      return;
	    }
	  
	  FITSImage OldFITSImage = OldFITSContainer->GetData();
	  double XPix = XPixel2Value(Event->x());
	  double YPix = YPixel2Value(Event->y());
	  double Val  = double(OldFITSImage.GetPixel(int(XPix), int(YPix)));
	  
	  emit SendCoordinatesAndValue(XPix, YPix, Val);
	  
	  return;
	}
      
      emit SendCoordinates(XPixel2Value(Event->x()), YPixel2Value(Event->y()));
    }
}

void Graph::mouseMoveEvent(QMouseEvent * Event)
{
  if(Event->x() >= X && Event->x() <= X+Width && Event->y() >= Y && Event->y() <= Y+Height)
    {
      if(!AllDatas.empty())
	{
	  FITSContainer * OldFITSContainer = dynamic_cast<FITSContainer *>(AllDatas[0]);
	  
	  if(!OldFITSContainer)
	    {
	      emit SendCoordinates(XPixel2Value(Event->x()), YPixel2Value(Event->y()));
	      return;
	    }
	  
	  FITSImage OldFITSImage = OldFITSContainer->GetData();
	  double XPix = XPixel2Value(Event->x());
	  double YPix = YPixel2Value(Event->y());
	  double Val  = double(OldFITSImage.GetPixel(int(XPix), int(YPix)));
	  
	  emit SendCoordinatesAndValue(XPix, YPix, Val);
	  
	  return;
	}
      
      emit SendCoordinates(XPixel2Value(Event->x()), YPixel2Value(Event->y()));
    }
  else
    emit UnSendCoordinates();
}

void Graph::mouseReleaseEvent(QMouseEvent *)
{
  emit UnSendCoordinates();
}

void Graph::DrawPixmapComplement()
{
  for(int i = 0; i < AllDatas.size(); i++)
    if(!AllDatas[i]->DrawSelf(this))
      emit DrawingProblem(AllDatas[i]->error());
  
  for(int i = 0; i < BigXTicksPositions.size(); i++)
    {
      AddLine(XValue2Pixel(BigXTicksPositions[i]), Y+Height, XValue2Pixel(BigXTicksPositions[i]), int(Y+Height-BigXTicksRatio*height()), BigXTicksLineWidth, BigXTicksColor, BigXTicksLineStyle);
      
      if(XTicksLabelsFlag)
	AddText(QString::number(BigXTicksPositions[i]), XValue2Pixel(BigXTicksPositions[i]), int(Y+Height+XAxis2TicksLabelsRatio*height()), 0, XTicksLabelsColor, XTicksLabelsFont, Qt::AlignHCenter, Qt::AlignTop);
      
      AddLine(XValue2Pixel(BigXTicksPositions[i]), int(Y+Height-BigXTicksRatio*height()), XValue2Pixel(BigXTicksPositions[i]), Y, XGridLineWidth, XGridColor, XGridLineStyle);
    }
  
  for(int i = 0; i < BigYTicksPositions.size(); i++)
    {
      AddLine(X, YValue2Pixel(BigYTicksPositions[i]), int(X+BigYTicksRatio*width()), YValue2Pixel(BigYTicksPositions[i]), BigYTicksLineWidth, BigYTicksColor, BigYTicksLineStyle);
      
      if(YTicksLabelsFlag)
	AddText(QString::number(BigYTicksPositions[i]), int(X-YAxis2TicksLabelsRatio*width()), YValue2Pixel(BigYTicksPositions[i]), 0, YTicksLabelsColor, YTicksLabelsFont, Qt::AlignRight, Qt::AlignVCenter);
      
      AddLine(int(X+BigYTicksRatio*width()), YValue2Pixel(BigYTicksPositions[i]), X+Width, YValue2Pixel(BigYTicksPositions[i]), YGridLineWidth, YGridColor, YGridLineStyle);
    }
  
  for(int i = 0; i < SmallXTicksPositions.size(); i++)
    AddLine(XValue2Pixel(SmallXTicksPositions[i]), Y+Height, XValue2Pixel(SmallXTicksPositions[i]), int(Y+Height-SmallXTicksRatio*height()), SmallXTicksLineWidth, SmallXTicksColor, SmallXTicksLineStyle);
  
  for(int i = 0; i < SmallYTicksPositions.size(); i++)
    AddLine(X, YValue2Pixel(SmallYTicksPositions[i]), int(X+SmallYTicksRatio*width()), YValue2Pixel(SmallYTicksPositions[i]), SmallYTicksLineWidth, SmallYTicksColor, SmallYTicksLineStyle);
  
  AddText(XAxisLabel, X+Width, int(Y+Height+XAxis2AxisLabelRatio*height()), 0., XAxisLabelColor, XAxisLabelFont, Qt::AlignRight, Qt::AlignTop);
  AddText(YAxisLabel, int(X-YAxis2AxisLabelRatio*width()), Y, 90., YAxisLabelColor, YAxisLabelFont, Qt::AlignRight, Qt::AlignBottom);
  
  if(StatsBoxFlag && !AllDatas.empty() && (dynamic_cast<Histo1D *>(AllDatas[0]) || dynamic_cast<Graph2D *>(AllDatas[0])))
    {
      AddLine(X+Width-int((StatsBoxWidthRatio+YAxis2StatsBoxRatio)*width()), Y+int(XAxis2StatsBoxRatio*width()), X+Width-int(YAxis2StatsBoxRatio*width()), Y+int(XAxis2StatsBoxRatio*width()), StatsBoxLineWidth, StatsBoxColor, StatsBoxLineStyle);
      
      AddLine(X+Width-int(YAxis2StatsBoxRatio*width()), Y+int(XAxis2StatsBoxRatio*width()), X+Width-int(YAxis2StatsBoxRatio*width()), Y+int((StatsBoxHeightRatio+XAxis2StatsBoxRatio)*width()), StatsBoxLineWidth, StatsBoxColor, StatsBoxLineStyle);
      
      AddLine(X+Width-int(YAxis2StatsBoxRatio*width()), Y+int((StatsBoxHeightRatio+XAxis2StatsBoxRatio)*width()), X+Width-int((StatsBoxWidthRatio+YAxis2StatsBoxRatio)*width()), Y+int((StatsBoxHeightRatio+XAxis2StatsBoxRatio)*width()), StatsBoxLineWidth, StatsBoxColor, StatsBoxLineStyle);
      
      AddLine(X+Width-int((StatsBoxWidthRatio+YAxis2StatsBoxRatio)*width()), Y+int((StatsBoxHeightRatio+XAxis2StatsBoxRatio)*width()), X+Width-int((StatsBoxWidthRatio+YAxis2StatsBoxRatio)*width()), Y+int(XAxis2StatsBoxRatio*width()), StatsBoxLineWidth, StatsBoxColor, StatsBoxLineStyle);
      
      AddText("Entries", X+Width-int((StatsBoxWidthRatio+YAxis2StatsBoxRatio-StatsBox2StatsRatio)*width()), Y+int((XAxis2StatsBoxRatio+StatsBoxHeightRatio/4)*width()), 0., StatsBoxTextColor, StatsBoxFont, Qt::AlignLeft, Qt::AlignVCenter);
      AddText("Mean", X+Width-int((StatsBoxWidthRatio+YAxis2StatsBoxRatio-StatsBox2StatsRatio)*width()), Y+int((XAxis2StatsBoxRatio+StatsBoxHeightRatio/2)*width()), 0., StatsBoxTextColor, StatsBoxFont, Qt::AlignLeft, Qt::AlignVCenter);
      AddText("Std. Dev.", X+Width-int((StatsBoxWidthRatio+YAxis2StatsBoxRatio-StatsBox2StatsRatio)*width()), Y+int((XAxis2StatsBoxRatio+StatsBoxHeightRatio*0.75)*width()), 0., StatsBoxTextColor, StatsBoxFont, Qt::AlignLeft, Qt::AlignVCenter);
      
      AddText(QString::number(AllDatas[0]->GetEntries()), X+Width-int((YAxis2StatsBoxRatio+StatsBox2StatsRatio)*width()), Y+int((XAxis2StatsBoxRatio+StatsBoxHeightRatio/4)*width()), 0., StatsBoxTextColor, StatsBoxFont, Qt::AlignRight, Qt::AlignVCenter);
      AddText(QString::number(AllDatas[0]->GetMean()), X+Width-int((YAxis2StatsBoxRatio+StatsBox2StatsRatio)*width()), Y+int((XAxis2StatsBoxRatio+StatsBoxHeightRatio/2)*width()), 0., StatsBoxTextColor, StatsBoxFont, Qt::AlignRight, Qt::AlignVCenter);
      AddText(QString::number(AllDatas[0]->GetSigma()), X+Width-int((YAxis2StatsBoxRatio+StatsBox2StatsRatio)*width()), Y+int((XAxis2StatsBoxRatio+StatsBoxHeightRatio*0.75)*width()), 0., StatsBoxTextColor, StatsBoxFont, Qt::AlignRight, Qt::AlignVCenter);
      
    }
}

double Graph::XPixel2Value(int XPixel)
{
  if(!LogarithmicXAxisFlag
     ||
     (LogarithmicXAxisFlag && (XInf <= 0. || XSup <= 0.)))
    return (XPixel-BetaX)/AlphaX;
  
  return pow(10., (XPixel-BetaX)/AlphaX);
}

double Graph::YPixel2Value(int YPixel)
{
  if(!LogarithmicYAxisFlag
     ||
     (LogarithmicYAxisFlag && (YInf <= 0. || YSup <= 0.)))
    return (2*Y+Height-YPixel-BetaY)/AlphaY;
  
  return pow(10., (2*Y+Height-YPixel-BetaY)/AlphaY);
}

int Graph::XValue2Pixel(double XValue)
{
  if(!LogarithmicXAxisFlag
     ||
     (LogarithmicXAxisFlag && (XInf <= 0. || XSup <= 0.)))
    return int(AlphaX*XValue+BetaX);
  
  return int(AlphaX*log10(XValue)+BetaX);
}

int Graph::YValue2Pixel(double YValue)
{
  if(!LogarithmicYAxisFlag
     ||
     (LogarithmicYAxisFlag && (YInf <= 0. || YSup <= 0.)))
    return int(2*Y+Height-(AlphaY*YValue+BetaY));
  
  return int(2*Y+Height-(AlphaY*log10(YValue)+BetaY));
}
