/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the BXMLToQLVHandler Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <bxmltoqlvhandler.h>

BXMLToQLVHandler::BXMLToQLVHandler()
  : QXmlDefaultHandler()
{
  /* We suppose that the parsed document is well formed. We don't check for errors nor warn the user for now. */
  
  NoContentQLVI = NULL;
}

BXMLToQLVHandler::~BXMLToQLVHandler()
{
}

bool BXMLToQLVHandler::startDocument()
{
  SectionStartedFlag = false;
  TitleStartedFlag   = false;
  
  SectionID         = "";
  SectionAnchor     = "";
  MainSectionAnchor = "";
  SectionTitle      = "";
  
  return true;
}

bool BXMLToQLVHandler::startElement(const QString &, const QString &, const QString & Name, const QXmlAttributes & Attributes)
{
  if(Name == "article" || Name.left(4) == "sect")
    {
      SectionStartedFlag = true;
      
      SectionID = Attributes.value(QString("id"));
      
      if(Name == "article" || Name == "sect1")
	{
	  SectionAnchor     = SectionID + ".html";
	  MainSectionAnchor = SectionAnchor;
	}
      else
	SectionAnchor = MainSectionAnchor + "#" + SectionID.upper();/* The generated HTMLs have anchors in caps */
    }
  
  if(Name == "title" && SectionStartedFlag)
    TitleStartedFlag = true;
  
  return true;
}

bool BXMLToQLVHandler::endElement(const QString &, const QString &, const QString & Name)
{
  if(Name == "article" || Name.left(4) == "sect")
    {
      SectionStartedFlag = false;
      
      SectionID     = "";
      SectionAnchor = "";
      
      if(Name == "article" || Name == "sect1")
	MainSectionAnchor = "";
      
      BLVIStack.pop();
    }
  
  if(Name == "title" && SectionStartedFlag)
    {
      if(NoContentQLVI)
	{
	  ListViewQLV->takeItem(NoContentQLVI);
	  NoContentQLVI = NULL;
	}
      
      TitleStartedFlag = false;
      
      BListViewItem * element;
      
      if(!BLVIStack.isEmpty())
	{
	  QListViewItem * LastChild = BLVIStack.top()->firstChild();
	  
	  if(LastChild)
	    while(LastChild->nextSibling())
	      LastChild = LastChild->nextSibling();
	  
	  element = new BListViewItem(BLVIStack.top(), LastChild, SectionTitle, SectionID, SectionAnchor);
	}
      else
	element = new BListViewItem(ListViewQLV, SectionTitle, SectionID, SectionAnchor);
      
      BLVIStack.push(element);
      
      SectionTitle = "";
    }
  
  return true;
}

bool BXMLToQLVHandler::characters(const QString & Characters)
{
  if(TitleStartedFlag)
    SectionTitle += Characters;
  
  return true;
}

void BXMLToQLVHandler::SetListView(QListView * NewListViewQLV)
{
  ListViewQLV = NewListViewQLV;
  
  NoContentQLVI = new QListViewItem(ListViewQLV, QObject::trUtf8("No content is available"));
}
