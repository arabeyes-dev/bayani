/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the Frame Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <frame.h>
#include <gui.h>

Frame::Frame(QWidget * Parent, const char * Name)
  : BBGO(Parent, Name)
{
  SetTitleUp       (true);
  SetYRatio        (0.1);
  SetWidthRatio    (0.8);
  SetBox2TitleRatio(0.05);
  
  XRatioOld      = XRatio;
  YRatioOld      = YRatio;
  WidthRatioOld  = WidthRatio;
  HeightRatioOld = HeightRatio;
  
  SetR2L              (false);
  SetHorLinesWidth    (1);
  SetVerLinesWidth    (1);
  SetHorLinesColor    (QColor("#000000"));
  SetVerLinesColor    (QColor("#000000"));
  SetHorLinesLineStyle(Qt::SolidLine);
  SetVerLinesLineStyle(Qt::SolidLine);
  SetSaveFileName     (QString(""));
  Zone                (1, 1);
}

Frame::~Frame()
{
}

void Frame::SetR2L(bool NewR2LFlag)
{
  R2LFlag = NewR2LFlag;
}

void Frame::SetHorLinesWidth(int NewHorLinesWidth)
{
  HorLinesWidth = NewHorLinesWidth;
}

void Frame::SetVerLinesWidth(int NewVerLinesWidth)
{
  VerLinesWidth = NewVerLinesWidth;
}

void Frame::SetHorLinesColor(QColor NewHorLinesColor)
{
  HorLinesColor = NewHorLinesColor;
}

void Frame::SetVerLinesColor(QColor NewVerLinesColor)
{
  VerLinesColor = NewVerLinesColor;
}

void Frame::SetHorLinesLineStyle(PenStyle NewHorLinesLineStyle)
{
  HorLinesLineStyle = NewHorLinesLineStyle;
}

void Frame::SetVerLinesLineStyle(PenStyle NewVerLinesLineStyle)
{
  VerLinesLineStyle = NewVerLinesLineStyle;
}

QString Frame::GetSaveFileName()
{
  return SaveFileName;
}

void Frame::SetSaveFileName(QString NewSaveFileName)
{
  SaveFileName = NewSaveFileName;
}

void Frame::Zone(int ZoneX, int ZoneY)
{
  if(ZoneX <= 0)
    ZoneX = 1;
  if(ZoneY <= 0)
    ZoneY = 1;
  
  PermPlotsNbr = ZoneX*ZoneY;
  
  for(int i = 0; i < ThePlots.size(); i++)
    if(ThePlots[i] != NULL)
      ThePlots[i]->close();
  
  ThePlots.clear();
  ThePlotsCoordinates.clear();
  
  LastSetPlot = -1;
  ActivePlot  = -1;
  
  for(int j = 0; j < ZoneY; j++)
    for(int i = 0; i < ZoneX; i++)
      {
	PlotsCoord Intermed;
	
	if(R2LFlag)
	  /* Zones are counted from Up-Right to Bottom-Left: X++ Then Y++ */
	  Intermed.XRatio = XRatio+(ZoneX-1-i)*(WidthRatio/ZoneX);
	else
	  /* Zones are counted from Up-Left to Bottom-Right: X++ Then Y++ */
	  Intermed.XRatio = XRatio+i*(WidthRatio/ZoneX);
	
	Intermed.YRatio      = YRatio+j*(HeightRatio/ZoneY);
	Intermed.WidthRatio  = WidthRatio/ZoneX;
	Intermed.HeightRatio = HeightRatio/ZoneY;
	
	Intermed.X      = int(Intermed.XRatio*width());
	Intermed.Y      = int(Intermed.YRatio*height());
	Intermed.Width  = int(Intermed.WidthRatio*width());
	Intermed.Height = int(Intermed.HeightRatio*height());
	
	ThePlotsCoordinates.push_back(Intermed);
      }
}

void Frame::SubZone(int SubZoneNbr, int SubZoneX, int SubZoneY)
{
  if(ThePlotsCoordinates.size() < 1)
    return;
  
  if(SubZoneNbr >= ThePlotsCoordinates.size())
    SubZoneNbr = ThePlotsCoordinates.size()-1;
  if(SubZoneNbr < 0)
    SubZoneNbr = 0;
  if(SubZoneX <= 0)
    SubZoneX = 1;
  if(SubZoneY <= 0)
    SubZoneY = 1;

  PermPlotsNbr += SubZoneX*SubZoneY-1;
  
  for(int i = 0; i < ThePlots.size(); i++)
    if(ThePlots[i] != NULL)
      ThePlots[i]->close();
  
  ThePlots.clear();
  
  LastSetPlot = -1;
  ActivePlot  = -1;
  
  vector <PlotsCoord> Temp;
  for(int i = 0; i < SubZoneNbr; i++)
    Temp.push_back(ThePlotsCoordinates[i]);
  
  for(int j = 0; j < SubZoneY; j++)
    for(int i = 0; i < SubZoneX; i++)
      {
	PlotsCoord Intermed;
	
	if(R2LFlag)
	  /* Zones are counted from Up-Right to Bottom-Left: X++ Then Y++ */
	  Intermed.XRatio = ThePlotsCoordinates[SubZoneNbr].XRatio+(SubZoneX-1-i)*(ThePlotsCoordinates[SubZoneNbr].WidthRatio/SubZoneX);
	else
	  /* Zones are counted from Up-Left to Bottom-Right: X++ Then Y++ */
	  Intermed.XRatio = ThePlotsCoordinates[SubZoneNbr].XRatio+i*(ThePlotsCoordinates[SubZoneNbr].WidthRatio/SubZoneX);
	
	Intermed.YRatio      = ThePlotsCoordinates[SubZoneNbr].YRatio+j*ThePlotsCoordinates[SubZoneNbr].HeightRatio/SubZoneY;
	Intermed.WidthRatio  = ThePlotsCoordinates[SubZoneNbr].WidthRatio/SubZoneX;
	Intermed.HeightRatio = ThePlotsCoordinates[SubZoneNbr].HeightRatio/SubZoneY;
	
	Intermed.X      = int(Intermed.XRatio*width());
	Intermed.Y      = int(Intermed.YRatio*height());
	Intermed.Width  = int(Intermed.WidthRatio*width());
	Intermed.Height = int(Intermed.HeightRatio*height());
	
	Temp.push_back(Intermed);
      }
  
  for(int i = SubZoneNbr+1; i < ThePlotsCoordinates.size(); i++)
    Temp.push_back(ThePlotsCoordinates[i]);
  
  ThePlotsCoordinates = Temp;
}

int Frame::PlotsNumber()
{
  return PermPlotsNbr;
}

int Frame::ExistingPlotsNumber()
{
  return ThePlots.size();
}

bool Frame::IsEmpty()
{
  return ThePlots.empty();
}

void Frame::AddPlot(Graph * NewPlot)
{
  connect(NewPlot, SIGNAL(Modified(bool)), this, SLOT(SetModified(bool)));
  
  if(ThePlots.size() < PermPlotsNbr)
    {
      ThePlots.push_back(NewPlot);
      ActivePlot = ThePlots.size()-1;
    }
  else
    {
      LastSetPlot ++;
      if(LastSetPlot == ThePlots.size())
	LastSetPlot = 0;
      
      if(ThePlots[LastSetPlot] != NULL)
	ThePlots[LastSetPlot]->close();
      
      ThePlots[LastSetPlot] = NewPlot;
      ActivePlot = LastSetPlot;
    }
  
  ThePlots[ActivePlot]->setGeometry(ThePlotsCoordinates[ActivePlot].X, ThePlotsCoordinates[ActivePlot].Y, ThePlotsCoordinates[ActivePlot].Width, ThePlotsCoordinates[ActivePlot].Height);
}

Graph * Frame::GetActivePlot()
{
  if(ActivePlot >= 0 && ActivePlot < ThePlots.size())
    return ThePlots[ActivePlot];
  else
    {
      Graph * NewGraph = NULL;
      
      return NewGraph;
    }
}

void Frame::SetActivePlot(int NewActivePlot)
{
  if(NewActivePlot < 0 || NewActivePlot >= ThePlots.size())
    return;
  
  ActivePlot = NewActivePlot;
}

void Frame::DeleteActivePlot()
{
  if(ThePlots.empty())
    return;
  
  if(ThePlots[ActivePlot] != NULL)
    ThePlots[ActivePlot]->close();
  
  ThePlots.erase(ThePlots.begin() + ActivePlot);
  
  ActivePlot = ThePlots.size()-1;
}

int Frame::activePlotId()
{
  return ActivePlot;
}

void Frame::ComputePositionsComplement()
{
  for(int i = 0; i < ThePlotsCoordinates.size(); i++)
    {
      ThePlotsCoordinates[i].WidthRatio  *= WidthRatio /WidthRatioOld;
      ThePlotsCoordinates[i].HeightRatio *= HeightRatio/HeightRatioOld;
      ThePlotsCoordinates[i].XRatio       = (ThePlotsCoordinates[i].XRatio-XRatioOld)*(WidthRatio /WidthRatioOld)  + XRatioOld;
      ThePlotsCoordinates[i].YRatio       = (ThePlotsCoordinates[i].YRatio-YRatioOld)*(HeightRatio/HeightRatioOld) + YRatioOld;
      ThePlotsCoordinates[i].XRatio      += (XRatio-XRatioOld);
      ThePlotsCoordinates[i].YRatio      += (YRatio-YRatioOld);
      
      ThePlotsCoordinates[i].X      = int(ThePlotsCoordinates[i].XRatio      * width());
      ThePlotsCoordinates[i].Y      = int(ThePlotsCoordinates[i].YRatio      * height());
      ThePlotsCoordinates[i].Width  = int(ThePlotsCoordinates[i].WidthRatio  * width());
      ThePlotsCoordinates[i].Height = int(ThePlotsCoordinates[i].HeightRatio * height());
    }
  
  XRatioOld      = XRatio;
  YRatioOld      = YRatio;
  WidthRatioOld  = WidthRatio;
  HeightRatioOld = HeightRatio;
}

void Frame::DrawPixmapComplement()
{
  for(int i = 0; i < ThePlotsCoordinates.size(); i++)
    {
      if(ThePlotsCoordinates[i].Y != Y)
	AddLine(ThePlotsCoordinates[i].X, ThePlotsCoordinates[i].Y, ThePlotsCoordinates[i].X+ThePlotsCoordinates[i].Width, ThePlotsCoordinates[i].Y, HorLinesWidth, HorLinesColor, HorLinesLineStyle);
      
      if(ThePlotsCoordinates[i].X != X)
	AddLine(ThePlotsCoordinates[i].X, ThePlotsCoordinates[i].Y, ThePlotsCoordinates[i].X, ThePlotsCoordinates[i].Y+ThePlotsCoordinates[i].Height, VerLinesWidth, VerLinesColor, VerLinesLineStyle);
    }
  
  for(int i = 0; i < ThePlots.size(); i++)
    ThePlots[i]->setGeometry(ThePlotsCoordinates[i].X, ThePlotsCoordinates[i].Y, ThePlotsCoordinates[i].Width, ThePlotsCoordinates[i].Height);
}

void Frame::showEvent(QShowEvent * Event)
{
  if(!Event->spontaneous())/* Necessary test */
    emit Shown(true);
}

void Frame::closeEvent(QCloseEvent * Event)
{
  if(IsModified()
     &&
     GUI::PrintMessage(this, MESSAGE_WARNING, trUtf8("The frame has been modified.<br />Are you sure you want to close it without saving it?"), trUtf8("Modified frame")) == QMessageBox::No)
    {
      Event->ignore();
      
      emit CanceledClose(trUtf8("Close canceled: the frame has been modified."));
    }
  else
    {
      Event->accept();
      
      emit Closed(false);
    }
}
