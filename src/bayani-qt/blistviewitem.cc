/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the BListView Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <blistviewitem.h>

BListViewItem::BListViewItem(QListView * Parent, QString Label1, QString ID, QString Anchor)
  : QListViewItem(Parent, Label1)
{
  SetID    (ID);
  SetAnchor(Anchor);
}

BListViewItem::BListViewItem(QListViewItem * Parent, QListViewItem * After, QString Label1, QString ID, QString Anchor)
  : QListViewItem(Parent, After, Label1)
{
  SetID    (ID);
  SetAnchor(Anchor);
}

BListViewItem::~BListViewItem()
{
}

void BListViewItem::SetID(QString NewID)
{
  ID = NewID;
}

QString BListViewItem::GetID()
{
  return ID;
}

void BListViewItem::SetAnchor(QString NewAnchor)
{
  Anchor = NewAnchor;
}

QString BListViewItem::GetAnchor()
{
  return Anchor;
}
