/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the BBGO Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_BBGO_H
#define BAYANI_BBGO_H

using namespace std;

/* "Usual" headers */
#include <math.h>
#include <vector>
/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qstring.h>
#include <qfont.h>
#include <qcolor.h>
#include <qwidget.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <qimage.h>
#include <qfile.h>
#include <qpointarray.h>

/* Shape types */
#define HOLCIRCLE 0
#define FILCIRCLE 1
#define HOLSQUARE 2
#define FILSQUARE 3

/*!
  \file bbgo.h
  \brief Header file for the BBGO class.
  
  The BBGO class is defined in this file.
*/

//! General class for graphical objects.
/*!
  BBGO stands for <b>Basic Bayani Graphical Object</b>. It is a general class that summarizes all the common methods used by the different Bayani graphical classes. It is directly subclassed into Frame and Graph. It inherits QWidget and consequently can be easily included in a QT GUI and behaves like any other widget. The graphical layout is totally configurable using many built-in methods.
  \sa Frame and Graph.
*/
class BBGO: public QWidget
{
  Q_OBJECT
  
 protected:
  struct   Line {int X1; int Y1; int X2; int Y2; int LineWidth; QColor Color; PenStyle LineStyle; bool WrapFlag;};
  struct   Text {QString Text; int X; int Y; double Angle; QFont Font; QColor Color; int HorJust; int VerJust; int FontTypeSizeEtc; int TextDirection;};
  struct   Shape{int X; int Y; int Size; int Type; QColor Color;};
  struct   Image{QImage BQImage; int X; int Y;};
  struct   Polyline{QPointArray PointArray; int LineWidth; QColor Color; PenStyle LineStyle;};
  struct   Polygon{QPointArray PointArray; QColor Color; BrushStyle FillStyle;};
  bool     TitleUpFlag;
  bool     ComputePositionsFlag;
  bool     ForceRepaintFlag;
  bool     ModifiedFlag;
  int      X;
  int      Y;
  int      Width;
  int      Height;
  int      BoxLineWidth;
  double   XRatio;
  double   YRatio;
  double   WidthRatio;
  double   HeightRatio;
  double   Box2TitleRatio;
  vector   <Line>     AllLines;
  vector   <Text>     AllTexts;
  vector   <Shape>    AllShapes;
  vector   <Image>    AllImages;
  vector   <Polyline> AllPolylines;
  vector   <Polygon>  AllPolygons;
  QColor   BGColor;
  QColor   BoxColor;
  QColor   TitleColor;
  QFont    TitleFont;
  QString  Title;
  PenStyle BoxLineStyle;
  QPixmap  InternalPixmap;
  
          void ComputePositions          ();
  virtual void ComputePositionsComplement();
    	  void resizeEvent               (QResizeEvent *);
          void paintEvent                (QPaintEvent *);
  virtual bool paintEventComplement      ();
	  void DrawText                  (QString, int, int, double, int, int, QColor, QFont);
	  void AddLine                   (int, int, int, int, int, QColor, PenStyle, bool WrapFlag = false);
	  void AddText                   (QString, int, int, double, QColor, QFont, int, int);
	  void AddShape                  (int, int, int, QColor, int);
	  void AddImage                  (QImage, int, int);
	  void AddPolyline               (QPointArray, int, QColor, PenStyle);
	  void AddPolygon                (QPointArray, QColor, BrushStyle);
	  void DrawPixmap                ();
  virtual void DrawPixmapComplement      ();
	  
 public:
                      BBGO              (QWidget * Parent = 0, const char * Name = 0);
  virtual            ~BBGO              ();
          void        SetTitleUp        (bool);
	  void        SetBoxLineWidth   (int);
	  void        SetXRatio         (double);
	  void        SetYRatio         (double);
	  void        SetWidthRatio     (double);
	  void        SetHeightRatio    (double);
	  void        SetBox2TitleRatio (double);
	  void        SetAxis2TitleRatio(double);
	  void        SetBGColor        (QColor);
	  void        SetBoxColor       (QColor);
	  void        SetTitleColor     (QColor);
	  void        SetTitleFont      (QFont);
	  void        SetTitle          (QString);
	  void        SetBoxLineStyle   (PenStyle);
	  void        WritePostscript   (QFile);
	  void        WriteEPostscript  (QFile);
	  void        ForceRepaint      ();
	  bool        IsModified        ();
	  QSizePolicy sizePolicy        () const;
	  
 public slots:
  void SetModified(bool);
 
 signals:
  void Modified(bool);
};

#endif
