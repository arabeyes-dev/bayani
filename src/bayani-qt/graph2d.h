/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the Graph2D Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_GRAPH2D_H
#define BAYANI_GRAPH2D_H

/* "Usual" headers */
#include <math.h>
/* QT headers */
#include <qcolor.h>
/* Bayani headers */
#include <dataarray.h>
#include <graph.h>
#include <bbdo.h>

/*!
  \file graph2d.h
  \brief Header file for the Graph2D class.
  
  The Graph2D class is defined in this file.
*/

//! Class to display 2D graphs.
/*!
  This class inherits BBDO. It is specially designed to display two dimensional graphs.
  \sa BBDO, Func1D, Histo1D and FITSContainer.
*/
class Graph2D: public BBDO
{
 protected:
  int          DataType;
  int          ErrorsLineWidth;
  double       DataRatio;
  QColor       DataColor;
  QColor       ErrorsLineColor;
  Qt::PenStyle ErrorsLineStyle;
  DataArray    InternalDataArray;
  
  virtual void ComputeMinMax();
  virtual bool DrawSelf     (Graph *);
  
 public:
                    Graph2D           ();
  virtual          ~Graph2D           ();
          void      SetDataRatio      (double);
	  void      SetDataType       (int);
	  void      SetDataColor      (QColor);
	  void      SetErrorsLineWidth(int);
	  void      SetErrorsLineStyle(Qt::PenStyle);
	  void      SetErrorsLineColor(QColor);
	  void      SetData           (DataArray);
  virtual int       GetEntries        ();
  virtual double    GetMean           ();
  virtual double    GetSigma          ();
	  DataArray GetData           ();
};

#endif
