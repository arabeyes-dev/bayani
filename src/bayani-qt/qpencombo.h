/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QPenCombo Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QPENCOMBO_H
#define BAYANI_QPENCOMBO_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qcombobox.h>
#include <qpixmap.h>
#include <qcolor.h>
#include <qpainter.h>
#include <qpen.h>

/* Number of QT pen styles */
#define PEN_STYLES_NUMBER 6

/*!
  \file qpencombo.h
  \brief Header file for the QPenCombo class.
  
  The QPenCombo class is defined in this file.
*/

//! A line style selector.
/*!
  This class inherits QComboBox. It gives the user the ability to select a specific line style.
*/
class QPenCombo: public QComboBox
{
 public:
                       QPenCombo      (QWidget * Parent = 0, const char * Name = 0);
  virtual             ~QPenCombo      ();
          void         SetCurrentStyle(Qt::PenStyle);
	  Qt::PenStyle GetCurrentStyle();
};

#endif
