/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QLineCombo Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qlinecombo.h>

QLineCombo::QLineCombo(QWidget * Parent, const char * Name)
  : QComboBox(Parent, Name)
{
  setEditable(false);
  
  for(int i = 1; i <= 10; i++)
    {
      QPixmap Pixmap(50, 16);
      Pixmap.fill(QColor(255, 255, 255));
      
      QPainter Painter(&Pixmap);
      QPen     Pen;
      
      Pen.setWidth(i);
      
      Painter.setPen(Pen);
      Painter.drawLine(0, int(Pixmap.height()/2.), Pixmap.width(), int(Pixmap.height()/2.));
      
      insertItem(Pixmap, QString::number(i));
    }
  
  setCurrentItem(0);
  
  setFixedSize(sizeHint());
}

QLineCombo::~QLineCombo()
{
}

void QLineCombo::SetCurrentWidth(int NewWidth)
{
  if(NewWidth < 1 || NewWidth > 10)
    return;
  
  if(NewWidth-1 != currentItem())
    setCurrentItem(NewWidth-1);
}

int QLineCombo::GetCurrentWidth()
{
  return currentItem()+1;
}
