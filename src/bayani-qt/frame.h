/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of Frame Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_FRAME_H
#define BAYANI_FRAME_H

using namespace std;

/* "Usual" headers */
#include <vector>
/* QT headers */
#include <qnamespace.h>
#include <qstring.h>
#include <qcolor.h>
#include <qwidget.h>
#include <qevent.h>
#include <qmessagebox.h>
/* Bayani headers */
#include <bbgo.h>
#include <graph.h>

/*!
  \file frame.h
  \brief Header file for the Frame class.
  
  The Frame class is defined in this file.
*/

//! Class to graphically wrap Graph objects.
/*!
  This class inherits BBGO. It is specially designed to <i>graphically</i> encapsulate Graph objects and align them in a two dimensional grid.
  \sa BBGO and Graph.
*/
class Frame: public BBGO
{
  Q_OBJECT
  
 protected:
  struct   PlotsCoord{int X; int Y; int Width; int Height; double XRatio; double YRatio; double WidthRatio; double HeightRatio;};
  bool     R2LFlag;
  int      HorLinesWidth;
  int      VerLinesWidth;
  int      PermPlotsNbr;
  int      ActivePlot;
  int      LastSetPlot;
  double   XRatioOld;
  double   YRatioOld;
  double   WidthRatioOld;
  double   HeightRatioOld;
  QString  SaveFileName;
  QColor   HorLinesColor;
  QColor   VerLinesColor;
  PenStyle HorLinesLineStyle;
  PenStyle VerLinesLineStyle;
  vector   <Graph *>    ThePlots;
  vector   <PlotsCoord> ThePlotsCoordinates;
  
  virtual void ComputePositionsComplement();
  virtual void DrawPixmapComplement      ();
  virtual void showEvent                 (QShowEvent  *);
  virtual void closeEvent                (QCloseEvent *);
  
 public:
                  Frame               (QWidget * Parent = 0, const char * Name = 0);
  virtual        ~Frame               ();
          void    SetR2L              (bool);
	  void    SetHorLinesWidth    (int);
	  void    SetVerLinesWidth    (int);
	  void    SetHorLinesColor    (QColor);
	  void    SetVerLinesColor    (QColor);
	  void    SetHorLinesLineStyle(PenStyle);
	  void    SetVerLinesLineStyle(PenStyle);
	  QString GetSaveFileName     ();
	  void    SetSaveFileName     (QString);
	  void    Zone                (int, int);
	  void    SubZone             (int, int, int);
	  int     PlotsNumber         (); 
	  int     ExistingPlotsNumber ();
	  bool    IsEmpty             ();
	  void    AddPlot             (Graph *);
	  Graph * GetActivePlot       ();
	  void    SetActivePlot       (int);
	  void    DeleteActivePlot    ();
	  int     activePlotId        ();
	  
 signals:
  void Shown        (bool   );
  void Closed       (bool   );
  void CanceledClose(QString);
};

#endif
