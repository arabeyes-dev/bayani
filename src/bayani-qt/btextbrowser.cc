/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the BTextBrowser Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <btextbrowser.h>
#include <gui.h>

BTextBrowser::BTextBrowser(QWidget * Parent, const char * Name)
  : QTextBrowser(Parent, Name)
{
}

BTextBrowser::~BTextBrowser()
{
}

void BTextBrowser::setSource(const QString & Source)
{
  if(Source.left(7) == "http://" || Source.left(4) == "www.")
    {
      system(QString("mozilla %1 &") .arg(Source));
      
      return;
    }
  
  if(Source.left(7) == "mailto:")
    {
      system(QString("mozilla %1 &") .arg(Source));
      
      return;
    }
  
  QString TestString(Source);
  
  int Index = TestString.find("#");
  
  if(Index != -1)
    TestString = TestString.remove(Index, TestString.length()-Index);
  
  QFile TestFile(mimeSourceFactory()->filePath().join("") + TestString);/* We assume we have one file path */
  
  if(!TestFile.exists())
    {
      GUI::PrintMessage(this, MESSAGE_CRITICAL, trUtf8("Document %1 not found in directory %2.") .arg(Source) .arg(mimeSourceFactory()->filePath().join("")));
      
      return;
    }
  
  QTextBrowser::setSource(Source);
}
