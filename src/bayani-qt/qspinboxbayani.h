/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QSpinBoxBayani Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QSPINBOXBAYANI_H
#define BAYANI_QSPINBOXBAYANI_H

/* QT headers */
#include <qwidget.h>
#include <qlineedit.h>
#include <qspinbox.h>

/*!
  \file qspinboxbayani.h
  \brief Header file for the QSpinBoxBayani class.
  
  The QSpinBoxBayani class is defined in this file.
*/

//! An advanced QSpinBox.
/*!
  This class inherits QSpinBox. It gives access to the internal editor and thus permits to set its alignment.
*/
class QSpinBoxBayani: public QSpinBox
{
 public:
                      QSpinBoxBayani(int, int, int Step = 1, QWidget * Parent = 0, const char * Name = 0);
  virtual            ~QSpinBoxBayani();
          QLineEdit * GetEditor     ();
};

#endif
