/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation the of BBGO Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <bbgo.h>

BBGO::BBGO(QWidget * Parent, const char * Name)
  : QWidget(Parent, Name)
{
  setWFlags(Qt::WDestructiveClose);
  
  SetTitleUp       (false);
  SetBoxLineWidth  (1);
  SetXRatio        (0.1);
  SetYRatio        (0.05);
  SetWidthRatio    (0.85);
  SetHeightRatio   (0.8);
  SetBox2TitleRatio(0.075);
  SetBGColor       (QColor("#FFFFFF"));
  SetBoxColor      (QColor("#000000"));
  SetTitleColor    (QColor("#000000"));
  SetTitleFont     (font());
  SetTitle         ("");
  SetBoxLineStyle  (Qt::SolidLine);
  
  ForceRepaintFlag = false;
  ModifiedFlag     = false;
  
  ComputePositions();
}

BBGO::~BBGO()
{
}

void BBGO::SetTitleUp(bool NewTitleUpFlag)
{
  TitleUpFlag = NewTitleUpFlag;
}

void BBGO::SetBoxLineWidth(int NewBoxLineWidth)
{
  BoxLineWidth = NewBoxLineWidth;
}

void BBGO::SetXRatio(double NewXRatio)
{
  XRatio = NewXRatio;
  
  if(!ComputePositionsFlag)
    ComputePositionsFlag = true;
}

void BBGO::SetYRatio(double NewYRatio)
{
  YRatio = NewYRatio;
  
  if(!ComputePositionsFlag)
    ComputePositionsFlag = true;
}

void BBGO::SetWidthRatio(double NewWidthRatio)
{
  if(NewWidthRatio <= 0.)
    return;
  
  WidthRatio = NewWidthRatio;
  
  if(!ComputePositionsFlag)
    ComputePositionsFlag = true;
}

void BBGO::SetHeightRatio(double NewHeightRatio)
{
  if(NewHeightRatio <= 0.)
    return;
  
  HeightRatio = NewHeightRatio;
  
  if(!ComputePositionsFlag)
    ComputePositionsFlag = true;
}

void BBGO::SetBox2TitleRatio(double NewBox2TitleRatio)
{
  Box2TitleRatio = NewBox2TitleRatio;
}

void BBGO::SetAxis2TitleRatio(double NewAxis2TitleRatio)
{
  SetBox2TitleRatio(NewAxis2TitleRatio);
}

void BBGO::SetBGColor(QColor NewBGColor)
{
  BGColor = NewBGColor;
}

void BBGO::SetBoxColor(QColor NewBoxColor)
{
  BoxColor = NewBoxColor;
}

void BBGO::SetTitleColor(QColor NewTitleColor)
{
  TitleColor = NewTitleColor;
}

void BBGO::SetTitleFont(QFont NewTitleFont)
{
  TitleFont = NewTitleFont;
}

void BBGO::SetTitle(QString NewTitle)
{
  Title = NewTitle;
}

void BBGO::SetBoxLineStyle(PenStyle NewBoxLineStyle)
{
  BoxLineStyle = NewBoxLineStyle;
}

void BBGO::WritePostscript(QFile PSFile)
{
  /*
    When the user wants to save in a postscript format the GUI class will open a QFile and call this method.
    This method will draw the Frame object in PSFile (Background, Box, Separators, Title) with the appropriate colors, fonts, and sizes.
    -------- Refer to Frame::DrawPixmap() --------
    Then it will call for all its children (Graph2D objects) Graph2D:::WritePostscript(PSFile)
    It's the GUI class that closes the file after that.
  */
  
  /*
    Example on how to draw the box for example (quote from the old code).
    
    PostScriptBuffer += "\n%%Debut du trace de la boite\n";
    PostScriptBuffer += "RecX1 RecY1 moveto\n";
    PostScriptBuffer += "BoxWidth 0 rlineto\n";
    PostScriptBuffer += "0 BoxHeight rlineto\n";
    PostScriptBuffer += "BoxWidth neg 0 rlineto\n";
    PostScriptBuffer += "0 BoxHeight neg rlineto\n";
    PostScriptBuffer += "closepath\n";
    PostScriptBuffer += "stroke\n";
    PostScriptBuffer += "%%Fin du trace de la boite\n\n";
  */
  
  /*
    This method will be called by Frame::WritePostscript()
  */
}

void BBGO::WriteEPostscript(QFile EPSFile)
{
  //Preferably we'll start by implementing this function first rather than the above one.
  
  //the object should not care about opening an dclosing the file, which should be done by gui.cc (writing of the header, making new pages, closing the file)
  //the current object should simply dump the actual drawings in the file.
  
  /*
    Same as Frame::WritePostscript() but writes an Encapsulated Postscript file (.eps)
    (definition of the Bounding Box).
    Not necessary to write twice the function, but there are small things to care about.
  */
  
  /*
    This method will be called by Frame::WriteEPostscript()
    Maybe there's no need to have 2 functions in Graph class ??? Perhaps they will be exactly the same.
    All the contents of the Graph must be drawn :lines, texts, datas and in the appropriate colors and fonts and sizes etc. and in the same
    corresponding locations. ie the EPS files should look exactly the same as what is fdrwan on the screen :)
    -------- Refer to Graph::DrawPixmap() --------
  */
  
  //old Xlib code
  //In the new implementation we'll rather use the QString API or the QFile(QTextStream) one in order to write in the QFile
  //This is far better than the sprintf() function (not Unicode friendly) since we need to write our Arabic texts correctly.
  //There were 2 functions. The second function will now be implemented in GUI class.
  //The code is a little dirty... cut and paste.

  /*    
  void Graph::InitializePostScript()
  {
  char Useful[1024] = "";
  string Usefulstr = "";
  PostScriptBuffer = "";
  PostScriptBuffer += "%%!PS-Adobe-2.0 EPSF-2.0\n";
  sprintf(Useful, "BoundingBox: 0 0 %g %g\n", WindowWidth, WindowHeight);
  Usefulstr = Useful;
  PostScriptBuffer += "%%%%" + Usefulstr;
  PostScriptBuffer += "%%%%Creator: ProgReS PostSript Writer (Youcef Rahal 10-06-2002)\n";		   
  PostScriptBuffer += "\n%%Debut de la definition des procedures utiles\n";
  PostScriptBuffer += "/plotpointdict 8 dict def\n";
  PostScriptBuffer += "plotpointdict /mtrx matrix put\n";
  PostScriptBuffer += "/plotpoint\n";
  PostScriptBuffer += "{\n";
  PostScriptBuffer += "plotpointdict begin\n";
  PostScriptBuffer += "/size exch def\n";
  PostScriptBuffer += "/savematrix mtrx currentmatrix def\n";
  PostScriptBuffer += "currentpoint translate\n";
  PostScriptBuffer += "0 0 size 2 div 0 360 arc\n";
  PostScriptBuffer += "savematrix setmatrix\n";
  PostScriptBuffer += "end\n";
  PostScriptBuffer += "fill\n";
  PostScriptBuffer += "}def\n";
  PostScriptBuffer += "%%Fin de la definition des procedures utiles\n\n";
  PostScriptBuffer += "\n%%Debut de la definition des variables\n";
  PostScriptBuffer += "/Times-Bold findfont 12. scalefont setfont\n";//Be careful here
  sprintf(Useful, "/WindowWidth %g def\n/WindowHeight %g def\n/RecX1 %g def\n/RecY1 %g def\n/BoxWidth %g def\n/BoxHeight %g def\n/XTickLength %g def\n/YTickLength %g def\n", WindowWidth, WindowHeight, RecX1, RecY1, BoxWidth, BoxHeight, XTickLength, YTickLength);
  Usefulstr = Useful;
  PostScriptBuffer += Usefulstr;
  PostScriptBuffer += "%%Fin de la definition des variables\n\n";

  //sprintf(Useful, "%g %g %g setrgbcolor newpath 0 0 moveto WindowWidth 0 rlineto 0 WindowHeight rlineto WindowWidth neg 0 rlineto 0 neg WindowHeight rlineto closepath fill 0. 0. 0. setrgbcolor\n", TheBackgroundColor.red/65535.,TheBackgroundColor.green/65535.,TheBackgroundColor.blue/65535.);
  Usefulstr = Useful;
  PostScriptBuffer += Usefulstr;
  
  PostScriptBuffer += "\n%%Debut du trace de la boite\n";
  PostScriptBuffer += "RecX1 RecY1 moveto\n";
  PostScriptBuffer += "BoxWidth 0 rlineto\n";
  PostScriptBuffer += "0 BoxHeight rlineto\n";
  PostScriptBuffer += "BoxWidth neg 0 rlineto\n";
  PostScriptBuffer += "0 BoxHeight neg rlineto\n";
  PostScriptBuffer += "closepath\n";
  PostScriptBuffer += "stroke\n";
  PostScriptBuffer += "%%Fin du trace de la boite\n\n";
  PostScriptBuffer += "\n%%Debut du trace des ticks et de la grille\n";
  for(int i = 0; i < XTICKS.size(); i++)
    {
      if(XTICKS[i].X == RecX1 || i == XTICKS[i].X == RecX2)
	sprintf(Useful, "%g %g moveto (%s) show\n", XTICKS[i].XLabel, WindowHeight-XTICKS[i].YLabel, XTICKS[i].Label.c_str());
      else
	{
	  if(XTICKS[i].Length == XSmallTickLength)//No Grid for the Small Ticks
	    sprintf(Useful, "%g %g moveto 0 %g rlineto stroke [1 1] 0 setdash %g %g moveto 0 %g rlineto stroke [] 0 setdash %g %g moveto (%s) show\n", XTICKS[i].X, WindowHeight-XTICKS[i].Y, XTICKS[i].Length, XTICKS[i].X, WindowHeight-(XTICKS[i].Y - XTICKS[i].Length), (BoxHeight-XTICKS[i].Length)*GridFlag*0, XTICKS[i].XLabel, WindowHeight-XTICKS[i].YLabel, XTICKS[i].Label.c_str());
	  else
	    sprintf(Useful, "%g %g moveto 0 %g rlineto stroke [1 1] 0 setdash %g %g moveto 0 %g rlineto stroke [] 0 setdash %g %g moveto (%s) show\n", XTICKS[i].X, WindowHeight-XTICKS[i].Y, XTICKS[i].Length, XTICKS[i].X, WindowHeight-(XTICKS[i].Y - XTICKS[i].Length), (BoxHeight-XTICKS[i].Length)*GridFlag, XTICKS[i].XLabel, WindowHeight-XTICKS[i].YLabel, XTICKS[i].Label.c_str());
	}
      //Verify what's the black-white alternance in xlib to have the same one, and also joinmiter et nocaplast
      Usefulstr = Useful;
      PostScriptBuffer += Usefulstr;
    }
  for(int i = 0; i < YTICKS.size(); i++)
    {
      if(YTICKS[i].Y == RecY1 || i == XTICKS[i].Y == RecY2)
	sprintf(Useful, "%g %g moveto (%s) show\n", YTICKS[i].XLabel, WindowHeight-YTICKS[i].YLabel, YTICKS[i].Label.c_str());
      else
	{
	  if(YTICKS[i].Length == YSmallTickLength)//No Grid for the Small Ticks
	    sprintf(Useful, "%g %g moveto %g 0 rlineto stroke [1 1] 0 setdash %g %g moveto %g 0 rlineto stroke [] 0 setdash %g %g moveto (%s) show\n", YTICKS[i].X, WindowHeight-YTICKS[i].Y, YTICKS[i].Length, YTICKS[i].X+YTICKS[i].Length, WindowHeight-YTICKS[i].Y, (BoxWidth-YTICKS[i].Length)*GridFlag*0, YTICKS[i].XLabel, WindowHeight-YTICKS[i].YLabel, YTICKS[i].Label.c_str());
	  else
	    sprintf(Useful, "%g %g moveto %g 0 rlineto stroke [1 1] 0 setdash %g %g moveto %g 0 rlineto stroke [] 0 setdash %g %g moveto (%s) show\n", YTICKS[i].X, WindowHeight-YTICKS[i].Y, YTICKS[i].Length, YTICKS[i].X+YTICKS[i].Length, WindowHeight-YTICKS[i].Y, (BoxWidth-YTICKS[i].Length)*GridFlag, YTICKS[i].XLabel, WindowHeight-YTICKS[i].YLabel, YTICKS[i].Label.c_str());
	}
      Usefulstr = Useful;
      PostScriptBuffer += Usefulstr;
    }
  PostScriptBuffer += "%%Fin du trace des ticks et de la grille\n\n";//Watch out the FontSize it doesn't seem to match !!!
  PostScriptBuffer += "\n%%Debut du trace des labels\n";
  for(int i = 0; i < ALLSTRINGS.size(); i++)
    {
      if(ALLSTRINGS[i].Angle != 0.)
	sprintf(Useful, "%g %g %g setrgbcolor %g %g moveto %g rotate (%s) show %g neg rotate\n", ALLSTRINGS[i].Color.red/65535., ALLSTRINGS[i].Color.green/65535., ALLSTRINGS[i].Color.blue/65535., ALLSTRINGS[i].X, WindowHeight-ALLSTRINGS[i].Y, ALLSTRINGS[i].Angle, ALLSTRINGS[i].Label.c_str(), ALLSTRINGS[i].Angle);
      else
	sprintf(Useful, "%g %g %g setrgbcolor %g %g moveto (%s) show\n", ALLSTRINGS[i].Color.red/65535., ALLSTRINGS[i].Color.green/65535., ALLSTRINGS[i].Color.blue/65535., ALLSTRINGS[i].X, WindowHeight-ALLSTRINGS[i].Y, ALLSTRINGS[i].Label.c_str());
      Usefulstr = Useful;
      PostScriptBuffer += Usefulstr;
    } 
  PostScriptBuffer += "0 0 0 setrgbcolor\n";
  PostScriptBuffer += "%%Fin du trace des labels\n\n";
  PostScriptBuffer += "\n%%Debut du trace des datas\n";
  for(int i = 0; i < ALLPOINTS.size(); i++)
    {
      sprintf(Useful, "%g %g %g setrgbcolor %g %g moveto %g plotpoint\n", ALLPOINTS[i].Color.red/65535., ALLPOINTS[i].Color.green/65535., ALLPOINTS[i].Color.blue/65535., ALLPOINTS[i].X, WindowHeight-ALLPOINTS[i].Y, ALLPOINTS[i].Size);//Weird correspondance between the pointsize on the window and in the postscript... Verify !
      Usefulstr = Useful;
      PostScriptBuffer += Usefulstr;
    }
    PostScriptBuffer += "0 0 0 setrgbcolor\n";
  PostScriptBuffer += "%%Fin du trace des datas\n\n";
  PostScriptBuffer += "showpage\n";
  }

  void Graph::WritePostScript(string PostScriptFileName)
  {
  InitializePostScript();
  
  FILE * PostScriptFile = fopen(PostScriptFileName.c_str(), "w");
  fprintf(PostScriptFile, PostScriptBuffer.c_str());
  fclose(PostScriptFile);
  }
  */
}

void BBGO::ForceRepaint()
{
  if(!ForceRepaintFlag)
    ForceRepaintFlag = true;
}

bool BBGO::IsModified()
{
  return ModifiedFlag;
}

QSizePolicy BBGO::sizePolicy() const
{
  return QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

void BBGO::SetModified(bool NewModifiedFlag)
{
  ModifiedFlag = NewModifiedFlag;
  
  emit Modified(ModifiedFlag);
}

void BBGO::ComputePositions()
{
  X      = int(XRatio * width());
  Y      = int(YRatio * height());
  Width  = int(WidthRatio * width());
  Height = int(HeightRatio * height());
  
  ComputePositionsFlag = false;

  ComputePositionsComplement();
}

void BBGO::ComputePositionsComplement()
{
}

void BBGO::resizeEvent(QResizeEvent *)
{
  if(!ComputePositionsFlag)
    ComputePositionsFlag = true;
}

void BBGO::paintEvent(QPaintEvent *)
{
  bool LocalFlag = false;
  
  if(ForceRepaintFlag)
    {
      ForceRepaintFlag = false;
      LocalFlag        = true;
    }
  
  if(ComputePositionsFlag)
    {
      ComputePositions();
      LocalFlag = true;
    }
  
  if(paintEventComplement())
    LocalFlag = true;
  
  if(LocalFlag)
    {
      DrawPixmap();
      
      SetModified(true);
    }
  
  QPainter Painter(this);
  Painter.drawPixmap(0, 0, InternalPixmap);
}

bool BBGO::paintEventComplement()
{
  return false;
}

void BBGO::DrawText(QString Text, int X, int Y, double Angle, int HorJust, int VerJust, QColor Color, QFont Font)
{
  QPainter Painter(&InternalPixmap);
  QPen Pen;
  
  Painter.setFont(Font);
  Pen.setColor(Color);
  Painter.setPen(Pen);
  
  int TextWidth =  Painter.fontMetrics().width(Text);
  int TextHeight = int(0.5*Painter.fontMetrics().height());
  
  int RealX = 0;
  int RealY = 0;
  
  if(HorJust == Qt::AlignRight)
    RealX -= TextWidth;
  if(HorJust == Qt::AlignHCenter)
    RealX -= int(0.5*TextWidth);
  if(VerJust == Qt::AlignTop)
    RealY += TextHeight;
  if(VerJust == Qt::AlignVCenter)
    RealY += int(0.5*TextHeight);
  
  Painter.translate(X, Y);
  Painter.rotate(-Angle);
      
  Painter.drawText(RealX, RealY, Text);
  
  Painter.rotate(Angle);
  Painter.translate(-X, -Y);
}

void BBGO::AddLine(int X1, int Y1, int X2, int Y2, int LineWidth, QColor Color, PenStyle LineStyle, bool WrapFlag)
{
  Line NewLine;
  
  NewLine.X1        = X1;
  NewLine.Y1        = Y1;
  NewLine.X2        = X2;
  NewLine.Y2        = Y2;
  NewLine.LineWidth = LineWidth;
  NewLine.Color     = Color;
  NewLine.LineStyle = LineStyle;
  NewLine.WrapFlag  = WrapFlag;
  
  AllLines.push_back(NewLine);
}

void BBGO::AddText(QString Label, int X, int Y, double Angle, QColor Color, QFont Font, int HorJust, int VerJust)
{
  Text NewText;
  
  NewText.Text    = Label;
  NewText.X       = X;
  NewText.Y       = Y;
  NewText.Angle   = Angle;
  NewText.Color   = Color;
  NewText.Font    = Font;
  NewText.HorJust = HorJust;
  NewText.VerJust = VerJust;
  
  AllTexts.push_back(NewText);
}

void BBGO::AddShape(int X, int Y, int Size, QColor Color, int Type)
{
  Shape NewShape;
  
  NewShape.X     = X;
  NewShape.Y     = Y;
  NewShape.Size  = Size;
  NewShape.Type  = Type;
  NewShape.Color = Color;
  
  AllShapes.push_back(NewShape);
}

void BBGO::AddImage(QImage BQImage, int X, int Y)
{
  Image NewImage;
  
  NewImage.BQImage = BQImage;
  NewImage.X       = X;
  NewImage.Y       = Y;
  
  AllImages.push_back(NewImage);
}

void BBGO::AddPolyline(QPointArray PointArray, int LineWidth, QColor Color, PenStyle LineStyle)
{
  Polyline NewPolyline;
  
  NewPolyline.PointArray = PointArray;
  NewPolyline.LineWidth  = LineWidth;
  NewPolyline.Color      = Color;
  NewPolyline.LineStyle  = LineStyle;
  
  AllPolylines.push_back(NewPolyline);
}

void BBGO::AddPolygon(QPointArray PointArray, QColor Color, BrushStyle FillStyle)
{
  Polygon NewPolygon;
  
  NewPolygon.PointArray = PointArray;
  NewPolygon.Color      = Color;
  NewPolygon.FillStyle  = FillStyle;
  
  AllPolygons.push_back(NewPolygon);
}

/*
  Draws the object's contents.
  This functions draws all the lines, texts and shapes that the object contains on the internal pixmap.
  
  \todo The computation of positions should be done when the resize is done, not at every drawing. If there is no resize, then there's no need to re-compute positions (we should store ratios in the datas and save XRatioOld and YRatioOld etc like in Frame ?).
  \todo No need to clear the AllLines, AllDatas etc vectors at every drawing (read above).
  \todo The XTicks seem to be translated to the right (seen when setting the X bounds from 0. to 1.). This makes the last tick to be drawn on the box for example. Seen also for the Frame separators.
  \todo There should be an option to allow the user to draw the data even if they overlap on the box. Otherwise, we should give the user the number of overflows (by a print, in the statistics box which is not implemented yet...).
  \todo There should be an option to allow the user to compute the bounds taking into account all the data (not only the main ones).
  \todo The number of ticks should really be fixed by hand (no more than 10 preferably). There should also be a test on the overlap of the digits on the axis. In that case, the ticks number should be lowered, unless the user wants it (by an option: AutoTicks or FixedTicksNbr).
  \bug fix the lines that dont work (rotation pb).
  \todo lines (1) and (2) achtung ! next thiong so be plotted (clipping, which should be used now)
  \todo Line (3) is useless  with clipping !
*/
void BBGO::DrawPixmap()
{
  /* Funny things may happen if the picture is 1 pixel wide or high */
  if(width() <= 1 || height() <= 1)
    return;
  
  AllLines.clear();
  AllTexts.clear();
  AllShapes.clear();
  AllImages.clear();
  AllPolylines.clear();
  AllPolygons.clear();
  
  /*
    These two commented lines don't work:
    
         1/ They are supposed to put te origin at left-bottom and the YAxis increasing up
         2/ They both do well the job, but only the drawing is well-done. The text is mirrored !
     
    We'll use X, height()-Y to draw until this problem is fixed
    
    QWMatrix m;
    m.setMatrix(1., 0., 0., -1., 0., height());
    
    Painter.setWorldMatrix(m, TRUE);
    
    Painter.setWindow(0, height(), width(), -height());
  */
  
  DrawPixmapComplement();
  
  AddLine(X      , Y       , X+Width, Y       , BoxLineWidth, BoxColor, BoxLineStyle);
  AddLine(X+Width, Y       , X+Width, Y+Height, BoxLineWidth, BoxColor, BoxLineStyle);
  AddLine(X+Width, Y+Height, X      , Y+Height, BoxLineWidth, BoxColor, BoxLineStyle);
  AddLine(X      , Y+Height, X      , Y       , BoxLineWidth, BoxColor, BoxLineStyle);
  
  if(TitleUpFlag)
    AddText(Title, int(X+Width/2.), int(Y-height()*Box2TitleRatio), 0., TitleColor, TitleFont, Qt::AlignHCenter, Qt::AlignBottom);
  else
    AddText(Title, int(X+Width/2.), int(Y+Height+Box2TitleRatio*height()), 0., TitleColor, TitleFont, Qt::AlignHCenter, Qt::AlignTop);
  
  /* Actual drawing starts here */
  
  InternalPixmap.resize(width(), height());
  InternalPixmap.fill(BGColor);
  
  QPainter Painter(&InternalPixmap);
  QPen     Pen;
  QBrush   Brush;
  
  for(int i = 0; i < AllShapes.size(); i++)
    {
      /* In order to avoid that a drawn data point overlaps on the box */
      if(
	 AllShapes[i].X-X < AllShapes[i].Size*0.5 ||
	 X+Width-AllShapes[i].X < AllShapes[i].Size*0.5 ||
	 AllShapes[i].Y-Y < AllShapes[i].Size*0.5 ||
	 Y+Height-AllShapes[i].Y < AllShapes[i].Size*0.5
	 )
	continue;
      
      Pen.setColor(AllShapes[i].Color);
      Painter.setPen(Pen);
      if(AllShapes[i].Type == HOLCIRCLE)
	Painter.drawEllipse(AllShapes[i].X-int(AllShapes[i].Size*0.5), AllShapes[i].Y-int(AllShapes[i].Size*0.5), AllShapes[i].Size, AllShapes[i].Size);
      else if(AllShapes[i].Type == FILCIRCLE)
	{
	  QBrush Brush;
	  Brush.setStyle(SolidPattern);
	  Brush.setColor(AllShapes[i].Color);
	  Painter.setBrush(Brush);
	  Painter.drawEllipse(AllShapes[i].X-int(AllShapes[i].Size*0.5), AllShapes[i].Y-int(AllShapes[i].Size*0.5), AllShapes[i].Size, AllShapes[i].Size);
	  Brush.setStyle(NoBrush);
	  Painter.setBrush(Brush);
	}
      else if(AllShapes[i].Type == HOLSQUARE)
	Painter.drawRect(AllShapes[i].X-int(AllShapes[i].Size*0.5), AllShapes[i].Y-int(AllShapes[i].Size*0.5), AllShapes[i].Size, AllShapes[i].Size);
      else if(AllShapes[i].Type == FILSQUARE)
	{
	  QBrush Brush;
	  Brush.setStyle(SolidPattern);
	  Brush.setColor(AllShapes[i].Color);
	  Painter.setBrush(Brush);
	  Painter.drawRect(AllShapes[i].X-int(AllShapes[i].Size*0.5), AllShapes[i].Y-int(AllShapes[i].Size*0.5), AllShapes[i].Size, AllShapes[i].Size);
	  Brush.setStyle(NoBrush);
	  Painter.setBrush(Brush);
	}
      else
	Painter.drawPoint(AllShapes[i].X, AllShapes[i].Y);
    }
  
  Painter.setClipRect(X, Y, Width, Height);
  
  for(int i = 0; i < AllImages.size(); i++)
    Painter.drawImage(AllImages[i].X, AllImages[i].Y,  AllImages[i].BQImage);
  
  Painter.setClipping(false);/* needs to be so otherwise the bottom axis wont be displayed -> bug */
  
  for(int i = 0; i < AllLines.size(); i++)
    {
      int XLeft  = AllLines[i].X1;
      int XRight = AllLines[i].X2;
      int YLeft  = AllLines[i].Y1;
      int YRight = AllLines[i].Y2;
      
      if(AllLines[i].WrapFlag)//(3)
	{
	  if(XLeft < X)
	    XLeft = X;
	  if(XLeft > X+Width)
	    XLeft = X+Width;
	  if(XRight < X)
	    XRight = X;
	  if(XRight > X+Width)
	    XRight = X+Width;
	  
	  if(YLeft < Y)
	    YLeft = Y;
	  if(YLeft > Y+Height)
	    YLeft = Y+Height;
	  if(YRight < Y)
	    YRight = Y;
	  if(YRight > Y+Height)
	    YRight = Y+Height;
	}
      
      Pen.setColor(AllLines[i].Color);
      Pen.setWidth(AllLines[i].LineWidth);
      Pen.setStyle(AllLines[i].LineStyle);
      Painter.setPen(Pen);
      
      Painter.drawLine(XLeft, YLeft, XRight, YRight);
    }
  
  /* Polygons need to be plotted before polylines */
  
  for(int i = 0; i < AllPolygons.size(); i++)
    {
      Brush.setColor(AllPolygons[i].Color);
      Brush.setStyle(AllPolygons[i].FillStyle);
      Painter.setBrush(Brush);
      
      Pen.setStyle(Qt::NoPen);/* If we want to draw a line rounding the polygon -> use a polyline */
      Painter.setPen(Pen);
      
      Painter.setClipRect(X, Y, Width, Height);//(1)
      
      Painter.drawPolygon(AllPolygons[i].PointArray);
    }
  
  for(int i = 0; i < AllPolylines.size(); i++)
    {
      Pen.setColor(AllPolylines[i].Color);
      Pen.setWidth(AllPolylines[i].LineWidth);
      Pen.setStyle(AllPolylines[i].LineStyle);
      Painter.setPen(Pen);
      
      Painter.setClipRect(X, Y, Width, Height);//(2)
      
      Painter.drawPolyline(AllPolylines[i].PointArray);
    }
  
  Painter.end();/* we need to close the painter, otherwise the the first call to DrawText (first X digit for ex, new painter) won't be considered - a solution would be to integrate DrawText in this method and not make it separately */
  
  for(int i = 0; i < AllTexts.size(); i++)
    DrawText(AllTexts[i].Text, AllTexts[i].X, AllTexts[i].Y, AllTexts[i].Angle, AllTexts[i].HorJust, AllTexts[i].VerJust, AllTexts[i].Color, AllTexts[i].Font);
}

void BBGO::DrawPixmapComplement()
{
}
