/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the BXMLToQLVHandler Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_BXMLTOQLVHANDLER_H
#define BAYANI_BXMLTOQLVHANDLER_H

/* QT headers */
#include <qobject.h>
#include <qstring.h>
#include <qptrstack.h>
#include <qlistview.h>
#include <qxml.h>
/* Bayani headers */
#include <blistviewitem.h>

/*!
  \file bxmltoqlvhandler.h
  \brief Header file for the BXMLToQLVHandler class.
  
  The BXMLToQLVHandler class is defined in this file.
*/

//! An advanced QXmlDefaultHandler.
/*!
  This class inherits QXmlDefaultHandler. This handler parses an XML file and fills a QListView with the list of contents (as a tree).
*/
class BXMLToQLVHandler: public QXmlDefaultHandler
{
 protected:
  bool            SectionStartedFlag;
  bool            TitleStartedFlag;
  QString         SectionID;
  QString         SectionAnchor;
  QString         MainSectionAnchor;
  QString         SectionTitle;
  QPtrStack       <BListViewItem> BLVIStack;
  QListView     * ListViewQLV;
  QListViewItem * NoContentQLVI;
  
 public:
               BXMLToQLVHandler();
  virtual     ~BXMLToQLVHandler();
  virtual bool startDocument   ();
  virtual bool startElement    (const QString &, const QString &, const QString &, const QXmlAttributes &);
  virtual bool endElement      (const QString &, const QString &, const QString &);
  virtual bool characters      (const QString &);
          void SetListView     (QListView *);
};

#endif
