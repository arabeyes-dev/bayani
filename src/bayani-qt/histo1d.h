/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the Histo1D Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_HISTO1D_H
#define BAYANI_HISTO1D_H

/* QT headers */
#include <qnamespace.h>
#include <qcolor.h>
#include <qpointarray.h>
/* Bayani headers */
#include <histogram.h>
#include <graph.h>
#include <bbdo.h>

/*!
  \file histo1d.h
  \brief Header file for the Histo1D class.
  
  The Histo1D class is defined in this file.
*/

//! Class to display histograms.
/*!
  This class inherits BBDO. It is specially designed to display one dimensional histograms.
  \sa BBDO, Func1D, Graph2D and FITSContainer.
*/
class Histo1D: public BBDO
{
 protected:
  int            LineWidth;
  QColor         LineColor;
  Qt::PenStyle   LineStyle;
  QColor         FillColor;
  Qt::BrushStyle FillStyle;
  Histogram      InternalHistogram;
  
  virtual void ComputeMinMax();
  virtual bool DrawSelf     (Graph *);
  
 public:
                    Histo1D     ();
  virtual          ~Histo1D     ();
          void      SetLineWidth(int);
	  void      SetLineColor(QColor);
	  void      SetLineStyle(Qt::PenStyle);
	  void      SetFillColor(QColor);
	  void      SetFillStyle(Qt::BrushStyle);
	  void      SetData     (Histogram);
  virtual int       GetEntries  ();
  virtual double    GetMean     ();
  virtual double    GetSigma    ();
	  Histogram GetData     ();
};

#endif
