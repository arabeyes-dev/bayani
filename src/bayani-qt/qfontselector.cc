/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QFontSelector Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qfontselector.h>

QFontSelector::QFontSelector(QWidget * Parent, const char * Name)
  : QHBox(Parent, Name)
{
  Font = new QFont;
  
  Label      = new QLabel(this);
  PushButton = new QPushButton(trUtf8("Change..."), this);
   
  ChangeFont();
  
  Label->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  Label->setFrameStyle(QFrame::Panel | QFrame::Sunken);
  
  Label->setFixedSize(Label->sizeHint());
  PushButton->setFixedSize(PushButton->sizeHint());
  
  setSpacing(5);
  setFixedSize(sizeHint());
  
  setFocusProxy(PushButton);
  
  connect(PushButton, SIGNAL(clicked()), this, SLOT(OpenDialog()));
}

QFontSelector::~QFontSelector()
{
}

void QFontSelector::SetFont(QFont NewFont)
{
  *Font = NewFont;
  
  ChangeFont();
}

QFont QFontSelector::GetFont()
{
  return *Font;
}

void QFontSelector::ChangeFont()
{
  Label->setFont(*Font);
  
  Label->setText(QString("%1 %2") .arg(Font->family()) .arg(Font->pointSize()));
}

void QFontSelector::OpenDialog()
{
  bool TestFlag = false;
  
  QFont TestFont = QFontDialog::getFont(&TestFlag, *Font, this);
  
  if(TestFlag)
    {
      *Font = TestFont;
      
      ChangeFont();
    }
}
