/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QPenCombo Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qpencombo.h>

Qt::PenStyle PenStyles[PEN_STYLES_NUMBER] = {Qt::NoPen, Qt::SolidLine, Qt::DashLine, Qt::DotLine, Qt::DashDotLine, Qt::DashDotDotLine};

QPenCombo::QPenCombo(QWidget * Parent, const char * Name)
  : QComboBox(Parent, Name)
{
  setEditable(false);
  
  for(int i = 0; i < PEN_STYLES_NUMBER; i++)
    {
      QPixmap Pixmap(50, 16);
      Pixmap.fill(QColor(255, 255, 255));
      
      QPainter Painter(&Pixmap);
      QPen     Pen;
      
      Painter.drawRect(0, 0, Pixmap.width(), Pixmap.height());
      
      Pen.setWidth(2);
      Pen.setStyle(PenStyles[i]);
      
      Painter.setPen(Pen);
      Painter.drawLine(0, int(Pixmap.height()/2.), Pixmap.width(), int(Pixmap.height()/2.));
      
      insertItem(Pixmap);
    }
  
  setCurrentItem(1);
  
  setFixedSize(sizeHint());
}

QPenCombo::~QPenCombo()
{
}

void QPenCombo::SetCurrentStyle(Qt::PenStyle NewStyle)
{
  for(int i = 0; i < PEN_STYLES_NUMBER; i++)
    if(PenStyles[i] == NewStyle)
      {
	if(i != currentItem())
	  setCurrentItem(i);
	
	return;
      }
}

Qt::PenStyle QPenCombo::GetCurrentStyle()
{
  return PenStyles[currentItem()];
}
