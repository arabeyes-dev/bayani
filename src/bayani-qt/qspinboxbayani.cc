/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QSpinBoxBayani Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qspinboxbayani.h>

QSpinBoxBayani::QSpinBoxBayani(int MinValue, int MaxValue, int Step, QWidget * Parent, const char * Name)
  : QSpinBox(MinValue, MaxValue, Step, Parent, Name)
{
}

QSpinBoxBayani::~QSpinBoxBayani()
{
}

QLineEdit * QSpinBoxBayani::GetEditor()
{
  return this->editor();
}
