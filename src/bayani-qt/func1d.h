/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the Func1D Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_FUNC1D_H
#define BAYANI_FUNC1D_H

/* "Usual" headers */
#include <math.h>
/* QT headers */
#include <qnamespace.h>
#include <qobject.h>
#include <qcolor.h>
#include <qpointarray.h>
/* Bayani headers */
#include <bexpression.h>
#include <graph.h>
#include <bbdo.h>

/*!
  \file func1d.h
  \brief Header file for the Func1D class.
  
  The Func1D class is defined in this file.
*/

//! Class to display functions.
/*!
  This class inherits BBDO. It is specially designed to display functions of a single variable.
  \sa BBDO, Histo1D, Graph2D and FITSContainer.
*/
class Func1D: public BBDO
{
 protected:
  int            LineWidth;
  QColor         LineColor;
  Qt::PenStyle   LineStyle;
  BExpression  * InternalExpression;
  
  virtual void ComputeMinMax();
  virtual bool DrawSelf     (Graph *);
  
 public:
               Func1D      ();
  virtual     ~Func1D      ();
          void SetLineWidth(int);
	  void SetLineColor(QColor);
	  void SetLineStyle(Qt::PenStyle);
	  void SetData     (BExpression *);
	  void SetXMinXMax (double, double);
};

#endif
