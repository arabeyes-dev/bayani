/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QLineCombo Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QLINECOMBO_H
#define BAYANI_QLINECOMBO_H

/* QT headers */
#include <qwidget.h>
#include <qstring.h>
#include <qcombobox.h>
#include <qpixmap.h>
#include <qcolor.h>
#include <qpainter.h>
#include <qpen.h>

/*!
  \file qlinecombo.h
  \brief Header file for the QLineCombo class.
  
  The QLineCombo class is defined in this file.
*/

//! A line width selector.
/*!
  This class inherits QComboBox. It gives the user the ability to select a specific line width.
*/
class QLineCombo: public QComboBox
{
 public:
               QLineCombo     (QWidget * Parent = 0, const char * Name = 0);
  virtual     ~QLineCombo     ();
          void SetCurrentWidth(int);
	  int  GetCurrentWidth();
};

#endif
