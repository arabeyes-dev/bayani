/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani main application file.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* QT headers */
#include <qnamespace.h>
#include <qobject.h>
#include <qstring.h>
#include <qpixmap.h>
#include <qtranslator.h>
#include <qapplication.h>
#include <qsplashscreen.h>
/* Bayani headers */
#include <gui.h>
#include <icons.h>

int main(int argc, char ** argv)
{
  QApplication BayaniQTApp(argc, argv);
  
  QPixmap SplashQPM(QString("%1/splash.png") .arg(GRAPHICS_DIR));
  
  if(SplashQPM.isNull())
    {
      GUI::PrintMessage(NULL, MESSAGE_CRITICAL, QObject::trUtf8("Splash screen pixmap '%1/splash.png' does not exist.<br />Aborting.") .arg(GRAPHICS_DIR));
      
      return -1;
    }
  
  int ScreenWidth  = BayaniQTApp.desktop()->width();
  int ScreenHeight = BayaniQTApp.desktop()->height();
  
  QSplashScreen * BayaniSplashScreen = new QSplashScreen(SplashQPM, Qt::WStyle_StaysOnTop);
  BayaniSplashScreen->show();
  
  QTranslator QTTranslator(0);
  QTranslator BayaniTranslator(0);
  
  QString Argument = "-en";
  int Alignement = Qt::AlignBottom | Qt::AlignLeft;
  
  if(BayaniQTApp.argc() > 1)
    {
      QString tst = BayaniQTApp.argv()[1];
      
      if(tst == "-ar" || tst == "-en" || tst == "-fr")
	Argument = tst;
    }
  
  if(Argument == "-fr")
    {
      QTTranslator.load("qt_fr.qm", TRANSLATS_DIR);
      BayaniQTApp.installTranslator(&QTTranslator);
      
      BayaniTranslator.load("fr.qm", TRANSLATS_DIR);
      BayaniQTApp.installTranslator(&BayaniTranslator);
    }
  if(Argument == "-ar") 
    {
      QTTranslator.load("qt_ar.qm", TRANSLATS_DIR);
      BayaniQTApp.installTranslator(&QTTranslator);
      
      BayaniTranslator.load("ar.qm", TRANSLATS_DIR);
      BayaniQTApp.installTranslator(&BayaniTranslator);
      
      BayaniQTApp.setReverseLayout(true);
      
      Alignement = Qt::AlignBottom | Qt::AlignRight;
    }
  
  BayaniSplashScreen->message(QObject::trUtf8("Bayani, the Free Scientific Environment, version %1") .arg(VERSION), Alignement);
  
  BayaniSplashScreen->message(QObject::trUtf8("Set locale and loaded translation..."), Alignement);
  
  BayaniSplashScreen->message(QObject::trUtf8("Creating the main window..."), Alignement);
  
  GUI BayaniGUI;
  
  BayaniGUI.setGeometry(int(0.125*ScreenWidth), int(0.125*ScreenHeight), int(0.75*ScreenWidth), int(0.75*ScreenHeight));
  BayaniGUI.setCaption(QObject::trUtf8("Bayani %1") .arg(VERSION));
  BayaniGUI.setIcon(QPixmap(BayaniLogoXPM));
  
  BayaniSplashScreen->message(QObject::trUtf8("Setting the graphical layout..."), Alignement);
  
  if(Argument == "-ar")
    BayaniGUI.SetR2L(true);
  
  BayaniQTApp.setMainWidget(&BayaniGUI);
  
  BayaniSplashScreen->message(QObject::trUtf8("Showing the main window and entering the global loop..."), Alignement);
  
  BayaniSplashScreen->finish(&BayaniGUI);
  delete BayaniSplashScreen;
  
  BayaniQTApp.processEvents();
  
  BayaniGUI.show();
  
  return BayaniQTApp.exec();
}
