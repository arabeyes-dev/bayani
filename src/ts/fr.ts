<!DOCTYPE TS><TS>
<context>
    <name>BHelpBrowser</name>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>&amp;Back</source>
        <translation>R&amp;etour</translation>
    </message>
    <message>
        <source>&amp;Forward</source>
        <translation>A&amp;vance</translation>
    </message>
    <message>
        <source>&amp;Home</source>
        <translation>Acc&amp;ueil</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Go</source>
        <translation>&amp;Aller à</translation>
    </message>
    <message>
        <source>Help Toolbar</source>
        <translation>Barre d&apos;outils de l&apos;aide</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Documentation index not found&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;L&apos;index de la documentation n&apos;a pas été trouvé&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&amp;Contents</source>
        <translation>&amp;Contenu</translation>
    </message>
    <message>
        <source>Help - Bayani Handbook</source>
        <translation>Aide - Le manuel de Bayani</translation>
    </message>
    <message>
        <source>Ready.</source>
        <translation>Prêt.</translation>
    </message>
    <message>
        <source>Loaded page.</source>
        <translation>Page chargée.</translation>
    </message>
</context>
<context>
    <name>BTextBrowser</name>
    <message>
        <source>Document %1 not found in directory %2.</source>
        <translation>Le document %1 n&apos;a pas été trouvé dans le répertoire %2.</translation>
    </message>
</context>
<context>
    <name>Frame</name>
    <message>
        <source>The frame has been modified.&lt;br /&gt;Are you sure you want to close it without saving it?</source>
        <translation>Le cadre a été modifié.&lt;br /&gt;Etes vous sûr de vouloir le fermer sans le sauvegarder ?</translation>
    </message>
    <message>
        <source>Modified frame</source>
        <translation>Cadre modifié</translation>
    </message>
    <message>
        <source>Close canceled: the frame has been modified.</source>
        <translation>Fermeture annulée: le cadre a été modifié.</translation>
    </message>
</context>
<context>
    <name>GUI</name>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <source>Define &amp;Zones...</source>
        <translation>Définir des &amp;zones...</translation>
    </message>
    <message>
        <source>Set Active &amp;Graph...</source>
        <translation>Définir le &amp;graphe actif...</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation>Sauvegarder s&amp;ous...</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>&amp;Fill from File...</source>
        <translation>&amp;Remplir à partir d&apos;un fichier...</translation>
    </message>
    <message>
        <source>&amp;Write to File...</source>
        <translation>&amp;Ecrire dans un fichier...</translation>
    </message>
    <message>
        <source>&amp;Swap Rows/Columns...</source>
        <translation>E&amp;changer les lignes/colonnes...</translation>
    </message>
    <message>
        <source>&amp;Transpose Table</source>
        <translation>&amp;Transposer le tableau</translation>
    </message>
    <message>
        <source>&amp;Manage Size...</source>
        <translation>&amp;Gérer la taille...</translation>
    </message>
    <message>
        <source>&amp;Empty Cells...</source>
        <translation>&amp;Vider des cellules...</translation>
    </message>
    <message>
        <source>&amp;Function...</source>
        <translation>&amp;Fonction...</translation>
    </message>
    <message>
        <source>&amp;Histogram...</source>
        <translation>&amp;Histogramme...</translation>
    </message>
    <message>
        <source>2D &amp;Graph...</source>
        <translation>&amp;Graphe 2D...</translation>
    </message>
    <message>
        <source>FITS &amp;Image...</source>
        <translation>&amp;Image FITS...</translation>
    </message>
    <message>
        <source>Set &amp;Bounds...</source>
        <translation>Régler les &amp;limites...</translation>
    </message>
    <message>
        <source>Set &amp;Axes...</source>
        <translation>Régler les &amp;axes...</translation>
    </message>
    <message>
        <source>Fit &amp;Data...</source>
        <translation>Ajuster des &amp;données...</translation>
    </message>
    <message>
        <source>&amp;Variables...</source>
        <translation>&amp;Variables...</translation>
    </message>
    <message>
        <source>&amp;Compute...</source>
        <translation>&amp;Calculer...</translation>
    </message>
    <message>
        <source>&amp;Frame...</source>
        <translation>&amp;Cadre...</translation>
    </message>
    <message>
        <source>&amp;Graph...</source>
        <translation>&amp;Graphe...</translation>
    </message>
    <message>
        <source>&amp;Data...</source>
        <translation>&amp;Données...</translation>
    </message>
    <message>
        <source>&amp;Save Current Settings</source>
        <translation>&amp;Sauvegarder les réglages actuels</translation>
    </message>
    <message>
        <source>Bayani &amp;Handbook...</source>
        <translation>Le &amp;manuel de Bayani...</translation>
    </message>
    <message>
        <source>What&apos;s &amp;This?</source>
        <translation>&amp;Qu&apos;est-ce que c&apos;est ?</translation>
    </message>
    <message>
        <source>&amp;About Bayani...</source>
        <translation>&amp;A propos de Bayani...</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <source>Fra&amp;me</source>
        <translation>&amp;Cadre</translation>
    </message>
    <message>
        <source>&amp;Table</source>
        <translation>&amp;Tableau</translation>
    </message>
    <message>
        <source>&amp;Graph</source>
        <translation>&amp;Graphe</translation>
    </message>
    <message>
        <source>&amp;Computation</source>
        <translation>&amp;Calcul</translation>
    </message>
    <message>
        <source>Te&amp;xts</source>
        <translation>Te&amp;xtes</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>&amp;Réglages</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>&amp;Show/Hide</source>
        <translation>&amp;Montrer/Cacher</translation>
    </message>
    <message>
        <source>Ter&amp;minal</source>
        <translation>Ter&amp;minal</translation>
    </message>
    <message>
        <source>&amp;Cascade Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Tile Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toolbar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <source>Table</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <source>Terminal</source>
        <translation>Terminal</translation>
    </message>
    <message>
        <source>Ready.</source>
        <translation>Prêt.</translation>
    </message>
    <message>
        <source>%1 could not be found.&lt;br /&gt;Online help will not be available.</source>
        <translation>%1 n&apos;a pu être trouvé.&lt;br /&gt;L&apos;aide en ligne ne sera pas disponible.</translation>
    </message>
    <message>
        <source>Information: %1</source>
        <translation>Information: %1</translation>
    </message>
    <message>
        <source>Information:</source>
        <translation>Information:</translation>
    </message>
    <message>
        <source>Warning: %1</source>
        <translation>Avertissement: %1</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation>Avertissement:</translation>
    </message>
    <message>
        <source>Error: %1</source>
        <translation>Erreur: %1</translation>
    </message>
    <message>
        <source>Error:</source>
        <translation>Erreur:</translation>
    </message>
    <message>
        <source>x</source>
        <comment>Variable</comment>
        <translation>x</translation>
    </message>
    <message>
        <source>c1</source>
        <comment>Parameter</comment>
        <translation>c1</translation>
    </message>
    <message>
        <source>c1 &gt; 0</source>
        <comment>Cut</comment>
        <translation>c1 &gt; 0</translation>
    </message>
    <message>
        <source>c2</source>
        <comment>Parameter</comment>
        <translation>c2</translation>
    </message>
    <message>
        <source>c3</source>
        <comment>Parameter</comment>
        <translation>c3</translation>
    </message>
    <message>
        <source>c4</source>
        <comment>Parameter</comment>
        <translation>c4</translation>
    </message>
    <message>
        <source>The x axis inferior value is greater than or equal to the superior one.</source>
        <translation>La valeur inférieure de l&apos;axe x est supérieure ou égale à la valeur supérieure.</translation>
    </message>
    <message>
        <source>The y axis inferior value is greater than or equal to the superior one.</source>
        <translation>La valeur inférieure de l&apos;axe y est supérieure ou égale à la valeur supérieure.</translation>
    </message>
    <message>
        <source>The table has been modified.&lt;br /&gt;Are you sure you want to quit without saving it?</source>
        <translation>Le tableau a été modifié.&lt;br /&gt;Êtes vous sûr de vouloir quitter sans le sauvegarder ?</translation>
    </message>
    <message>
        <source>Table modified</source>
        <translation>Tableau modifié</translation>
    </message>
    <message>
        <source>Quit canceled: the table has been modified.</source>
        <translation>Sortie annulée: le tableau a été modifié.</translation>
    </message>
    <message>
        <source>There are modified frames.&lt;br /&gt;Are you sure you want to quit without saving them?</source>
        <translation>Il y a des cadres modifiés.&lt;br /&gt;Êtes vous sûr de vouloir quitter sans les sauvegarder ?</translation>
    </message>
    <message>
        <source>Modified frames</source>
        <translation>Cadres modifiés</translation>
    </message>
    <message>
        <source>Quit canceled: there are modified frames.</source>
        <translation>Sortie annulée: il y a des cadres modifiés.</translation>
    </message>
    <message>
        <source>Information: New directory</source>
        <translation>Information: Nouveau répertoire</translation>
    </message>
    <message>
        <source>Created directory %1 in order to store the commands history.</source>
        <translation>Créé le répertoire %1 afin d&apos;y stocker l&apos;historique des commandes.</translation>
    </message>
    <message>
        <source>Error: File error</source>
        <translation>Erreur: Erreur de fichier</translation>
    </message>
    <message>
        <source>A problem occured while attempting to open file %1 in write-only mode.</source>
        <translation>Un problème s&apos;est produit en tentant d&apos;ouvrir le fichier %1 en mode d&apos;écriture seule.</translation>
    </message>
    <message>
        <source>Bye!</source>
        <translation>Au revoir !</translation>
    </message>
    <message>
        <source>Table hidden.</source>
        <translation>Tableau caché.</translation>
    </message>
    <message>
        <source>Table shown.</source>
        <translation>Tableau affiché.</translation>
    </message>
    <message>
        <source>Terminal hidden.</source>
        <translation>Terminal caché.</translation>
    </message>
    <message>
        <source>Terminal shown.</source>
        <translation>Terminal affiché.</translation>
    </message>
    <message>
        <source>Frame %1</source>
        <translation>Cadre %1</translation>
    </message>
    <message>
        <source>New frame, number %1.</source>
        <translation>Nouveau cadre, numéro %1.</translation>
    </message>
    <message>
        <source>There&apos;s no available frame.</source>
        <translation>Il n&apos;y a pas de cadre disponible.</translation>
    </message>
    <message>
        <source>Please indicate the number of horizontal zones.</source>
        <translation>Merci d&apos;indiquer le nombre de zones horizontales.</translation>
    </message>
    <message>
        <source>Please indicate the number of vertical zones.</source>
        <translation>Merci d&apos;indiquer le nombre de zones verticales.</translation>
    </message>
    <message>
        <source>%1 zones are now available in the frame.</source>
        <translation>%1 zones sont à présent disponibles dans le cadre.</translation>
    </message>
    <message>
        <source>The number of existing graphs is less than or equal to 1.</source>
        <translation>Le nombre de graphes existants est inférieur ou égal à 1.</translation>
    </message>
    <message>
        <source>Graph number %1 is already active.</source>
        <translation>Le graphe numéro %1 est déjà actif.</translation>
    </message>
    <message>
        <source>Graph number %1 is now active.</source>
        <translation>Le graphe numéro %1 est à présent actif.</translation>
    </message>
    <message>
        <source>PNG pictures (*.png);;All files (*)</source>
        <translation>Images PNG (*.png);;Tous les fichiers (*)</translation>
    </message>
    <message>
        <source>Save canceled.</source>
        <translation>Sauvegarde annulée.</translation>
    </message>
    <message>
        <source>File %1 already exists.&lt;br /&gt;Are you sure you want to overwrite it?</source>
        <translation>Le fichier %1 existe déjà.&lt;br /&gt;Voulez vous vraiment l&apos;écraser ?</translation>
    </message>
    <message>
        <source>File exists</source>
        <translation>Le fichier existe</translation>
    </message>
    <message>
        <source>Save canceled: file already exists.</source>
        <translation>Sauvegarde annulée: le fichier existe déjà.</translation>
    </message>
    <message>
        <source>A problem occured while attempting to save frame in %1.</source>
        <translation>Un problème s&apos;est produit en tentant de sauvegarder le cadre dans %1.</translation>
    </message>
    <message>
        <source>Saved frame in %1.</source>
        <translation>Sauvegardé le cadre dans %1.</translation>
    </message>
    <message>
        <source>Frame closed.</source>
        <translation>Cadre fermé.</translation>
    </message>
    <message>
        <source>Please indicate the file name.</source>
        <translation>Merci d&apos;indiquer le nom de fichier.</translation>
    </message>
    <message>
        <source>Please indicate the columns number.</source>
        <translation>Merci d&apos;indiquer le nombre de colonnes.</translation>
    </message>
    <message>
        <source>Please indicate the columns separator.</source>
        <translation>Merci d&apos;indiquer le séparateur de colonnes.</translation>
    </message>
    <message>
        <source>Please indicate the default fill.</source>
        <translation>Merci d&apos;indiquer le remplissage par défaut.</translation>
    </message>
    <message>
        <source>This will remove all the current content from the selected cells.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va effacer tout le contenu actuel des cellules sélectionnées.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Fill table?</source>
        <translation>Remplir le tableau ?</translation>
    </message>
    <message>
        <source>Fill canceled.</source>
        <translation>Remplissage annulé.</translation>
    </message>
    <message>
        <source>This will remove all the current content from the other cells.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va effacer tout le contenu actuel des autres cellules.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Empty other cells?</source>
        <translation>Vider les autres cellules ?</translation>
    </message>
    <message>
        <source>Read %1 lines from %2. Considered %3 lines.</source>
        <translation>Lu %1 lignes depuis %2. Tenu compte de %3 lignes.</translation>
    </message>
    <message>
        <source>A problem occured while attempting to open file %1 in read-only mode.</source>
        <translation>Un problème s&apos;est produit en tentant d&apos;ouvrir le fichier %1 en mode de lecture seule.</translation>
    </message>
    <message>
        <source>Write canceled.</source>
        <translation>Écriture annulée.</translation>
    </message>
    <message>
        <source>Written %1 lines in %2.</source>
        <translation>Écrit %1 lignes dans %2.</translation>
    </message>
    <message>
        <source>A problem occured while attempting to open file %1 in write mode.</source>
        <translation>Un problème s&apos;est produit en tentant d&apos;ouvrir le fichier %1 en mode d&apos;écriture.</translation>
    </message>
    <message>
        <source>Elements are equal. There&apos;s nothing to do.</source>
        <translation>Les éléments sont égaux. Il n&apos;y a rien à faire.</translation>
    </message>
    <message>
        <source>This will remove all the current content from the destination element.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va effacer tout le contenu actuel de l&apos;élément destination.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Copy content?</source>
        <translation>Copier le contenu ?</translation>
    </message>
    <message>
        <source>Copy canceled.</source>
        <translation>Copie annulée.</translation>
    </message>
    <message>
        <source>Updated table.</source>
        <translation>Mis-à-jour le tableau.</translation>
    </message>
    <message>
        <source>Updated table: %1 rows, %2 columns.</source>
        <translation>Mis-à-jour le tableau: %1 lignes, %2 colonnes.</translation>
    </message>
    <message>
        <source>Please indicate the elements number.</source>
        <translation>Merci d&apos;indiquer le nombre d&apos;éléments.</translation>
    </message>
    <message>
        <source>You are attempting to remove inexisting elements.</source>
        <translation>Vous tentez de supprimer des éléments inexistants.</translation>
    </message>
    <message>
        <source>This will remove all the selected elements from the table.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va effacer tous les éléments sélectionnés du tableau.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Remove elements?</source>
        <translation>Supprimer des éléments ?</translation>
    </message>
    <message>
        <source>Remove canceled.</source>
        <translation>Suppression annulée.</translation>
    </message>
    <message>
        <source>This will delete all the data from the selected cells.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va effacer toutes les données des cellules sélectionnées.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Empty cells?</source>
        <translation>Vider les cellules ?</translation>
    </message>
    <message>
        <source>Empty canceled.</source>
        <translation>Vidage annulé.</translation>
    </message>
    <message>
        <source>Emptied %1 cells between [%2,%3] and [%4,%5].</source>
        <translation>Vidé %1 cellules entre [%2,%3] et [%4,%5].</translation>
    </message>
    <message>
        <source>Please indicate the expression.</source>
        <translation>Merci d&apos;indiquer l&apos;expression.</translation>
    </message>
    <message>
        <source>Please indicate the inferior value.</source>
        <translation>Merci d&apos;indiquer la valeur inférieure.</translation>
    </message>
    <message>
        <source>Please indicate the superior value.</source>
        <translation>Merci d&apos;indiquer la valeur supérieure.</translation>
    </message>
    <message>
        <source>The inferior value is greater than or equal to the superior one.</source>
        <translation>La valeur inférieure est supérieure ou égale à la valeur supérieure.</translation>
    </message>
    <message>
        <source>Error in expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation>Erreur dans l&apos;expression: &lt;font color=%1&gt;%2&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Represented %1 between %2 and %3.</source>
        <translation>Représenté %1 entre %2 et %3.</translation>
    </message>
    <message>
        <source>Please indicate the number of bins.</source>
        <translation>Merci d&apos;indiquer le nombre de bins.</translation>
    </message>
    <message>
        <source>Please indicate the minimal value.</source>
        <translation>Merci d&apos;indiquer la valeur minimale.</translation>
    </message>
    <message>
        <source>Please indicate the maximal value.</source>
        <translation>Merci d&apos;indiquer la valeur maximale.</translation>
    </message>
    <message>
        <source>The minimal value is greater than or equal to the maximal one.</source>
        <translation>La valeur minimale est supérieure ou égale à la valeur maximale.</translation>
    </message>
    <message>
        <source>c%1</source>
        <comment>Parameter</comment>
        <translation>c%1</translation>
    </message>
    <message>
        <source>Error in cut: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation>Erreur dans la coupure: &lt;font color=%1&gt;%2&lt;/font&gt;</translation>
    </message>
    <message>
        <source>There is nothing to represent.</source>
        <translation>Il n&apos;y a rien à représenter.</translation>
    </message>
    <message>
        <source>%1 entries in histogram.</source>
        <translation>%1 entrées dans l&apos;histogramme.</translation>
    </message>
    <message>
        <source>%1 entries in graph.</source>
        <translation>%1 entrées dans le graphe.</translation>
    </message>
    <message>
        <source>A problem occured while attempting to represent file %1.</source>
        <translation>Un problème s&apos;est produit en tentant de représenter le fichier %1.</translation>
    </message>
    <message>
        <source>Represented %1 (%2*%3 square pixels).</source>
        <translation>Représenté %1 (%2*%3 pixels carrés).</translation>
    </message>
    <message>
        <source>There&apos;s no available graph.</source>
        <translation>Il n&apos;y a pas de graphe disponible.</translation>
    </message>
    <message>
        <source>Updated the graph&apos;s bounds.</source>
        <translation>Mis-à-jour les limites du graphe.</translation>
    </message>
    <message>
        <source>Updated the graph&apos;s axes.</source>
        <translation>Mis-à-jour les axes du graphe.</translation>
    </message>
    <message>
        <source>The active graph is not fittable.</source>
        <translation>Le graphe actif n&apos;est pas ajustable.</translation>
    </message>
    <message>
        <source>p&lt;sub&gt;0&lt;/sub&gt; = %1</source>
        <translation>p&lt;sub&gt;0&lt;/sub&gt; = %1</translation>
    </message>
    <message>
        <source>p&lt;sub&gt;0&lt;/sub&gt; = %1&lt;br /&gt;p&lt;sub&gt;1&lt;/sub&gt; = %2</source>
        <translation>p&lt;sub&gt;0&lt;/sub&gt; = %1&lt;br /&gt;p&lt;sub&gt;1&lt;/sub&gt; = %2</translation>
    </message>
    <message>
        <source>p&lt;sub&gt;0&lt;/sub&gt; = %1&lt;br /&gt;p&lt;sub&gt;1&lt;/sub&gt; = %2&lt;br /&gt;p&lt;sub&gt;2&lt;/sub&gt; = %3</source>
        <translation>p&lt;sub&gt;0&lt;/sub&gt; = %1&lt;br /&gt;p&lt;sub&gt;1&lt;/sub&gt; = %2&lt;br /&gt;p&lt;sub&gt;2&lt;/sub&gt; = %3</translation>
    </message>
    <message>
        <source>Please indicate the initial value of p&lt;sub&gt;0&lt;/sub&gt;.</source>
        <translation>Merci d&apos;indiquer la valeur initiale de p&lt;sub&gt;0&lt;/sub&gt;.</translation>
    </message>
    <message>
        <source>Please indicate the initial value of p&lt;sub&gt;1&lt;/sub&gt;.</source>
        <translation>Merci d&apos;indiquer la valeur initiale de p&lt;sub&gt;1&lt;/sub&gt;.</translation>
    </message>
    <message>
        <source>Please indicate the initial value of p&lt;sub&gt;2&lt;/sub&gt;.</source>
        <translation>Merci d&apos;indiquer la valeur initiale de p&lt;sub&gt;2&lt;/sub&gt;.</translation>
    </message>
    <message>
        <source>Please indicate the initial value of p&lt;sub&gt;3&lt;/sub&gt;.</source>
        <translation>Merci d&apos;indiquer la valeur initiale de p&lt;sub&gt;3&lt;/sub&gt;.</translation>
    </message>
    <message>
        <source>Please indicate the maximal iterations number.</source>
        <translation>Merci d&apos;indiquer le nombre maximal d&apos;itérations.</translation>
    </message>
    <message>
        <source>Please indicate the &amp;chi;&lt;sup&gt;2&lt;/sup&gt; convergence criterion.</source>
        <translation>Merci d&apos;indiquer le critère de convergence du &amp;chi;&lt;sup&gt;2&lt;/sup&gt;.</translation>
    </message>
    <message>
        <source>p&lt;sub&gt;0&lt;/sub&gt; = (%1)&amp;plusmn;(%2)&lt;br /&gt;p&lt;sub&gt;1&lt;/sub&gt; = (%3)&amp;plusmn;(%4)&lt;br /&gt;p&lt;sub&gt;2&lt;/sub&gt; = (%5)&amp;plusmn;(%6)&lt;br /&gt;p&lt;sub&gt;3&lt;/sub&gt; = (%7)&amp;plusmn;(%8)&lt;br /&gt;</source>
        <translation>p&lt;sub&gt;0&lt;/sub&gt; = (%1)&amp;plusmn;(%2)&lt;br /&gt;p&lt;sub&gt;1&lt;/sub&gt; = (%3)&amp;plusmn;(%4)&lt;br /&gt;p&lt;sub&gt;2&lt;/sub&gt; = (%5)&amp;plusmn;(%6)&lt;br /&gt;p&lt;sub&gt;3&lt;/sub&gt; = (%7)&amp;plusmn;(%8)&lt;br /&gt;</translation>
    </message>
    <message>
        <source>&amp;chi;&lt;sup&gt;2&lt;/sup&gt; = %1&lt;br /&gt;Iteration = %2&lt;br /&gt;Number of degrees of freedom = %3</source>
        <translation>&amp;chi;&lt;sup&gt;2&lt;/sup&gt; = %1&lt;br /&gt;Itération = %2&lt;br /&gt;Nombre de degrés de liberté = %3</translation>
    </message>
    <message>
        <source>The number of degrees of freedom (%1) is negative.</source>
        <translation>Le nombre de degrés de liberté (%1) est négatif.</translation>
    </message>
    <message>
        <source>There is no fitting solution.</source>
        <translation>Il n&apos;y a pas de solution d&apos;ajustement.</translation>
    </message>
    <message>
        <source>&lt;br /&gt;The fit did not converge.</source>
        <translation>&lt;br /&gt;L&apos;ajustement n&apos;a pas convergé.</translation>
    </message>
    <message>
        <source>%1</source>
        <comment>Fit expression</comment>
        <translation>%1</translation>
    </message>
    <message>
        <source>%1*x+(%2)</source>
        <comment>Fit expression</comment>
        <translation>%1*x+(%2)</translation>
    </message>
    <message>
        <source>(%1)*x^2+(%2)*x+(%3)</source>
        <comment>Fit expression</comment>
        <translation>(%1)*x^2+(%2)*x+(%3)</translation>
    </message>
    <message>
        <source>(%1)*x^3+(%2)*x^2+(%3)*x+(%4)</source>
        <comment>Fit expression</comment>
        <translation>(%1)*x^3+(%2)*x^2+(%3)*x+(%4)</translation>
    </message>
    <message>
        <source>Fit result</source>
        <translation>Résultat de l&apos;ajustement</translation>
    </message>
    <message>
        <source>Graph closed.</source>
        <translation>Graphe fermé.</translation>
    </message>
    <message>
        <source>%1 defined variables.</source>
        <translation>%1 variables définies.</translation>
    </message>
    <message>
        <source>Expression simplification: %1</source>
        <translation>Simplification de l&apos;expression: %1</translation>
    </message>
    <message>
        <source>Error in expression: &lt;font color=%1&gt;%2&lt;/font&gt;&lt;br /&gt;Intermediate result: %3</source>
        <translation>Erreur dans l&apos;expression: &lt;font color=%1&gt;%2&lt;/font&gt;&lt;br /&gt;Résultat intermédiaire: %3</translation>
    </message>
    <message>
        <source>Expression evaluation: %1</source>
        <translation>Évaluation de l&apos;expression: %1</translation>
    </message>
    <message>
        <source>Expression derivation: %1</source>
        <translation>Dérivation de l&apos;expression: %1</translation>
    </message>
    <message>
        <source>Updated the frame&apos;s texts.</source>
        <translation>Mis-à-jour les textes du cadre.</translation>
    </message>
    <message>
        <source>Updated the graph&apos;s texts.</source>
        <translation>Mis-à-jour les textes du graphe.</translation>
    </message>
    <message>
        <source>Applied the new global settings.</source>
        <translation>Appliqué les nouveaux réglages globaux.</translation>
    </message>
    <message>
        <source>This will unset the current global settings and set the default ones as current.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va désactiver les réglages globaux courants et activer les valeurs par défauts.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Revert to defaults?</source>
        <translation>Retour aux valeurs par défaut ?</translation>
    </message>
    <message>
        <source>Back to the default global settings canceled.</source>
        <translation>Retour aux réglages globaux par défaut annulé.</translation>
    </message>
    <message>
        <source>Back to the default global settings.</source>
        <translation>Retour aux réglages globaux par défaut.</translation>
    </message>
    <message>
        <source>Applied the new frame settings.</source>
        <translation>Appliqué les nouveaux réglages des cadres.</translation>
    </message>
    <message>
        <source>This will unset the current frame settings and set the default ones as current.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va désactiver les réglages des cadres courants et activer les valeurs par défauts.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Back to the default frame settings canceled.</source>
        <translation>Retour aux réglages des cadres par défaut annulé.</translation>
    </message>
    <message>
        <source>Back to the default frame settings.</source>
        <translation>Retour aux réglages des cadres par défaut.</translation>
    </message>
    <message>
        <source>Applied the new graph settings.</source>
        <translation>Appliqué les nouveaux réglages des graphes.</translation>
    </message>
    <message>
        <source>This will unset the current graph settings and set the default ones as current.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va désactiver les réglages des graphes courants et activer les valeurs par défauts.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Back to the default graph settings canceled.</source>
        <translation>Retour aux réglages des graphes par défaut annulé.</translation>
    </message>
    <message>
        <source>Back to the default graph settings.</source>
        <translation>Retour aux réglages des graphes par défaut.</translation>
    </message>
    <message>
        <source>Applied the new data settings.</source>
        <translation>Appliqué les nouveaux réglages des données.</translation>
    </message>
    <message>
        <source>This will unset the current data settings and set the default ones as current.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va désactiver les réglages des données courants et activer les valeurs par défauts.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Back to the default data settings canceled.</source>
        <translation>Retour aux réglages des données par défaut annulé.</translation>
    </message>
    <message>
        <source>Back to the default data settings.</source>
        <translation>Retour aux réglages des données par défaut.</translation>
    </message>
    <message>
        <source>This will overwrite the existing default settings and set the current ones as default.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation>Cela va remplacer les réglages par défaut par les valeurs courantes.&lt;br /&gt;Voulez vous vraiment continuer ?</translation>
    </message>
    <message>
        <source>Overwrite?</source>
        <translation>Écraser ?</translation>
    </message>
    <message>
        <source>Save current settings canceled.</source>
        <translation>Sauvegarde des réglages courants annulée.</translation>
    </message>
    <message>
        <source>Created directory %1 in order to store the default settings.</source>
        <translation>Créé le répertoire %1 afin d&apos;y stocker les réglages par défaut.</translation>
    </message>
    <message>
        <source>New directory</source>
        <translation>Nouveau répertoire</translation>
    </message>
    <message>
        <source>Saved current settings.</source>
        <translation>Sauvegardé les réglages actuels.</translation>
    </message>
    <message>
        <source>Usage: %1</source>
        <translation>Utilisation: %1</translation>
    </message>
    <message>
        <source>quit</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Syntax error</source>
        <translation>Erreur de syntaxe</translation>
    </message>
    <message>
        <source>table</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Display flag&amp;gt;</source>
        <translation>%1 &amp;lt;Drapeau d&apos;affichage&amp;gt;</translation>
    </message>
    <message>
        <source>terminal</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cascade</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>tile</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>frame</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>zone</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Horizontally&amp;gt; &amp;lt;Vertically&amp;gt; [Secondary zone]</source>
        <translation>%1 &amp;lt;Horizontalement&amp;gt; &amp;lt;Verticalement&amp;gt; [Zone secondaire]</translation>
    </message>
    <message>
        <source>activegraph</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Zone&amp;gt;</source>
        <translation>%1 &amp;lt;Zone&amp;gt;</translation>
    </message>
    <message>
        <source>saveframe</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 [Frame&apos;s new filename]</source>
        <translation>%1 [Nouveau nom du fichier du cadre]</translation>
    </message>
    <message>
        <source>Please set a filename for the active frame first.</source>
        <translation>Merci d&apos;indiquer au préalable un nom de fichier pour le cadre actif.</translation>
    </message>
    <message>
        <source>closeframe</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fill</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Filename&amp;gt; &amp;lt;Columns number&amp;gt; &amp;lt;Starting row&amp;gt; &amp;lt;Starting column&amp;gt; [Add rows flag] [Add columns flag] [Empty cells flag] [Superior columns number flag] [Default fill]</source>
        <translation>%1 &amp;lt;Nom de fichier&amp;gt; &amp;lt;Nombre de colonnes&amp;gt; &amp;lt;Ligne de départ&amp;gt; &amp;lt;Colonne de départ&amp;gt; [Drapeau d&apos;ajout de lignes] [Drapeau d&apos;ajout de colonnes] [Drapeau de vidage des cellules] [Drapeau de nombre supérieur de colonnes] [Remplissage par défaut]</translation>
    </message>
    <message>
        <source>write</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Filename&amp;gt; &amp;lt;Row 1&amp;gt; &amp;lt;Column 1&amp;gt; &amp;lt;Row 2&amp;gt; &amp;lt;Column 2&amp;gt; [Append flag]</source>
        <translation>%1 &amp;lt;Nom de fichier&amp;gt; &amp;lt;Ligne 1&amp;gt; &amp;lt;Colonne 1&amp;gt; &amp;lt;Ligne 2&amp;gt; &amp;lt;Colonne 2&amp;gt; [Drapeau d&apos;ajout]</translation>
    </message>
    <message>
        <source>swaprows</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Row 1&amp;gt; &amp;lt;Row 2&amp;gt; [Copy flag]</source>
        <translation>%1 &amp;lt;Ligne 1&amp;gt; &amp;lt;Ligne 2&amp;gt; [Drapeau de copie]</translation>
    </message>
    <message>
        <source>swapcolumns</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Column 1&amp;gt; &amp;lt;Column 2&amp;gt; [Copy flag]</source>
        <translation>%1 &amp;lt;Colonne 1&amp;gt; &amp;lt;Colonne 2&amp;gt; [Drapeau de copie]</translation>
    </message>
    <message>
        <source>transpose</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>insertrows</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Rows number&amp;gt; &amp;lt;Starting row&amp;gt;</source>
        <translation>%1 &amp;lt;Nombre de lignes&amp;gt; &amp;lt;Ligne de départ&amp;gt;</translation>
    </message>
    <message>
        <source>removerows</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>insertcolumns</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Columns number&amp;gt; &amp;lt;Starting column&amp;gt;</source>
        <translation>%1 &amp;lt;Nombre de colonnes&amp;gt; &amp;lt;Colonne de départ&amp;gt;</translation>
    </message>
    <message>
        <source>removecolumns</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>empty</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Row 1&amp;gt; &amp;lt;Column 1&amp;gt; &amp;lt;Row 2&amp;gt; &amp;lt;Column 2&amp;gt;</source>
        <translation>%1 &amp;lt;Ligne 1&amp;gt; &amp;lt;Colonne 1&amp;gt; &amp;lt;Ligne 2&amp;gt; &amp;lt;Colonne 2&amp;gt;</translation>
    </message>
    <message>
        <source>function</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Expression&amp;gt; &amp;lt;x&lt;sub&gt;0&lt;/sub&gt;&amp;gt; &amp;lt;x&lt;sub&gt;1&lt;/sub&gt;&amp;gt;</source>
        <translation>%1 &amp;lt;Expression&amp;gt; &amp;lt;x&lt;sub&gt;0&lt;/sub&gt;&amp;gt; &amp;lt;x&lt;sub&gt;1&lt;/sub&gt;&amp;gt;</translation>
    </message>
    <message>
        <source>histogram</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Expression&amp;gt; [Bins number] [Minimum] [Maximum] [Cut]</source>
        <translation>%1 &amp;lt;Expression&amp;gt; [Nombre de bins] [Minimum] [Maximum] [Coupure]</translation>
    </message>
    <message>
        <source>graph2d</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;X axis expression&amp;gt; &amp;lt;Y axis expression&amp;gt; [Inferior x errors] [Inferior y errors] [Superior x errors] [Superior y errors] [Cut]</source>
        <translation>%1 &amp;lt;Expression de l&apos;axe des x&amp;gt; &amp;lt;Expression de l&apos;axe des y&amp;gt; [Erreurs inférieures de l&apos;axe des x] [Erreurs inférieures de l&apos;axe des y] [Erreurs supérieures de l&apos;axe des x] [Erreurs supérieures de l&apos;axe des y] [Coupure]</translation>
    </message>
    <message>
        <source>fitsimage</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Filename&amp;gt;</source>
        <translation>%1 &amp;lt;Nom de fichier&amp;gt;</translation>
    </message>
    <message>
        <source>bounds</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 [X inf] [X sup] [Y inf] [Y sup] [New graphs flag]</source>
        <translation>%1 [X inf] [X sup] [Y inf] [Y sup] [Drapeau de nouveaux graphes]</translation>
    </message>
    <message>
        <source>axes</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 [X log flag] [Y log flag] [X reverse flag] [Y reverse flag] [New graphs flag]</source>
        <translation>%1 [Drapeau de l&apos;axe des x logarithmique] [Drapeau de l&apos;axe des y logarithmique] [Drapeau d&apos;inversion de l&apos;axe des x] [Drapeau d&apos;inversion de l&apos;axe des y] [Drapeau de nouveaux graphes]</translation>
    </message>
    <message>
        <source>fit</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Degree&amp;gt; [Representation flag]</source>
        <translation>%1 &amp;lt;Degré&amp;gt; [Drapeau de représentation]</translation>
    </message>
    <message>
        <source>fitnum</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;p&lt;sub&gt;0&lt;/sub&gt;&amp;gt; &amp;lt;p&lt;sub&gt;1&lt;/sub&gt;&amp;gt; &amp;lt;p&lt;sub&gt;2&lt;/sub&gt;&amp;gt; &amp;lt;p&lt;sub&gt;3&lt;/sub&gt;&amp;gt; &amp;lt;Iterations number&amp;gt; &amp;lt;&amp;chi;&lt;sup&gt;2&lt;/sup&gt; criterion&amp;gt; [Representation flag] [No convergence flag]</source>
        <translation>%1 &amp;lt;p&lt;sub&gt;0&lt;/sub&gt;&amp;gt; &amp;lt;p&lt;sub&gt;1&lt;/sub&gt;&amp;gt; &amp;lt;p&lt;sub&gt;2&lt;/sub&gt;&amp;gt; &amp;lt;p&lt;sub&gt;3&lt;/sub&gt;&amp;gt; &amp;lt;Nombre d&apos;itérations&amp;gt; &amp;lt;Critère &amp;chi;&lt;sup&gt;2&lt;/sup&gt;&amp;gt; [Drapeau de représentation] [Drapeau de non convergence]</translation>
    </message>
    <message>
        <source>same</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>closegraph</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>variable</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Name&amp;gt; [Value]</source>
        <translation>%1 &amp;lt;Nom&amp;gt; [Valeur]</translation>
    </message>
    <message>
        <source>Name &apos;%1&apos; is not allowed.</source>
        <translation>Le nom &apos;%1&apos; n&apos;est pas permis.</translation>
    </message>
    <message>
        <source>variables</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The variables list is empty.</source>
        <translation>La liste des variables est vide.</translation>
    </message>
    <message>
        <source>remove_variable</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Name&amp;gt;</source>
        <translation>%1 &amp;lt;Nom&amp;gt;</translation>
    </message>
    <message>
        <source>Variable &apos;%1&apos; does not exist.</source>
        <translation>La variable &apos;%1&apos; n&apos;existe pas.</translation>
    </message>
    <message>
        <source>remove_variables</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>simplify</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Expression&amp;gt;</source>
        <translation>%1 &amp;lt;Expression&amp;gt;</translation>
    </message>
    <message>
        <source>evaluate</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>resolve</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>derive</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>integrate</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>titleframe</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Frame title&amp;gt;</source>
        <translation>%1 &amp;lt;Titre du cadre&amp;gt;</translation>
    </message>
    <message>
        <source>titlegraph</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Graph title&amp;gt;</source>
        <translation>%1 &amp;lt;Titre du graphe&amp;gt;</translation>
    </message>
    <message>
        <source>titlex</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;X axis title&amp;gt;</source>
        <translation>%1 &amp;lt;Titre de l&apos;axe des x&amp;gt;</translation>
    </message>
    <message>
        <source>titley</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Y axis title&amp;gt;</source>
        <translation>%1 &amp;lt;Titre de l&apos;axe des y&amp;gt;</translation>
    </message>
    <message>
        <source>digitsx</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>digitsy</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>optiondata</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Option&amp;gt; &amp;lt;Value&amp;gt;</source>
        <translation>%1 &amp;lt;Option&amp;gt; &amp;lt;Valeur&amp;gt;</translation>
    </message>
    <message>
        <source>f_width</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>f_style</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>f_color</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_width</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_style</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_color</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_fstyle</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_fcolor</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_size</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_marker</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_color</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_ewidth</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_estyle</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_ecolor</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fi_palette</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fi_min</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fi_max</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown option: &apos;%1&apos;</source>
        <translation>Option inconnue: &apos;%1&apos;</translation>
    </message>
    <message>
        <source>defaults</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Type&amp;gt;</source>
        <translation>%1 &amp;lt;Type&amp;gt;</translation>
    </message>
    <message>
        <source>global</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>frame</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>graph</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>data</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown type: &apos;%1&apos;</source>
        <translation>Type inconnu: &apos;%1&apos;</translation>
    </message>
    <message>
        <source>saveoptions</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>help</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>about</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>execute</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>I could not open file %1 in read-only mode.</source>
        <translation>Je n&apos;ai pas pu ouvrir le fichier en mode de lecture seule.</translation>
    </message>
    <message>
        <source>&lt;br /&gt;I could not open file %1 either.</source>
        <translation>&lt;br /&gt;Je n&apos;ai pas pu ouvrir le fichier %1 non plus.</translation>
    </message>
    <message>
        <source>shell</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Command&amp;gt;</source>
        <translation>%1 &amp;lt;Commande&amp;gt;</translation>
    </message>
    <message>
        <source>sleep</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Seconds&amp;gt;</source>
        <translation>%1 &amp;lt;Secondes&amp;gt;</translation>
    </message>
    <message>
        <source>Sleeping %1 second(s)...</source>
        <translation>Sommeil de %1 seconde(s)...</translation>
    </message>
    <message>
        <source>print</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown command: &apos;%1&apos;</source>
        <translation>Commande inconnue: &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Error in draw: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation>Erreur lors de la représentation: &lt;font color=%1&gt;%2&lt;/font&gt;</translation>
    </message>
    <message>
        <source>x=%1 y=%2</source>
        <translation>x=%1 y=%2</translation>
    </message>
    <message>
        <source>x=%1 y=%2 value=%3</source>
        <translation>x=%1 y=%2 valeur=%3</translation>
    </message>
    <message>
        <source>Glo&amp;bal...</source>
        <translation>Glo&amp;bal...</translation>
    </message>
    <message>
        <source>Please indicate the x axis inferior bound.</source>
        <translation>Merci d&apos;indiquer la limite inférieure de l&apos;axe des x.</translation>
    </message>
    <message>
        <source>Please indicate the x axis superior bound.</source>
        <translation>Merci d&apos;indiquer la limite supérieure de l&apos;axe des x.</translation>
    </message>
    <message>
        <source>Please indicate the y axis inferior bound.</source>
        <translation>Merci d&apos;indiquer la limite inférieure de l&apos;axe des y.</translation>
    </message>
    <message>
        <source>Please indicate the y axis superior bound.</source>
        <translation>Merci d&apos;indiquer la limite supérieure de l&apos;axe des y.</translation>
    </message>
    <message>
        <source>Error in x axis expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation>Erreur dans l&apos;expression de l&apos;axe des x: &lt;font color=%1&gt;%2&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Error in y axis expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation>Erreur dans l&apos;expression de l&apos;axe des y: &lt;font color=%1&gt;%2&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Error in x axis (inferior) errors expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation>Erreur dans l&apos;expression des erreurs (inférieures) de l&apos;axe des x: &lt;font color=%1&gt;%2&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Error in y axis (inferior) errors expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation>Erreur dans l&apos;expression des erreurs (inférieures) de l&apos;axe des y: &lt;font color=%1&gt;%2&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Error in x axis (superior) errors expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation>Erreur dans l&apos;expression des erreurs (supérieures) de l&apos;axe des x: &lt;font color=%1&gt;%2&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Error in y axis (superior) errors expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation>Erreur dans l&apos;expression des erreurs (supérieures) de l&apos;axe des y: &lt;font color=%1&gt;%2&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>QFileSelector</name>
    <message>
        <source>&amp;Browse...</source>
        <translation>&amp;Parcourir...</translation>
    </message>
    <message>
        <source>%1All files (*)</source>
        <translation>%1Tous les fichiers (*)</translation>
    </message>
</context>
<context>
    <name>QFontSelector</name>
    <message>
        <source>Change...</source>
        <translation>Changer...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>!</source>
        <comment>Operator</comment>
        <translation>!</translation>
    </message>
    <message>
        <source>^</source>
        <comment>Operator</comment>
        <translation>^</translation>
    </message>
    <message>
        <source>*</source>
        <comment>Operator</comment>
        <translation>*</translation>
    </message>
    <message>
        <source>/</source>
        <comment>Operator</comment>
        <translation>/</translation>
    </message>
    <message>
        <source>+</source>
        <comment>Operator</comment>
        <translation>+</translation>
    </message>
    <message>
        <source>-</source>
        <comment>Operator</comment>
        <translation>-</translation>
    </message>
    <message>
        <source>&lt;</source>
        <comment>Operator</comment>
        <translation>&lt;</translation>
    </message>
    <message>
        <source>&lt;=</source>
        <comment>Operator</comment>
        <translation>&lt;=</translation>
    </message>
    <message>
        <source>&gt;</source>
        <comment>Operator</comment>
        <translation>&gt;</translation>
    </message>
    <message>
        <source>&gt;=</source>
        <comment>Operator</comment>
        <translation>&gt;=</translation>
    </message>
    <message>
        <source>==</source>
        <comment>Operator</comment>
        <translation>==</translation>
    </message>
    <message>
        <source>!=</source>
        <comment>Operator</comment>
        <translation>!=</translation>
    </message>
    <message>
        <source>&amp;&amp;</source>
        <comment>Operator</comment>
        <translation>&amp;&amp;</translation>
    </message>
    <message>
        <source>||</source>
        <comment>Operator</comment>
        <translation>||</translation>
    </message>
    <message>
        <source>sqrt</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fabs</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sign</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sin</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>asin</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cos</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>acos</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>tan</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>atan</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>log</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>log10</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>exp</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p%1</source>
        <comment>Parameter</comment>
        <translation>p%1</translation>
    </message>
    <message>
        <source>x</source>
        <comment>Variable</comment>
        <translation>x</translation>
    </message>
    <message>
        <source>.</source>
        <comment>Digits separator (comma)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Empty expression</source>
        <translation>Expression vide</translation>
    </message>
    <message>
        <source>Undefined symbol &apos;%1&apos; (index %2)</source>
        <translation>Symbol indéfini &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>Extra closing bracket (index %1)</source>
        <translation>Parenthèse fermante supplémentaire (index %1)</translation>
    </message>
    <message>
        <source>Element &apos;%1&apos; (index %2) not allowed after a closing bracket. Expecting an operator or a closing bracket</source>
        <translation>L&apos;élément &apos;%1&apos; (index %2) n&apos;est pas autorisé après une parenthèse fermante. Un opérateur ou une parenthèse fermante sont attendus</translation>
    </message>
    <message>
        <source>Undefined operator &apos;%1&apos; (index %2)</source>
        <translation>Opérateur indéfini &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>Undefined variable &apos;%1&apos; (index %2)</source>
        <translation>Variable indéfinie &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>Undefined function &apos;%1&apos; (index %2)</source>
        <translation>Fonction indéfinie &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>Expecting a closing bracket or an operator after element &apos;%1&apos; (index %2)</source>
        <translation>Une parenthèse fermante ou un opérateur sont attendus après l&apos;élément &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>%1 closing brackets are missing</source>
        <translation>%1 parenthèses fermantes sont manquantes</translation>
    </message>
    <message>
        <source>Wrong operands number for operator &apos;%1&apos; (index %2)</source>
        <translation>Mauvais nombre d&apos;opérandes pour l&apos;opérateur &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>Operand cannot preceed unary operator &apos;%1&apos; (index %2)</source>
        <translation>Un opérande ne peut pas précéder l&apos;opérateur unaire &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>Wrong arguments number for function &apos;%1&apos; (index %2)</source>
        <translation>Mauvais nombre d&apos;arguments pour la fonction &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>Bad operands for operator &apos;%1&apos; (index %2)</source>
        <translation>Mauvais opérandes pour l&apos;opérateur &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>Bad argument for function &apos;%1&apos; (index %2)</source>
        <translation>Mauvais argument pour la fonction &apos;%1&apos; (index %2)</translation>
    </message>
    <message>
        <source>I cannot draw a FITS image in logarithmic scale</source>
        <translation>Je ne peux tracer une image FITS en échelle logarithmique</translation>
    </message>
    <message>
        <source>The FITS image is empty</source>
        <translation>L&apos;image FITS est vide</translation>
    </message>
    <message>
        <source>I could not allocate the FITS image</source>
        <translation>Je n&apos;ai pas pu allouer l&apos;image FITS</translation>
    </message>
    <message>
        <source>No content is available</source>
        <translation>Aucun contenu n&apos;est disponible</translation>
    </message>
    <message>
        <source>Splash screen pixmap &apos;%1/splash.png&apos; does not exist.&lt;br /&gt;Aborting.</source>
        <translation>Le pixmap de l&apos;écran de démarrage &apos;%1/splash.png&apos; n&apos;existe pas.&lt;br /&gt;Annulation.</translation>
    </message>
    <message>
        <source>Bayani, the Free Scientific Environment, version %1</source>
        <translation>Bayani, l&apos;environment scientifique libre, version %1</translation>
    </message>
    <message>
        <source>Set locale and loaded translation...</source>
        <translation>Réglé la locale et chargé la traduction...</translation>
    </message>
    <message>
        <source>Creating the main window...</source>
        <translation>Création de la fenêtre principale...</translation>
    </message>
    <message>
        <source>Bayani %1</source>
        <translation>Bayani %1</translation>
    </message>
    <message>
        <source>Setting the graphical layout...</source>
        <translation>Réglage de la disposition graphique...</translation>
    </message>
    <message>
        <source>Showing the main window and entering the global loop...</source>
        <translation>Affichage de la fenêtre principale et entrée dans la boucle globale...</translation>
    </message>
</context>
<context>
    <name>QTabDialogComputationCompute</name>
    <message>
        <source>Computation - Compute</source>
        <translation>Calcul - Calculer</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>Ai&amp;de</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Expression</source>
        <translation>Expression</translation>
    </message>
    <message>
        <source>&amp;Expression</source>
        <translation>&amp;Expression</translation>
    </message>
    <message>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <source>Si&amp;mplify</source>
        <translation>Si&amp;mplifier</translation>
    </message>
    <message>
        <source>E&amp;valuate</source>
        <translation>E&amp;valuer</translation>
    </message>
    <message>
        <source>&amp;Derive</source>
        <translation>Dé&amp;river</translation>
    </message>
    <message>
        <source>Output</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <source>Varia&amp;bles...</source>
        <translation>Varia&amp;bles...</translation>
    </message>
</context>
<context>
    <name>QTabDialogComputationVariables</name>
    <message>
        <source>Computation - Variables</source>
        <translation>Calcul - Variables</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>Ai&amp;de</translation>
    </message>
    <message>
        <source>&amp;Variables</source>
        <translation>&amp;Variables</translation>
    </message>
    <message>
        <source>Variables List</source>
        <translation>Liste des variables</translation>
    </message>
    <message>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <source>&amp;Name</source>
        <translation>&amp;Nom</translation>
    </message>
    <message>
        <source>Va&amp;lue</source>
        <translation>Va&amp;leur</translation>
    </message>
    <message>
        <source>A&amp;dd to list</source>
        <translation>Ajou&amp;ter à la liste</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>&amp;Remove selection</source>
        <translation>&amp;Supprimer la sélection</translation>
    </message>
    <message>
        <source>Are you sure you want to remove the selected items?</source>
        <translation>Etes vous sûr de vouloir supprimer les éléments sélectionnés ?</translation>
    </message>
</context>
<context>
    <name>QTabDialogFrameSetActivePlot</name>
    <message>
        <source>Graph</source>
        <translation>Graphe</translation>
    </message>
    <message>
        <source>&amp;Zone</source>
        <translation>&amp;Zone</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Frame - Set Active Graph</source>
        <translation>Cadre - Définir le graphe actif</translation>
    </message>
</context>
<context>
    <name>QTabDialogFrameZone</name>
    <message>
        <source>Main Zones</source>
        <translation>Zones principales</translation>
    </message>
    <message>
        <source>Secondary Zones</source>
        <translation>Zones secondaires</translation>
    </message>
    <message>
        <source>&amp;Horizontally</source>
        <translation>&amp;Horizontalement</translation>
    </message>
    <message>
        <source>&amp;Vertically</source>
        <translation>&amp;Verticalement</translation>
    </message>
    <message>
        <source>&amp;Split the already existing main zone</source>
        <translation>&amp;Fractionner la zone principale déjà existante</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Frame - Define Zones</source>
        <translation>Cadre - Définir des zones</translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphAxes</name>
    <message>
        <source>Logarithmic Scale</source>
        <translation>Echelle logarithmique</translation>
    </message>
    <message>
        <source>Reverse Direction</source>
        <translation>Inverser la direction</translation>
    </message>
    <message>
        <source>&amp;X axis</source>
        <translation>Axe des &amp;x</translation>
    </message>
    <message>
        <source>&amp;Y axis</source>
        <translation>Axe des &amp;y</translation>
    </message>
    <message>
        <source>X ax&amp;is</source>
        <translation>Ax&amp;e des x</translation>
    </message>
    <message>
        <source>Y axi&amp;s</source>
        <translation>Axe des &amp;y</translation>
    </message>
    <message>
        <source>Also apply to &amp;new graphs</source>
        <translation>Appliquer aussi aux nou&amp;veaux graphes</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Graph - Set Axes</source>
        <translation>Graphe - Régler les axes</translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphBounds</name>
    <message>
        <source>X Axis</source>
        <translation>Axe des x</translation>
    </message>
    <message>
        <source>Y Axis</source>
        <translation>Axe des y</translation>
    </message>
    <message>
        <source>Fix the &amp;inferior bound</source>
        <translation>Fixer la limite &amp;inférieure</translation>
    </message>
    <message>
        <source>Fix the &amp;superior bound</source>
        <translation>Fixer la limite &amp;supérieure</translation>
    </message>
    <message>
        <source>Fix the in&amp;ferior bound</source>
        <translation>Fixer la limite in&amp;férieure</translation>
    </message>
    <message>
        <source>Fix the su&amp;perior bound</source>
        <translation>Fixer la limite s&amp;upérieure</translation>
    </message>
    <message>
        <source>Also apply to &amp;new graphs</source>
        <translation>Appliquer aussi aux nou&amp;veaux graphes</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Graph - Set Bounds</source>
        <translation>Graphe - Régler les limites</translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphFITS</name>
    <message>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>&amp;File name</source>
        <translation>Nom de &amp;fichier</translation>
    </message>
    <message>
        <source>FITS images (*.fits)</source>
        <translation>Images FITS (*.fits)</translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation>S&amp;uperposer sur le graphe actif</translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation>&amp;Réglages graphiques...</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Graph - FITS Image</source>
        <translation>Graphe - Image FITS</translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphFit</name>
    <message>
        <source>General</source>
        <translation>Géneral</translation>
    </message>
    <message>
        <source>Graphical</source>
        <translation>Graphisme</translation>
    </message>
    <message>
        <source>Output</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <source>Nu&amp;merical fit (3-degree polynomial only)</source>
        <translation>Ajus&amp;tement numérique (polynôme de degré 3 seulement)</translation>
    </message>
    <message>
        <source>&amp;Represent the result graphically</source>
        <translation>R&amp;eprésenter le résultat graphiquement</translation>
    </message>
    <message>
        <source>Represent even if the fit did not con&amp;verge</source>
        <translation>Représenter même si l&apos;ajustement n&apos;a pas con&amp;vergé</translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation>S&amp;uperposer sur le graphe actif</translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation>&amp;Réglages graphiques...</translation>
    </message>
    <message>
        <source>Polynomial</source>
        <translation>Polynôme</translation>
    </message>
    <message>
        <source>&amp;Degree</source>
        <translation>&amp;Degré</translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Initial value of p&lt;sub&gt;0&lt;/sub&gt;</source>
        <translation>Valeur initiale de p&lt;sub&gt;0&lt;/sub&gt;</translation>
    </message>
    <message>
        <source>Initial value of p&lt;sub&gt;1&lt;/sub&gt;</source>
        <translation>Valeur initiale de p&lt;sub&gt;1&lt;/sub&gt;</translation>
    </message>
    <message>
        <source>Initial value of p&lt;sub&gt;2&lt;/sub&gt;</source>
        <translation>Valeur initiale de p&lt;sub&gt;2&lt;/sub&gt;</translation>
    </message>
    <message>
        <source>Initial value of p&lt;sub&gt;3&lt;/sub&gt;</source>
        <translation>Valeur initiale de p&lt;sub&gt;3&lt;/sub&gt;</translation>
    </message>
    <message>
        <source>Maximal iterations number</source>
        <translation>Nombre d&apos;itérations maximal</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;&amp;chi;&lt;sup&gt;2&lt;/sup&gt; convergence criterion&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;Critère de convergence du &amp;chi;&lt;sup&gt;2&lt;/sup&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Graph - Fit Data</source>
        <translation>Graphe - Ajuster des données</translation>
    </message>
    <message>
        <source>O&amp;ptions</source>
        <translation>O&amp;ptions</translation>
    </message>
    <message>
        <source>Ana&amp;lytical</source>
        <translation>Ana&amp;lytique</translation>
    </message>
    <message>
        <source>&amp;Numerical</source>
        <translation>Nu&amp;mérique</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphFunction</name>
    <message>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>&amp;Expression</source>
        <translation>&amp;Expression</translation>
    </message>
    <message>
        <source>&amp;Inferior value</source>
        <translation>Valeur &amp;inférieure</translation>
    </message>
    <message>
        <source>Su&amp;perior value</source>
        <translation>Valeur &amp;supérieure</translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation>S&amp;uperposer sur le graphe actif</translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation>&amp;Réglages graphiques...</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Graph - Function</source>
        <translation>Graphe - Fonction</translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphGraph2D</name>
    <message>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <source>Use e&amp;rrors</source>
        <translation>U&amp;tiliser les erreurs</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>X Axis</source>
        <translation>Axe des x</translation>
    </message>
    <message>
        <source>Y Axis</source>
        <translation>Axe des y</translation>
    </message>
    <message>
        <source>Use a table&apos;s co&amp;lumn</source>
        <translation>Utiliser une co&amp;lonne du tableau</translation>
    </message>
    <message>
        <source>Use an e&amp;xpression</source>
        <translation>Utiliser une e&amp;xpression</translation>
    </message>
    <message>
        <source>Column</source>
        <translation>Colonne</translation>
    </message>
    <message>
        <source>Expression</source>
        <translation>Expression</translation>
    </message>
    <message>
        <source>Use a table&apos;s colu&amp;mn</source>
        <translation>Utiliser une colonne du ta&amp;bleau</translation>
    </message>
    <message>
        <source>Use an ex&amp;pression</source>
        <translation>Utiliser une ex&amp;pression</translation>
    </message>
    <message>
        <source>Use a c&amp;ut to select rows</source>
        <translation>Utiliser une coupure pour sélectionner les ligne&amp;s</translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation>S&amp;uperposer sur le graphe actif</translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation>&amp;Réglages graphiques...</translation>
    </message>
    <message>
        <source>Errors (superior values)</source>
        <translation>Erreurs (valeurs supérieures)</translation>
    </message>
    <message>
        <source>&amp;Use errors</source>
        <translation>U&amp;tiliser les erreurs</translation>
    </message>
    <message>
        <source>U&amp;se errors</source>
        <translation>U&amp;tiliser les erreurs</translation>
    </message>
    <message>
        <source>Use a ta&amp;ble&apos;s column</source>
        <translation>Utiliser une &amp;colonne du tableau</translation>
    </message>
    <message>
        <source>Use a&amp;n expression</source>
        <translation>Utiliser une expre&amp;ssion</translation>
    </message>
    <message>
        <source>Use a &amp;table&apos;s column</source>
        <translation>Utilise&amp;r une colonne du tableau</translation>
    </message>
    <message>
        <source>Use an express&amp;ion</source>
        <translation>Utiliser une express&amp;ion</translation>
    </message>
    <message>
        <source>Use err&amp;ors</source>
        <translation>U&amp;tiliser les erreurs</translation>
    </message>
    <message>
        <source>Use as&amp;ymmetric errors</source>
        <translation>Utiliser des erreurs as&amp;ymétriques</translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation>&amp;Données</translation>
    </message>
    <message>
        <source>&amp;Errors</source>
        <translation>&amp;Erreurs</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Graph - 2D Graph</source>
        <translation>Graphe - Graphe 2D</translation>
    </message>
    <message>
        <source>Errors (inferior values)</source>
        <translation>Erreurs (valeurs inférieures)</translation>
    </message>
    <message>
        <source>Errors</source>
        <translation>Erreurs</translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphHistogram</name>
    <message>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <source>Use a table&apos;s co&amp;lumn</source>
        <translation>Utiliser une co&amp;lonne du tableau</translation>
    </message>
    <message>
        <source>Use an e&amp;xpression</source>
        <translation>Utiliser une e&amp;xpression</translation>
    </message>
    <message>
        <source>Column</source>
        <translation>Colonne</translation>
    </message>
    <message>
        <source>Expression</source>
        <translation>Expression</translation>
    </message>
    <message>
        <source>Fix the number of &amp;bins</source>
        <translation>Fixer le nombre de &amp;bins</translation>
    </message>
    <message>
        <source>Fix the mi&amp;nimal value</source>
        <translation>Fixer la valeur m&amp;inimale</translation>
    </message>
    <message>
        <source>Fix the maxi&amp;mal value</source>
        <translation>Fixer la valeur maxi&amp;male</translation>
    </message>
    <message>
        <source>Use a c&amp;ut to select rows</source>
        <translation>Utiliser une &amp;coupure pour sélectionner les lignes</translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation>S&amp;uperposer sur le graphe actif</translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation>&amp;Réglages graphiques...</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Graph - Histogram</source>
        <translation>Graphe - Histogramme</translation>
    </message>
</context>
<context>
    <name>QTabDialogHelpAbout</name>
    <message>
        <source>&amp;About</source>
        <translation>&amp;A propos</translation>
    </message>
    <message>
        <source>A&amp;uthors</source>
        <translation>A&amp;uteurs</translation>
    </message>
    <message>
        <source>&amp;Thanks</source>
        <translation>&amp;Remerciements</translation>
    </message>
    <message>
        <source>&amp;License</source>
        <translation>&amp;Licence</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Help - About Bayani (Using QT %1)</source>
        <translation>Aide - A propos de Bayani (Utilisant QT %1)</translation>
    </message>
</context>
<context>
    <name>QTabDialogSettingsData</name>
    <message>
        <source>Settings - Data</source>
        <translation>Réglages - Données</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation>&amp;Défaut</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>&amp;Function</source>
        <translation>&amp;Fonction</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <source>Line &amp;width</source>
        <translation>&amp;Largeur de ligne</translation>
    </message>
    <message>
        <source>Line &amp;style</source>
        <translation>&amp;Style de ligne</translation>
    </message>
    <message>
        <source>Line colo&amp;r</source>
        <translation>&amp;Couleur de ligne</translation>
    </message>
    <message>
        <source>&amp;Histogram</source>
        <translation>&amp;Histogramme</translation>
    </message>
    <message>
        <source>Outline</source>
        <translation>Périmètre</translation>
    </message>
    <message>
        <source>&amp;Width</source>
        <translation>&amp;Largeur</translation>
    </message>
    <message>
        <source>&amp;Style</source>
        <translation>&amp;Style</translation>
    </message>
    <message>
        <source>Colo&amp;r</source>
        <translation>&amp;Couleur</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Remplissage</translation>
    </message>
    <message>
        <source>St&amp;yle</source>
        <translation>S&amp;tyle</translation>
    </message>
    <message>
        <source>2D &amp;Graph</source>
        <translation>&amp;Graphe 2D</translation>
    </message>
    <message>
        <source>Data Points</source>
        <translation>Points de données</translation>
    </message>
    <message>
        <source>Relative si&amp;ze</source>
        <translation>&amp;Taille relative</translation>
    </message>
    <message>
        <source>&amp;Marker</source>
        <translation>&amp;Marqueur</translation>
    </message>
    <message>
        <source>Hollow circle</source>
        <translation>Cercle vide</translation>
    </message>
    <message>
        <source>Filled circle</source>
        <translation>Cercle plein</translation>
    </message>
    <message>
        <source>Hollow square</source>
        <translation>Carré vide</translation>
    </message>
    <message>
        <source>Filled square</source>
        <translation>Carré plein</translation>
    </message>
    <message>
        <source>Error Bars</source>
        <translation>Barres d&apos;erreurs</translation>
    </message>
    <message>
        <source>FITS &amp;Image</source>
        <translation>&amp;Image FITS</translation>
    </message>
    <message>
        <source>&amp;Palette</source>
        <translation>&amp;Palette</translation>
    </message>
    <message>
        <source>Monochrome</source>
        <translation>Monochrome</translation>
    </message>
    <message>
        <source>Four colors</source>
        <translation>Quatre couleurs</translation>
    </message>
    <message>
        <source>Six colors</source>
        <translation>Six couleurs</translation>
    </message>
    <message>
        <source>Dynamics</source>
        <translation>Dynamique</translation>
    </message>
    <message>
        <source>Set minim&amp;um</source>
        <translation>Régler le minim&amp;um</translation>
    </message>
    <message>
        <source>Set ma&amp;ximum</source>
        <translation>Régler le ma&amp;ximum</translation>
    </message>
    <message>
        <source>Co&amp;lor</source>
        <translation>Co&amp;uleur</translation>
    </message>
    <message>
        <source>Line co&amp;lor</source>
        <translation>Co&amp;uleur de ligne</translation>
    </message>
</context>
<context>
    <name>QTabDialogSettingsFrame</name>
    <message>
        <source>Settings - Frame</source>
        <translation>Réglages - Cadre</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation>&amp;Défaut</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Di&amp;mensions</source>
        <translation>Di&amp;mensions</translation>
    </message>
    <message>
        <source>Relative Origin&apos;s Coordinates</source>
        <translation>Coordonnées relatives de l&apos;origine</translation>
    </message>
    <message>
        <source>A&amp;bcissa</source>
        <translation>A&amp;bcisse</translation>
    </message>
    <message>
        <source>O&amp;rdinate</source>
        <translation>O&amp;rdonnée</translation>
    </message>
    <message>
        <source>Relative Width and Height</source>
        <translation>Largeur et hauteur relatives</translation>
    </message>
    <message>
        <source>&amp;Width</source>
        <translation>Larg&amp;eur</translation>
    </message>
    <message>
        <source>&amp;Height</source>
        <translation>&amp;Hauteur</translation>
    </message>
    <message>
        <source>Relative Distances</source>
        <translation>Distances relatives</translation>
    </message>
    <message>
        <source>Between bo&amp;x and title</source>
        <translation>Entre la boi&amp;te et le titre</translation>
    </message>
    <message>
        <source>&amp;Lines</source>
        <translation>&amp;Lignes</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <source>&amp;Box</source>
        <translation>&amp;Boite</translation>
    </message>
    <message>
        <source>&amp;Horizontal separators</source>
        <translation>Séparateurs &amp;horizontaux</translation>
    </message>
    <message>
        <source>&amp;Vertical separators</source>
        <translation>Séparateurs &amp;verticaux</translation>
    </message>
    <message>
        <source>&amp;Texts</source>
        <translation>Te&amp;xtes</translation>
    </message>
    <message>
        <source>Fonts</source>
        <translation>Fontes</translation>
    </message>
    <message>
        <source>Titl&amp;e</source>
        <translation>Tit&amp;re</translation>
    </message>
    <message>
        <source>Miscella&amp;neous</source>
        <translation>Diver&amp;s</translation>
    </message>
    <message>
        <source>Background</source>
        <translation>Arrière plan</translation>
    </message>
    <message>
        <source>Colo&amp;r</source>
        <translation>Coule&amp;ur</translation>
    </message>
</context>
<context>
    <name>QTabDialogSettingsGlobal</name>
    <message>
        <source>Settings - Global</source>
        <translation>Réglages - Global</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation>&amp;Défaut</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>&amp;Terminal</source>
        <translation>&amp;Terminal</translation>
    </message>
    <message>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <source>&amp;History size</source>
        <translation>Taille de l&apos;&amp;historique</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <source>&amp;Font</source>
        <translation>&amp;Fonte</translation>
    </message>
    <message>
        <source>Te&amp;xt color</source>
        <translation>Couleur du te&amp;xte</translation>
    </message>
    <message>
        <source>&amp;Information</source>
        <translation>&amp;Information</translation>
    </message>
    <message>
        <source>&amp;Warning</source>
        <translation>A&amp;vertissement</translation>
    </message>
    <message>
        <source>C&amp;ritical</source>
        <translation>&amp;Critique</translation>
    </message>
    <message>
        <source>&amp;Status</source>
        <translation>&amp;Statut</translation>
    </message>
    <message>
        <source>&amp;Background</source>
        <translation>A&amp;rrière plan</translation>
    </message>
</context>
<context>
    <name>QTabDialogSettingsGraph</name>
    <message>
        <source>Settings - Graph</source>
        <translation>Réglages - Graphe</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation>&amp;Défaut</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Di&amp;mensions</source>
        <translation>Di&amp;mensions</translation>
    </message>
    <message>
        <source>Relative Origin&apos;s Coordinates</source>
        <translation>Coordonnées relatives de l&apos;origine</translation>
    </message>
    <message>
        <source>A&amp;bcissa</source>
        <translation>A&amp;bcisse</translation>
    </message>
    <message>
        <source>O&amp;rdinate</source>
        <translation>O&amp;rdonnée</translation>
    </message>
    <message>
        <source>Relative Width and Height</source>
        <translation>Largeur et hauteur relatives</translation>
    </message>
    <message>
        <source>&amp;Width</source>
        <translation>&amp;Largeur</translation>
    </message>
    <message>
        <source>&amp;Height</source>
        <translation>&amp;Hauteur</translation>
    </message>
    <message>
        <source>Relative Distances</source>
        <translation>Distances relatives</translation>
    </message>
    <message>
        <source>Between axis and titl&amp;e</source>
        <translation>Entre l&apos;axe et le titr&amp;e</translation>
    </message>
    <message>
        <source>Between &amp;x axis and digits</source>
        <translation>Entre l&apos;axe des &amp;x et les chiffres</translation>
    </message>
    <message>
        <source>Between &amp;y axis and digits</source>
        <translation>Entre l&apos;axe des &amp;y et les chiffres</translation>
    </message>
    <message>
        <source>Between x ax&amp;is and label</source>
        <translation>Entre l&apos;ax&amp;e des x et l&apos;étiquette</translation>
    </message>
    <message>
        <source>Indicators&apos; Length</source>
        <translation>Longueur des indicateurs</translation>
    </message>
    <message>
        <source>X axis bi&amp;g indicators</source>
        <translation>&amp;Grands indicateurs de l&apos;axe des x</translation>
    </message>
    <message>
        <source>&amp;Lines</source>
        <translation>&amp;Lignes</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <source>&amp;Box</source>
        <translation>&amp;Boite</translation>
    </message>
    <message>
        <source>&amp;X axis big indicators</source>
        <translation>Grands indicateurs de l&apos;axe des &amp;x</translation>
    </message>
    <message>
        <source>&amp;Y axis big indicators</source>
        <translation>Grands indicateurs de l&apos;axe des &amp;y</translation>
    </message>
    <message>
        <source>X ax&amp;is small indicators</source>
        <translation>&amp;Petits indicateurs de l&apos;axe des x</translation>
    </message>
    <message>
        <source>Y axi&amp;s small indicators</source>
        <translation>P&amp;etits indicateurs de l&apos;axe des y</translation>
    </message>
    <message>
        <source>X axis &amp;grid</source>
        <translation>G&amp;rille de l&apos;axe des x</translation>
    </message>
    <message>
        <source>Y axis g&amp;rid</source>
        <translation>Gri&amp;lle de l&apos;axe des y</translation>
    </message>
    <message>
        <source>&amp;Texts</source>
        <translation>Te&amp;xtes</translation>
    </message>
    <message>
        <source>Fonts</source>
        <translation>Fontes</translation>
    </message>
    <message>
        <source>Titl&amp;e</source>
        <translation>Tit&amp;re</translation>
    </message>
    <message>
        <source>&amp;X axis label</source>
        <translation>L&apos;étiquette de l&apos;axe des &amp;x</translation>
    </message>
    <message>
        <source>&amp;Y axis label</source>
        <translation>L&apos;étiquette de l&apos;axe des &amp;y</translation>
    </message>
    <message>
        <source>X ax&amp;is digits</source>
        <translation>Les &amp;chiffres de l&apos;axe des x</translation>
    </message>
    <message>
        <source>Y axi&amp;s digits</source>
        <translation>Les c&amp;hiffres de l&apos;axe des y</translation>
    </message>
    <message>
        <source>Miscella&amp;neous</source>
        <translation>Diver&amp;s</translation>
    </message>
    <message>
        <source>Background</source>
        <translation>Arrière plan</translation>
    </message>
    <message>
        <source>Colo&amp;r</source>
        <translation>Couleu&amp;r</translation>
    </message>
    <message>
        <source>Between y ax&amp;is and label</source>
        <translation>Entre l&apos;ax&amp;e des y et l&apos;étiquette</translation>
    </message>
    <message>
        <source>Y axis bi&amp;g indicators</source>
        <translation>&amp;Grands indicateurs de l&apos;axe des y</translation>
    </message>
    <message>
        <source>X axis &amp;small indicators</source>
        <translation>&amp;Petits indicateurs de l&apos;axe des x</translation>
    </message>
    <message>
        <source>Y axis &amp;small indicators</source>
        <translation>&amp;Petits indicateurs de l&apos;axe des y</translation>
    </message>
</context>
<context>
    <name>QTabDialogTableEmpty</name>
    <message>
        <source>Cell 1 (included)</source>
        <translation>Cellule 1 (incluse)</translation>
    </message>
    <message>
        <source>Cell 2 (included)</source>
        <translation>Cellule 2 (incluse)</translation>
    </message>
    <message>
        <source>&amp;Row</source>
        <translation>&amp;Ligne</translation>
    </message>
    <message>
        <source>Co&amp;lumn</source>
        <translation>&amp;Colonne</translation>
    </message>
    <message>
        <source>Ro&amp;w</source>
        <translation>L&amp;igne</translation>
    </message>
    <message>
        <source>Colu&amp;mn</source>
        <translation>Colonn&amp;e</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Table - Empty Cells</source>
        <translation>Tableau - Vider des cellules</translation>
    </message>
</context>
<context>
    <name>QTabDialogTableFillFromFile</name>
    <message>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>&amp;File name</source>
        <translation>Nom de &amp;fichier</translation>
    </message>
    <message>
        <source>Text files (*.txt)</source>
        <translation>Fichiers texte (*.txt)</translation>
    </message>
    <message>
        <source>Columns &amp;number in file</source>
        <translation>Nom&amp;bre de colonnes dans le fichier</translation>
    </message>
    <message>
        <source>Columns &amp;separator</source>
        <translation>&amp;Séparateur de colonnes</translation>
    </message>
    <message>
        <source>Starting &amp;row in table (included)</source>
        <translation>&amp;Ligne de début dans le tableau (incluse)</translation>
    </message>
    <message>
        <source>Starting co&amp;lumn in table (included)</source>
        <translation>&amp;Colonne de début dans le tableau (incluse)</translation>
    </message>
    <message>
        <source>Add ro&amp;ws to table if necessary</source>
        <translation>Ajouter des l&amp;ignes au tableau si nécessaire</translation>
    </message>
    <message>
        <source>Add colu&amp;mns to table if necessary</source>
        <translation>Ajouter des colonn&amp;es au tableau si nécessaire</translation>
    </message>
    <message>
        <source>&amp;Empty other cells</source>
        <translation>&amp;Vider les autres cellules</translation>
    </message>
    <message>
        <source>Acce&amp;pt lines from file with more than the set columns number</source>
        <translation>Accepte&amp;r des lignes du fichier avec plus de colonnes que le nombre préréglé</translation>
    </message>
    <message>
        <source>Accep&amp;t lines from file with less than the set columns number. Fill the remaining cells with:</source>
        <translation>Accep&amp;ter des lignes du fichier avec moins de colonnes que le nombre préréglé. Remplir les cellules restantes avec:</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Table - Fill from File</source>
        <translation>Tableau - Remplir à partir d&apos;un fichier</translation>
    </message>
</context>
<context>
    <name>QTabDialogTableManage</name>
    <message>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>&amp;Insert</source>
        <translation>&amp;Insérer</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <source>Ro&amp;ws</source>
        <translation>&amp;Lignes</translation>
    </message>
    <message>
        <source>Co&amp;lumns</source>
        <translation>&amp;Colonnes</translation>
    </message>
    <message>
        <source>&amp;Elements number</source>
        <translation>Nom&amp;bre d&apos;éléments</translation>
    </message>
    <message>
        <source>&amp;Starting element (included)</source>
        <translation>Elément de &amp;début (inclus)</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Table - Manage Size</source>
        <translation>Tableau - Gérer la taille</translation>
    </message>
</context>
<context>
    <name>QTabDialogTableSwap</name>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Elements</source>
        <translation>Eléments</translation>
    </message>
    <message>
        <source>&amp;Rows</source>
        <translation>&amp;Lignes</translation>
    </message>
    <message>
        <source>Co&amp;lumns</source>
        <translation>&amp;Colonnes</translation>
    </message>
    <message>
        <source>&amp;First</source>
        <translation>Pre&amp;mier</translation>
    </message>
    <message>
        <source>&amp;Second</source>
        <translation>&amp;Deuxième</translation>
    </message>
    <message>
        <source>Co&amp;py contents of element 1 to element 2</source>
        <translation>Copie&amp;r le contenu de l&apos;élément 1 vers l&apos;élément 2</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Table - Swap Rows/Columns</source>
        <translation>Tableau - Echanger les lignes/colonnes</translation>
    </message>
</context>
<context>
    <name>QTabDialogTableWriteToFile</name>
    <message>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>&amp;File name</source>
        <translation>Nom de &amp;fichier</translation>
    </message>
    <message>
        <source>Text files (*.txt)</source>
        <translation>Fichiers texte (*.txt)</translation>
    </message>
    <message>
        <source>Cell 1 (included)</source>
        <translation>Cellule 1 (incluse)</translation>
    </message>
    <message>
        <source>Cell 2 (included)</source>
        <translation>Cellule 2 (incluse)</translation>
    </message>
    <message>
        <source>&amp;Row</source>
        <translation>&amp;Ligne</translation>
    </message>
    <message>
        <source>Co&amp;lumn</source>
        <translation>&amp;Colonne</translation>
    </message>
    <message>
        <source>Ro&amp;w</source>
        <translation>L&amp;igne</translation>
    </message>
    <message>
        <source>Colu&amp;mn</source>
        <translation>Colonn&amp;e</translation>
    </message>
    <message>
        <source>Columns &amp;separator</source>
        <translation>&amp;Séparateur de colonnes</translation>
    </message>
    <message>
        <source>A&amp;ppend existing file</source>
        <translation>Ajou&amp;ter à un fichier existant</translation>
    </message>
    <message>
        <source></source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Table - Write to File</source>
        <translation>Tableau - Ecrire dans un fichier</translation>
    </message>
</context>
<context>
    <name>QTabDialogTextsFrame</name>
    <message>
        <source>Texts</source>
        <translation>Textes</translation>
    </message>
    <message>
        <source>&amp;Title</source>
        <translation>&amp;Titre</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Texts - Frame</source>
        <translation>Textes - Cadre</translation>
    </message>
</context>
<context>
    <name>QTabDialogTextsGraph</name>
    <message>
        <source>Texts</source>
        <translation>Textes</translation>
    </message>
    <message>
        <source>&amp;Title</source>
        <translation>&amp;Titre</translation>
    </message>
    <message>
        <source>&amp;X axis title</source>
        <translation>Tit&amp;re de l&apos;axe des x</translation>
    </message>
    <message>
        <source>&amp;Y axis title</source>
        <translation>Titr&amp;e de l&apos;axe des y</translation>
    </message>
    <message>
        <source>Display Digits</source>
        <translation>Afficher les chiffres</translation>
    </message>
    <message>
        <source>X ax&amp;is</source>
        <translation>Axe des &amp;x</translation>
    </message>
    <message>
        <source>Y axi&amp;s</source>
        <translation>Axe des &amp;y</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Texts - Graph</source>
        <translation>Textes - Graphe</translation>
    </message>
</context>
<context>
    <name>QTextEditBayani</name>
    <message>
        <source>Bayani&gt; </source>
        <translation>Bayani&gt; </translation>
    </message>
    <message>
        <source>[Y/N]</source>
        <translation>[O/N]</translation>
    </message>
</context>
</TS>
