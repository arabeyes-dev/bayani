<!DOCTYPE TS><TS>
<context>
    <name>BHelpBrowser</name>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Go</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h1&gt;Documentation index not found&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help - Bayani Handbook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ready.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loaded page.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BTextBrowser</name>
    <message>
        <source>Document %1 not found in directory %2.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Frame</name>
    <message>
        <source>The frame has been modified.&lt;br /&gt;Are you sure you want to close it without saving it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modified frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close canceled: the frame has been modified.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GUI</name>
    <message>
        <source>&amp;Quit</source>
        <translation>أ&amp;خرج</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;جديد</translation>
    </message>
    <message>
        <source>Define &amp;Zones...</source>
        <translation>حدّد ال&amp;مناطق...</translation>
    </message>
    <message>
        <source>Set Active &amp;Graph...</source>
        <translation>حدّد الم&amp;نحنى النّشط...</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>إ&amp;حفظ</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation>إحفظ &amp;تحت...</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>أ&amp;غلق</translation>
    </message>
    <message>
        <source>&amp;Fill from File...</source>
        <translation>إ&amp;ملأ من الملفّ...</translation>
    </message>
    <message>
        <source>&amp;Write to File...</source>
        <translation>أ&amp;كتب إلى الملفّ...</translation>
    </message>
    <message>
        <source>&amp;Swap Rows/Columns...</source>
        <translation>&amp;بدّل الصّفوف\الأعمدة...</translation>
    </message>
    <message>
        <source>&amp;Transpose Table</source>
        <translation>أ&amp;نقل الجدول</translation>
    </message>
    <message>
        <source>&amp;Manage Size...</source>
        <translation>أ&amp;در الحجم...</translation>
    </message>
    <message>
        <source>&amp;Empty Cells...</source>
        <translation>أ&amp;فرغ خلايا...</translation>
    </message>
    <message>
        <source>&amp;Function...</source>
        <translation>&amp;دالّة...</translation>
    </message>
    <message>
        <source>&amp;Histogram...</source>
        <translation>م&amp;درّج تكراري...</translation>
    </message>
    <message>
        <source>2D &amp;Graph...</source>
        <translation>&amp;منحنى ثنائي الأبعاد...</translation>
    </message>
    <message>
        <source>FITS &amp;Image...</source>
        <translation>&amp;صورة FITS...</translation>
    </message>
    <message>
        <source>Set &amp;Bounds...</source>
        <translation>حدّد ال&amp;حدود...</translation>
    </message>
    <message>
        <source>Set &amp;Axes...</source>
        <translation>حدّد ال&amp;محاور...</translation>
    </message>
    <message>
        <source>Fit &amp;Data...</source>
        <translation>وا&amp;فق بيانات...</translation>
    </message>
    <message>
        <source>&amp;Variables...</source>
        <translation>&amp;متغيّرات...</translation>
    </message>
    <message>
        <source>&amp;Compute...</source>
        <translation>أ&amp;حسب...</translation>
    </message>
    <message>
        <source>&amp;Frame...</source>
        <translation>إ&amp;طار...</translation>
    </message>
    <message>
        <source>&amp;Graph...</source>
        <translation>م&amp;نحنى...</translation>
    </message>
    <message>
        <source>&amp;Data...</source>
        <translation>&amp;بيانات...</translation>
    </message>
    <message>
        <source>&amp;Save Current Settings</source>
        <translation>إ&amp;حفظ التّعيينات الحالية</translation>
    </message>
    <message>
        <source>Bayani &amp;Handbook...</source>
        <translation>&amp;كتاب بياني...</translation>
    </message>
    <message>
        <source>What&apos;s &amp;This?</source>
        <translation>ما &amp;هذا ؟</translation>
    </message>
    <message>
        <source>&amp;About Bayani...</source>
        <translation>&amp;حول بياني...</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;ملفّ</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>إع&amp;رض</translation>
    </message>
    <message>
        <source>Fra&amp;me</source>
        <translation>إ&amp;طار</translation>
    </message>
    <message>
        <source>&amp;Table</source>
        <translation>&amp;جدول</translation>
    </message>
    <message>
        <source>&amp;Graph</source>
        <translation>&amp;منحنى</translation>
    </message>
    <message>
        <source>&amp;Computation</source>
        <translation>&amp;حساب</translation>
    </message>
    <message>
        <source>Te&amp;xts</source>
        <translation>&amp;نصوص</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>ت&amp;عيينات</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>م&amp;ساعدة</translation>
    </message>
    <message>
        <source>&amp;Show/Hide</source>
        <translation>أ&amp;ظهر\إخفي</translation>
    </message>
    <message>
        <source>Ter&amp;minal</source>
        <translation>&amp;طرفية</translation>
    </message>
    <message>
        <source>&amp;Cascade Windows</source>
        <translation>&amp;سلسل النّوافذ</translation>
    </message>
    <message>
        <source>&amp;Tile Windows</source>
        <translation>&amp;رتّب النّوافذ</translation>
    </message>
    <message>
        <source>Toolbar</source>
        <translation>لوحة الأدوات</translation>
    </message>
    <message>
        <source>Table</source>
        <translation>جدول</translation>
    </message>
    <message>
        <source>Terminal</source>
        <translation>طرفية</translation>
    </message>
    <message>
        <source>Ready.</source>
        <translation>مستعدّ.</translation>
    </message>
    <message>
        <source>%1 could not be found.&lt;br /&gt;Online help will not be available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Information: %1</source>
        <translation>معلومة: %1</translation>
    </message>
    <message>
        <source>Information:</source>
        <translation>معلومة:</translation>
    </message>
    <message>
        <source>Warning: %1</source>
        <translation>تحذير: %1</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation>تحذير:</translation>
    </message>
    <message>
        <source>Error: %1</source>
        <translation>خطأ: %1</translation>
    </message>
    <message>
        <source>Error:</source>
        <translation>خطأ:</translation>
    </message>
    <message>
        <source>x</source>
        <comment>Variable</comment>
        <translation>س</translation>
    </message>
    <message>
        <source>c1</source>
        <comment>Parameter</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>c1 &gt; 0</source>
        <comment>Cut</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>c2</source>
        <comment>Parameter</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>c3</source>
        <comment>Parameter</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>c4</source>
        <comment>Parameter</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The x axis inferior value is greater than or equal to the superior one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The y axis inferior value is greater than or equal to the superior one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The table has been modified.&lt;br /&gt;Are you sure you want to quit without saving it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quit canceled: the table has been modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There are modified frames.&lt;br /&gt;Are you sure you want to quit without saving them?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modified frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quit canceled: there are modified frames.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Information: New directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created directory %1 in order to store the commands history.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error: File error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A problem occured while attempting to open file %1 in write-only mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bye!</source>
        <translation>إلى اللّقاء !</translation>
    </message>
    <message>
        <source>Table hidden.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table shown.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Terminal hidden.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Terminal shown.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frame %1</source>
        <translation>إطار %1</translation>
    </message>
    <message>
        <source>New frame, number %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There&apos;s no available frame.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the number of horizontal zones.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the number of vertical zones.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 zones are now available in the frame.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The number of existing graphs is less than or equal to 1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph number %1 is already active.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph number %1 is now active.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PNG pictures (*.png);;All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File %1 already exists.&lt;br /&gt;Are you sure you want to overwrite it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save canceled: file already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A problem occured while attempting to save frame in %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saved frame in %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frame closed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the file name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the columns number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the columns separator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the default fill.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will remove all the current content from the selected cells.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill table?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will remove all the current content from the other cells.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Empty other cells?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Read %1 lines from %2. Considered %3 lines.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A problem occured while attempting to open file %1 in read-only mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Write canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Written %1 lines in %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A problem occured while attempting to open file %1 in write mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elements are equal. There&apos;s nothing to do.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will remove all the current content from the destination element.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy content?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updated table.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updated table: %1 rows, %2 columns.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the elements number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You are attempting to remove inexisting elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will remove all the selected elements from the table.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove elements?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will delete all the data from the selected cells.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Empty cells?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Empty canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Emptied %1 cells between [%2,%3] and [%4,%5].</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the expression.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the inferior value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the superior value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The inferior value is greater than or equal to the superior one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error in expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Represented %1 between %2 and %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the number of bins.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the minimal value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the maximal value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The minimal value is greater than or equal to the maximal one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>c%1</source>
        <comment>Parameter</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error in cut: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is nothing to represent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 entries in histogram.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 entries in graph.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A problem occured while attempting to represent file %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Represented %1 (%2*%3 square pixels).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There&apos;s no available graph.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updated the graph&apos;s bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updated the graph&apos;s axes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The active graph is not fittable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p&lt;sub&gt;0&lt;/sub&gt; = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p&lt;sub&gt;0&lt;/sub&gt; = %1&lt;br /&gt;p&lt;sub&gt;1&lt;/sub&gt; = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p&lt;sub&gt;0&lt;/sub&gt; = %1&lt;br /&gt;p&lt;sub&gt;1&lt;/sub&gt; = %2&lt;br /&gt;p&lt;sub&gt;2&lt;/sub&gt; = %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the initial value of p&lt;sub&gt;0&lt;/sub&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the initial value of p&lt;sub&gt;1&lt;/sub&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the initial value of p&lt;sub&gt;2&lt;/sub&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the initial value of p&lt;sub&gt;3&lt;/sub&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the maximal iterations number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the &amp;chi;&lt;sup&gt;2&lt;/sup&gt; convergence criterion.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p&lt;sub&gt;0&lt;/sub&gt; = (%1)&amp;plusmn;(%2)&lt;br /&gt;p&lt;sub&gt;1&lt;/sub&gt; = (%3)&amp;plusmn;(%4)&lt;br /&gt;p&lt;sub&gt;2&lt;/sub&gt; = (%5)&amp;plusmn;(%6)&lt;br /&gt;p&lt;sub&gt;3&lt;/sub&gt; = (%7)&amp;plusmn;(%8)&lt;br /&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;chi;&lt;sup&gt;2&lt;/sup&gt; = %1&lt;br /&gt;Iteration = %2&lt;br /&gt;Number of degrees of freedom = %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The number of degrees of freedom (%1) is negative.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no fitting solution.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;br /&gt;The fit did not converge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1</source>
        <comment>Fit expression</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1*x+(%2)</source>
        <comment>Fit expression</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>(%1)*x^2+(%2)*x+(%3)</source>
        <comment>Fit expression</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>(%1)*x^3+(%2)*x^2+(%3)*x+(%4)</source>
        <comment>Fit expression</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fit result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph closed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 defined variables.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expression simplification: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error in expression: &lt;font color=%1&gt;%2&lt;/font&gt;&lt;br /&gt;Intermediate result: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expression evaluation: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expression derivation: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updated the frame&apos;s texts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updated the graph&apos;s texts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Applied the new global settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will unset the current global settings and set the default ones as current.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Revert to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to the default global settings canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to the default global settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Applied the new frame settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will unset the current frame settings and set the default ones as current.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to the default frame settings canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to the default frame settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Applied the new graph settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will unset the current graph settings and set the default ones as current.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to the default graph settings canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to the default graph settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Applied the new data settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will unset the current data settings and set the default ones as current.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to the default data settings canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to the default data settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This will overwrite the existing default settings and set the current ones as default.&lt;br /&gt;Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overwrite?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save current settings canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created directory %1 in order to store the default settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saved current settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Usage: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>quit</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Syntax error</source>
        <translation>خطأ نحوي</translation>
    </message>
    <message>
        <source>table</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Display flag&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>terminal</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cascade</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>tile</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>frame</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>zone</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Horizontally&amp;gt; &amp;lt;Vertically&amp;gt; [Secondary zone]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>activegraph</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Zone&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>saveframe</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 [Frame&apos;s new filename]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please set a filename for the active frame first.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>closeframe</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fill</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Filename&amp;gt; &amp;lt;Columns number&amp;gt; &amp;lt;Starting row&amp;gt; &amp;lt;Starting column&amp;gt; [Add rows flag] [Add columns flag] [Empty cells flag] [Superior columns number flag] [Default fill]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>write</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Filename&amp;gt; &amp;lt;Row 1&amp;gt; &amp;lt;Column 1&amp;gt; &amp;lt;Row 2&amp;gt; &amp;lt;Column 2&amp;gt; [Append flag]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>swaprows</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Row 1&amp;gt; &amp;lt;Row 2&amp;gt; [Copy flag]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>swapcolumns</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Column 1&amp;gt; &amp;lt;Column 2&amp;gt; [Copy flag]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>transpose</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>insertrows</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Rows number&amp;gt; &amp;lt;Starting row&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>removerows</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>insertcolumns</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Columns number&amp;gt; &amp;lt;Starting column&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>removecolumns</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>empty</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Row 1&amp;gt; &amp;lt;Column 1&amp;gt; &amp;lt;Row 2&amp;gt; &amp;lt;Column 2&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>function</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Expression&amp;gt; &amp;lt;x&lt;sub&gt;0&lt;/sub&gt;&amp;gt; &amp;lt;x&lt;sub&gt;1&lt;/sub&gt;&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>histogram</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Expression&amp;gt; [Bins number] [Minimum] [Maximum] [Cut]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>graph2d</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;X axis expression&amp;gt; &amp;lt;Y axis expression&amp;gt; [Inferior x errors] [Inferior y errors] [Superior x errors] [Superior y errors] [Cut]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fitsimage</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Filename&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bounds</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 [X inf] [X sup] [Y inf] [Y sup] [New graphs flag]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>axes</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 [X log flag] [Y log flag] [X reverse flag] [Y reverse flag] [New graphs flag]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fit</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Degree&amp;gt; [Representation flag]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fitnum</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;p&lt;sub&gt;0&lt;/sub&gt;&amp;gt; &amp;lt;p&lt;sub&gt;1&lt;/sub&gt;&amp;gt; &amp;lt;p&lt;sub&gt;2&lt;/sub&gt;&amp;gt; &amp;lt;p&lt;sub&gt;3&lt;/sub&gt;&amp;gt; &amp;lt;Iterations number&amp;gt; &amp;lt;&amp;chi;&lt;sup&gt;2&lt;/sup&gt; criterion&amp;gt; [Representation flag] [No convergence flag]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>same</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>closegraph</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>variable</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Name&amp;gt; [Value]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name &apos;%1&apos; is not allowed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>variables</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The variables list is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>remove_variable</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Name&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Variable &apos;%1&apos; does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>remove_variables</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>simplify</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Expression&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>evaluate</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>resolve</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>derive</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>integrate</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>titleframe</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Frame title&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>titlegraph</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Graph title&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>titlex</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;X axis title&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>titley</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Y axis title&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>digitsx</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>digitsy</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>optiondata</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Option&amp;gt; &amp;lt;Value&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>f_width</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>f_style</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>f_color</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_width</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_style</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_color</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_fstyle</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h_fcolor</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_size</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_marker</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_color</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_ewidth</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_estyle</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g_ecolor</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fi_palette</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fi_min</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fi_max</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown option: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>defaults</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Type&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>global</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>frame</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>graph</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>data</source>
        <comment>Command - Option</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown type: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>saveoptions</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>help</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>about</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>execute</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>I could not open file %1 in read-only mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;br /&gt;I could not open file %1 either.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>shell</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Command&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sleep</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 &amp;lt;Seconds&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sleeping %1 second(s)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>print</source>
        <comment>Command</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown command: &apos;%1&apos;</source>
        <translation>أمر مجهول: &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Error in draw: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>x=%1 y=%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>x=%1 y=%2 value=%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Glo&amp;bal...</source>
        <translation>&amp;شاملة...</translation>
    </message>
    <message>
        <source>Please indicate the x axis inferior bound.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the x axis superior bound.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the y axis inferior bound.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please indicate the y axis superior bound.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error in x axis expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error in y axis expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error in x axis (inferior) errors expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error in y axis (inferior) errors expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error in x axis (superior) errors expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error in y axis (superior) errors expression: &lt;font color=%1&gt;%2&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QFileSelector</name>
    <message>
        <source>&amp;Browse...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QFontSelector</name>
    <message>
        <source>Change...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>!</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>^</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>*</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>/</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>+</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>-</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;=</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&gt;</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&gt;=</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>==</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>!=</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;&amp;</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>||</source>
        <comment>Operator</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sqrt</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fabs</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sign</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sin</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>asin</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cos</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>acos</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>tan</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>atan</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>log</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>log10</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>exp</source>
        <comment>Function</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p%1</source>
        <comment>Parameter</comment>
        <translation>ط%1</translation>
    </message>
    <message>
        <source>x</source>
        <comment>Variable</comment>
        <translation>س</translation>
    </message>
    <message>
        <source>.</source>
        <comment>Digits separator (comma)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Empty expression</source>
        <translation>عبارة فارغة</translation>
    </message>
    <message>
        <source>Undefined symbol &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Extra closing bracket (index %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Element &apos;%1&apos; (index %2) not allowed after a closing bracket. Expecting an operator or a closing bracket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Undefined operator &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Undefined variable &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Undefined function &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expecting a closing bracket or an operator after element &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 closing brackets are missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong operands number for operator &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Operand cannot preceed unary operator &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong arguments number for function &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bad operands for operator &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bad argument for function &apos;%1&apos; (index %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>I cannot draw a FITS image in logarithmic scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The FITS image is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>I could not allocate the FITS image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No content is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Splash screen pixmap &apos;%1/splash.png&apos; does not exist.&lt;br /&gt;Aborting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bayani, the Free Scientific Environment, version %1</source>
        <translation>بياني، المحيط العلمي الحرّ، النّسخة %1</translation>
    </message>
    <message>
        <source>Set locale and loaded translation...</source>
        <translation>جرى تعيين الجهة و تحميل التّرجمة...</translation>
    </message>
    <message>
        <source>Creating the main window...</source>
        <translation>يجري إنشاء النّافذة الرّئيسية...</translation>
    </message>
    <message>
        <source>Bayani %1</source>
        <translation>بياني %1</translation>
    </message>
    <message>
        <source>Setting the graphical layout...</source>
        <translation>يجري تعيين التّنسيق التّخطيطي...</translation>
    </message>
    <message>
        <source>Showing the main window and entering the global loop...</source>
        <translation>يجري إظهار النّافذة الرّئيسية و الدّخول في الحلقة الشّاملة...</translation>
    </message>
</context>
<context>
    <name>QTabDialogComputationCompute</name>
    <message>
        <source>Computation - Compute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Si&amp;mplify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>E&amp;valuate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Derive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Varia&amp;bles...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogComputationVariables</name>
    <message>
        <source>Computation - Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Variables List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Va&amp;lue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A&amp;dd to list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Remove selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure you want to remove the selected items?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogFrameSetActivePlot</name>
    <message>
        <source>Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frame - Set Active Graph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogFrameZone</name>
    <message>
        <source>Main Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Secondary Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Split the already existing main zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frame - Define Zones</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphAxes</name>
    <message>
        <source>Logarithmic Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reverse Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;X axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X ax&amp;is</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axi&amp;s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Also apply to &amp;new graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph - Set Axes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphBounds</name>
    <message>
        <source>X Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fix the &amp;inferior bound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fix the &amp;superior bound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fix the in&amp;ferior bound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fix the su&amp;perior bound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Also apply to &amp;new graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph - Set Bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphFITS</name>
    <message>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FITS images (*.fits)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph - FITS Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphFit</name>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graphical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nu&amp;merical fit (3-degree polynomial only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Represent the result graphically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Represent even if the fit did not con&amp;verge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Polynomial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Degree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initial value of p&lt;sub&gt;0&lt;/sub&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initial value of p&lt;sub&gt;1&lt;/sub&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initial value of p&lt;sub&gt;2&lt;/sub&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initial value of p&lt;sub&gt;3&lt;/sub&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximal iterations number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;nobr&gt;&amp;chi;&lt;sup&gt;2&lt;/sup&gt; convergence criterion&lt;/nobr&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph - Fit Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>O&amp;ptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ana&amp;lytical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Numerical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphFunction</name>
    <message>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Inferior value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Su&amp;perior value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph - Function</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphGraph2D</name>
    <message>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use e&amp;rrors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use a table&apos;s co&amp;lumn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use an e&amp;xpression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use a table&apos;s colu&amp;mn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use an ex&amp;pression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use a c&amp;ut to select rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Errors (superior values)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Use errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>U&amp;se errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use a ta&amp;ble&apos;s column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use a&amp;n expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use a &amp;table&apos;s column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use an express&amp;ion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use err&amp;ors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use as&amp;ymmetric errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph - 2D Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Errors (inferior values)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Errors</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogGraphHistogram</name>
    <message>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use a table&apos;s co&amp;lumn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use an e&amp;xpression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fix the number of &amp;bins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fix the mi&amp;nimal value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fix the maxi&amp;mal value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use a c&amp;ut to select rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Superimpose on the active graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Graphical settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graph - Histogram</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogHelpAbout</name>
    <message>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A&amp;uthors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Thanks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help - About Bayani (Using QT %1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogSettingsData</name>
    <message>
        <source>Settings - Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Line &amp;width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Line &amp;style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Line colo&amp;r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Colo&amp;r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>St&amp;yle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>2D &amp;Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative si&amp;ze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hollow circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Filled circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hollow square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Filled square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error Bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FITS &amp;Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Monochrome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Four colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Six colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dynamics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set minim&amp;um</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set ma&amp;ximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Co&amp;lor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Line co&amp;lor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogSettingsFrame</name>
    <message>
        <source>Settings - Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Di&amp;mensions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative Origin&apos;s Coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A&amp;bcissa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>O&amp;rdinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative Width and Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative Distances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Between bo&amp;x and title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Horizontal separators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Vertical separators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Texts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Titl&amp;e</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Miscella&amp;neous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Colo&amp;r</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogSettingsGlobal</name>
    <message>
        <source>Settings - Global</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Terminal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;History size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Te&amp;xt color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C&amp;ritical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Background</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogSettingsGraph</name>
    <message>
        <source>Settings - Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Di&amp;mensions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative Origin&apos;s Coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A&amp;bcissa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>O&amp;rdinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative Width and Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Relative Distances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Between axis and titl&amp;e</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Between &amp;x axis and digits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Between &amp;y axis and digits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Between x ax&amp;is and label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Indicators&apos; Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X axis bi&amp;g indicators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;X axis big indicators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Y axis big indicators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X ax&amp;is small indicators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axi&amp;s small indicators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X axis &amp;grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axis g&amp;rid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Texts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Titl&amp;e</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;X axis label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Y axis label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X ax&amp;is digits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axi&amp;s digits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Miscella&amp;neous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Colo&amp;r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Between y ax&amp;is and label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axis bi&amp;g indicators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X axis &amp;small indicators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axis &amp;small indicators</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogTableEmpty</name>
    <message>
        <source>Cell 1 (included)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell 2 (included)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Co&amp;lumn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ro&amp;w</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Colu&amp;mn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table - Empty Cells</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogTableFillFromFile</name>
    <message>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Columns &amp;number in file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Columns &amp;separator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting &amp;row in table (included)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting co&amp;lumn in table (included)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add ro&amp;ws to table if necessary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add colu&amp;mns to table if necessary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Empty other cells</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Acce&amp;pt lines from file with more than the set columns number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Accep&amp;t lines from file with less than the set columns number. Fill the remaining cells with:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table - Fill from File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogTableManage</name>
    <message>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ro&amp;ws</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Co&amp;lumns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Elements number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Starting element (included)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table - Manage Size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogTableSwap</name>
    <message>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Co&amp;lumns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Co&amp;py contents of element 1 to element 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table - Swap Rows/Columns</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogTableWriteToFile</name>
    <message>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell 1 (included)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell 2 (included)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Co&amp;lumn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ro&amp;w</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Colu&amp;mn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Columns &amp;separator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A&amp;ppend existing file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source></source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table - Write to File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogTextsFrame</name>
    <message>
        <source>Texts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Texts - Frame</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabDialogTextsGraph</name>
    <message>
        <source>Texts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;X axis title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Y axis title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display Digits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X ax&amp;is</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axi&amp;s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Texts - Graph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTextEditBayani</name>
    <message>
        <source>Bayani&gt; </source>
        <translation>بياني&gt; </translation>
    </message>
    <message>
        <source>[Y/N]</source>
        <translation>[ن\ل]</translation>
    </message>
</context>
</TS>
