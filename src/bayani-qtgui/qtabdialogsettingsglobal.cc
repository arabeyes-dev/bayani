/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogSettingsGlobal Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogsettingsglobal.h>

QTabDialogSettingsGlobal::QTabDialogSettingsGlobal(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  setCaption(trUtf8("Settings - Global"));
  
  setOKButton     (trUtf8("&OK"      ));
  setApplyButton  (trUtf8("&Apply"   ));
  setDefaultButton(trUtf8("&Defaults"));
  setCancelButton (trUtf8("&Cancel"  ));
  
  //SettingsMenu->insertItem(trUtf8("Appearance..."));
  /* Terminal */
  
  TerminalQVB = new QVBox(Parent);
  TerminalQVB->setSpacing(5);
  TerminalQVB->setMargin(5);
  
  addTab(TerminalQVB, trUtf8("&Terminal"));
  
  Terminal1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("History"), TerminalQVB);
  
  Terminal11QVB = new QVBox(Terminal1QGB);
  Terminal11QVB->setSpacing(5);
  Terminal11QVB->setMargin(5);
  
  HistorySizeQL = new QLabel(trUtf8("&History size"), Terminal11QVB);
  
  Terminal12QVB = new QVBox(Terminal1QGB);
  Terminal12QVB->setSpacing(5);
  Terminal12QVB->setMargin(5);
  
  HistorySizeQLE = new QLineEdit(Terminal12QVB);
  HistorySizeQLE->setMaximumWidth(50);
  HistorySizeQLE->setValidator(new QIntValidator(HistorySizeQLE));
  
  Terminal2QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Properties"), TerminalQVB);
  
  Terminal21QVB = new QVBox(Terminal2QGB);
  Terminal21QVB->setSpacing(5);
  Terminal21QVB->setMargin(5);
  
  TextFontQL         = new QLabel(trUtf8("&Font"       ), Terminal21QVB);
  TextColorQL        = new QLabel(trUtf8("Te&xt color" ), Terminal21QVB);
  InformationColorQL = new QLabel(trUtf8("&Information"), Terminal21QVB);
  WarningColorQL     = new QLabel(trUtf8("&Warning"    ), Terminal21QVB);
  CriticalColorQL    = new QLabel(trUtf8("C&ritical"   ), Terminal21QVB);
  StatusColorQL      = new QLabel(trUtf8("&Status"     ), Terminal21QVB);
  BGColorQL          = new QLabel(trUtf8("&Background" ), Terminal21QVB);
  
  Terminal22QVB = new QVBox(Terminal2QGB);
  Terminal22QVB->setSpacing(5);
  Terminal22QVB->setMargin(5);
  
  TextFontQFS           = new QFontSelector         (Terminal22QVB);
  TextColorQPBCS        = new QPushButtonColorSelect(Terminal22QVB);
  InformationColorQPBCS = new QPushButtonColorSelect(Terminal22QVB);
  WarningColorQPBCS     = new QPushButtonColorSelect(Terminal22QVB);
  CriticalColorQPBCS    = new QPushButtonColorSelect(Terminal22QVB);
  StatusColorQPBCS      = new QPushButtonColorSelect(Terminal22QVB);
  BGColorQPBCS          = new QPushButtonColorSelect(Terminal22QVB);
  
  HistorySizeQL     ->setBuddy(HistorySizeQLE       );
  TextFontQL        ->setBuddy(TextFontQFS          );
  TextColorQL       ->setBuddy(TextColorQPBCS       );
  InformationColorQL->setBuddy(InformationColorQPBCS);
  WarningColorQL    ->setBuddy(WarningColorQPBCS    );
  CriticalColorQL   ->setBuddy(CriticalColorQPBCS   );
  StatusColorQL     ->setBuddy(StatusColorQPBCS     );
  BGColorQL         ->setBuddy(BGColorQPBCS         );
  
  /* General */
  
  HistorySizeQLE->setFocus();
  
  setFixedSize(sizeHint());
}

QTabDialogSettingsGlobal::~QTabDialogSettingsGlobal()
{
}

QLineEdit * QTabDialogSettingsGlobal::GetHistorySizeQLE()
{
  return HistorySizeQLE;
}

QPushButtonColorSelect * QTabDialogSettingsGlobal::GetTextColorQPBCS()
{
  return TextColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGlobal::GetInformationColorQPBCS()
{
  return InformationColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGlobal::GetWarningColorQPBCS()
{
  return WarningColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGlobal::GetCriticalColorQPBCS()
{
  return CriticalColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGlobal::GetStatusColorQPBCS()
{
  return StatusColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGlobal::GetBGColorQPBCS()
{
  return BGColorQPBCS;
}

QFontSelector * QTabDialogSettingsGlobal::GetTextFontQFS()
{
  return TextFontQFS;
}

void QTabDialogSettingsGlobal::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      HistorySizeQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      HistorySizeQLE->setAlignment(Qt::AlignLeft);
    }
}
