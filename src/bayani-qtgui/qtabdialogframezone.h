/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogFrameZone Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGFRAMEZONE_H
#define BAYANI_QTABDIALOGFRAMEZONE_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qcheckbox.h>
#include <qspinboxbayani.h>
#include <qvalidator.h>
#include <qregexp.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialogframezone.h
  \brief Header file for the QTabDialogFrameZone class.
  
  The QTabDialogFrameZone class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to define frames' zones.
*/
class QTabDialogFrameZone: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel         * HorZoneQL;
  QLabel         * VerZoneQL;
  QLineEdit      * HorZoneQLE;
  QLineEdit      * VerZoneQLE;
  QCheckBox      * SecondaryQCKB;
  QSpinBoxBayani * ZoneQSB;
  QGroupBox      * General1QGB;
  QGroupBox      * General2QGB;
  QVBox          * GeneralQVB;
  QVBox          * Sub1QVB;
  QVBox          * Sub2QVB;
  
 public:
                           QTabDialogFrameZone(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogFrameZone();
          QLineEdit      * GetHorZoneQLE      ();
	  QLineEdit      * GetVerZoneQLE      ();
	  QCheckBox      * GetSecondaryQCKB   ();
	  QSpinBoxBayani * GetZoneQSB         ();
	  void             SetR2L             (bool);
};

#endif
