/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogSettingsFrame Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGSETTINGSFRAME_H
#define BAYANI_QTABDIALOGSETTINGSFRAME_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qtabdialog.h>
#include <qvbox.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qvalidator.h>
/* Bayani headers */
#include <qlinecombo.h>
#include <qpushbuttoncolorselect.h>
#include <qfontselector.h>
#include <qpencombo.h>

/*!
  \file qtabdialogsettingsframe.h
  \brief Header file for the QTabDialogSettingsFrame class.
  
  The QTabDialogSettingsFrame class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set frames' options.
*/
class QTabDialogSettingsFrame: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QVBox * DimensionsQVB;
  QVBox * Dimensions11QVB;
  QVBox * Dimensions12QVB;
  QVBox * Dimensions21QVB;
  QVBox * Dimensions22QVB;
  QVBox * Dimensions31QVB;
  QVBox * Dimensions32QVB;
  QVBox * LinesQVB;
  QVBox * Lines11QVB;
  QVBox * Lines12QVB;
  QVBox * Lines13QVB;
  QVBox * Lines14QVB;
  QVBox * TextsQVB;
  QVBox * Texts11QVB;
  QVBox * Texts12QVB;
  QVBox * Texts13QVB;
  QVBox * MiscellaneousQVB;
  QVBox * Miscellaneous11QVB;
  QVBox * Miscellaneous12QVB;
  
  QGroupBox * Dimensions1QGB;
  QGroupBox * Dimensions2QGB;
  QGroupBox * Dimensions3QGB;
  QGroupBox * Lines1QGB;
  QGroupBox * Texts1QGB;
  QGroupBox * Miscellaneous1QGB;
  
  QLabel * XRatioQL;
  QLabel * YRatioQL;
  QLabel * WidthRatioQL;
  QLabel * HeightRatioQL;
  QLabel * Box2TitleRatioQL;
  QLabel * BoxLineWidthQL;
  QLabel * HSepLineWidthQL;
  QLabel * VSepLineWidthQL;
  QLabel * BGColorQL;
  QLabel * TitleFontQL;
  
  QLineEdit * XRatioQLE;
  QLineEdit * YRatioQLE;
  QLineEdit * WidthRatioQLE;
  QLineEdit * HeightRatioQLE;
  QLineEdit * Box2TitleRatioQLE;
  
  QLineCombo * BoxLineWidthQLC;
  QLineCombo * HSepLineWidthQLC;
  QLineCombo * VSepLineWidthQLC;
  
  QPushButtonColorSelect * BGColorQPBCS;
  QPushButtonColorSelect * TitleColorQPBCS;
  QPushButtonColorSelect * BoxColorQPBCS;
  QPushButtonColorSelect * HSepColorQPBCS;
  QPushButtonColorSelect * VSepColorQPBCS;
  
  QFontSelector * TitleFontQFS;
  
  QPenCombo * BoxLineStyleQPC;
  QPenCombo * HSepLineStyleQPC;
  QPenCombo * VSepLineStyleQPC;
  
 public:
                                   QTabDialogSettingsFrame(QWidget * Parent = 0, const char * Name = 0);
  virtual                         ~QTabDialogSettingsFrame();
          QLineEdit              * GetXRatioQLE           ();
	  QLineEdit              * GetYRatioQLE           ();
	  QLineEdit              * GetWidthRatioQLE       ();
	  QLineEdit              * GetHeightRatioQLE      ();
	  QLineEdit              * GetBox2TitleRatioQLE   ();
	  QLineCombo             * GetBoxLineWidthQLC     ();
	  QLineCombo             * GetHSepLineWidthQLC    ();
	  QLineCombo             * GetVSepLineWidthQLC    ();
	  QPushButtonColorSelect * GetBGColorQPBCS        ();
	  QPushButtonColorSelect * GetTitleColorQPBCS     ();
	  QPushButtonColorSelect * GetBoxColorQPBCS       ();
	  QPushButtonColorSelect * GetHSepColorQPBCS      ();
	  QPushButtonColorSelect * GetVSepColorQPBCS      ();
	  QFontSelector          * GetTitleFontQFS        ();
	  QPenCombo              * GetBoxLineStyleQPC     ();
	  QPenCombo              * GetHSepLineStyleQPC    ();
	  QPenCombo              * GetVSepLineStyleQPC    ();
	  void                     SetR2L                 (bool);
};

#endif
