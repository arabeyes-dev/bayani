/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogTableEmpty Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogtableempty.h>

QTabDialogTableEmpty::QTabDialogTableEmpty(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQHB = new QHBox(Parent);
  Sub1QGB    = new QGroupBox(2, Qt::Horizontal, trUtf8("Cell 1 (included)"), GeneralQHB);
  Sub2QGB    = new QGroupBox(2, Qt::Horizontal, trUtf8("Cell 2 (included)"), GeneralQHB);
  
  Sub1QVB  = new QVBox(Sub1QGB);
  Sub2QVB  = new QVBox(Sub1QGB);
  Sub3QVB  = new QVBox(Sub2QGB);
  Sub4QVB  = new QVBox(Sub2QGB);
  
  Row1QL  = new QLabel(trUtf8("&Row"), Sub1QVB);
  Row1QSB = new QSpinBoxBayani(1, 1, 1, Sub2QVB);
  
  Column1QL  = new QLabel(trUtf8("Co&lumn"), Sub1QVB);
  Column1QSB = new QSpinBoxBayani(1, 1, 1, Sub2QVB);
  
  Row2QL  = new QLabel(trUtf8("Ro&w"), Sub3QVB);
  Row2QSB = new QSpinBoxBayani(1, 1, 1, Sub4QVB);
  
  Column2QL  = new QLabel(trUtf8("Colu&mn"), Sub3QVB);
  Column2QSB = new QSpinBoxBayani(1, 1, 1, Sub4QVB);
  
  GeneralQHB->setSpacing(5);
  GeneralQHB->setMargin(5);
  
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  Sub3QVB->setSpacing(5);
  Sub3QVB->setMargin(5);
  Sub4QVB->setSpacing(5);
  Sub4QVB->setMargin(5);
  
  Row1QL   ->setBuddy(Row1QSB   );
  Column1QL->setBuddy(Column1QSB);
  Row2QL   ->setBuddy(Row2QSB   );
  Column2QL->setBuddy(Column2QSB);
  
  Row1QSB->setMaximumWidth(50);
  Column1QSB->setMaximumWidth(50);
  Row2QSB->setMaximumWidth(50);
  Column2QSB->setMaximumWidth(50);
  
  addTab(GeneralQHB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Table - Empty Cells"));
  
  Row1QSB->setFocus();
  
  connect(Row1QSB   , SIGNAL(valueChanged(int)), this, SLOT(Row1QSBChangedSlot(int)));
  connect(Column1QSB, SIGNAL(valueChanged(int)), this, SLOT(Column1QSBChangedSlot(int)));
  connect(Row2QSB   , SIGNAL(valueChanged(int)), this, SLOT(Row2QSBChangedSlot(int)));
  connect(Column2QSB, SIGNAL(valueChanged(int)), this, SLOT(Column2QSBChangedSlot(int)));
}

QTabDialogTableEmpty::~QTabDialogTableEmpty()
{
}

QSpinBoxBayani * QTabDialogTableEmpty::GetRow1QSB()
{
  return Row1QSB;
}

QSpinBoxBayani * QTabDialogTableEmpty::GetColumn1QSB()
{
  return Column1QSB;
}

QSpinBoxBayani * QTabDialogTableEmpty::GetRow2QSB()
{
  return Row2QSB;
}

QSpinBoxBayani * QTabDialogTableEmpty::GetColumn2QSB()
{
  return Column2QSB;
}

void QTabDialogTableEmpty::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      Row1QSB->GetEditor()->setAlignment(Qt::AlignRight);
      Column1QSB->GetEditor()->setAlignment(Qt::AlignRight);
      Row2QSB->GetEditor()->setAlignment(Qt::AlignRight);
      Column2QSB->GetEditor()->setAlignment(Qt::AlignRight);
    }
  else
    {
      Row1QSB->GetEditor()->setAlignment(Qt::AlignLeft);
      Column1QSB->GetEditor()->setAlignment(Qt::AlignLeft);
      Row2QSB->GetEditor()->setAlignment(Qt::AlignLeft);
      Column2QSB->GetEditor()->setAlignment(Qt::AlignLeft);
    }
}

void QTabDialogTableEmpty::Row1QSBChangedSlot(int Value)
{
  if(Row2QSB->value() < Value)
    Row2QSB->setValue(Value);
}

void QTabDialogTableEmpty::Column1QSBChangedSlot(int Value)
{
  if(Column2QSB->value() < Value)
    Column2QSB->setValue(Value);
}

void QTabDialogTableEmpty::Row2QSBChangedSlot(int Value)
{
  if(Row1QSB->value() > Value)
    Row1QSB->setValue(Value);
}

void QTabDialogTableEmpty::Column2QSBChangedSlot(int Value)
{
  if(Column1QSB->value() > Value)
    Column1QSB->setValue(Value);
}
