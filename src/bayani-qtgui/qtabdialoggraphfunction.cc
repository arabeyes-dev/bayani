/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogGraphFunction Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialoggraphfunction.h>

QTabDialogGraphFunction::QTabDialogGraphFunction(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB  = new QVBox(Parent);
  General1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Parameters"), GeneralQVB);
  General2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Options"), GeneralQVB);
  
  Sub1QVB = new QVBox(General1QGB);
  Sub2QVB = new QVBox(General1QGB);
  
  ExpressionQL  = new QLabel(trUtf8("&Expression"), Sub1QVB);
  ExpressionQLE = new QLineEdit(Sub2QVB);
  
  XMinQL  = new QLabel(trUtf8("&Inferior value"), Sub1QVB);
  XMinQLE = new QLineEdit(Sub2QVB);
  
  XMaxQL  = new QLabel(trUtf8("Su&perior value"), Sub1QVB);
  XMaxQLE = new QLineEdit(Sub2QVB);
  
  SameQCKB = new QCheckBox(trUtf8("&Superimpose on the active graph"), General2QGB);
  
  PlotSettingsQPB = new QPushButton(trUtf8("&Graphical settings..."), General2QGB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  
  ExpressionQL->setBuddy(ExpressionQLE);
  XMinQL      ->setBuddy(XMinQLE      );
  XMaxQL      ->setBuddy(XMaxQLE      );
  
  XMinQLE->setMaximumWidth(50);
  XMinQLE->setValidator(new QDoubleValidator(XMinQLE));
  XMaxQLE->setMaximumWidth(50);
  XMaxQLE->setValidator(new QDoubleValidator(XMaxQLE));
  
  PlotSettingsQPB->setFixedSize(PlotSettingsQPB->sizeHint());
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Graph - Function"));
  
  ExpressionQLE->setFocus();
}

QTabDialogGraphFunction::~QTabDialogGraphFunction()
{
}

QLineEdit * QTabDialogGraphFunction::GetExpressionQLE()
{
  return ExpressionQLE;
}

QLineEdit * QTabDialogGraphFunction::GetXMinQLE()
{
  return XMinQLE;
}

QLineEdit * QTabDialogGraphFunction::GetXMaxQLE()
{
  return XMaxQLE;
}

QPushButton * QTabDialogGraphFunction::GetPlotSettingsQPB()
{
  return PlotSettingsQPB;
}

QCheckBox * QTabDialogGraphFunction::GetSameQCKB()
{
  return SameQCKB;
}

void QTabDialogGraphFunction::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      ExpressionQLE->setAlignment(Qt::AlignRight);
      XMinQLE->setAlignment(Qt::AlignRight);
      XMaxQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      ExpressionQLE->setAlignment(Qt::AlignLeft);
      XMinQLE->setAlignment(Qt::AlignLeft);
      XMaxQLE->setAlignment(Qt::AlignLeft);
    }
}
