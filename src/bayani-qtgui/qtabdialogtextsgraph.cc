/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogTextsGraph Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogtextsgraph.h>

QTabDialogTextsGraph::QTabDialogTextsGraph(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB  = new QVBox(this);
  General1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Texts"), GeneralQVB);
  
  Sub1QVB = new QVBox(General1QGB);
  Sub2QVB = new QVBox(General1QGB);
  
  TitleQL  = new QLabel(trUtf8("&Title"), Sub1QVB);
  TitleQLE = new QLineEdit(Sub2QVB);
  
  XAxisLabelQL  = new QLabel(trUtf8("&X axis title"), Sub1QVB);
  XAxisLabelQLE = new QLineEdit(Sub2QVB);
  
  YAxisLabelQL  = new QLabel(trUtf8("&Y axis title"), Sub1QVB);
  YAxisLabelQLE = new QLineEdit(Sub2QVB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  
  TitleQL     ->setBuddy(TitleQLE     );
  XAxisLabelQL->setBuddy(XAxisLabelQLE);
  YAxisLabelQL->setBuddy(YAxisLabelQLE);
  
  TitleQLE->setMaximumWidth(200);
  XAxisLabelQLE->setMaximumWidth(200);
  YAxisLabelQLE->setMaximumWidth(200);
  
  General2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Display Digits"), GeneralQVB);
  
  XTicksLabelsFlagQCKB = new QCheckBox(trUtf8("X ax&is"), General2QGB);
  YTicksLabelsFlagQCKB = new QCheckBox(trUtf8("Y axi&s"), General2QGB);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Texts - Graph"));
  
  TitleQLE->setFocus();
}

QTabDialogTextsGraph::~QTabDialogTextsGraph()
{
}

QLineEdit * QTabDialogTextsGraph::GetTitleQLE()
{
  return TitleQLE;
}

QLineEdit * QTabDialogTextsGraph::GetXAxisLabelQLE()
{
  return XAxisLabelQLE;
}

QLineEdit * QTabDialogTextsGraph::GetYAxisLabelQLE()
{
  return YAxisLabelQLE;
}

QCheckBox * QTabDialogTextsGraph::GetXTicksLabelsFlagQCKB()
{
  return XTicksLabelsFlagQCKB;
}

QCheckBox * QTabDialogTextsGraph::GetYTicksLabelsFlagQCKB()
{
  return YTicksLabelsFlagQCKB;
}

void QTabDialogTextsGraph::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      TitleQLE->setAlignment(Qt::AlignRight);
      XAxisLabelQLE->setAlignment(Qt::AlignRight);
      YAxisLabelQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      TitleQLE->setAlignment(Qt::AlignLeft);
      XAxisLabelQLE->setAlignment(Qt::AlignLeft);
      YAxisLabelQLE->setAlignment(Qt::AlignLeft);
    }
}
