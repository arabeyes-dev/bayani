/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogTableFillFromFile Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGTABLEFILLFROMFILE_H
#define BAYANI_QTABDIALOGTABLEFILLFROMFILE_H

/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qvalidator.h>
#include <qregexp.h>
#include <qspinboxbayani.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
#include <qhbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>
/* Bayani headers */
#include <qfileselector.h>

/*!
  \file qtabdialogtablefillfromfile.h
  \brief Header file for the QTabDialogTableFillFromFile class.
  
  The QTabDialogTableFillFromFile class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to fill the table from a file.
*/
class QTabDialogTableFillFromFile: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel         * FileNameQL;
  QLabel         * ColumnsNumberQL;
  QLabel         * SeparatorQL;
  QLabel         * StartingRowQL;
  QLabel         * StartingColumnQL;
  QLineEdit      * ColumnsNumberQLE;
  QLineEdit      * SeparatorQLE;
  QLineEdit      * DefaultFillQLE;
  QFileSelector  * FileNameQFLS;
  QSpinBoxBayani * StartingRowQSB;
  QSpinBoxBayani * StartingColumnQSB;
  QCheckBox      * AddRowsQCKB;
  QCheckBox      * AddColumnsQCKB;
  QCheckBox      * EmptyCellsQCKB;
  QCheckBox      * SupLinesQCKB;
  QCheckBox      * SubLinesQCKB;
  QGroupBox      * General1QGB;
  QGroupBox      * General2QGB;
  QHBox          * Sub1QHB;
  QHBox          * Sub2QHB;
  QVBox          * GeneralQVB;
  QVBox          * Sub1QVB;
  QVBox          * Sub2QVB;
  
 public:
                           QTabDialogTableFillFromFile(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogTableFillFromFile();
          QLineEdit      * GetColumnsNumberQLE        ();
	  QLineEdit      * GetSeparatorQLE            ();
	  QSpinBoxBayani * GetStartingRowQSB          ();
	  QSpinBoxBayani * GetStartingColumnQSB       ();
	  QFileSelector  * GetFileNameQFLS            ();
	  QCheckBox      * GetAddRowsQCKB             ();
	  QCheckBox      * GetAddColumnsQCKB          ();
	  QCheckBox      * GetEmptyCellsQCKB          ();
	  QCheckBox      * GetSupLinesQCKB            ();
	  QCheckBox      * GetSubLinesQCKB            ();
	  QLineEdit      * GetDefaultFillQLE          ();
	  void             SetR2L                     (bool);
};

#endif
