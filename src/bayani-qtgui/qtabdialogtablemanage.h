/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogTableManage Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGTABLEMANAGE_H
#define BAYANI_QTABDIALOGTABLEMANAGE_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qvalidator.h>
#include <qregexp.h>
#include <qspinboxbayani.h>
#include <qradiobutton.h>
#include <qgroupbox.h>
#include <qbuttongroup.h>
#include <qhbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialogtablemanage.h
  \brief Header file for the QTabDialogTableManage class.
  
  The QTabDialogTableManage class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to manage the table's size.
*/
class QTabDialogTableManage: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel         * NumberQL;
  QLabel         * StartQL;
  QRadioButton   * InsertQRB;
  QRadioButton   * RemoveQRB;
  QRadioButton   * RowsQRB;
  QRadioButton   * ColumnsQRB;
  QLineEdit      * NumberQLE;
  QSpinBoxBayani * StartQSB;
  QGroupBox      * GeneralQGB;
  QButtonGroup   * Sub1QBG;
  QButtonGroup   * Sub2QBG;
  QHBox          * SubQHB;
  QVBox          * GeneralQVB;
  QVBox          * Sub1QVB;
  QVBox          * Sub2QVB;
  
 public:
                           QTabDialogTableManage(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogTableManage();
          QRadioButton   * GetInsertQRB         ();
	  QRadioButton   * GetRemoveQRB         ();
	  QRadioButton   * GetRowsQRB           ();
	  QRadioButton   * GetColumnsQRB        ();
	  QLineEdit      * GetNumberQLE         ();
	  QSpinBoxBayani * GetStartQSB          ();
	  void             SetR2L               (bool);
};

#endif
