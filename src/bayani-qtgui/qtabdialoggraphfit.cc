/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogGraphFit Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialoggraphfit.h>

QTabDialogGraphFit::QTabDialogGraphFit(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  /* Options */
  
  OptionsQVB = new QVBox(Parent);
  
  Options1QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("General"  ), OptionsQVB);
  Options2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Graphical"), OptionsQVB);
  Options3QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Output"   ), OptionsQVB);
  
  NumericalQCKB = new QCheckBox(trUtf8("Nu&merical fit (3-degree polynomial only)"), Options1QGB);
  
  PlotQCKB = new QCheckBox(trUtf8("&Represent the result graphically"), Options2QGB);
  
  PlotNoConvQCKB = new QCheckBox(trUtf8("Represent even if the fit did not con&verge"), Options2QGB);
  
  SameQCKB = new QCheckBox(trUtf8("&Superimpose on the active graph"), Options2QGB);
  
  PlotSettingsQPB = new QPushButton(trUtf8("&Graphical settings..."), Options2QGB);
  
  OutputQTE = new QTextEdit(Options3QGB);
  OutputQTE->setReadOnly(true);
  OutputQTE->setMinimumHeight(200);
  
  OptionsQVB->setSpacing(5);
  OptionsQVB->setMargin(5);
  
  PlotSettingsQPB->setFixedSize(PlotSettingsQPB->sizeHint());
  
  /* Analytical */
  
  AnalyticalQVB = new QVBox(Parent);
  
  Analytical1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Polynomial"), AnalyticalQVB);
  
  Analytical11QVB = new QVBox(Analytical1QGB);
  Analytical12QVB = new QVBox(Analytical1QGB);
  
  DegreeQL = new QLabel(trUtf8("&Degree"), Analytical11QVB);
  
  DegreeQCB = new QComboBox(false, Analytical12QVB);
  DegreeQCB->insertItem(QString("%1") .arg(0), 0);
  DegreeQCB->insertItem(QString("%1") .arg(1), 1);
  DegreeQCB->insertItem(QString("%1") .arg(2), 2);
  
  AnalyticalQVB->setSpacing(5);
  AnalyticalQVB->setMargin(5);
  
  Analytical11QVB->setSpacing(5);
  Analytical11QVB->setMargin(5);
  Analytical12QVB->setSpacing(5);
  Analytical12QVB->setMargin(5);
  
  DegreeQL->setBuddy(DegreeQCB);
  
  DegreeQCB->setFixedSize(DegreeQCB->sizeHint());
  
  /* Numerical */
  
  NumericalQVB = new QVBox(Parent);
  
  Numerical1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Parameters"), NumericalQVB);
  
  Numerical11QVB = new QVBox(Numerical1QGB);
  Numerical12QVB = new QVBox(Numerical1QGB);
  
  Parameter0QL  = new QLabel(trUtf8("Initial value of p<sub>0</sub>"), Numerical11QVB);
  Parameter0QLE = new QLineEdit(Numerical12QVB);
  
  Parameter1QL  = new QLabel(trUtf8("Initial value of p<sub>1</sub>"), Numerical11QVB);
  Parameter1QLE = new QLineEdit(Numerical12QVB);
  
  Parameter2QL  = new QLabel(trUtf8("Initial value of p<sub>2</sub>"), Numerical11QVB);
  Parameter2QLE = new QLineEdit(Numerical12QVB);
  
  Parameter3QL  = new QLabel(trUtf8("Initial value of p<sub>3</sub>"), Numerical11QVB);
  Parameter3QLE = new QLineEdit(Numerical12QVB);
  
  MaxStepQL  = new QLabel(trUtf8("Maximal iterations number"), Numerical11QVB);
  MaxStepQLE = new QLineEdit(Numerical12QVB);
  
  Chi2LimQL  = new QLabel(trUtf8("<nobr>&chi;<sup>2</sup> convergence criterion</nobr>"), Numerical11QVB);
  Chi2LimQLE = new QLineEdit(Numerical12QVB);
  
  MaxStepQV = new QIntValidator(MaxStepQLE);
  Chi2LimQV = new QDoubleValidator(Chi2LimQLE);
  
  NumericalQVB->setSpacing(5);
  NumericalQVB->setMargin(5);
  
  Numerical11QVB->setSpacing(5);
  Numerical11QVB->setMargin(5);
  Numerical12QVB->setSpacing(5);
  Numerical12QVB->setMargin(5);
  
  Parameter0QL->setTextFormat(Qt::RichText);
  Parameter1QL->setTextFormat(Qt::RichText);
  Parameter2QL->setTextFormat(Qt::RichText);
  Parameter3QL->setTextFormat(Qt::RichText);
  Chi2LimQL->setTextFormat(Qt::RichText);
  
  Parameter0QLE->setMaximumWidth(100);
  Parameter0QLE->setValidator(new QDoubleValidator(Parameter0QLE));
  Parameter1QLE->setMaximumWidth(100);
  Parameter1QLE->setValidator(new QDoubleValidator(Parameter1QLE));
  Parameter2QLE->setMaximumWidth(100);
  Parameter2QLE->setValidator(new QDoubleValidator(Parameter2QLE));
  Parameter3QLE->setMaximumWidth(100);
  Parameter3QLE->setValidator(new QDoubleValidator(Parameter3QLE));
  MaxStepQLE->setMaximumWidth(100);
  MaxStepQLE->setValidator(MaxStepQV);
  Chi2LimQLE->setMaximumWidth(100);
  Chi2LimQLE->setValidator(Chi2LimQV);
  
  MaxStepQV->setBottom(0);
  Chi2LimQV->setBottom(0.);
  
  /* General */
  
  setCaption(trUtf8("Graph - Fit Data"));
  
  addTab(OptionsQVB   , trUtf8("O&ptions"));
  addTab(AnalyticalQVB, trUtf8("Ana&lytical"));
  addTab(NumericalQVB , trUtf8("&Numerical"));
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  NumericalChangedSlot(false);
  PlotChangedSlot(false);
  
  Parameter0QLE->setFocus();
  
  setFixedSize(sizeHint());
  
  connect(NumericalQCKB, SIGNAL(toggled(bool)), this, SLOT(NumericalChangedSlot(bool)));
  connect(PlotQCKB     , SIGNAL(toggled(bool)), this, SLOT(PlotChangedSlot(bool)));
}

QTabDialogGraphFit::~QTabDialogGraphFit()
{
}

QLineEdit * QTabDialogGraphFit::GetParameter0QLE()
{
  return Parameter0QLE;
}

QLineEdit * QTabDialogGraphFit::GetParameter1QLE()
{
  return Parameter1QLE;
}

QLineEdit * QTabDialogGraphFit::GetParameter2QLE()
{
  return Parameter2QLE;
}

QLineEdit * QTabDialogGraphFit::GetParameter3QLE()
{
  return Parameter3QLE;
}

QLineEdit * QTabDialogGraphFit::GetMaxStepQLE()
{
  return MaxStepQLE;
}

QLineEdit * QTabDialogGraphFit::GetChi2LimQLE()
{
  return Chi2LimQLE;
}

QTextEdit * QTabDialogGraphFit::GetOutputQTE()
{
  return OutputQTE;
}

QPushButton * QTabDialogGraphFit::GetPlotSettingsQPB()
{
  return PlotSettingsQPB;
}

QCheckBox * QTabDialogGraphFit::GetNumericalQCKB()
{
  return NumericalQCKB;
}

QCheckBox * QTabDialogGraphFit::GetPlotQCKB()
{
  return PlotQCKB;
}

QCheckBox * QTabDialogGraphFit::GetPlotNoConvQCKB()
{
  return PlotNoConvQCKB;
}

QCheckBox * QTabDialogGraphFit::GetSameQCKB()
{
  return SameQCKB;
}

QComboBox * QTabDialogGraphFit::GetDegreeQCB()
{
  return DegreeQCB;
}

void QTabDialogGraphFit::SetCurrentTab(int TabID)
{
  tabBar()->setCurrentTab(TabID);
}

void QTabDialogGraphFit::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      OutputQTE    ->setAlignment(Qt::AlignRight);
      Parameter0QLE->setAlignment(Qt::AlignRight);
      Parameter1QLE->setAlignment(Qt::AlignRight);
      Parameter2QLE->setAlignment(Qt::AlignRight);
      Parameter3QLE->setAlignment(Qt::AlignRight);
      MaxStepQLE   ->setAlignment(Qt::AlignRight);
      Chi2LimQLE   ->setAlignment(Qt::AlignRight);
    }
  else
    {
      OutputQTE    ->setAlignment(Qt::AlignLeft);
      Parameter0QLE->setAlignment(Qt::AlignLeft);
      Parameter1QLE->setAlignment(Qt::AlignLeft);
      Parameter2QLE->setAlignment(Qt::AlignLeft);
      Parameter3QLE->setAlignment(Qt::AlignLeft);
      MaxStepQLE   ->setAlignment(Qt::AlignLeft);
      Chi2LimQLE   ->setAlignment(Qt::AlignLeft);
    }
}

void QTabDialogGraphFit::NumericalChangedSlot(bool State)
{
  tabBar()->setTabEnabled(1, !State);
  tabBar()->setTabEnabled(2, State);
  PlotNoConvQCKB->setEnabled(State && PlotQCKB->isChecked());
}

void QTabDialogGraphFit::PlotChangedSlot(bool State)
{
  PlotNoConvQCKB->setEnabled(State && NumericalQCKB->isChecked());
  SameQCKB->setEnabled(State);
  PlotSettingsQPB->setEnabled(State);
}
