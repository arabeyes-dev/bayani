/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogTableSwap Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogtableswap.h>

QTabDialogTableSwap::QTabDialogTableSwap(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB = new QVBox(Parent);
  GeneralQBG = new QButtonGroup(1, Qt::Horizontal, trUtf8("Type"), GeneralQVB);
  GeneralQGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Elements"), GeneralQVB);
  
  RowsQRB    = new QRadioButton(trUtf8("&Rows"), GeneralQBG);
  ColumnsQRB = new QRadioButton(trUtf8("Co&lumns"), GeneralQBG);
  
  Sub11QVB = new QVBox(GeneralQGB);
  Sub12QVB = new QVBox(GeneralQGB);
  
  Element1QL  = new QLabel(trUtf8("&First"), Sub11QVB);
  Element1QSB = new QSpinBoxBayani(1, 1, 1, Sub12QVB);
  
  Element2QL  = new QLabel(trUtf8("&Second"), Sub11QVB);
  Element2QSB = new QSpinBoxBayani(1, 1, 1, Sub12QVB);
  
  CopyQCKB = new QCheckBox(trUtf8("Co&py contents of element 1 to element 2"), GeneralQVB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub11QVB->setSpacing(5);
  Sub11QVB->setMargin(5);
  Sub12QVB->setSpacing(5);
  Sub12QVB->setMargin(5);
  
  Element1QL->setBuddy(Element1QSB);
  Element2QL->setBuddy(Element2QSB);
  
  Element1QSB->setMaximumWidth(50);
  Element2QSB->setMaximumWidth(50);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Table - Swap Rows/Columns"));
  
  Element1QSB->setFocus();
}

QTabDialogTableSwap::~QTabDialogTableSwap()
{
}

QRadioButton * QTabDialogTableSwap::GetRowsQRB()
{
  return RowsQRB;
}

QRadioButton * QTabDialogTableSwap::GetColumnsQRB()
{
  return ColumnsQRB;
}

QSpinBoxBayani * QTabDialogTableSwap::GetElement1QSB()
{
  return Element1QSB;
}

QSpinBoxBayani * QTabDialogTableSwap::GetElement2QSB()
{
  return Element2QSB;
}

QCheckBox * QTabDialogTableSwap::GetCopyQCKB()
{
  return CopyQCKB;
}

void QTabDialogTableSwap::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      Element1QSB->GetEditor()->setAlignment(Qt::AlignRight);
      Element2QSB->GetEditor()->setAlignment(Qt::AlignRight);
    }
  else
    {
      Element1QSB->GetEditor()->setAlignment(Qt::AlignLeft);
      Element2QSB->GetEditor()->setAlignment(Qt::AlignLeft);
    }
}
