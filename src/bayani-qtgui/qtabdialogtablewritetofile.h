/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogTableWriteToFile Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGTABLEWRITETOFILE_H
#define BAYANI_QTABDIALOGTABLEWRITETOFILE_H

/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qspinboxbayani.h>
#include <qcheckbox.h>
#include <qlineedit.h>
#include <qgroupbox.h>
#include <qhbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>
/* Bayani headers */
#include <qfileselector.h>

/*!
  \file qtabdialogtablewritetofile.h
  \brief Header file for the QTabDialogTableWriteToFile class.
  
  The QTabDialogTableWriteToFile class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to write the table to a file.
*/
class QTabDialogTableWriteToFile: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel         * FileNameQL;
  QLabel         * AppendQL;
  QLabel         * SeparatorQL;
  QLabel         * Row1QL;
  QLabel         * Column1QL;
  QLabel         * Row2QL;
  QLabel         * Column2QL;
  QLineEdit      * SeparatorQLE;
  QFileSelector  * FileNameQFLS;
  QSpinBoxBayani * Row1QSB;
  QSpinBoxBayani * Column1QSB;
  QSpinBoxBayani * Row2QSB;
  QSpinBoxBayani * Column2QSB;
  QCheckBox      * AppendQCKB;
  QGroupBox      * General1QGB;
  QGroupBox      * General2QGB;
  QGroupBox      * Sub1QGB;
  QGroupBox      * Sub2QGB;
  QVBox          * GeneralQVB;
  QVBox          * Sub1QVB;
  QVBox          * Sub2QVB;
  QVBox          * Sub3QVB;
  QVBox          * Sub4QVB;
  QVBox          * Sub5QVB;
  QVBox          * Sub6QVB;
  QHBox          * Sub1QHB;
  QHBox          * Sub2QHB;
  
 public:
                           QTabDialogTableWriteToFile(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogTableWriteToFile();
          QFileSelector  * GetFileNameQFLS           ();
	  QSpinBoxBayani * GetRow1QSB                ();
	  QSpinBoxBayani * GetColumn1QSB             ();
	  QSpinBoxBayani * GetRow2QSB                ();
	  QSpinBoxBayani * GetColumn2QSB             ();
	  QCheckBox      * GetAppendQCKB             ();
	  QLineEdit      * GetSeparatorQLE           (); 
	  void             SetR2L                    (bool);
	  
 public slots:
  void Row1QSBChangedSlot   (int);
  void Column1QSBChangedSlot(int);
  void Row2QSBChangedSlot   (int);
  void Column2QSBChangedSlot(int);
};

#endif
