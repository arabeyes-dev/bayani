/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogHelpAbout Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGHELPABOUT_H
#define BAYANI_QTABDIALOGHELPABOUT_H

/* QT headers */
#include <qglobal.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <qtabdialog.h>
#include <qrect.h>
/* Bayani headers */
#include <btextbrowser.h>

/*!
  \file qtabdialoghelpabout.h
  \brief Header file for the QTabDialogHelpAbout class.
  
  The QTabDialogHelpAbout class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to view information about Bayani.
*/
class QTabDialogHelpAbout: public QTabDialog
{
  Q_OBJECT
  
 protected:
  BTextBrowser * AboutBTB;
  BTextBrowser * AuthorsBTB;
  BTextBrowser * ThanksBTB;
  BTextBrowser * LicenseBTB;
  
  virtual void showEvent(QShowEvent *);
  
 public:
               QTabDialogHelpAbout(QWidget * Parent = 0, const char * Name = 0);
  virtual     ~QTabDialogHelpAbout();
          void SetR2L             (bool);
};

#endif
