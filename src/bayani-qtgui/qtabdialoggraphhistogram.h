/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogGraphHistogram Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGGRAPHHISTOGRAM_H
#define BAYANI_QTABDIALOGGRAPHHISTOGRAM_H

/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qcheckbox.h>
#include <qspinboxbayani.h>
#include <qradiobutton.h>
#include <qgroupbox.h>
#include <qbuttongroup.h>
#include <qhbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qvalidator.h>
#include <qregexp.h>
#include <qtabbar.h>

/*!
  \file qtabdialoggraphhistogram.h
  \brief Header file for the QTabDialogGraphHistogram class.
  
  The QTabDialogGraphHistogram class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to represent histograms.
*/
class QTabDialogGraphHistogram: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel         * ColumnQL;
  QLabel         * ExpressionQL;
  QSpinBoxBayani * ColumnQSB;
  QLineEdit      * CutQLE;
  QLineEdit      * BinsNumberQLE;
  QLineEdit      * XMinQLE;
  QLineEdit      * XMaxQLE;
  QLineEdit      * ExpressionQLE;
  QPushButton    * PlotSettingsQPB;
  QCheckBox      * BinsNumberQCKB;
  QCheckBox      * XMinQCKB;
  QCheckBox      * XMaxQCKB;
  QCheckBox      * SameQCKB;
  QCheckBox      * CutQCKB;
  QRadioButton   * ColumnQRB;
  QRadioButton   * ExpressionQRB;
  QGroupBox      * General1QGB;
  QGroupBox      * General2QGB;
  QButtonGroup   * SubQBG;
  QHBox          * Sub1QHB;
  QHBox          * Sub2QHB;
  QVBox          * GeneralQVB;
  QVBox          * Sub1QVB;
  QVBox          * Sub2QVB;
  
 public:
                           QTabDialogGraphHistogram(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogGraphHistogram();
          QSpinBoxBayani * GetColumnQSB            ();
	  QLineEdit      * GetExpressionQLE        ();
	  QLineEdit      * GetBinsNumberQLE        ();
	  QLineEdit      * GetXMinQLE              ();
	  QLineEdit      * GetXMaxQLE              ();
	  QLineEdit      * GetCutQLE               ();
	  QPushButton    * GetPlotSettingsQPB      ();
	  QCheckBox      * GetBinsNumberQCKB       ();
	  QCheckBox      * GetXMinQCKB             ();
	  QCheckBox      * GetXMaxQCKB             ();
	  QCheckBox      * GetCutQCKB              ();
	  QCheckBox      * GetSameQCKB             ();
	  QRadioButton   * GetColumnQRB            ();
	  QRadioButton   * GetExpressionQRB        ();
	  void             SetR2L                  (bool);
};

#endif
