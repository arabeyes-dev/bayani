/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogGraphHistogram Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialoggraphhistogram.h>

QTabDialogGraphHistogram::QTabDialogGraphHistogram(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB  = new QVBox(Parent);
  General1QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Parameters"), GeneralQVB);
  Sub2QHB     = new QHBox(GeneralQVB);
  General2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Options"), GeneralQVB);
  
  SubQBG = new QButtonGroup(1, Qt::Horizontal, trUtf8("Data"), General1QGB);
  
  Sub1QHB = new QHBox(General1QGB);
  
  Sub1QVB = new QVBox(Sub1QHB);
  Sub2QVB = new QVBox(Sub1QHB);
  
  ColumnQRB     = new QRadioButton(trUtf8("Use a table's co&lumn"), SubQBG);
  ExpressionQRB = new QRadioButton(trUtf8("Use an e&xpression"), SubQBG);
  
  ColumnQL  = new QLabel(trUtf8("Column"), Sub1QVB);
  ColumnQSB = new QSpinBoxBayani(1, 1, 1, Sub2QVB);
  
  ExpressionQL  = new QLabel(trUtf8("Expression"), Sub1QVB);
  ExpressionQLE = new QLineEdit(Sub2QVB);
  
  BinsNumberQCKB = new QCheckBox(trUtf8("Fix the number of &bins"), Sub1QVB);
  BinsNumberQLE  = new QLineEdit(Sub2QVB);
  
  XMinQCKB = new QCheckBox(trUtf8("Fix the mi&nimal value"), Sub1QVB);
  XMinQLE  = new QLineEdit(Sub2QVB);
  
  XMaxQCKB = new QCheckBox(trUtf8("Fix the maxi&mal value"), Sub1QVB);
  XMaxQLE  = new QLineEdit(Sub2QVB);
  
  CutQCKB = new QCheckBox(trUtf8("Use a c&ut to select rows"), Sub2QHB);
  CutQLE = new QLineEdit(Sub2QHB);
  
  SameQCKB = new QCheckBox(trUtf8("&Superimpose on the active graph"), General2QGB);
  
  PlotSettingsQPB = new QPushButton(trUtf8("&Graphical settings..."), General2QGB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub1QHB->setSpacing(5);
  Sub1QHB->setMargin(5);
  Sub2QHB->setSpacing(5);
  //Sub2QHB->setMargin(5);
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  
  ExpressionQLE->setMaximumWidth(200);
  BinsNumberQLE->setMaximumWidth(50);
  BinsNumberQLE->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{1,5}"), BinsNumberQLE));
  XMinQLE->setMaximumWidth(50);
  XMinQLE->setValidator(new QDoubleValidator(XMinQLE));
  XMaxQLE->setMaximumWidth(50);
  XMaxQLE->setValidator(new QDoubleValidator(XMaxQLE));
  CutQLE->setMaximumWidth(200);
  ColumnQSB->setMaximumWidth(50);
  
  PlotSettingsQPB->setFixedSize(PlotSettingsQPB->sizeHint());
  
  ColumnQRB    ->setChecked(true );
  ExpressionQL ->setEnabled(false);
  ExpressionQLE->setEnabled(false);
  BinsNumberQLE->setEnabled(false);
  XMinQLE      ->setEnabled(false);
  XMaxQLE      ->setEnabled(false);
  CutQLE       ->setEnabled(false);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setCaption(trUtf8("Graph - Histogram"));
  
  setFixedSize(sizeHint());
  
  ColumnQSB->setFocus();
  
  connect(ColumnQRB     , SIGNAL(toggled(bool)), ColumnQL     , SLOT(setEnabled(bool)));
  connect(ColumnQRB     , SIGNAL(toggled(bool)), ColumnQSB    , SLOT(setEnabled(bool)));
  connect(ColumnQRB     , SIGNAL(clicked()    ), ColumnQSB    , SLOT(setFocus()      ));
  connect(ColumnQRB     , SIGNAL(pressed()    ), ColumnQSB    , SLOT(clearFocus()    ));
  connect(ExpressionQRB , SIGNAL(toggled(bool)), ExpressionQL , SLOT(setEnabled(bool)));
  connect(ExpressionQRB , SIGNAL(toggled(bool)), ExpressionQLE, SLOT(setEnabled(bool)));
  connect(ExpressionQRB , SIGNAL(clicked()    ), ExpressionQLE, SLOT(setFocus()      ));
  connect(ExpressionQRB , SIGNAL(pressed()    ), ExpressionQLE, SLOT(clearFocus()    ));
  connect(BinsNumberQCKB, SIGNAL(toggled(bool)), BinsNumberQLE, SLOT(setEnabled(bool)));
  connect(BinsNumberQCKB, SIGNAL(clicked()    ), BinsNumberQLE, SLOT(setFocus()      ));
  connect(BinsNumberQCKB, SIGNAL(pressed()    ), BinsNumberQLE, SLOT(clearFocus()    ));
  connect(XMinQCKB      , SIGNAL(toggled(bool)), XMinQLE      , SLOT(setEnabled(bool)));
  connect(XMinQCKB      , SIGNAL(clicked()    ), XMinQLE      , SLOT(setFocus()      ));
  connect(XMinQCKB      , SIGNAL(pressed()    ), XMinQLE      , SLOT(clearFocus()    ));
  connect(XMaxQCKB      , SIGNAL(toggled(bool)), XMaxQLE      , SLOT(setEnabled(bool)));
  connect(XMaxQCKB      , SIGNAL(clicked()    ), XMaxQLE      , SLOT(setFocus()      ));
  connect(XMaxQCKB      , SIGNAL(pressed()    ), XMaxQLE      , SLOT(clearFocus()    ));
  connect(CutQCKB       , SIGNAL(toggled(bool)), CutQLE       , SLOT(setEnabled(bool)));
  connect(CutQCKB       , SIGNAL(clicked()    ), CutQLE       , SLOT(setFocus()      ));
  connect(CutQCKB       , SIGNAL(pressed()    ), CutQLE       , SLOT(clearFocus()    ));
}

QTabDialogGraphHistogram::~QTabDialogGraphHistogram()
{
}

QSpinBoxBayani * QTabDialogGraphHistogram::GetColumnQSB()
{
  return ColumnQSB;
}

QLineEdit * QTabDialogGraphHistogram::GetBinsNumberQLE()
{
  return BinsNumberQLE;
}

QLineEdit * QTabDialogGraphHistogram::GetXMinQLE()
{
  return XMinQLE;
}

QLineEdit * QTabDialogGraphHistogram::GetXMaxQLE()
{
  return XMaxQLE;
}

QLineEdit * QTabDialogGraphHistogram::GetExpressionQLE()
{
  return ExpressionQLE;
}

QLineEdit * QTabDialogGraphHistogram::GetCutQLE()
{
  return CutQLE;
}

QPushButton * QTabDialogGraphHistogram::GetPlotSettingsQPB()
{
  return PlotSettingsQPB;
}

QCheckBox * QTabDialogGraphHistogram::GetBinsNumberQCKB()
{
  return BinsNumberQCKB;
}

QCheckBox * QTabDialogGraphHistogram::GetXMinQCKB()
{
  return XMinQCKB;
}

QCheckBox * QTabDialogGraphHistogram::GetXMaxQCKB()
{
  return XMaxQCKB;
}

QCheckBox * QTabDialogGraphHistogram::GetCutQCKB()
{
  return CutQCKB;
}

QCheckBox * QTabDialogGraphHistogram::GetSameQCKB()
{
  return SameQCKB;
}

QRadioButton * QTabDialogGraphHistogram::GetColumnQRB()
{
  return ColumnQRB;
}

QRadioButton * QTabDialogGraphHistogram::GetExpressionQRB()
{
  return ExpressionQRB;
}

void QTabDialogGraphHistogram::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      ColumnQSB->GetEditor()->setAlignment(Qt::AlignRight);
      ExpressionQLE->setAlignment(Qt::AlignRight);
      CutQLE->setAlignment(Qt::AlignRight);
      BinsNumberQLE->setAlignment(Qt::AlignRight);
      XMinQLE->setAlignment(Qt::AlignRight);
      XMaxQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      ColumnQSB->GetEditor()->setAlignment(Qt::AlignLeft);
      ExpressionQLE->setAlignment(Qt::AlignLeft);
      CutQLE->setAlignment(Qt::AlignLeft);
      BinsNumberQLE->setAlignment(Qt::AlignLeft);
      XMinQLE->setAlignment(Qt::AlignLeft);
      XMaxQLE->setAlignment(Qt::AlignLeft);
    }
}
