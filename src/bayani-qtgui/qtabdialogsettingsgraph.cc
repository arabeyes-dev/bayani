/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogSettingsGraph Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogsettingsgraph.h>

QTabDialogSettingsGraph::QTabDialogSettingsGraph(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  setCaption(trUtf8("Settings - Graph"));
  
  setOKButton     (trUtf8("&OK"      ));
  setApplyButton  (trUtf8("&Apply"   ));
  setDefaultButton(trUtf8("&Defaults"));
  setCancelButton (trUtf8("&Cancel"  ));
  
  /* Dimensions */
  
  DimensionsQVB = new QVBox(Parent);
  DimensionsQVB->setSpacing(5);
  DimensionsQVB->setMargin(5);
  
  addTab(DimensionsQVB, trUtf8("Di&mensions"));
  
  Dimensions1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Relative Origin's Coordinates"), DimensionsQVB);
  
  Dimensions11QVB = new QVBox(Dimensions1QGB);
  Dimensions11QVB->setSpacing(5);
  Dimensions11QVB->setMargin(5);
  
  XRatioQL = new QLabel(trUtf8("A&bcissa"), Dimensions11QVB);
  YRatioQL = new QLabel(trUtf8("O&rdinate"), Dimensions11QVB);
  
  Dimensions12QVB = new QVBox(Dimensions1QGB);
  Dimensions12QVB->setSpacing(5);
  Dimensions12QVB->setMargin(5);
  
  XRatioQLE = new QLineEdit(Dimensions12QVB);
  XRatioQLE->setMaximumWidth(50);
  XRatioQLE->setValidator(new QDoubleValidator(XRatioQLE));
  
  YRatioQLE = new QLineEdit(Dimensions12QVB);
  YRatioQLE->setMaximumWidth(50);
  YRatioQLE->setValidator(new QDoubleValidator(YRatioQLE));
  
  Dimensions2QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Relative Width and Height"), DimensionsQVB);
  
  Dimensions21QVB = new QVBox(Dimensions2QGB);
  Dimensions21QVB->setSpacing(5);
  Dimensions21QVB->setMargin(5);
  
  WidthRatioQL  = new QLabel(trUtf8("&Width"), Dimensions21QVB);
  HeightRatioQL = new QLabel(trUtf8("&Height"), Dimensions21QVB);
  
  Dimensions22QVB = new QVBox(Dimensions2QGB);
  Dimensions22QVB->setSpacing(5);
  Dimensions22QVB->setMargin(5);
  
  WidthRatioQLE = new QLineEdit(Dimensions22QVB);
  WidthRatioQLE->setMaximumWidth(50);
  WidthRatioQLE->setValidator(new QDoubleValidator(WidthRatioQLE));
  
  HeightRatioQLE = new QLineEdit(Dimensions22QVB);
  HeightRatioQLE->setMaximumWidth(50);
  HeightRatioQLE->setValidator(new QDoubleValidator(HeightRatioQLE));
  
  Dimensions3QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Relative Distances"), DimensionsQVB);
  
  Dimensions31QVB = new QVBox(Dimensions3QGB);
  Dimensions31QVB->setSpacing(5);
  Dimensions31QVB->setMargin(5);
  
  Axis2TitleRatioQL        = new QLabel(trUtf8("Between axis and titl&e"), Dimensions31QVB);
  XAxis2TicksLabelsRatioQL = new QLabel(trUtf8("Between &x axis and digits"), Dimensions31QVB);
  YAxis2TicksLabelsRatioQL = new QLabel(trUtf8("Between &y axis and digits"), Dimensions31QVB);
  XAxis2AxisLabelRatioQL   = new QLabel(trUtf8("Between x ax&is and label"), Dimensions31QVB);
  YAxis2AxisLabelRatioQL   = new QLabel(trUtf8("Between y ax&is and label"), Dimensions31QVB);
  
  Dimensions32QVB = new QVBox(Dimensions3QGB);
  Dimensions32QVB->setSpacing(5);
  Dimensions32QVB->setMargin(5);
  
  Axis2TitleRatioQLE = new QLineEdit(Dimensions32QVB);
  Axis2TitleRatioQLE->setMaximumWidth(50);
  Axis2TitleRatioQLE->setValidator(new QDoubleValidator(Axis2TitleRatioQLE));
  
  XAxis2TicksLabelsRatioQLE = new QLineEdit(Dimensions32QVB);
  XAxis2TicksLabelsRatioQLE->setMaximumWidth(50);
  XAxis2TicksLabelsRatioQLE->setValidator(new QDoubleValidator(XAxis2TicksLabelsRatioQLE));
  
  YAxis2TicksLabelsRatioQLE = new QLineEdit(Dimensions32QVB);
  YAxis2TicksLabelsRatioQLE->setMaximumWidth(50);
  YAxis2TicksLabelsRatioQLE->setValidator(new QDoubleValidator(YAxis2TicksLabelsRatioQLE));
  
  XAxis2AxisLabelRatioQLE = new QLineEdit(Dimensions32QVB);
  XAxis2AxisLabelRatioQLE->setMaximumWidth(50);
  XAxis2AxisLabelRatioQLE->setValidator(new QDoubleValidator(XAxis2AxisLabelRatioQLE));
  
  YAxis2AxisLabelRatioQLE = new QLineEdit(Dimensions32QVB);
  YAxis2AxisLabelRatioQLE->setMaximumWidth(50);
  YAxis2AxisLabelRatioQLE->setValidator(new QDoubleValidator(YAxis2AxisLabelRatioQLE));
  
  Dimensions4QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Indicators' Length"), DimensionsQVB);
  
  Dimensions41QVB = new QVBox(Dimensions4QGB);
  Dimensions41QVB->setSpacing(5);
  Dimensions41QVB->setMargin(5);
  
  BigXTicksRatioQL   = new QLabel(trUtf8("X axis bi&g indicators"), Dimensions41QVB);
  BigYTicksRatioQL   = new QLabel(trUtf8("Y axis bi&g indicators"), Dimensions41QVB);
  SmallXTicksRatioQL = new QLabel(trUtf8("X axis &small indicators"), Dimensions41QVB);
  SmallYTicksRatioQL = new QLabel(trUtf8("Y axis &small indicators"), Dimensions41QVB);
  
  Dimensions42QVB = new QVBox(Dimensions4QGB);
  Dimensions42QVB->setSpacing(5);
  Dimensions42QVB->setMargin(5);
  
  BigXTicksRatioQLE = new QLineEdit(Dimensions42QVB);
  BigXTicksRatioQLE->setMaximumWidth(50);
  BigXTicksRatioQLE->setValidator(new QDoubleValidator(BigXTicksRatioQLE));
  
  BigYTicksRatioQLE = new QLineEdit(Dimensions42QVB);
  BigYTicksRatioQLE->setMaximumWidth(50);
  BigYTicksRatioQLE->setValidator(new QDoubleValidator(BigYTicksRatioQLE));
  
  SmallXTicksRatioQLE = new QLineEdit(Dimensions42QVB);
  SmallXTicksRatioQLE->setMaximumWidth(50);
  SmallXTicksRatioQLE->setValidator(new QDoubleValidator(SmallXTicksRatioQLE));
  
  SmallYTicksRatioQLE = new QLineEdit(Dimensions42QVB);
  SmallYTicksRatioQLE->setMaximumWidth(50);
  SmallYTicksRatioQLE->setValidator(new QDoubleValidator(SmallYTicksRatioQLE));
  
  XRatioQL                ->setBuddy(XRatioQLE                );
  YRatioQL                ->setBuddy(YRatioQLE                );
  WidthRatioQL            ->setBuddy(WidthRatioQLE            );
  HeightRatioQL           ->setBuddy(HeightRatioQLE           );
  Axis2TitleRatioQL       ->setBuddy(Axis2TitleRatioQLE       );
  XAxis2TicksLabelsRatioQL->setBuddy(XAxis2TicksLabelsRatioQLE);
  YAxis2TicksLabelsRatioQL->setBuddy(YAxis2TicksLabelsRatioQLE);
  XAxis2AxisLabelRatioQL  ->setBuddy(XAxis2AxisLabelRatioQLE  );
  YAxis2AxisLabelRatioQL  ->setBuddy(YAxis2AxisLabelRatioQLE  );
  BigXTicksRatioQL        ->setBuddy(BigXTicksRatioQLE        );
  BigYTicksRatioQL        ->setBuddy(BigYTicksRatioQLE        );
  SmallXTicksRatioQL      ->setBuddy(SmallXTicksRatioQLE      );
  SmallYTicksRatioQL      ->setBuddy(SmallYTicksRatioQLE      );
  
  /* Lines */
  
  LinesQVB = new QVBox(Parent);
  LinesQVB->setSpacing(5);
  LinesQVB->setMargin(5);
  
  addTab(LinesQVB, trUtf8("&Lines"));
  
  Lines1QGB = new QGroupBox(4, Qt::Horizontal, trUtf8("Lines"), LinesQVB);
  
  Lines11QVB = new QVBox(Lines1QGB);
  Lines11QVB->setSpacing(5);
  Lines11QVB->setMargin(5);
  
  Lines12QVB = new QVBox(Lines1QGB);
  Lines12QVB->setSpacing(5);
  Lines12QVB->setMargin(5);
  
  Lines13QVB = new QVBox(Lines1QGB);
  Lines13QVB->setSpacing(5);
  Lines13QVB->setMargin(5);
  
  Lines14QVB = new QVBox(Lines1QGB);
  Lines14QVB->setSpacing(5);
  Lines14QVB->setMargin(5);
  
  BoxLineWidthQL  = new QLabel(trUtf8("&Box"), Lines11QVB);
  BoxLineWidthQLC = new QLineCombo(Lines12QVB);
  BoxLineStyleQPC = new QPenCombo(Lines13QVB);
  BoxColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  BigXTicksLineWidthQL  = new QLabel(trUtf8("&X axis big indicators"), Lines11QVB);
  BigXTicksLineWidthQLC = new QLineCombo(Lines12QVB);
  BigXTicksLineStyleQPC = new QPenCombo(Lines13QVB);
  BigXTicksColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  BigYTicksLineWidthQL  = new QLabel(trUtf8("&Y axis big indicators"), Lines11QVB);
  BigYTicksLineWidthQLC = new QLineCombo(Lines12QVB);
  BigYTicksLineStyleQPC = new QPenCombo(Lines13QVB);
  BigYTicksColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  SmallXTicksLineWidthQL  = new QLabel(trUtf8("X ax&is small indicators"), Lines11QVB);
  SmallXTicksLineWidthQLC = new QLineCombo(Lines12QVB);
  SmallXTicksLineStyleQPC = new QPenCombo(Lines13QVB);
  SmallXTicksColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  SmallYTicksLineWidthQL  = new QLabel(trUtf8("Y axi&s small indicators"), Lines11QVB);
  SmallYTicksLineWidthQLC = new QLineCombo(Lines12QVB);
  SmallYTicksLineStyleQPC = new QPenCombo(Lines13QVB);
  SmallYTicksColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  XGridLineWidthQL  = new QLabel(trUtf8("X axis &grid"), Lines11QVB);
  XGridLineWidthQLC = new QLineCombo(Lines12QVB);
  XGridLineStyleQPC = new QPenCombo(Lines13QVB);
  XGridColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  YGridLineWidthQL  = new QLabel(trUtf8("Y axis g&rid"), Lines11QVB);
  YGridLineWidthQLC = new QLineCombo(Lines12QVB);
  YGridLineStyleQPC = new QPenCombo(Lines13QVB);
  YGridColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  BoxLineWidthQL        ->setBuddy(BoxLineWidthQLC        );
  BigXTicksLineWidthQL  ->setBuddy(BigXTicksLineWidthQLC  );
  BigYTicksLineWidthQL  ->setBuddy(BigYTicksLineWidthQLC  );
  SmallXTicksLineWidthQL->setBuddy(SmallXTicksLineWidthQLC);
  SmallYTicksLineWidthQL->setBuddy(SmallYTicksLineWidthQLC);
  XGridLineWidthQL      ->setBuddy(XGridLineWidthQLC      );
  YGridLineWidthQL      ->setBuddy(YGridLineWidthQLC      );
  
  /* Texts */
  
  TextsQVB = new QVBox(Parent);
  TextsQVB->setSpacing(5);
  TextsQVB->setMargin(5);
  
  addTab(TextsQVB, trUtf8("&Texts"));
  
  Texts1QGB = new QGroupBox(3, Qt::Horizontal, trUtf8("Fonts"), TextsQVB);
  
  Texts11QVB = new QVBox(Texts1QGB);
  Texts11QVB->setSpacing(5);
  Texts11QVB->setMargin(5);
  
  Texts12QVB = new QVBox(Texts1QGB);
  Texts12QVB->setSpacing(5);
  Texts12QVB->setMargin(5);
  
  Texts13QVB = new QVBox(Texts1QGB);
  Texts13QVB->setSpacing(5);
  Texts13QVB->setMargin(5);
  
  TitleFontQL     = new QLabel(trUtf8("Titl&e"), Texts11QVB);
  TitleFontQFS    = new QFontSelector(Texts12QVB);
  TitleColorQPBCS = new QPushButtonColorSelect(Texts13QVB);
  
  XAxisLabelFontQL     = new QLabel(trUtf8("&X axis label"), Texts11QVB);
  XAxisLabelFontQFS    = new QFontSelector(Texts12QVB);
  XAxisLabelColorQPBCS = new QPushButtonColorSelect(Texts13QVB);
  
  YAxisLabelFontQL     = new QLabel(trUtf8("&Y axis label"), Texts11QVB);
  YAxisLabelFontQFS    = new QFontSelector(Texts12QVB);
  YAxisLabelColorQPBCS = new QPushButtonColorSelect(Texts13QVB);
  
  XTicksLabelsFontQL     = new QLabel(trUtf8("X ax&is digits"), Texts11QVB);
  XTicksLabelsFontQFS    = new QFontSelector(Texts12QVB);
  XTicksLabelsColorQPBCS = new QPushButtonColorSelect(Texts13QVB);
  
  YTicksLabelsFontQL     = new QLabel(trUtf8("Y axi&s digits"), Texts11QVB);
  YTicksLabelsFontQFS    = new QFontSelector(Texts12QVB);
  YTicksLabelsColorQPBCS = new QPushButtonColorSelect(Texts13QVB);
  
  TitleFontQL       ->setBuddy(TitleFontQFS       );
  XAxisLabelFontQL  ->setBuddy(XAxisLabelFontQFS  );
  YAxisLabelFontQL  ->setBuddy(YAxisLabelFontQFS  );
  XTicksLabelsFontQL->setBuddy(XTicksLabelsFontQFS);
  YTicksLabelsFontQL->setBuddy(YTicksLabelsFontQFS);
  
  /* Miscellaneous */
  
  MiscellaneousQVB = new QVBox(Parent);
  MiscellaneousQVB->setSpacing(5);
  MiscellaneousQVB->setMargin(5);
  
  addTab(MiscellaneousQVB, trUtf8("Miscella&neous"));
  
  Miscellaneous1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Background"), MiscellaneousQVB);
  
  Miscellaneous11QVB = new QVBox(Miscellaneous1QGB);
  Miscellaneous11QVB->setSpacing(5);
  Miscellaneous11QVB->setMargin(5);
  
  Miscellaneous12QVB = new QVBox(Miscellaneous1QGB);
  Miscellaneous12QVB->setSpacing(5);
  Miscellaneous12QVB->setMargin(5);
  
  BGColorQL    = new QLabel(trUtf8("Colo&r"), Miscellaneous11QVB);
  BGColorQPBCS = new QPushButtonColorSelect(Miscellaneous12QVB);
  
  BGColorQL->setBuddy(BGColorQPBCS);
  
  /* General */
  
  setFixedSize(sizeHint());
  
  XRatioQLE->setFocus();
}

QTabDialogSettingsGraph::~QTabDialogSettingsGraph()
{
}

QLineEdit * QTabDialogSettingsGraph::GetXRatioQLE()
{
  return XRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetYRatioQLE()
{
  return YRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetWidthRatioQLE()
{
  return WidthRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetHeightRatioQLE()
{
  return HeightRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetAxis2TitleRatioQLE()
{
  return Axis2TitleRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetBigXTicksRatioQLE()
{
  return BigXTicksRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetBigYTicksRatioQLE()
{
  return BigYTicksRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetSmallXTicksRatioQLE()
{
  return SmallXTicksRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetSmallYTicksRatioQLE()
{
  return SmallYTicksRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetXAxis2TicksLabelsRatioQLE()
{
  return XAxis2TicksLabelsRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetYAxis2TicksLabelsRatioQLE()
{
  return YAxis2TicksLabelsRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetXAxis2AxisLabelRatioQLE()
{
  return XAxis2AxisLabelRatioQLE;
}

QLineEdit * QTabDialogSettingsGraph::GetYAxis2AxisLabelRatioQLE()
{
  return YAxis2AxisLabelRatioQLE;
}

QLineCombo * QTabDialogSettingsGraph::GetBoxLineWidthQLC()
{
  return BoxLineWidthQLC;
}

QLineCombo * QTabDialogSettingsGraph::GetBigXTicksLineWidthQLC()
{
  return BigXTicksLineWidthQLC;
}

QLineCombo * QTabDialogSettingsGraph::GetBigYTicksLineWidthQLC()
{
  return BigYTicksLineWidthQLC;
}

QLineCombo * QTabDialogSettingsGraph::GetSmallXTicksLineWidthQLC()
{
  return SmallXTicksLineWidthQLC;
}

QLineCombo * QTabDialogSettingsGraph::GetSmallYTicksLineWidthQLC()
{
  return SmallYTicksLineWidthQLC;
}

QLineCombo * QTabDialogSettingsGraph::GetXGridLineWidthQLC()
{
  return XGridLineWidthQLC;
}

QLineCombo * QTabDialogSettingsGraph::GetYGridLineWidthQLC()
{
  return YGridLineWidthQLC;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetBGColorQPBCS()
{
  return BGColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetBoxColorQPBCS()
{
  return BoxColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetBigXTicksColorQPBCS()
{
  return BigXTicksColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetBigYTicksColorQPBCS()
{
  return BigYTicksColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetSmallXTicksColorQPBCS()
{
  return SmallXTicksColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetSmallYTicksColorQPBCS()
{
  return SmallYTicksColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetXTicksLabelsColorQPBCS()
{
  return XTicksLabelsColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetYTicksLabelsColorQPBCS()
{
  return YTicksLabelsColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetXAxisLabelColorQPBCS()
{
  return XAxisLabelColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetYAxisLabelColorQPBCS()
{
  return YAxisLabelColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetTitleColorQPBCS()
{
  return TitleColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetXGridColorQPBCS()
{
  return XGridColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsGraph::GetYGridColorQPBCS()
{
  return YGridColorQPBCS;
}

QFontSelector * QTabDialogSettingsGraph::GetXAxisLabelFontQFS()
{
  return XAxisLabelFontQFS;
}

QFontSelector * QTabDialogSettingsGraph::GetYAxisLabelFontQFS()
{
  return YAxisLabelFontQFS;
}

QFontSelector * QTabDialogSettingsGraph::GetXTicksLabelsFontQFS()
{
  return XTicksLabelsFontQFS;
}

QFontSelector * QTabDialogSettingsGraph::GetYTicksLabelsFontQFS()
{
  return YTicksLabelsFontQFS;
}

QFontSelector * QTabDialogSettingsGraph::GetTitleFontQFS()
{
  return TitleFontQFS;
}

QPenCombo * QTabDialogSettingsGraph::GetBoxLineStyleQPC()
{
  return BoxLineStyleQPC;
}

QPenCombo * QTabDialogSettingsGraph::GetBigXTicksLineStyleQPC()
{
  return BigXTicksLineStyleQPC;
}

QPenCombo * QTabDialogSettingsGraph::GetBigYTicksLineStyleQPC()
{
  return BigYTicksLineStyleQPC;
}

QPenCombo * QTabDialogSettingsGraph::GetSmallXTicksLineStyleQPC()
{
  return SmallXTicksLineStyleQPC;
}

QPenCombo * QTabDialogSettingsGraph::GetSmallYTicksLineStyleQPC()
{
  return SmallYTicksLineStyleQPC;
}

QPenCombo * QTabDialogSettingsGraph::GetXGridLineStyleQPC()
{
  return XGridLineStyleQPC;
}

QPenCombo * QTabDialogSettingsGraph::GetYGridLineStyleQPC()
{
  return YGridLineStyleQPC;
}

void QTabDialogSettingsGraph::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      XRatioQLE->setAlignment(Qt::AlignRight);
      YRatioQLE->setAlignment(Qt::AlignRight);
      WidthRatioQLE->setAlignment(Qt::AlignRight);
      HeightRatioQLE->setAlignment(Qt::AlignRight);
      Axis2TitleRatioQLE->setAlignment(Qt::AlignRight);
      BigXTicksRatioQLE->setAlignment(Qt::AlignRight);
      BigYTicksRatioQLE->setAlignment(Qt::AlignRight);
      SmallXTicksRatioQLE->setAlignment(Qt::AlignRight);
      SmallYTicksRatioQLE->setAlignment(Qt::AlignRight);
      XAxis2TicksLabelsRatioQLE->setAlignment(Qt::AlignRight);
      YAxis2TicksLabelsRatioQLE->setAlignment(Qt::AlignRight);
      XAxis2AxisLabelRatioQLE->setAlignment(Qt::AlignRight);
      YAxis2AxisLabelRatioQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      XRatioQLE->setAlignment(Qt::AlignLeft);
      YRatioQLE->setAlignment(Qt::AlignLeft);
      WidthRatioQLE->setAlignment(Qt::AlignLeft);
      HeightRatioQLE->setAlignment(Qt::AlignLeft);
      Axis2TitleRatioQLE->setAlignment(Qt::AlignLeft);
      BigXTicksRatioQLE->setAlignment(Qt::AlignLeft);
      BigYTicksRatioQLE->setAlignment(Qt::AlignLeft);
      SmallXTicksRatioQLE->setAlignment(Qt::AlignLeft);
      SmallYTicksRatioQLE->setAlignment(Qt::AlignLeft);
      XAxis2TicksLabelsRatioQLE->setAlignment(Qt::AlignLeft);
      YAxis2TicksLabelsRatioQLE->setAlignment(Qt::AlignLeft);
      XAxis2AxisLabelRatioQLE->setAlignment(Qt::AlignLeft);
      YAxis2AxisLabelRatioQLE->setAlignment(Qt::AlignLeft);
    }
}
