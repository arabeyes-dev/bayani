/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogSettingsFrame Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogsettingsframe.h>

QTabDialogSettingsFrame::QTabDialogSettingsFrame(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  setCaption(trUtf8("Settings - Frame"));
  
  setOKButton     (trUtf8("&OK"      ));
  setApplyButton  (trUtf8("&Apply"   ));
  setDefaultButton(trUtf8("&Defaults"));
  setCancelButton (trUtf8("&Cancel"  ));
  
  /* Dimensions */
  
  DimensionsQVB = new QVBox(Parent);
  DimensionsQVB->setSpacing(5);
  DimensionsQVB->setMargin(5);
  
  addTab(DimensionsQVB, trUtf8("Di&mensions"));
  
  Dimensions1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Relative Origin's Coordinates"), DimensionsQVB);
  
  Dimensions11QVB = new QVBox(Dimensions1QGB);
  Dimensions11QVB->setSpacing(5);
  Dimensions11QVB->setMargin(5);
  
  XRatioQL = new QLabel(trUtf8("A&bcissa"), Dimensions11QVB);
  YRatioQL = new QLabel(trUtf8("O&rdinate"), Dimensions11QVB);
  
  Dimensions12QVB = new QVBox(Dimensions1QGB);
  Dimensions12QVB->setSpacing(5);
  Dimensions12QVB->setMargin(5);
  
  XRatioQLE = new QLineEdit(Dimensions12QVB);
  XRatioQLE->setMaximumWidth(50);
  XRatioQLE->setValidator(new QDoubleValidator(XRatioQLE));
  
  YRatioQLE = new QLineEdit(Dimensions12QVB);
  YRatioQLE->setMaximumWidth(50);
  YRatioQLE->setValidator(new QDoubleValidator(YRatioQLE));
  
  Dimensions2QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Relative Width and Height"), DimensionsQVB);
  
  Dimensions21QVB = new QVBox(Dimensions2QGB);
  Dimensions21QVB->setSpacing(5);
  Dimensions21QVB->setMargin(5);
  
  WidthRatioQL  = new QLabel(trUtf8("&Width"), Dimensions21QVB);
  HeightRatioQL = new QLabel(trUtf8("&Height"), Dimensions21QVB);
  
  Dimensions22QVB = new QVBox(Dimensions2QGB);
  Dimensions22QVB->setSpacing(5);
  Dimensions22QVB->setMargin(5);
  
  WidthRatioQLE = new QLineEdit(Dimensions22QVB);
  WidthRatioQLE->setMaximumWidth(50);
  WidthRatioQLE->setValidator(new QDoubleValidator(WidthRatioQLE));
  
  HeightRatioQLE = new QLineEdit(Dimensions22QVB);
  HeightRatioQLE->setMaximumWidth(50);
  HeightRatioQLE->setValidator(new QDoubleValidator(HeightRatioQLE));
  
  Dimensions3QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Relative Distances"), DimensionsQVB);
  
  Dimensions31QVB = new QVBox(Dimensions3QGB);
  Dimensions31QVB->setSpacing(5);
  Dimensions31QVB->setMargin(5);
  
  Box2TitleRatioQL = new QLabel(trUtf8("Between bo&x and title"), Dimensions31QVB);
  
  Dimensions32QVB = new QVBox(Dimensions3QGB);
  Dimensions32QVB->setSpacing(5);
  Dimensions32QVB->setMargin(5);
  
  Box2TitleRatioQLE = new QLineEdit(Dimensions32QVB);
  Box2TitleRatioQLE->setMaximumWidth(50);
  Box2TitleRatioQLE->setValidator(new QDoubleValidator(Box2TitleRatioQLE));
  
  XRatioQL        ->setBuddy(XRatioQLE        );
  YRatioQL        ->setBuddy(YRatioQLE        );
  WidthRatioQL    ->setBuddy(WidthRatioQLE    );
  HeightRatioQL   ->setBuddy(HeightRatioQLE   );
  Box2TitleRatioQL->setBuddy(Box2TitleRatioQLE);
  
  /* Lines  */
  
  LinesQVB = new QVBox(Parent);
  LinesQVB->setSpacing(5);
  LinesQVB->setMargin(5);
  
  addTab(LinesQVB, trUtf8("&Lines"));
  
  Lines1QGB = new QGroupBox(4, Qt::Horizontal, trUtf8("Lines"), LinesQVB);
  
  Lines11QVB = new QVBox(Lines1QGB);
  Lines11QVB->setSpacing(5);
  Lines11QVB->setMargin(5);
  
  Lines12QVB = new QVBox(Lines1QGB);
  Lines12QVB->setSpacing(5);
  Lines12QVB->setMargin(5);
  
  Lines13QVB = new QVBox(Lines1QGB);
  Lines13QVB->setSpacing(5);
  Lines13QVB->setMargin(5);
  
  Lines14QVB = new QVBox(Lines1QGB);
  Lines14QVB->setSpacing(5);
  Lines14QVB->setMargin(5);
  
  BoxLineWidthQL  = new QLabel(trUtf8("&Box"                  ), Lines11QVB);
  BoxLineWidthQLC = new QLineCombo(Lines12QVB);
  BoxLineStyleQPC = new QPenCombo(Lines13QVB);
  BoxColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  HSepLineWidthQL  = new QLabel(trUtf8("&Horizontal separators"), Lines11QVB);
  HSepLineWidthQLC = new QLineCombo(Lines12QVB);
  HSepLineStyleQPC = new QPenCombo(Lines13QVB);
  HSepColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  VSepLineWidthQL  = new QLabel(trUtf8("&Vertical separators"  ), Lines11QVB);
  VSepLineWidthQLC = new QLineCombo(Lines12QVB);
  VSepLineStyleQPC = new QPenCombo(Lines13QVB);
  VSepColorQPBCS   = new QPushButtonColorSelect(Lines14QVB);
  
  BoxLineWidthQL ->setBuddy(BoxLineWidthQLC );
  HSepLineWidthQL->setBuddy(HSepLineWidthQLC);
  VSepLineWidthQL->setBuddy(VSepLineWidthQLC);
  
  /* Texts */
  
  TextsQVB = new QVBox(Parent);
  TextsQVB->setSpacing(5);
  TextsQVB->setMargin(5);
  
  addTab(TextsQVB, trUtf8("&Texts"));
   
  Texts1QGB = new QGroupBox(3, Qt::Horizontal, trUtf8("Fonts"), TextsQVB);
  
  Texts11QVB = new QVBox(Texts1QGB);
  Texts11QVB->setSpacing(5);
  Texts11QVB->setMargin(5);
  
  Texts12QVB = new QVBox(Texts1QGB);
  Texts12QVB->setSpacing(5);
  Texts12QVB->setMargin(5);
  
  Texts13QVB = new QVBox(Texts1QGB);
  Texts13QVB->setSpacing(5);
  Texts13QVB->setMargin(5);
  
  TitleFontQL     = new QLabel(trUtf8("Titl&e"), Texts11QVB);
  TitleFontQFS    = new QFontSelector(Texts12QVB);
  TitleColorQPBCS = new QPushButtonColorSelect(Texts13QVB);
  
  TitleFontQL->setBuddy(TitleFontQFS);
  
  /* Miscellaneous */
  
  MiscellaneousQVB = new QVBox(Parent);
  MiscellaneousQVB->setSpacing(5);
  MiscellaneousQVB->setMargin(5);
  
  addTab(MiscellaneousQVB, trUtf8("Miscella&neous"));
  
  Miscellaneous1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Background"), MiscellaneousQVB);
  
  Miscellaneous11QVB = new QVBox(Miscellaneous1QGB);
  Miscellaneous11QVB->setSpacing(5);
  Miscellaneous11QVB->setMargin(5);
  
  BGColorQL = new QLabel(trUtf8("Colo&r"), Miscellaneous11QVB);
  
  Miscellaneous12QVB = new QVBox(Miscellaneous1QGB);
  Miscellaneous12QVB->setSpacing(5);
  Miscellaneous12QVB->setMargin(5);
  
  BGColorQPBCS = new QPushButtonColorSelect(Miscellaneous12QVB);
  
  BGColorQL->setBuddy(BGColorQPBCS);
  
  /* General */
  
  setFixedSize(sizeHint());
  
  XRatioQLE->setFocus();
}

QTabDialogSettingsFrame::~QTabDialogSettingsFrame()
{
}

QLineEdit * QTabDialogSettingsFrame::GetXRatioQLE()
{
  return XRatioQLE;
}

QLineEdit * QTabDialogSettingsFrame::GetYRatioQLE()
{
  return YRatioQLE;
}

QLineEdit * QTabDialogSettingsFrame::GetWidthRatioQLE()
{
  return WidthRatioQLE;
}

QLineEdit * QTabDialogSettingsFrame::GetHeightRatioQLE()
{
  return HeightRatioQLE;
}

QLineEdit * QTabDialogSettingsFrame::GetBox2TitleRatioQLE()
{
  return Box2TitleRatioQLE;
}

QLineCombo * QTabDialogSettingsFrame::GetBoxLineWidthQLC()
{
  return BoxLineWidthQLC;
}

QLineCombo * QTabDialogSettingsFrame::GetHSepLineWidthQLC()
{
  return HSepLineWidthQLC;
}

QLineCombo * QTabDialogSettingsFrame::GetVSepLineWidthQLC()
{
  return VSepLineWidthQLC;
}

QPushButtonColorSelect * QTabDialogSettingsFrame::GetBGColorQPBCS()
{
  return BGColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsFrame::GetTitleColorQPBCS()
{
  return TitleColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsFrame::GetBoxColorQPBCS()
{
  return BoxColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsFrame::GetHSepColorQPBCS()
{
  return HSepColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsFrame::GetVSepColorQPBCS()
{
  return VSepColorQPBCS;
}

QFontSelector * QTabDialogSettingsFrame::GetTitleFontQFS()
{
  return TitleFontQFS;
}

QPenCombo * QTabDialogSettingsFrame::GetBoxLineStyleQPC()
{
  return BoxLineStyleQPC;
}

QPenCombo * QTabDialogSettingsFrame::GetHSepLineStyleQPC()
{
  return HSepLineStyleQPC;
}

QPenCombo * QTabDialogSettingsFrame::GetVSepLineStyleQPC()
{
  return VSepLineStyleQPC;
}

void QTabDialogSettingsFrame::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      XRatioQLE->setAlignment(Qt::AlignRight);
      YRatioQLE->setAlignment(Qt::AlignRight);
      WidthRatioQLE->setAlignment(Qt::AlignRight);
      HeightRatioQLE->setAlignment(Qt::AlignRight);
      Box2TitleRatioQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      XRatioQLE->setAlignment(Qt::AlignLeft);
      YRatioQLE->setAlignment(Qt::AlignLeft);
      WidthRatioQLE->setAlignment(Qt::AlignLeft);
      HeightRatioQLE->setAlignment(Qt::AlignLeft);
      Box2TitleRatioQLE->setAlignment(Qt::AlignLeft);
    }
}
