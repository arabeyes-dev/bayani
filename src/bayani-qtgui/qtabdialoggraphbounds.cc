/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogGraphBounds Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialoggraphbounds.h>

QTabDialogGraphBounds::QTabDialogGraphBounds(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB  = new QVBox(Parent);
  GeneralXQGB = new QGroupBox(3, Qt::Horizontal, trUtf8("X Axis"), GeneralQVB);
  GeneralYQGB = new QGroupBox(3, Qt::Horizontal, trUtf8("Y Axis"), GeneralQVB);
  
  Sub1QVB = new QVBox(GeneralXQGB);
  Sub2QVB = new QVBox(GeneralXQGB);
  Sub3QVB = new QVBox(GeneralYQGB);
  Sub4QVB = new QVBox(GeneralYQGB);
  
  XInfQCKB = new QCheckBox(trUtf8("Fix the &inferior bound"), Sub1QVB);
  XInfQLE  = new QLineEdit(Sub2QVB);
  
  XSupQCKB = new QCheckBox(trUtf8("Fix the &superior bound"), Sub1QVB);
  XSupQLE  = new QLineEdit(Sub2QVB);
  
  YInfQCKB = new QCheckBox(trUtf8("Fix the in&ferior bound"), Sub3QVB);
  YInfQLE  = new QLineEdit(Sub4QVB);
  
  YSupQCKB = new QCheckBox(trUtf8("Fix the su&perior bound"), Sub3QVB);
  YSupQLE  = new QLineEdit(Sub4QVB);
  
  NewQCKB = new QCheckBox(trUtf8("Also apply to &new graphs"), GeneralQVB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  Sub3QVB->setSpacing(5);
  Sub3QVB->setMargin(5);
  Sub4QVB->setSpacing(5);
  Sub4QVB->setMargin(5);
  
  XInfQLE->setMaximumWidth(50);
  XInfQLE->setValidator(new QDoubleValidator(XInfQLE));
  XSupQLE->setMaximumWidth(50);
  XSupQLE->setValidator(new QDoubleValidator(XSupQLE));
  YInfQLE->setMaximumWidth(50);
  YInfQLE->setValidator(new QDoubleValidator(YInfQLE));
  YSupQLE->setMaximumWidth(50);
  YSupQLE->setValidator(new QDoubleValidator(YSupQLE));
  
  XInfQLE->setEnabled(false);
  XSupQLE->setEnabled(false);
  YInfQLE->setEnabled(false);
  YSupQLE->setEnabled(false);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Graph - Set Bounds"));
  
  XInfQLE->setFocus();
  
  connect(XInfQCKB, SIGNAL(toggled(bool)), XInfQLE, SLOT(setEnabled(bool)));
  connect(XInfQCKB, SIGNAL(clicked()    ), XInfQLE, SLOT(setFocus()      ));
  connect(XInfQCKB, SIGNAL(pressed()    ), XInfQLE, SLOT(clearFocus()    ));
  connect(XSupQCKB, SIGNAL(toggled(bool)), XSupQLE, SLOT(setEnabled(bool)));
  connect(XSupQCKB, SIGNAL(clicked()    ), XSupQLE, SLOT(setFocus()      ));
  connect(XSupQCKB, SIGNAL(pressed()    ), XSupQLE, SLOT(clearFocus()    ));
  connect(YInfQCKB, SIGNAL(toggled(bool)), YInfQLE, SLOT(setEnabled(bool)));
  connect(YInfQCKB, SIGNAL(clicked()    ), YInfQLE, SLOT(setFocus()      ));
  connect(YInfQCKB, SIGNAL(pressed()    ), YInfQLE, SLOT(clearFocus()    ));
  connect(YSupQCKB, SIGNAL(toggled(bool)), YSupQLE, SLOT(setEnabled(bool)));
  connect(YSupQCKB, SIGNAL(clicked()    ), YSupQLE, SLOT(setFocus()      ));
  connect(YSupQCKB, SIGNAL(pressed()    ), YSupQLE, SLOT(clearFocus()    ));
}

QTabDialogGraphBounds::~QTabDialogGraphBounds()
{
}

QLineEdit * QTabDialogGraphBounds::GetXInfQLE()
{
  return XInfQLE;
}

QLineEdit * QTabDialogGraphBounds::GetXSupQLE()
{
  return XSupQLE;
}

QLineEdit * QTabDialogGraphBounds::GetYInfQLE()
{
  return YInfQLE;
}

QLineEdit * QTabDialogGraphBounds::GetYSupQLE()
{
  return YSupQLE;
}

QCheckBox * QTabDialogGraphBounds::GetXInfQCKB()
{
  return XInfQCKB;
}

QCheckBox * QTabDialogGraphBounds::GetXSupQCKB()
{
  return XSupQCKB;
}

QCheckBox * QTabDialogGraphBounds::GetYInfQCKB()
{
  return YInfQCKB;
}

QCheckBox * QTabDialogGraphBounds::GetYSupQCKB()
{
  return YSupQCKB;
}

QCheckBox * QTabDialogGraphBounds::GetNewQCKB()
{
  return NewQCKB;
}

void QTabDialogGraphBounds::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      XInfQLE->setAlignment(Qt::AlignRight);
      XSupQLE->setAlignment(Qt::AlignRight);
      YInfQLE->setAlignment(Qt::AlignRight);
      YSupQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      XInfQLE->setAlignment(Qt::AlignLeft);
      XSupQLE->setAlignment(Qt::AlignLeft);
      YInfQLE->setAlignment(Qt::AlignLeft);
      YSupQLE->setAlignment(Qt::AlignLeft);
    }
}
