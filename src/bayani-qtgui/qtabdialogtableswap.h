/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogTableSwap Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGTABLESWAP_H
#define BAYANI_QTABDIALOGTABLESWAP_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qradiobutton.h>
#include <qspinboxbayani.h>
#include <qcheckbox.h>
#include <qbuttongroup.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialogtableswap.h
  \brief Header file for the QTabDialogTableSwap class.
  
  The QTabDialogTableSwap class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to swap elements in the table.
*/
class QTabDialogTableSwap: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel         * Element1QL;
  QLabel         * Element2QL;
  QRadioButton   * RowsQRB;
  QRadioButton   * ColumnsQRB;
  QSpinBoxBayani * Element1QSB;
  QSpinBoxBayani * Element2QSB;
  QCheckBox      * CopyQCKB;
  QButtonGroup   * GeneralQBG;
  QGroupBox      * GeneralQGB;
  QVBox          * GeneralQVB;
  QVBox          * Sub11QVB;
  QVBox          * Sub12QVB;
  
 public:
                           QTabDialogTableSwap(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogTableSwap();
          QRadioButton   * GetRowsQRB         ();
	  QRadioButton   * GetColumnsQRB      ();
	  QSpinBoxBayani * GetElement1QSB     ();
	  QSpinBoxBayani * GetElement2QSB     ();
	  QCheckBox      * GetCopyQCKB        ();
	  void             SetR2L             (bool);
};

#endif
