/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the BHelpBrowser Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <bhelpbrowser.h>
#include <icons.h>

BHelpBrowser::BHelpBrowser(QWidget * Parent, const char * Name)
  : QMainWindow(Parent, Name)
{
  FileQuitQA   = new QAction(QIconSet(QPixmap(FileQuitXPM  )), trUtf8("&Quit"   ), CTRL + Key_Q   , this);
  GoBackwardQA = new QAction(QIconSet(QPixmap(GoBackwardXPM)), trUtf8("&Back"   ), ALT + Key_Left , this);
  GoForwardQA  = new QAction(QIconSet(QPixmap(GoForwardXPM )), trUtf8("&Forward"), ALT + Key_Right, this);
  GoHomeQA     = new QAction(QIconSet(QPixmap(GoHomeXPM    )), trUtf8("&Home"   ), ALT + Key_Home , this);
  
  FileMenuQPM = new QPopupMenu(this);
  GoMenuQPM   = new QPopupMenu(this);
  
  menuBar()->insertItem(trUtf8("&File"), FileMenuQPM);
  menuBar()->insertItem(trUtf8("&Go"  ), GoMenuQPM  );
  
  FileQuitQA->addTo(FileMenuQPM);
  
  GoBackwardQA->addTo(GoMenuQPM);
  GoForwardQA ->addTo(GoMenuQPM);
  GoMenuQPM->insertSeparator();
  GoHomeQA    ->addTo(GoMenuQPM);
  
  ToolBarQTB = new QToolBar(trUtf8("Help Toolbar"), this);
  
  GoBackwardQA->addTo(ToolBarQTB);
  GoForwardQA ->addTo(ToolBarQTB);
  GoHomeQA    ->addTo(ToolBarQTB);
  
  TextBrowserBTB = new BTextBrowser(this         );
  DockWindowQDW  = new QDockWindow (this         );
  TabWidgetQTW   = new QTabWidget  (DockWindowQDW);
  ContentsQLV    = new QListView   (TabWidgetQTW );
  //GlossaryQLV    = new QListView   (TabWidgetQTW );
  
  TextBrowserBTB->setText(trUtf8("<h1>Documentation index not found</h1>"));
  
  ContentsQLV->addColumn         (QString(""));
  ContentsQLV->setSorting        (-1         );
  ContentsQLV->setRootIsDecorated(true       );
  
  //GlossaryQLV->addColumn         (QString(""));
  //GlossaryQLV->setSorting        (-1         );
  //GlossaryQLV->setRootIsDecorated(true       );
  
  DockWindowQDW->setResizeEnabled(true );
  DockWindowQDW->setMovingEnabled(false);
  
  TabWidgetQTW->addTab(ContentsQLV, trUtf8("&Contents"));
  //TabWidgetQTW->addTab(GlossaryQLV, trUtf8("G&lossary"));
  
  DockWindowQDW->setWidget(TabWidgetQTW);
  
  addDockWindow   (DockWindowQDW , Qt::DockLeft);
  setDockEnabled  (Qt::DockLeft  , false       );
  setCentralWidget(TextBrowserBTB);
  
  setCaption(trUtf8("Help - Bayani Handbook"));
  
  TextBrowserBTB->setFocus();
  
  GoBackwardQA->setEnabled(false);
  GoForwardQA ->setEnabled(false);
  GoHomeQA    ->setEnabled(false);
  TabWidgetQTW->setEnabled(false);
  
  statusBar()->message(trUtf8("Ready."));
  
  connect(this          , SIGNAL(ListViewItemSetSource(const QString &)), TextBrowserBTB, SLOT(setSource                 (const QString &)));
  connect(FileQuitQA    , SIGNAL(activated            (               )), this          , SLOT(close                     (               )));
  connect(GoBackwardQA  , SIGNAL(activated            (               )), TextBrowserBTB, SLOT(backward                  (               )));
  connect(GoForwardQA   , SIGNAL(activated            (               )), TextBrowserBTB, SLOT(forward                   (               )));
  connect(GoHomeQA      , SIGNAL(activated            (               )), TextBrowserBTB, SLOT(home                      (               )));
  connect(TextBrowserBTB, SIGNAL(backwardAvailable    (bool           )), GoBackwardQA  , SLOT(setEnabled                (bool           )));
  connect(TextBrowserBTB, SIGNAL(forwardAvailable     (bool           )), GoForwardQA   , SLOT(setEnabled                (bool           )));
  connect(TextBrowserBTB, SIGNAL(highlighted          (const QString &)), statusBar()   , SLOT(message                   (const QString &)));
  connect(TextBrowserBTB, SIGNAL(sourceChanged        (const QString &)), this          , SLOT(ListViewItemSetCurrentSlot(const QString &)));
  connect(ContentsQLV   , SIGNAL(pressed              (QListViewItem *)), this          , SLOT(ListViewItemPressedSlot   (QListViewItem *)));
  connect(ContentsQLV   , SIGNAL(clicked              (QListViewItem *)), this          , SLOT(ListViewItemChangedSlot   (QListViewItem *)));
  connect(ContentsQLV   , SIGNAL(selectionChanged     (QListViewItem *)), this          , SLOT(ListViewItemChangedSlot   (QListViewItem *)));
}

BHelpBrowser::~BHelpBrowser()
{
}

void BHelpBrowser::SetIndex(QString Source)
{
  QFile            XMLFile  (Source);
  QXmlInputSource  XMLSource(&XMLFile);
  QXmlSimpleReader XMLReader;
  
  BXMLToQLVHandler XML2QLV;
  XML2QLV.SetListView(ContentsQLV);
  
  XMLReader.setContentHandler(&XML2QLV);
  XMLReader.parse(XMLSource);
  
  //XML2QLV.setListView(GlossaryQLV);
  //XMLFile.reset();
  //XMLReader.parse(XMLSource);
  
  BListViewItem * BItem = dynamic_cast<BListViewItem *>(ContentsQLV->firstChild());
  
  if(BItem)
    {
      TextBrowserBTB->setSource(BItem->GetAnchor());
      
      GoHomeQA    ->setEnabled(true);
      TabWidgetQTW->setEnabled(true);
    }
}

void BHelpBrowser::SetFilePath(QString FilePath)
{
  TextBrowserBTB->mimeSourceFactory()->setFilePath(FilePath);
}

void BHelpBrowser::ShowHelpPage(QString ID)
{
  /* We try to find the BLVI with the good ID and display it. If it does not exist, we show the index. */
  
  BListViewItem * BItem = dynamic_cast<BListViewItem *>(ContentsQLV->currentItem());
  
  /* If the current item is the good one, do nothing, except making sure the source is visible */
  
  if(BItem && BItem->GetID() == ID)
    {
      TextBrowserBTB->setSource(BItem->GetAnchor());
      
      show ();
      raise();
      
      return;
    }
  
  /* Else, find the good one and make its source active and visible if it exists */
  
  QListViewItemIterator Iterator(ContentsQLV);
  
  while(Iterator.current())
    {
      BItem = dynamic_cast<BListViewItem *>(Iterator.current());
      
      if(BItem && BItem->GetID() == ID)
	{
	  TextBrowserBTB->setSource(BItem->GetAnchor());
	  
	  show ();
	  raise();
	  
	  return;
	}
      
      Iterator++;
    }
  
  BItem = dynamic_cast<BListViewItem *>(ContentsQLV->firstChild());
  
  if(BItem)
    TextBrowserBTB->setSource(BItem->GetAnchor());
  
  show ();
  raise();
}

void BHelpBrowser::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      TextBrowserBTB->setAlignment(Qt::AlignRight);
      
      GoBackwardQA->setAccel(ALT + Key_Right);
      GoForwardQA ->setAccel(ALT + Key_Left );
      
      moveDockWindow(DockWindowQDW, Qt::DockRight);
      setDockEnabled(Qt::DockRight, false        );
      setDockEnabled(Qt::DockLeft , true         );
    }
  else
    {
      TextBrowserBTB->setAlignment(Qt::AlignLeft);
      
      GoBackwardQA->setAccel(ALT + Key_Left );
      GoForwardQA ->setAccel(ALT + Key_Right);
      
      moveDockWindow(DockWindowQDW, Qt::DockLeft);
      setDockEnabled(Qt::DockRight, true        );
      setDockEnabled(Qt::DockLeft , false       );
    }
}

void BHelpBrowser::ListViewItemSetCurrentSlot(const QString & Source)
{
  QString Anchor = Source;
  
  int Index = Anchor.findRev("/");
  
  if(Index != -1)
    Anchor = Anchor.remove(0, Index+1);
  
  BListViewItem * BItem = dynamic_cast<BListViewItem *>(ContentsQLV->currentItem());
  
  /* If the current item is the good one, do nothing, except making sure it is visible */
  
  if(BItem && BItem->GetAnchor() == Anchor)
    {
      ContentsQLV->ensureItemVisible(BItem);
      
      return;
    }
  
  /* Else, find the good one and make it active and visible if it exists */
  
  QListViewItemIterator Iterator(ContentsQLV);
  
  while(Iterator.current())
    {
      BItem = dynamic_cast<BListViewItem *>(Iterator.current());
      
      if(BItem && BItem->GetAnchor() == Anchor)
	{
	  ContentsQLV->setCurrentItem(BItem);
	  ContentsQLV->ensureItemVisible(BItem);
	  
	  statusBar()->message(trUtf8("Loaded page."));
	  
	  return;
	}
      
      Iterator++;
    }
}

void BHelpBrowser::ListViewItemPressedSlot(QListViewItem * QItem)
{
  if(QItem)
    QItem->setOpen(!QItem->isOpen());
}

void BHelpBrowser::ListViewItemChangedSlot(QListViewItem * QItem)
{
  BListViewItem * BItem = dynamic_cast<BListViewItem *>(QItem);
  
  if(BItem)
    {
      statusBar()->message(trUtf8("Loaded page."));
      
      emit ListViewItemSetSource(BItem->GetAnchor());
    }
}
