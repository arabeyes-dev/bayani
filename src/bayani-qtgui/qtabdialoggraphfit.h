/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogGraphFit Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGGRAPHFIT_H
#define BAYANI_QTABDIALOGGRAPHFIT_H

/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qstring.h>
#include <qwidget.h>
#include <qtabdialog.h>
#include <qtabbar.h>
#include <qvbox.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qtextedit.h>
#include <qpushbutton.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qvalidator.h>

/*!
  \file qtabdialoggraphfit.h
  \brief Header file for the QTabDialogGraphFit class.
  
  The QTabDialogGraphFit class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to fit data.
*/
class QTabDialogGraphFit: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QVBox            * OptionsQVB;
  QVBox            * AnalyticalQVB;
  QVBox            * Analytical11QVB;
  QVBox            * Analytical12QVB;
  QVBox            * NumericalQVB;
  QVBox            * Numerical11QVB;
  QVBox            * Numerical12QVB;
  QGroupBox        * Options1QGB;
  QGroupBox        * Options2QGB;
  QGroupBox        * Options3QGB;
  QGroupBox        * Numerical1QGB;
  QGroupBox        * Analytical1QGB;
  QLabel           * DegreeQL;
  QLabel           * Parameter0QL;
  QLabel           * Parameter1QL;
  QLabel           * Parameter2QL;
  QLabel           * Parameter3QL;
  QLabel           * MaxStepQL;
  QLabel           * Chi2LimQL;
  QLineEdit        * Parameter0QLE;
  QLineEdit        * Parameter1QLE;
  QLineEdit        * Parameter2QLE;
  QLineEdit        * Parameter3QLE;
  QLineEdit        * MaxStepQLE;
  QLineEdit        * Chi2LimQLE;
  QTextEdit        * OutputQTE;
  QPushButton      * PlotSettingsQPB;
  QCheckBox        * NumericalQCKB;
  QCheckBox        * PlotQCKB;
  QCheckBox        * PlotNoConvQCKB;
  QCheckBox        * SameQCKB;
  QComboBox        * DegreeQCB;
  QIntValidator    * MaxStepQV;
  QDoubleValidator * Chi2LimQV;
  
 public:
                        QTabDialogGraphFit(QWidget * Parent = 0, const char * Name = 0);
  virtual              ~QTabDialogGraphFit();
          QLineEdit   * GetParameter0QLE  ();
	  QLineEdit   * GetParameter1QLE  ();
	  QLineEdit   * GetParameter2QLE  ();
	  QLineEdit   * GetParameter3QLE  ();
	  QLineEdit   * GetMaxStepQLE     ();
	  QLineEdit   * GetChi2LimQLE     ();
	  QTextEdit   * GetOutputQTE      ();
	  QPushButton * GetPlotSettingsQPB();
	  QCheckBox   * GetNumericalQCKB  ();
	  QCheckBox   * GetPlotQCKB       ();
	  QCheckBox   * GetPlotNoConvQCKB ();
	  QCheckBox   * GetSameQCKB       ();
	  QComboBox   * GetDegreeQCB      ();
	  void          SetCurrentTab     (int);
	  void          SetR2L            (bool);
	  
 public slots:
  void NumericalChangedSlot(bool);
  void PlotChangedSlot     (bool);
};

#endif
