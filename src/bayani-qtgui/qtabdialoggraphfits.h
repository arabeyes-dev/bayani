/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogGraphFITS Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGGRAPHFITS_H
#define BAYANI_QTABDIALOGGRAPHFITS_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>
/* Bayani headers */
#include <qfileselector.h>

/*!
  \file qtabdialoggraphfits.h
  \brief Header file for the QTabDialogGraphFITS class.
  
  The QTabDialogGraphFITS class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to represent FITS Images.
*/
class QTabDialogGraphFITS: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel        * FileNameQL;
  QFileSelector * FileNameQFLS;
  QPushButton   * PlotSettingsQPB;
  QCheckBox     * SameQCKB;
  QGroupBox     * General1QGB;
  QGroupBox     * General2QGB;
  QVBox         * GeneralQVB;
  
 public:
                          QTabDialogGraphFITS(QWidget * Parent = 0, const char * Name = 0);
  virtual                ~QTabDialogGraphFITS();
          QFileSelector * GetFileNameQFLS    ();
	  QPushButton   * GetPlotSettingsQPB ();
	  QCheckBox     * GetSameQCKB        ();
	  void            SetR2L             (bool);
};

#endif
