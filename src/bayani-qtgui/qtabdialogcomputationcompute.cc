/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogComputationCompute Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogcomputationcompute.h>

QTabDialogComputationCompute::QTabDialogComputationCompute(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  setCaption(trUtf8("Computation - Compute"));
  
  setOKButton    (trUtf8("&OK"    ));
  setHelpButton  (trUtf8("&Help"  ));
  setApplyButton (trUtf8("&Apply" ));
  setCancelButton(trUtf8("&Cancel"));
  
  GeneralQVB = new QVBox(Parent);
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  addTab(GeneralQVB, "");
  
  General1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Expression"), GeneralQVB);
  
  ExpressionQL  = new QLabel(trUtf8("&Expression"), General1QGB);
  ExpressionQLE = new QLineEdit(General1QGB);
  //ExpressionQLE->setMaximumWidth(200);
  
  ExpressionQL->setBuddy(ExpressionQLE);
  
  GeneralQBG = new QButtonGroup(1, Qt::Horizontal, trUtf8("Actions"), GeneralQVB);
  
  SimplifyQRB  = new QRadioButton(trUtf8("Si&mplify"        ), GeneralQBG);
  EvaluateQRB  = new QRadioButton(trUtf8("E&valuate"        ), GeneralQBG);
  //ResolveQRB   = new QRadioButton(trUtf8("&Resolve equation"), GeneralQBG);
  DeriveQRB    = new QRadioButton(trUtf8("&Derive"          ), GeneralQBG);
  //IntegrateQRB = new QRadioButton(trUtf8("&Integrate"       ), GeneralQBG);
  
  SimplifyQRB->setChecked(true);
  
  General2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Output"), GeneralQVB);
  
  OutputQTE = new QTextEdit(General2QGB);
  OutputQTE->setReadOnly(true);
  OutputQTE->setMinimumHeight(200);
  
  VariablesQPB = new QPushButton(trUtf8("Varia&bles..."), GeneralQVB);
  VariablesQPB->setFixedSize(VariablesQPB->sizeHint());
  
  setFixedSize(sizeHint());
  
  ExpressionQLE->setFocus();
}

QTabDialogComputationCompute::~QTabDialogComputationCompute()
{
}

QLineEdit * QTabDialogComputationCompute::GetExpressionQLE()
{
  return ExpressionQLE;
}

QRadioButton * QTabDialogComputationCompute::GetSimplifyQRB()
{
  return SimplifyQRB;
}

QRadioButton * QTabDialogComputationCompute::GetEvaluateQRB()
{
  return EvaluateQRB;
}

QRadioButton * QTabDialogComputationCompute::GetResolveQRB()
{
  return ResolveQRB;
}

QRadioButton * QTabDialogComputationCompute::GetDeriveQRB()
{
  return DeriveQRB;
}

QRadioButton * QTabDialogComputationCompute::GetIntegrateQRB()
{
  return IntegrateQRB;
}

QTextEdit * QTabDialogComputationCompute::GetOutputQTE()
{
  return OutputQTE;
}

QPushButton * QTabDialogComputationCompute::GetVariablesQPB()
{
  return VariablesQPB;
}

void QTabDialogComputationCompute::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      ExpressionQLE->setAlignment(Qt::AlignRight);
      OutputQTE    ->setAlignment(Qt::AlignRight);
    }
  else
    {
      ExpressionQLE->setAlignment(Qt::AlignLeft);
      OutputQTE    ->setAlignment(Qt::AlignLeft);
    }
}
