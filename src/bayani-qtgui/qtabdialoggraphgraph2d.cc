/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogGraphGraph2D Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialoggraphgraph2d.h>

QTabDialogGraphGraph2D::QTabDialogGraphGraph2D(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  /* Data */
  
  DataQVB = new QVBox(Parent);
  
  Data1QGB   = new QGroupBox(2, Qt::Horizontal, trUtf8("Data" ), DataQVB);
  Data21QHB  = new QHBox    (                                          DataQVB);
  ErrorsQCKB = new QCheckBox(                   trUtf8("Use e&rrors"), DataQVB);
  Data2QGB   = new QGroupBox(1, Qt::Horizontal, trUtf8("Options"    ), DataQVB);
  
  Data11QBG = new QButtonGroup(1, Qt::Horizontal, trUtf8("X Axis"), Data1QGB);  
  Data12QBG = new QButtonGroup(1, Qt::Horizontal, trUtf8("Y Axis"), Data1QGB);
  Data11QHB = new QHBox       (                                          Data1QGB);
  Data12QHB = new QHBox       (                                          Data1QGB);
  
  Data111QVB = new QVBox(Data11QHB);
  Data112QVB = new QVBox(Data11QHB);
  Data121QVB = new QVBox(Data12QHB);
  Data122QVB = new QVBox(Data12QHB);
  
  Data211QVB = new QVBox(Data21QHB);
  Data212QVB = new QVBox(Data21QHB);
  
  ColumnXQRB     = new QRadioButton(trUtf8("Use a table's co&lumn"), Data11QBG);
  ExpressionXQRB = new QRadioButton(trUtf8("Use an e&xpression"             ), Data11QBG);
  
  ColumnXQL  = new QLabel(trUtf8("Column"), Data111QVB);
  ColumnXQSB = new QSpinBoxBayani(1, 1, 1, Data112QVB);
  
  ExpressionXQL  = new QLabel(trUtf8("Expression"), Data111QVB);
  ExpressionXQLE = new QLineEdit(Data112QVB);
  
  ColumnYQRB     = new QRadioButton(trUtf8("Use a table's colu&mn"), Data12QBG);
  ExpressionYQRB = new QRadioButton(trUtf8("Use an ex&pression"             ), Data12QBG);
  
  ColumnYQL  = new QLabel(trUtf8("Column"), Data121QVB);
  ColumnYQSB = new QSpinBoxBayani(1, 1, 1, Data122QVB);
  
  ExpressionYQL  = new QLabel(trUtf8("Expression"), Data121QVB);
  ExpressionYQLE = new QLineEdit(Data122QVB);
  
  CutQLE  = new QLineEdit(Data212QVB);
  CutQCKB = new QCheckBox(trUtf8("Use a c&ut to select rows"), Data211QVB);
  
  SameQCKB = new QCheckBox(trUtf8("&Superimpose on the active graph"), Data2QGB);
  
  PlotSettingsQPB = new QPushButton(trUtf8("&Graphical settings..."), Data2QGB);
  
  DataQVB->setSpacing(5);
  DataQVB->setMargin(5);
  
  Data11QHB->setSpacing(5);
  Data11QHB->setMargin(5);
  Data12QHB->setSpacing(5);
  Data12QHB->setMargin(5);
  Data21QHB->setSpacing(5);
  //Data21QHB->setMargin(5);
  Data111QVB->setSpacing(5);
  Data111QVB->setMargin(5);
  Data112QVB->setSpacing(5);
  Data112QVB->setMargin(5);
  Data211QVB->setSpacing(5);
  //Data211QVB->setMargin(5);
  Data212QVB->setSpacing(5);
  Data212QVB->setMargin(5);
  Data121QVB->setSpacing(5);
  Data121QVB->setMargin(5);
  Data122QVB->setSpacing(5);
  Data122QVB->setMargin(5);
  
  ExpressionXQLE->setMaximumWidth(200);
  ExpressionYQLE->setMaximumWidth(200);
  CutQLE->setMaximumWidth(200);
  PlotSettingsQPB->setFixedSize(PlotSettingsQPB->sizeHint());
  ColumnXQSB->setMaximumWidth(50);
  ColumnYQSB->setMaximumWidth(50);
  
  /* Errors */
  
  ErrorsQVB = new QVBox(Parent);
  
  Errors1QGB = new QGroupBox(2, Qt::Horizontal, "", ErrorsQVB);
  Errors2QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Errors (superior values)"), ErrorsQVB);  
  
  Errors11QBG = new QButtonGroup(1, Qt::Horizontal, trUtf8("X Axis"), Errors1QGB);
  Errors12QBG = new QButtonGroup(1, Qt::Horizontal, trUtf8("Y Axis"), Errors1QGB);
  Errors21QBG = new QButtonGroup(1, Qt::Horizontal, trUtf8("X Axis"), Errors2QGB);
  Errors22QBG = new QButtonGroup(1, Qt::Horizontal, trUtf8("Y Axis"), Errors2QGB);
  
  Errors11QHB = new QHBox(Errors1QGB);
  Errors12QHB = new QHBox(Errors1QGB);
  Errors21QHB = new QHBox(Errors2QGB);
  Errors22QHB = new QHBox(Errors2QGB);
  
  Errors111QVB = new QVBox(Errors11QHB);
  Errors112QVB = new QVBox(Errors11QHB);
  Errors121QVB = new QVBox(Errors12QHB);
  Errors122QVB = new QVBox(Errors12QHB);
  Errors211QVB = new QVBox(Errors21QHB);
  Errors212QVB = new QVBox(Errors21QHB);
  Errors221QVB = new QVBox(Errors22QHB);
  Errors222QVB = new QVBox(Errors22QHB);
  
  ColumnDownXQRB     = new QRadioButton(trUtf8("Use a table's co&lumn"), Errors11QBG);
  ExpressionDownXQRB = new QRadioButton(trUtf8("Use an e&xpression"), Errors11QBG);
  
  ColumnDownXQL  = new QLabel(trUtf8("Column"), Errors111QVB);
  ColumnDownXQSB = new QSpinBoxBayani(1, 1, 1, Errors112QVB);
  
  ExpressionDownXQL  = new QLabel(trUtf8("Expression"), Errors111QVB);
  ExpressionDownXQLE = new QLineEdit(Errors112QVB);
  
  ColumnDownYQRB     = new QRadioButton(trUtf8("Use a table's colu&mn"), Errors12QBG);
  ExpressionDownYQRB = new QRadioButton(trUtf8("Use an ex&pression"), Errors12QBG);
  
  ColumnDownYQL  = new QLabel(trUtf8("Column"), Errors121QVB);
  ColumnDownYQSB = new QSpinBoxBayani(1, 1, 1, Errors122QVB);
  
  ExpressionDownYQL  = new QLabel(trUtf8("Expression"), Errors121QVB);
  ExpressionDownYQLE = new QLineEdit(Errors122QVB);
  
  ErrorsDownXQCKB = new QCheckBox(trUtf8("&Use errors"), Errors1QGB);
  ErrorsDownYQCKB = new QCheckBox(trUtf8("U&se errors"), Errors1QGB);
  
  ColumnUpXQRB     = new QRadioButton(trUtf8("Use a ta&ble's column"), Errors21QBG);
  ExpressionUpXQRB = new QRadioButton(trUtf8("Use a&n expression"), Errors21QBG);
    
  ColumnUpXQL  = new QLabel(trUtf8("Column"), Errors211QVB);
  ColumnUpXQSB = new QSpinBoxBayani(1, 1, 1, Errors212QVB);
  
  ExpressionUpXQL  = new QLabel(trUtf8("Expression"), Errors211QVB);
  ExpressionUpXQLE = new QLineEdit(Errors212QVB);
  
  ColumnUpYQRB     = new QRadioButton(trUtf8("Use a &table's column"), Errors22QBG);
  ExpressionUpYQRB = new QRadioButton(trUtf8("Use an express&ion"), Errors22QBG);
  
  ColumnUpYQL  = new QLabel(trUtf8("Column"), Errors221QVB);
  ColumnUpYQSB = new QSpinBoxBayani(1, 1, 1, Errors222QVB);
  
  ExpressionUpYQL  = new QLabel(trUtf8("Expression"), Errors221QVB);
  ExpressionUpYQLE = new QLineEdit(Errors222QVB);
  
  ErrorsUpXQCKB = new QCheckBox(trUtf8("Use e&rrors"), Errors2QGB);
  ErrorsUpYQCKB = new QCheckBox(trUtf8("Use err&ors"), Errors2QGB);
  
  AsymmetricQCKB = new QCheckBox(trUtf8("Use as&ymmetric errors"), ErrorsQVB);
  
  ErrorsQVB->setSpacing(5);
  ErrorsQVB->setMargin(5);
  
  Errors11QHB->setSpacing(5);
  Errors11QHB->setMargin(5);
  Errors12QHB->setSpacing(5);
  Errors12QHB->setMargin(5);
  Errors21QHB->setSpacing(5);
  Errors21QHB->setMargin(5);
  Errors22QHB->setSpacing(5);
  Errors22QHB->setMargin(5);
  Errors111QVB->setSpacing(5);
  Errors111QVB->setMargin(5);
  Errors112QVB->setSpacing(5);
  Errors112QVB->setMargin(5);
  Errors121QVB->setSpacing(5);
  Errors121QVB->setMargin(5);
  Errors122QVB->setSpacing(5);
  Errors122QVB->setMargin(5);
  Errors211QVB->setSpacing(5);
  Errors211QVB->setMargin(5);
  Errors212QVB->setSpacing(5);
  Errors212QVB->setMargin(5);
  Errors221QVB->setSpacing(5);
  Errors221QVB->setMargin(5);
  Errors222QVB->setSpacing(5);
  Errors222QVB->setMargin(5);
  
  ExpressionDownXQLE->setMaximumWidth(200);
  ExpressionDownYQLE->setMaximumWidth(200);
  ExpressionUpXQLE->setMaximumWidth(200);
  ExpressionUpYQLE->setMaximumWidth(200);
  ColumnDownXQSB->setMaximumWidth(50);
  ColumnDownYQSB->setMaximumWidth(50);
  ColumnUpXQSB->setMaximumWidth(50);
  ColumnUpYQSB->setMaximumWidth(50);
  
  ColumnXQRB        ->setChecked(true );
  ColumnYQRB        ->setChecked(true );
  ColumnDownXQRB    ->setChecked(true );
  ColumnDownYQRB    ->setChecked(true );
  ColumnUpXQRB      ->setChecked(true );
  ColumnUpYQRB      ->setChecked(true );
  ExpressionXQL     ->setEnabled(false);
  ExpressionXQLE    ->setEnabled(false);
  ExpressionYQL     ->setEnabled(false);
  ExpressionYQLE    ->setEnabled(false);
  ExpressionDownXQL ->setEnabled(false);
  ExpressionDownXQLE->setEnabled(false);
  ExpressionDownYQL ->setEnabled(false);
  ExpressionDownYQLE->setEnabled(false);
  ExpressionUpXQL   ->setEnabled(false);
  ExpressionUpXQLE  ->setEnabled(false);
  ExpressionUpYQL   ->setEnabled(false);
  ExpressionUpYQLE  ->setEnabled(false);
  Errors11QBG       ->setEnabled(false);
  Errors11QHB       ->setEnabled(false);
  Errors12QBG       ->setEnabled(false);
  Errors12QHB       ->setEnabled(false);
  Errors21QBG       ->setEnabled(false);
  Errors21QHB       ->setEnabled(false);
  Errors22QBG       ->setEnabled(false);
  Errors22QHB       ->setEnabled(false);
  CutQLE            ->setEnabled(false);
  AsymmetricChangedSlot(false);
  
  /* General */
  
  addTab(DataQVB  , trUtf8("&Data"));
  addTab(ErrorsQVB, trUtf8("&Errors"));
  
  ErrorsChangedSlot(false);
  
  setOKButton    (trUtf8("O&K")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setCaption(trUtf8("Graph - 2D Graph"));
  
  setFixedSize(sizeHint());
  
  ColumnXQSB->setFocus();
  
  connect(ColumnXQRB        , SIGNAL(toggled(bool)), ColumnXQL         , SLOT(setEnabled(bool)));
  connect(ColumnXQRB        , SIGNAL(toggled(bool)), ColumnXQSB        , SLOT(setEnabled(bool)));
  connect(ColumnXQRB        , SIGNAL(clicked()    ), ColumnXQSB        , SLOT(setFocus()      ));
  connect(ColumnXQRB        , SIGNAL(pressed()    ), ColumnXQSB        , SLOT(clearFocus()    ));
  connect(ExpressionXQRB    , SIGNAL(toggled(bool)), ExpressionXQL     , SLOT(setEnabled(bool)));
  connect(ExpressionXQRB    , SIGNAL(toggled(bool)), ExpressionXQLE    , SLOT(setEnabled(bool)));
  connect(ExpressionXQRB    , SIGNAL(clicked()    ), ExpressionXQLE    , SLOT(setFocus()      ));
  connect(ExpressionXQRB    , SIGNAL(pressed()    ), ExpressionXQLE    , SLOT(clearFocus()    ));
  connect(ColumnYQRB        , SIGNAL(toggled(bool)), ColumnYQL         , SLOT(setEnabled(bool)));
  connect(ColumnYQRB        , SIGNAL(toggled(bool)), ColumnYQSB        , SLOT(setEnabled(bool)));
  connect(ColumnYQRB        , SIGNAL(clicked()    ), ColumnYQSB        , SLOT(setFocus()      ));
  connect(ColumnYQRB        , SIGNAL(pressed()    ), ColumnYQSB        , SLOT(clearFocus()    ));
  connect(ExpressionYQRB    , SIGNAL(toggled(bool)), ExpressionYQL     , SLOT(setEnabled(bool)));
  connect(ExpressionYQRB    , SIGNAL(toggled(bool)), ExpressionYQLE    , SLOT(setEnabled(bool)));
  connect(ExpressionYQRB    , SIGNAL(clicked()    ), ExpressionYQLE    , SLOT(setFocus()      ));
  connect(ExpressionYQRB    , SIGNAL(pressed()    ), ExpressionYQLE    , SLOT(clearFocus()    ));
  connect(ColumnDownXQRB    , SIGNAL(toggled(bool)), ColumnDownXQL     , SLOT(setEnabled(bool)));
  connect(ColumnDownXQRB    , SIGNAL(toggled(bool)), ColumnDownXQSB    , SLOT(setEnabled(bool)));
  connect(ColumnDownXQRB    , SIGNAL(clicked()    ), ColumnDownXQSB    , SLOT(setFocus()      ));
  connect(ColumnDownXQRB    , SIGNAL(pressed()    ), ColumnDownXQSB    , SLOT(clearFocus()    ));
  connect(ExpressionDownXQRB, SIGNAL(toggled(bool)), ExpressionDownXQL , SLOT(setEnabled(bool)));
  connect(ExpressionDownXQRB, SIGNAL(toggled(bool)), ExpressionDownXQLE, SLOT(setEnabled(bool)));
  connect(ExpressionDownXQRB, SIGNAL(clicked()    ), ExpressionDownXQLE, SLOT(setFocus()      ));
  connect(ExpressionDownXQRB, SIGNAL(pressed()    ), ExpressionDownXQLE, SLOT(clearFocus()    ));
  connect(ColumnDownYQRB    , SIGNAL(toggled(bool)), ColumnDownYQL     , SLOT(setEnabled(bool)));
  connect(ColumnDownYQRB    , SIGNAL(toggled(bool)), ColumnDownYQSB    , SLOT(setEnabled(bool)));
  connect(ColumnDownYQRB    , SIGNAL(clicked()    ), ColumnDownYQSB    , SLOT(setFocus()      ));
  connect(ColumnDownYQRB    , SIGNAL(pressed()    ), ColumnDownYQSB    , SLOT(clearFocus()    ));
  connect(ExpressionDownYQRB, SIGNAL(toggled(bool)), ExpressionDownYQL , SLOT(setEnabled(bool)));
  connect(ExpressionDownYQRB, SIGNAL(toggled(bool)), ExpressionDownYQLE, SLOT(setEnabled(bool)));
  connect(ExpressionDownYQRB, SIGNAL(clicked()    ), ExpressionDownYQLE, SLOT(setFocus()      ));
  connect(ExpressionDownYQRB, SIGNAL(pressed()    ), ExpressionDownYQLE, SLOT(clearFocus()    ));
  connect(ColumnUpXQRB      , SIGNAL(toggled(bool)), ColumnUpXQL       , SLOT(setEnabled(bool)));
  connect(ColumnUpXQRB      , SIGNAL(toggled(bool)), ColumnUpXQSB      , SLOT(setEnabled(bool)));
  connect(ColumnUpXQRB      , SIGNAL(clicked()    ), ColumnUpXQSB      , SLOT(setFocus()      ));
  connect(ColumnUpXQRB      , SIGNAL(pressed()    ), ColumnUpXQSB      , SLOT(clearFocus()    ));
  connect(ExpressionUpXQRB  , SIGNAL(toggled(bool)), ExpressionUpXQL   , SLOT(setEnabled(bool)));
  connect(ExpressionUpXQRB  , SIGNAL(toggled(bool)), ExpressionUpXQLE  , SLOT(setEnabled(bool)));
  connect(ExpressionUpXQRB  , SIGNAL(clicked()    ), ExpressionUpXQLE  , SLOT(setFocus()      ));
  connect(ExpressionUpXQRB  , SIGNAL(pressed()    ), ExpressionUpXQLE  , SLOT(clearFocus()    ));
  connect(ColumnUpYQRB      , SIGNAL(toggled(bool)), ColumnUpYQL       , SLOT(setEnabled(bool)));
  connect(ColumnUpYQRB      , SIGNAL(toggled(bool)), ColumnUpYQSB      , SLOT(setEnabled(bool)));
  connect(ColumnUpYQRB      , SIGNAL(clicked()    ), ColumnUpYQSB      , SLOT(setFocus()      ));
  connect(ColumnUpYQRB      , SIGNAL(pressed()    ), ColumnUpYQSB      , SLOT(clearFocus()    ));
  connect(ExpressionUpYQRB  , SIGNAL(toggled(bool)), ExpressionUpYQL   , SLOT(setEnabled(bool)));
  connect(ExpressionUpYQRB  , SIGNAL(toggled(bool)), ExpressionUpYQLE  , SLOT(setEnabled(bool)));
  connect(ExpressionUpYQRB  , SIGNAL(clicked()    ), ExpressionUpYQLE  , SLOT(setFocus()      ));
  connect(ExpressionUpYQRB  , SIGNAL(pressed()    ), ExpressionUpYQLE  , SLOT(clearFocus()    ));
  connect(ErrorsQCKB, SIGNAL(toggled(bool)), this, SLOT(ErrorsChangedSlot(bool)));
  connect(ErrorsDownXQCKB   , SIGNAL(toggled(bool)), Errors11QBG       , SLOT(setEnabled(bool)));
  connect(ErrorsDownXQCKB   , SIGNAL(toggled(bool)), Errors11QHB       , SLOT(setEnabled(bool)));
  connect(ErrorsDownYQCKB   , SIGNAL(toggled(bool)), Errors12QBG       , SLOT(setEnabled(bool)));
  connect(ErrorsDownYQCKB   , SIGNAL(toggled(bool)), Errors12QHB       , SLOT(setEnabled(bool)));
  connect(ErrorsUpXQCKB     , SIGNAL(toggled(bool)), Errors21QBG       , SLOT(setEnabled(bool)));
  connect(ErrorsUpXQCKB     , SIGNAL(toggled(bool)), Errors21QHB       , SLOT(setEnabled(bool)));
  connect(ErrorsUpYQCKB     , SIGNAL(toggled(bool)), Errors22QBG       , SLOT(setEnabled(bool)));
  connect(ErrorsUpYQCKB     , SIGNAL(toggled(bool)), Errors22QHB       , SLOT(setEnabled(bool)));
  connect(CutQCKB           , SIGNAL(toggled(bool)), CutQLE            , SLOT(setEnabled(bool)));
  connect(CutQCKB           , SIGNAL(clicked()    ), CutQLE            , SLOT(setFocus()      ));
  connect(CutQCKB           , SIGNAL(pressed()    ), CutQLE            , SLOT(clearFocus()    ));
  connect(AsymmetricQCKB, SIGNAL(toggled(bool)), this, SLOT(AsymmetricChangedSlot(bool)));
  connect(ErrorsDownXQCKB, SIGNAL(pressed()    ), ColumnDownXQSB    , SLOT(clearFocus()    ));
  connect(ErrorsDownXQCKB, SIGNAL(pressed()    ), ExpressionDownXQLE, SLOT(clearFocus()    ));
  connect(ErrorsDownYQCKB, SIGNAL(pressed()    ), ColumnDownYQSB    , SLOT(clearFocus()    ));
  connect(ErrorsDownYQCKB, SIGNAL(pressed()    ), ExpressionDownYQLE, SLOT(clearFocus()    ));
  connect(ErrorsUpXQCKB  , SIGNAL(pressed()    ), ColumnUpXQSB      , SLOT(clearFocus()    ));
  connect(ErrorsUpXQCKB  , SIGNAL(pressed()    ), ExpressionUpXQLE  , SLOT(clearFocus()    ));
  connect(ErrorsUpYQCKB  , SIGNAL(pressed()    ), ColumnUpYQSB      , SLOT(clearFocus()    ));
  connect(ErrorsUpYQCKB  , SIGNAL(pressed()    ), ExpressionUpYQLE  , SLOT(clearFocus()    ));
  connect(AsymmetricQCKB , SIGNAL(pressed()    ), ColumnUpXQSB      , SLOT(clearFocus()    ));
  connect(AsymmetricQCKB , SIGNAL(pressed()    ), ExpressionUpXQLE  , SLOT(clearFocus()    ));
  connect(AsymmetricQCKB , SIGNAL(pressed()    ), ColumnUpYQSB      , SLOT(clearFocus()    ));
  connect(AsymmetricQCKB , SIGNAL(pressed()    ), ExpressionUpYQLE  , SLOT(clearFocus()    ));
}

QTabDialogGraphGraph2D::~QTabDialogGraphGraph2D()
{
}

QLineEdit * QTabDialogGraphGraph2D::GetExpressionXQLE()
{
  return ExpressionXQLE;
}

QLineEdit * QTabDialogGraphGraph2D::GetExpressionYQLE()
{
  return ExpressionYQLE;
}

QLineEdit * QTabDialogGraphGraph2D::GetCutQLE()
{
  return CutQLE;
}

QLineEdit * QTabDialogGraphGraph2D::GetExpressionDownXQLE()
{
  return ExpressionDownXQLE;
}

QLineEdit * QTabDialogGraphGraph2D::GetExpressionDownYQLE()
{
  return ExpressionDownYQLE;
}

QLineEdit * QTabDialogGraphGraph2D::GetExpressionUpXQLE()
{
  return ExpressionUpXQLE;
}

QLineEdit * QTabDialogGraphGraph2D::GetExpressionUpYQLE()
{
  return ExpressionUpYQLE;
}
  
QPushButton * QTabDialogGraphGraph2D::GetPlotSettingsQPB()
{
  return PlotSettingsQPB;
}

QCheckBox * QTabDialogGraphGraph2D::GetErrorsQCKB()
{
  return ErrorsQCKB;
}


QCheckBox * QTabDialogGraphGraph2D::GetCutQCKB()
{
  return CutQCKB;
}

QCheckBox * QTabDialogGraphGraph2D::GetSameQCKB()
{
  return SameQCKB;
}

QCheckBox * QTabDialogGraphGraph2D::GetErrorsDownXQCKB()
{
  return ErrorsDownXQCKB;
}

QCheckBox * QTabDialogGraphGraph2D::GetErrorsDownYQCKB()
{
  return ErrorsDownYQCKB;
}

QCheckBox * QTabDialogGraphGraph2D::GetErrorsUpXQCKB()
{
  return ErrorsUpXQCKB;
}

QCheckBox * QTabDialogGraphGraph2D::GetErrorsUpYQCKB()
{
  return ErrorsUpYQCKB;
}

QCheckBox * QTabDialogGraphGraph2D::GetAsymmetricQCKB()
{
  return AsymmetricQCKB;
}

QRadioButton * QTabDialogGraphGraph2D::GetColumnXQRB()
{
  return ColumnXQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetExpressionXQRB()
{
  return ExpressionXQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetColumnYQRB()
{
  return ColumnYQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetExpressionYQRB()
{
  return ExpressionYQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetColumnDownXQRB()
{
  return ColumnDownXQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetExpressionDownXQRB()
{
  return ExpressionDownXQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetColumnDownYQRB()
{
  return ColumnDownYQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetExpressionDownYQRB()
{
  return ExpressionDownYQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetColumnUpXQRB()
{
  return ColumnUpXQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetExpressionUpXQRB()
{
  return ExpressionUpXQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetColumnUpYQRB()
{
  return ColumnUpYQRB;
}

QRadioButton * QTabDialogGraphGraph2D::GetExpressionUpYQRB()
{
  return ExpressionUpYQRB;
}

QSpinBoxBayani * QTabDialogGraphGraph2D::GetColumnXQSB()
{
  return ColumnXQSB;
}

QSpinBoxBayani * QTabDialogGraphGraph2D::GetColumnYQSB()
{
  return ColumnYQSB;
}

QSpinBoxBayani * QTabDialogGraphGraph2D::GetColumnDownXQSB()
{
  return ColumnDownXQSB;
}

QSpinBoxBayani * QTabDialogGraphGraph2D::GetColumnDownYQSB()
{
  return ColumnDownYQSB;
}

QSpinBoxBayani * QTabDialogGraphGraph2D::GetColumnUpXQSB()
{
  return ColumnUpXQSB;
}

QSpinBoxBayani * QTabDialogGraphGraph2D::GetColumnUpYQSB()
{
  return ColumnUpYQSB;
}

void QTabDialogGraphGraph2D::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      ExpressionXQLE->setAlignment(Qt::AlignRight);
      ExpressionYQLE->setAlignment(Qt::AlignRight);
      CutQLE->setAlignment(Qt::AlignRight);
      ExpressionDownXQLE->setAlignment(Qt::AlignRight);
      ExpressionDownYQLE->setAlignment(Qt::AlignRight);
      ExpressionUpXQLE->setAlignment(Qt::AlignRight);
      ExpressionUpYQLE->setAlignment(Qt::AlignRight);
      ColumnXQSB->GetEditor()->setAlignment(Qt::AlignRight);
      ColumnYQSB->GetEditor()->setAlignment(Qt::AlignRight);
      ColumnDownXQSB->GetEditor()->setAlignment(Qt::AlignRight);
      ColumnDownYQSB->GetEditor()->setAlignment(Qt::AlignRight);
      ColumnUpXQSB->GetEditor()->setAlignment(Qt::AlignRight);
      ColumnUpYQSB->GetEditor()->setAlignment(Qt::AlignRight);
    }
  else
    {
      ExpressionXQLE->setAlignment(Qt::AlignLeft);
      ExpressionYQLE->setAlignment(Qt::AlignLeft);
      CutQLE->setAlignment(Qt::AlignLeft);
      ExpressionDownXQLE->setAlignment(Qt::AlignLeft);
      ExpressionDownYQLE->setAlignment(Qt::AlignLeft);
      ExpressionUpXQLE->setAlignment(Qt::AlignLeft);
      ExpressionUpYQLE->setAlignment(Qt::AlignLeft);
      ColumnXQSB->GetEditor()->setAlignment(Qt::AlignLeft);
      ColumnYQSB->GetEditor()->setAlignment(Qt::AlignLeft);
      ColumnDownXQSB->GetEditor()->setAlignment(Qt::AlignLeft);
      ColumnDownYQSB->GetEditor()->setAlignment(Qt::AlignLeft);
      ColumnUpXQSB->GetEditor()->setAlignment(Qt::AlignLeft);
      ColumnUpYQSB->GetEditor()->setAlignment(Qt::AlignLeft);
    }
}

void QTabDialogGraphGraph2D::ErrorsChangedSlot(bool State)
{
  tabBar()->setTabEnabled(1, State);
}

void QTabDialogGraphGraph2D::AsymmetricChangedSlot(bool State)
{
  Errors2QGB->setEnabled(State);
  
  if(State)
    Errors1QGB->setTitle(trUtf8("Errors (inferior values)"));
  else
    Errors1QGB->setTitle(trUtf8("Errors"));
}
