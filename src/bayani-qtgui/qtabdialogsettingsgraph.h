/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogSettingsGraph Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGSETTINGSGRAPH_H
#define BAYANI_QTABDIALOGSETTINGSGRAPH_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qtabdialog.h>
#include <qvbox.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qvalidator.h>
/* Bayani headers */
#include <qlinecombo.h>
#include <qpushbuttoncolorselect.h>
#include <qfontselector.h>
#include <qpencombo.h>

/*!
  \file qtabdialogsettingsgraph.h
  \brief Header file for the QTabDialogSettingsGraph class.
  
  The QTabDialogSettingsGraph class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set graphs' options.
*/
class QTabDialogSettingsGraph: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QVBox * DimensionsQVB;
  QVBox * Dimensions11QVB;
  QVBox * Dimensions12QVB;
  QVBox * Dimensions21QVB;
  QVBox * Dimensions22QVB;
  QVBox * Dimensions31QVB;
  QVBox * Dimensions32QVB;
  QVBox * Dimensions41QVB;
  QVBox * Dimensions42QVB;
  QVBox * LinesQVB;
  QVBox * Lines11QVB;
  QVBox * Lines12QVB;
  QVBox * Lines13QVB;
  QVBox * Lines14QVB;
  QVBox * TextsQVB;
  QVBox * Texts11QVB;
  QVBox * Texts12QVB;
  QVBox * Texts13QVB;
  QVBox * MiscellaneousQVB;
  QVBox * Miscellaneous11QVB;
  QVBox * Miscellaneous12QVB;
  
  QGroupBox * Dimensions1QGB;
  QGroupBox * Dimensions2QGB;
  QGroupBox * Dimensions3QGB;
  QGroupBox * Dimensions4QGB;
  QGroupBox * Lines1QGB;
  QGroupBox * Texts1QGB;
  QGroupBox * Miscellaneous1QGB;
  
  QLabel * XRatioQL;
  QLabel * YRatioQL;
  QLabel * WidthRatioQL;
  QLabel * HeightRatioQL;
  QLabel * Axis2TitleRatioQL;
  QLabel * BigXTicksRatioQL;
  QLabel * BigYTicksRatioQL;
  QLabel * SmallXTicksRatioQL;
  QLabel * SmallYTicksRatioQL;
  QLabel * XAxis2TicksLabelsRatioQL;
  QLabel * YAxis2TicksLabelsRatioQL;
  QLabel * XAxis2AxisLabelRatioQL;
  QLabel * YAxis2AxisLabelRatioQL;
  QLabel * BoxLineWidthQL;
  QLabel * BigXTicksLineWidthQL;
  QLabel * BigYTicksLineWidthQL;
  QLabel * SmallXTicksLineWidthQL;
  QLabel * SmallYTicksLineWidthQL;
  QLabel * XGridLineWidthQL;
  QLabel * YGridLineWidthQL;
  QLabel * BGColorQL;
  QLabel * XAxisLabelFontQL;
  QLabel * YAxisLabelFontQL;
  QLabel * XTicksLabelsFontQL;
  QLabel * YTicksLabelsFontQL;
  QLabel * TitleFontQL;
  
  QLineEdit * XRatioQLE;
  QLineEdit * YRatioQLE;
  QLineEdit * WidthRatioQLE;
  QLineEdit * HeightRatioQLE;
  QLineEdit * Axis2TitleRatioQLE;
  QLineEdit * BigXTicksRatioQLE;
  QLineEdit * BigYTicksRatioQLE;
  QLineEdit * SmallXTicksRatioQLE;
  QLineEdit * SmallYTicksRatioQLE;
  QLineEdit * XAxis2TicksLabelsRatioQLE;
  QLineEdit * YAxis2TicksLabelsRatioQLE;
  QLineEdit * XAxis2AxisLabelRatioQLE;
  QLineEdit * YAxis2AxisLabelRatioQLE;
  
  QLineCombo * BoxLineWidthQLC;
  QLineCombo * BigXTicksLineWidthQLC;
  QLineCombo * BigYTicksLineWidthQLC;
  QLineCombo * SmallXTicksLineWidthQLC;
  QLineCombo * SmallYTicksLineWidthQLC;
  QLineCombo * XGridLineWidthQLC;
  QLineCombo * YGridLineWidthQLC;
  
  QPushButtonColorSelect * BGColorQPBCS;
  QPushButtonColorSelect * BoxColorQPBCS;
  QPushButtonColorSelect * BigXTicksColorQPBCS;
  QPushButtonColorSelect * BigYTicksColorQPBCS;
  QPushButtonColorSelect * SmallXTicksColorQPBCS;
  QPushButtonColorSelect * SmallYTicksColorQPBCS;
  QPushButtonColorSelect * XTicksLabelsColorQPBCS;
  QPushButtonColorSelect * YTicksLabelsColorQPBCS;
  QPushButtonColorSelect * XAxisLabelColorQPBCS;
  QPushButtonColorSelect * YAxisLabelColorQPBCS;
  QPushButtonColorSelect * TitleColorQPBCS;
  QPushButtonColorSelect * XGridColorQPBCS;
  QPushButtonColorSelect * YGridColorQPBCS;
  
  QFontSelector * XAxisLabelFontQFS;
  QFontSelector * YAxisLabelFontQFS;
  QFontSelector * XTicksLabelsFontQFS;
  QFontSelector * YTicksLabelsFontQFS;
  QFontSelector * TitleFontQFS;
  
  QPenCombo * BoxLineStyleQPC;
  QPenCombo * BigXTicksLineStyleQPC;
  QPenCombo * BigYTicksLineStyleQPC;
  QPenCombo * SmallXTicksLineStyleQPC;
  QPenCombo * SmallYTicksLineStyleQPC;
  QPenCombo * XGridLineStyleQPC;
  QPenCombo * YGridLineStyleQPC;
  
 public:
                                   QTabDialogSettingsGraph     (QWidget * Parent = 0, const char * Name = 0);
  virtual                         ~QTabDialogSettingsGraph     ();
          QLineEdit              * GetXRatioQLE                ();
	  QLineEdit              * GetYRatioQLE                ();
	  QLineEdit              * GetWidthRatioQLE            ();
	  QLineEdit              * GetHeightRatioQLE           ();
	  QLineEdit              * GetAxis2TitleRatioQLE       ();
	  QLineEdit              * GetBigXTicksRatioQLE        ();
	  QLineEdit              * GetBigYTicksRatioQLE        ();
	  QLineEdit              * GetSmallXTicksRatioQLE      ();
	  QLineEdit              * GetSmallYTicksRatioQLE      ();
	  QLineEdit              * GetXAxis2TicksLabelsRatioQLE();
	  QLineEdit              * GetYAxis2TicksLabelsRatioQLE();
	  QLineEdit              * GetXAxis2AxisLabelRatioQLE  ();
	  QLineEdit              * GetYAxis2AxisLabelRatioQLE  ();
	  QLineCombo             * GetBoxLineWidthQLC          ();
	  QLineCombo             * GetBigXTicksLineWidthQLC    ();
	  QLineCombo             * GetBigYTicksLineWidthQLC    ();
	  QLineCombo             * GetSmallXTicksLineWidthQLC  ();
	  QLineCombo             * GetSmallYTicksLineWidthQLC  ();
	  QLineCombo             * GetXGridLineWidthQLC        ();
	  QLineCombo             * GetYGridLineWidthQLC        ();
	  QPushButtonColorSelect * GetBGColorQPBCS             ();
	  QPushButtonColorSelect * GetBoxColorQPBCS            ();
	  QPushButtonColorSelect * GetBigXTicksColorQPBCS      ();
	  QPushButtonColorSelect * GetBigYTicksColorQPBCS      ();
	  QPushButtonColorSelect * GetSmallXTicksColorQPBCS    ();
	  QPushButtonColorSelect * GetSmallYTicksColorQPBCS    ();
	  QPushButtonColorSelect * GetXTicksLabelsColorQPBCS   ();
	  QPushButtonColorSelect * GetYTicksLabelsColorQPBCS   ();
	  QPushButtonColorSelect * GetXAxisLabelColorQPBCS     ();
	  QPushButtonColorSelect * GetYAxisLabelColorQPBCS     ();
	  QPushButtonColorSelect * GetTitleColorQPBCS          ();
	  QPushButtonColorSelect * GetXGridColorQPBCS          ();
	  QPushButtonColorSelect * GetYGridColorQPBCS          ();
	  QFontSelector          * GetXAxisLabelFontQFS        ();
	  QFontSelector          * GetYAxisLabelFontQFS        ();
	  QFontSelector          * GetXTicksLabelsFontQFS      ();
	  QFontSelector          * GetYTicksLabelsFontQFS      ();
	  QFontSelector          * GetTitleFontQFS             ();
	  QPenCombo              * GetBoxLineStyleQPC          ();
	  QPenCombo              * GetBigXTicksLineStyleQPC    ();
	  QPenCombo              * GetBigYTicksLineStyleQPC    ();
	  QPenCombo              * GetSmallXTicksLineStyleQPC  ();
	  QPenCombo              * GetSmallYTicksLineStyleQPC  ();
	  QPenCombo              * GetXGridLineStyleQPC        ();
	  QPenCombo              * GetYGridLineStyleQPC        ();
	  void                     SetR2L                      (bool);
};

#endif
