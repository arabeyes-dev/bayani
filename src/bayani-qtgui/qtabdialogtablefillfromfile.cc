/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogTableFillFromFile Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogtablefillfromfile.h>

QTabDialogTableFillFromFile::QTabDialogTableFillFromFile(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB  = new QVBox(Parent);
  General1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("File"), GeneralQVB);
  General2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Options"), GeneralQVB);
  
  Sub1QHB = new QHBox(General2QGB);
  Sub1QVB = new QVBox(Sub1QHB);
  Sub2QVB = new QVBox(Sub1QHB);
  
  FileNameQL   = new QLabel(trUtf8("&File name"), General1QGB);
  FileNameQFLS = new QFileSelector(General1QGB);
  FileNameQFLS->AddFileType(trUtf8("Text files (*.txt)"));
  
  ColumnsNumberQL  = new QLabel(trUtf8("Columns &number in file"), Sub1QVB);
  ColumnsNumberQLE = new QLineEdit(Sub2QVB);
  
  SeparatorQL  = new QLabel(trUtf8("Columns &separator"), Sub1QVB);
  SeparatorQLE = new QLineEdit(Sub2QVB);
  
  StartingRowQL  = new QLabel(trUtf8("Starting &row in table (included)"), Sub1QVB);
  StartingRowQSB = new QSpinBoxBayani(1, 1, 1, Sub2QVB);
  
  StartingColumnQL  = new QLabel(trUtf8("Starting co&lumn in table (included)"), Sub1QVB);
  StartingColumnQSB = new QSpinBoxBayani(1, 1, 1, Sub2QVB);
  
  AddRowsQCKB    = new QCheckBox(trUtf8("Add ro&ws to table if necessary"   ), General2QGB);
  AddColumnsQCKB = new QCheckBox(trUtf8("Add colu&mns to table if necessary"), General2QGB);
  EmptyCellsQCKB = new QCheckBox(trUtf8("&Empty other cells"), General2QGB);
  SupLinesQCKB   = new QCheckBox(trUtf8("Acce&pt lines from file with more than the set columns number"), General2QGB);
  
  Sub2QHB = new QHBox(General2QGB);
  
  SubLinesQCKB   = new QCheckBox(trUtf8("Accep&t lines from file with less than the set columns number. Fill the remaining cells with:"), Sub2QHB);
  DefaultFillQLE = new QLineEdit(Sub2QHB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub1QHB->setSpacing(5);
  Sub1QHB->setMargin(5);
  Sub2QHB->setSpacing(5);
  //Sub2QHB->setMargin(5);
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  
  FileNameQL      ->setBuddy(FileNameQFLS     );
  ColumnsNumberQL ->setBuddy(ColumnsNumberQLE );
  SeparatorQL     ->setBuddy(SeparatorQLE     );
  StartingRowQL   ->setBuddy(StartingRowQSB   );
  StartingColumnQL->setBuddy(StartingColumnQSB);
  
  ColumnsNumberQLE->setMaximumWidth(50);
  ColumnsNumberQLE->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{1,5}"), ColumnsNumberQLE));
  SeparatorQLE->setMaximumWidth(50);
  StartingRowQSB->setMaximumWidth(50);
  StartingColumnQSB->setMaximumWidth(50);
  DefaultFillQLE->setMaximumWidth(50);
  DefaultFillQLE->setValidator(new QDoubleValidator(DefaultFillQLE));
  
  DefaultFillQLE->setEnabled(false);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Table - Fill from File"));
  
  FileNameQFLS->setFocus();
  
  connect(SubLinesQCKB, SIGNAL(toggled(bool)), DefaultFillQLE, SLOT(setEnabled(bool)));
  connect(SubLinesQCKB, SIGNAL(clicked()    ), DefaultFillQLE, SLOT(setFocus()      ));
  connect(SubLinesQCKB, SIGNAL(pressed()    ), DefaultFillQLE, SLOT(clearFocus()    ));
}

QTabDialogTableFillFromFile::~QTabDialogTableFillFromFile()
{
}

QLineEdit * QTabDialogTableFillFromFile::GetColumnsNumberQLE()
{
  return ColumnsNumberQLE;
}

QLineEdit * QTabDialogTableFillFromFile::GetSeparatorQLE()
{
  return SeparatorQLE;
}

QSpinBoxBayani * QTabDialogTableFillFromFile::GetStartingRowQSB()
{
  return StartingRowQSB;
}

QSpinBoxBayani * QTabDialogTableFillFromFile::GetStartingColumnQSB()
{
  return StartingColumnQSB;
}

QFileSelector * QTabDialogTableFillFromFile::GetFileNameQFLS()
{
  return FileNameQFLS;
}

QCheckBox * QTabDialogTableFillFromFile::GetAddRowsQCKB()
{
  return AddRowsQCKB;
}

QCheckBox * QTabDialogTableFillFromFile::GetAddColumnsQCKB()
{
  return AddColumnsQCKB;
}

QCheckBox * QTabDialogTableFillFromFile::GetEmptyCellsQCKB()
{
  return EmptyCellsQCKB;
}

QCheckBox * QTabDialogTableFillFromFile::GetSupLinesQCKB()
{
  return SupLinesQCKB;
}

QCheckBox * QTabDialogTableFillFromFile::GetSubLinesQCKB()
{
  return SubLinesQCKB;
}

QLineEdit * QTabDialogTableFillFromFile::GetDefaultFillQLE()
{
  return DefaultFillQLE;
}

void QTabDialogTableFillFromFile::SetR2L(bool R2LFlag)
{
  FileNameQFLS->SetR2L(R2LFlag);
  
  if(R2LFlag)
    {
      ColumnsNumberQLE->setAlignment(Qt::AlignRight);
      SeparatorQLE->setAlignment(Qt::AlignRight);
      StartingRowQSB->GetEditor()->setAlignment(Qt::AlignRight);
      StartingColumnQSB->GetEditor()->setAlignment(Qt::AlignRight);
      DefaultFillQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      ColumnsNumberQLE->setAlignment(Qt::AlignLeft);
      SeparatorQLE->setAlignment(Qt::AlignLeft);
      StartingRowQSB->GetEditor()->setAlignment(Qt::AlignLeft);
      StartingColumnQSB->GetEditor()->setAlignment(Qt::AlignLeft);
      DefaultFillQLE->setAlignment(Qt::AlignLeft);
    }
}
