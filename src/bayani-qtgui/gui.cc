/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the GUI Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <icons.h>
#include <gui.h>

GUI::GUI(QWidget * Parent, const char * Name)
  :QMainWindow(Parent, Name)
{
  /* Initialization of some global variables */
  
  FirstShow                = true;
  TerminalSenderFlag       = false;
  FramesCounter            = 0;
  SettingsDataCurrentTabID = 0;
  TopFrame                 = NULL;
  
  /* Initialization of the workspace */
  
  WorkSpaceQWS = new QWorkspace(this);
  WorkSpaceQWS->setScrollBarsEnabled(true);
  
  setCentralWidget(WorkSpaceQWS);
  
  /* Initialization of actions */
  
  FileQuitQA             = new QAction(QIconSet(QPixmap(FileQuitXPM            )), trUtf8("&Quit"                 ), CTRL + Key_Q        , this);
  FrameNewQA             = new QAction(QIconSet(QPixmap(FrameNewXPM            )), trUtf8("&New"                  ), CTRL + Key_N        , this);
  FrameZoneQA            = new QAction(QIconSet(QPixmap(FrameZoneXPM           )), trUtf8("Define &Zones..."      ), CTRL + Key_Z        , this);
  FrameSetActiveGraphQA  = new QAction(QIconSet(QPixmap(FrameSetActiveGraphXPM )), trUtf8("Set Active &Graph..."  ), QKeySequence()      , this);
  FrameSaveQA            = new QAction(QIconSet(QPixmap(FrameSaveXPM           )), trUtf8("&Save"                 ), CTRL + Key_S        , this);
  FrameSaveAsQA          = new QAction(QIconSet(QPixmap(FrameSaveAsXPM         )), trUtf8("Save &As..."           ), CTRL + SHIFT + Key_S, this);
  FrameCloseQA           = new QAction(QIconSet(QPixmap(FrameCloseXPM          )), trUtf8("&Close"                ), CTRL + Key_W        , this);
  TableFillQA            = new QAction(QIconSet(QPixmap(TableFillXPM           )), trUtf8("&Fill from File..."    ), CTRL + Key_T        , this);
  TableWriteQA           = new QAction(QIconSet(QPixmap(TableWriteXPM          )), trUtf8("&Write to File..."     ), CTRL + SHIFT + Key_T, this);
  TableSwapQA            = new QAction(QIconSet(QPixmap(TableSwapXPM           )), trUtf8("&Swap Rows/Columns..." ), QKeySequence()      , this);
  TableTransposeQA       = new QAction(QIconSet(QPixmap(TableTransposeXPM      )), trUtf8("&Transpose Table"      ), QKeySequence()      , this);
  TableManageQA          = new QAction(QIconSet(QPixmap(TableManageXPM         )), trUtf8("&Manage Size..."       ), CTRL + Key_J        , this);
  TableEmptyQA           = new QAction(QIconSet(QPixmap(TableEmptyXPM          )), trUtf8("&Empty Cells..."       ), CTRL + Key_E        , this);
  GraphFunctionQA        = new QAction(QIconSet(QPixmap(GraphFunctionXPM       )), trUtf8("&Function..."          ), CTRL + Key_F        , this);
  GraphHistogramQA       = new QAction(QIconSet(QPixmap(GraphHistogramXPM      )), trUtf8("&Histogram..."         ), CTRL + Key_H        , this);
  GraphGraph2DQA         = new QAction(QIconSet(QPixmap(GraphGraph2DXPM        )), trUtf8("2D &Graph..."          ), CTRL + Key_G        , this);
  GraphFITSQA            = new QAction(QIconSet(QPixmap(GraphFITSXPM           )), trUtf8("FITS &Image..."        ), CTRL + Key_I        , this);
  GraphBoundsQA          = new QAction(QIconSet(QPixmap(GraphBoundsXPM         )), trUtf8("Set &Bounds..."        ), CTRL + Key_B        , this);
  GraphAxesQA            = new QAction(QIconSet(QPixmap(GraphAxesXPM           )), trUtf8("Set &Axes..."          ), CTRL + Key_A        , this);
  GraphFitQA             = new QAction(QIconSet(QPixmap(GraphFitXPM            )), trUtf8("Fit &Data..."          ), CTRL + Key_D        , this);
  GraphCloseQA           = new QAction(QIconSet(QPixmap(GraphCloseXPM          )), trUtf8("&Close"                ), CTRL + SHIFT + Key_W, this);
  ComputationVariablesQA = new QAction(QIconSet(QPixmap(ComputationVariablesXPM)), trUtf8("&Variables..."         ), CTRL + Key_V        , this);
  ComputationComputeQA   = new QAction(QIconSet(QPixmap(ComputationComputeXPM  )), trUtf8("&Compute..."           ), CTRL + Key_C        , this);
  TextsFrameQA           = new QAction(QIconSet(QPixmap(TextsFrameXPM          )), trUtf8("&Frame..."             ), CTRL + Key_M        , this);
  TextsGraphQA           = new QAction(QIconSet(QPixmap(TextsGraphXPM          )), trUtf8("&Graph..."             ), CTRL + Key_R        , this);
  SettingsGlobalQA       = new QAction(QIconSet(QPixmap(SettingsGlobalXPM      )), trUtf8("Glo&bal..."            ), Key_F2              , this);
  SettingsFrameQA        = new QAction(QIconSet(QPixmap(SettingsFrameXPM       )), trUtf8("&Frame..."             ), Key_F3              , this);
  SettingsGraphQA        = new QAction(QIconSet(QPixmap(SettingsGraphXPM       )), trUtf8("&Graph..."             ), Key_F4              , this);
  SettingsDataQA         = new QAction(QIconSet(QPixmap(SettingsDataXPM        )), trUtf8("&Data..."              ), Key_F5              , this);
  SettingsSaveQA         = new QAction(QIconSet(QPixmap(SettingsSaveXPM        )), trUtf8("&Save Current Settings"), QKeySequence()      , this);
  HelpContentsQA         = new QAction(QIconSet(QPixmap(HelpContentsXPM        )), trUtf8("Bayani &Handbook..."   ), Key_F1              , this);
  HelpWhatsThisQA        = new QAction(QIconSet(QPixmap(HelpWhatsThisXPM       )), trUtf8("What's &This?"         ), SHIFT + Key_F1      , this);
  HelpAboutQA            = new QAction(QIconSet(QPixmap(BayaniLogoXPM          )), trUtf8("&About Bayani..."      ), QKeySequence()      , this);
  
  /* Initialization of the main menu */
  
  FileMenuQPM        = new QPopupMenu(this);
  ViewMenuQPM        = new QPopupMenu(this);
  ShowHideMenuQPM    = new QPopupMenu(this);
  FrameMenuQPM       = new QPopupMenu(this);
  TableMenuQPM       = new QPopupMenu(this);
  GraphMenuQPM       = new QPopupMenu(this);
  ComputationMenuQPM = new QPopupMenu(this);
  TextsMenuQPM       = new QPopupMenu(this);
  SettingsMenuQPM    = new QPopupMenu(this);
  HelpMenuQPM        = new QPopupMenu(this);
  
                menuBar()->insertItem(trUtf8("&File"       ), FileMenuQPM       );
		menuBar()->insertItem(trUtf8("&View"       ), ViewMenuQPM       );
		menuBar()->insertItem(trUtf8("Fra&me"      ), FrameMenuQPM      );
		menuBar()->insertItem(trUtf8("&Table"      ), TableMenuQPM      );
  GraphMenuID = menuBar()->insertItem(trUtf8("&Graph"      ), GraphMenuQPM      );
		menuBar()->insertItem(trUtf8("&Computation"), ComputationMenuQPM);
  TextsMenuID = menuBar()->insertItem(trUtf8("Te&xts"      ), TextsMenuQPM      );
                menuBar()->insertItem(trUtf8("&Settings"   ), SettingsMenuQPM   );
		menuBar()->insertItem(trUtf8("&Help"       ), HelpMenuQPM       );
  
  /* Initializing each menu */
  
  /*
    FileMenuQPM->insertItem(trUtf8("Open session..."));
    FileMenuQPM->insertSeparator();
    FileMenuQPM->insertItem(trUtf8("Save session"));
    FileMenuQPM->insertItem(trUtf8("Save session as..."));
    FileMenuQPM->insertSeparator();
    FileMenuQPM->insertItem(trUtf8("Close session"));
    FileMenuQPM->insertSeparator();
  */
  FileQuitQA->addTo(FileMenuQPM);
  
                              ViewMenuQPM    ->insertItem(trUtf8("&Show/Hide"      ), ShowHideMenuQPM                                 );
  ViewDataTableMenuID       = ShowHideMenuQPM->insertItem(trUtf8("&Table"          ), this           , SLOT(ViewDataTableSlot      ()));
  ViewScriptingWindowMenuID = ShowHideMenuQPM->insertItem(trUtf8("Ter&minal"       ), this           , SLOT(ViewScriptingWindowSlot()));
                              ViewMenuQPM    ->insertItem(trUtf8("&Cascade Windows"), WorkSpaceQWS   , SLOT(cascade                ()));
			      ViewMenuQPM    ->insertItem(trUtf8("&Tile Windows"   ), WorkSpaceQWS   , SLOT(tile                   ()));
  /* ViewMenuQPM->insertItem(trUtf8("Show windows")); */
  /* ViewMenuQPM->insertItem(trUtf8("Hide windows")); */
  
  FrameNewQA           ->addTo(FrameMenuQPM);
  FrameMenuQPM->insertSeparator();
  FrameZoneQA          ->addTo(FrameMenuQPM);
  FrameSetActiveGraphQA->addTo(FrameMenuQPM);
  FrameMenuQPM->insertSeparator();
  FrameSaveQA          ->addTo(FrameMenuQPM);
  FrameSaveAsQA        ->addTo(FrameMenuQPM);
  FrameMenuQPM->insertSeparator();
  FrameCloseQA         ->addTo(FrameMenuQPM);
  
  TableFillQA     ->addTo(TableMenuQPM);
  /* TableMenuQPM->insertItem(trUtf8("Fill from plot...")); */
  TableWriteQA    ->addTo(TableMenuQPM);
  TableMenuQPM->insertSeparator();
  TableSwapQA     ->addTo(TableMenuQPM);
  TableTransposeQA->addTo(TableMenuQPM);
  TableManageQA   ->addTo(TableMenuQPM);
  TableEmptyQA    ->addTo(TableMenuQPM);
  
  GraphFunctionQA ->addTo(GraphMenuQPM);
  GraphHistogramQA->addTo(GraphMenuQPM);
  GraphGraph2DQA  ->addTo(GraphMenuQPM);
  GraphFITSQA     ->addTo(GraphMenuQPM);
  GraphMenuQPM->insertSeparator();
  GraphBoundsQA   ->addTo(GraphMenuQPM);
  GraphAxesQA     ->addTo(GraphMenuQPM);
  GraphMenuQPM->insertSeparator();
  GraphFitQA      ->addTo(GraphMenuQPM);
  GraphMenuQPM->insertSeparator();
  GraphCloseQA    ->addTo(GraphMenuQPM);  
  
  ComputationVariablesQA->addTo(ComputationMenuQPM);
  ComputationComputeQA  ->addTo(ComputationMenuQPM);
  
  TextsFrameQA->addTo(TextsMenuQPM);
  TextsGraphQA->addTo(TextsMenuQPM);
  
  SettingsGlobalQA->addTo(SettingsMenuQPM);
  SettingsFrameQA ->addTo(SettingsMenuQPM);
  SettingsGraphQA ->addTo(SettingsMenuQPM);
  SettingsDataQA  ->addTo(SettingsMenuQPM);
  SettingsMenuQPM->insertSeparator();
  SettingsSaveQA  ->addTo(SettingsMenuQPM);
  
  HelpContentsQA ->addTo(HelpMenuQPM);
  HelpWhatsThisQA->addTo(HelpMenuQPM);
  HelpMenuQPM->insertSeparator();
  HelpAboutQA    ->addTo(HelpMenuQPM);
  
  /* Initialization of the tool bar */
  
  ToolBarQTB = new QToolBar(trUtf8("Toolbar"), this);
  
  /* Adding actions to the tool bar */
  
  FrameNewQA          ->addTo(ToolBarQTB);
  FrameZoneQA         ->addTo(ToolBarQTB);
  FrameSaveQA         ->addTo(ToolBarQTB);
  FrameSaveAsQA       ->addTo(ToolBarQTB);
  ToolBarQTB->addSeparator();
  TableFillQA         ->addTo(ToolBarQTB);
  TableWriteQA        ->addTo(ToolBarQTB);
  TableManageQA       ->addTo(ToolBarQTB);
  TableEmptyQA        ->addTo(ToolBarQTB);
  ToolBarQTB->addSeparator();
  GraphFunctionQA     ->addTo(ToolBarQTB);
  GraphHistogramQA    ->addTo(ToolBarQTB);
  GraphGraph2DQA      ->addTo(ToolBarQTB);
  GraphFITSQA         ->addTo(ToolBarQTB);
  GraphBoundsQA       ->addTo(ToolBarQTB);
  GraphAxesQA         ->addTo(ToolBarQTB);
  GraphFitQA          ->addTo(ToolBarQTB);
  GraphCloseQA        ->addTo(ToolBarQTB);
  ToolBarQTB->addSeparator();
  ComputationComputeQA->addTo(ToolBarQTB);
  ToolBarQTB->addSeparator();
  TextsFrameQA        ->addTo(ToolBarQTB);
  TextsGraphQA        ->addTo(ToolBarQTB);
  ToolBarQTB->addSeparator();
  SettingsFrameQA     ->addTo(ToolBarQTB);
  SettingsGraphQA     ->addTo(ToolBarQTB);
  SettingsDataQA      ->addTo(ToolBarQTB);
  ToolBarQTB->addSeparator();
  HelpContentsQA      ->addTo(ToolBarQTB);
  
  /* Initialization of the data table */
  
  DataTable = new QTableBayani(WorkSpaceQWS);
  DataTable->setCaption(trUtf8("Table"));
  
  /* Initialization of the scripting window */
  
  /*
    ScriptingWindowDock = new QDockWindow();
    ScriptingWindowDock->setResizeEnabled(true);
    ScriptingWindow = new QTextEditBayani(ScriptingWindowDock);
    ScriptingWindowDock->setWidget(ScriptingWindow);
    addDockWindow(ScriptingWindowDock, trUtf8("?????"), Qt::DockBottom);
  */
  ScriptingWindow = new QTextEditBayani(WorkSpaceQWS);
  ScriptingWindow->setCaption(trUtf8("Terminal"));
  
  /* Initialization of the dialogs */
  
  FrameZoneQTD            = new QTabDialogFrameZone           (this);
  FrameSetActivePlotQTD   = new QTabDialogFrameSetActivePlot  (this);
  TableFillFromFileQTD    = new QTabDialogTableFillFromFile   (this);
  TableWriteToFileQTD     = new QTabDialogTableWriteToFile    (this);
  TableSwapQTD            = new QTabDialogTableSwap           (this);
  TableManageQTD          = new QTabDialogTableManage         (this);
  TableEmptyQTD           = new QTabDialogTableEmpty          (this);
  GraphFunctionQTD        = new QTabDialogGraphFunction       (this);
  GraphHistogramQTD       = new QTabDialogGraphHistogram      (this);
  GraphGraph2DQTD         = new QTabDialogGraphGraph2D        (this);
  GraphFITSQTD            = new QTabDialogGraphFITS           (this);
  GraphBoundsQTD          = new QTabDialogGraphBounds         (this);
  GraphAxesQTD            = new QTabDialogGraphAxes           (this);
  GraphFitQTD             = new QTabDialogGraphFit            (this);
  ComputationVariablesQTD = new QTabDialogComputationVariables(this);
  ComputationComputeQTD   = new QTabDialogComputationCompute  (this);
  TextsFrameQTD           = new QTabDialogTextsFrame          (this);
  TextsGraphQTD           = new QTabDialogTextsGraph          (this);
  SettingsGlobalQTD       = new QTabDialogSettingsGlobal      (this);
  SettingsFrameQTD        = new QTabDialogSettingsFrame       (this);
  SettingsGraphQTD        = new QTabDialogSettingsGraph       (this);
  SettingsDataQTD         = new QTabDialogSettingsData        (this);
  HelpContentsBHB         = new BHelpBrowser                  (this);
  HelpAboutQTD            = new QTabDialogHelpAbout           (this);
  
  /* Connecting slots */
  
  connect(WorkSpaceQWS           , SIGNAL(windowActivated(QWidget *)), this, SLOT(ChangeTopFrameSlot(QWidget *)));
  connect(FileQuitQA             , SIGNAL(activated())               , this, SLOT(FileQuitSlot            ()));
  connect(FrameNewQA             , SIGNAL(activated())               , this, SLOT(FrameNewSlot            ()));
  connect(FrameZoneQA            , SIGNAL(activated())               , this, SLOT(FrameZoneSlot           ()));
  connect(FrameSetActiveGraphQA  , SIGNAL(activated())               , this, SLOT(FrameSetActivePlotSlot  ()));
  connect(FrameSaveQA            , SIGNAL(activated())               , this, SLOT(FrameSaveSlot           ()));
  connect(FrameSaveAsQA          , SIGNAL(activated())               , this, SLOT(FrameSaveAsSlot         ()));
  connect(FrameCloseQA           , SIGNAL(activated())               , this, SLOT(FrameCloseSlot          ()));
  connect(TableFillQA            , SIGNAL(activated())               , this, SLOT(TableFillFromFileSlot   ()));
  connect(TableWriteQA           , SIGNAL(activated())               , this, SLOT(TableWriteToFileSlot    ()));
  connect(TableSwapQA            , SIGNAL(activated())               , this, SLOT(TableSwapSlot           ()));
  connect(TableTransposeQA       , SIGNAL(activated())               , this, SLOT(TableTransposeSlot      ()));
  connect(TableManageQA          , SIGNAL(activated())               , this, SLOT(TableManageSlot         ()));
  connect(TableEmptyQA           , SIGNAL(activated())               , this, SLOT(TableEmptySlot          ()));
  connect(GraphFunctionQA        , SIGNAL(activated())               , this, SLOT(GraphFunctionSlot       ()));
  connect(GraphHistogramQA       , SIGNAL(activated())               , this, SLOT(GraphHistogramSlot      ()));
  connect(GraphGraph2DQA         , SIGNAL(activated())               , this, SLOT(GraphGraph2DSlot        ()));
  connect(GraphFITSQA            , SIGNAL(activated())               , this, SLOT(GraphFITSSlot           ()));
  connect(GraphBoundsQA          , SIGNAL(activated())               , this, SLOT(GraphBoundsSlot         ()));
  connect(GraphAxesQA            , SIGNAL(activated())               , this, SLOT(GraphAxesSlot           ()));
  connect(GraphFitQA             , SIGNAL(activated())               , this, SLOT(GraphFitSlot            ()));
  connect(GraphCloseQA           , SIGNAL(activated())               , this, SLOT(GraphCloseSlot          ()));
  connect(ComputationVariablesQA , SIGNAL(activated())               , this, SLOT(ComputationVariablesSlot()));
  connect(ComputationComputeQA   , SIGNAL(activated())               , this, SLOT(ComputationComputeSlot  ()));
  connect(TextsFrameQA           , SIGNAL(activated())               , this, SLOT(TextsFrameSlot          ()));
  connect(TextsGraphQA           , SIGNAL(activated())               , this, SLOT(TextsGraphSlot          ()));
  connect(SettingsGlobalQA       , SIGNAL(activated())               , this, SLOT(SettingsGlobalSlot      ()));
  connect(SettingsFrameQA        , SIGNAL(activated())               , this, SLOT(SettingsFrameSlot       ()));
  connect(SettingsGraphQA        , SIGNAL(activated())               , this, SLOT(SettingsGraphSlot       ()));
  connect(SettingsDataQA         , SIGNAL(activated())               , this, SLOT(SettingsDataSlot        ()));
  connect(SettingsSaveQA         , SIGNAL(activated())               , this, SLOT(SettingsSaveSlot        ()));
  connect(HelpContentsQA         , SIGNAL(activated())               , this, SLOT(HelpContentsSlot        ()));
  connect(HelpWhatsThisQA        , SIGNAL(activated())               , this, SLOT(whatsThis               ()));
  connect(HelpAboutQA            , SIGNAL(activated())               , this, SLOT(HelpAboutSlot           ()));
  connect(DataTable              , SIGNAL(Shown())                   , this, SLOT(CheckViewDataTableMenuSlot()));
  connect(DataTable              , SIGNAL(Closed())                  , this, SLOT(UnCheckViewDataTableMenuSlot()));
  connect(ScriptingWindow        , SIGNAL(Shown())                   , this, SLOT(CheckViewScriptingWindowMenuSlot()));
  connect(ScriptingWindow        , SIGNAL(Closed())                  , this, SLOT(UnCheckViewScriptingWindowMenuSlot()));
  connect(ScriptingWindow        , SIGNAL(CommandEntered(QString))   , this, SLOT(ParseCommandSlot(QString)));
  connect(FrameZoneQTD           , SIGNAL(applyButtonPressed())      , this, SLOT(FrameZoneApplySlot()));
  connect(FrameSetActivePlotQTD  , SIGNAL(applyButtonPressed())      , this, SLOT(FrameSetActivePlotApplySlot()));
  connect(TableFillFromFileQTD   , SIGNAL(applyButtonPressed())      , this, SLOT(TableFillFromFileApplySlot()));
  connect(TableWriteToFileQTD    , SIGNAL(applyButtonPressed())      , this, SLOT(TableWriteToFileApplySlot()));
  connect(TableSwapQTD           , SIGNAL(applyButtonPressed())      , this, SLOT(TableSwapApplySlot()));
  connect(TableManageQTD         , SIGNAL(applyButtonPressed())      , this, SLOT(TableManageApplySlot()));
  connect(TableEmptyQTD          , SIGNAL(applyButtonPressed())      , this, SLOT(TableEmptyApplySlot()));
  connect(GraphFunctionQTD       , SIGNAL(applyButtonPressed())      , this, SLOT(GraphFunctionApplySlot()));
  connect(GraphHistogramQTD      , SIGNAL(applyButtonPressed())      , this, SLOT(GraphHistogramApplySlot()));
  connect(GraphGraph2DQTD        , SIGNAL(applyButtonPressed())      , this, SLOT(GraphGraph2DApplySlot()));
  connect(GraphFITSQTD           , SIGNAL(applyButtonPressed())      , this, SLOT(GraphFITSApplySlot()));
  connect(GraphBoundsQTD         , SIGNAL(applyButtonPressed())      , this, SLOT(GraphBoundsApplySlot()));
  connect(GraphAxesQTD           , SIGNAL(applyButtonPressed())      , this, SLOT(GraphAxesApplySlot()));
  connect(GraphFitQTD            , SIGNAL(applyButtonPressed())      , this, SLOT(GraphFitApplySlot()));
  connect(ComputationVariablesQTD, SIGNAL(applyButtonPressed())      , this, SLOT(ComputationVariablesApplySlot()));
  connect(ComputationVariablesQTD, SIGNAL(helpButtonPressed ())      , this, SLOT(ComputationVariablesHelpSlot ()));
  connect(ComputationComputeQTD  , SIGNAL(applyButtonPressed())      , this, SLOT(ComputationComputeApplySlot  ()));
  connect(ComputationComputeQTD  , SIGNAL(helpButtonPressed ())      , this, SLOT(ComputationComputeHelpSlot   ()));
  connect(TextsFrameQTD          , SIGNAL(applyButtonPressed())      , this, SLOT(TextsFrameApplySlot()));
  connect(TextsGraphQTD          , SIGNAL(applyButtonPressed())      , this, SLOT(TextsGraphApplySlot()));
  connect(SettingsGlobalQTD      , SIGNAL(applyButtonPressed())      , this, SLOT(SettingsGlobalApplySlot()));
  connect(SettingsGlobalQTD      , SIGNAL(defaultButtonPressed())    , this, SLOT(SettingsGlobalDefaultsSlot()));
  connect(SettingsGlobalQTD      , SIGNAL(cancelButtonPressed())     , this, SLOT(SettingsGlobalCancelSlot()));
  connect(SettingsFrameQTD       , SIGNAL(applyButtonPressed())      , this, SLOT(SettingsFrameApplySlot()));
  connect(SettingsFrameQTD       , SIGNAL(defaultButtonPressed())    , this, SLOT(SettingsFrameDefaultsSlot()));
  connect(SettingsFrameQTD       , SIGNAL(cancelButtonPressed())     , this, SLOT(SettingsFrameCancelSlot()));
  connect(SettingsGraphQTD       , SIGNAL(applyButtonPressed())      , this, SLOT(SettingsGraphApplySlot()));
  connect(SettingsGraphQTD       , SIGNAL(defaultButtonPressed())    , this, SLOT(SettingsGraphDefaultsSlot()));
  connect(SettingsGraphQTD       , SIGNAL(cancelButtonPressed())     , this, SLOT(SettingsGraphCancelSlot()));
  connect(SettingsDataQTD        , SIGNAL(applyButtonPressed())      , this, SLOT(SettingsDataApplySlot()));
  connect(SettingsDataQTD        , SIGNAL(defaultButtonPressed())    , this, SLOT(SettingsDataDefaultsSlot()));
  connect(SettingsDataQTD        , SIGNAL(cancelButtonPressed())     , this, SLOT(SettingsDataCancelSlot()));
  connect(SettingsDataQTD        , SIGNAL(currentChanged(QWidget *)) , this, SLOT(SettingsDataChangedSlot(QWidget *)));
  
  /* Items in the dialogs themselves */
  
  connect(TableSwapQTD         ->GetRowsQRB        (), SIGNAL(toggled(bool)), this, SLOT(TableSwapRowsQRBSlot     (bool)));
  connect(TableManageQTD       ->GetRowsQRB        (), SIGNAL(toggled(bool)), this, SLOT(TableManageRowsQRBSlot   (bool)));
  connect(GraphFunctionQTD     ->GetPlotSettingsQPB(), SIGNAL(clicked()    ), this, SLOT(SettingsDataFunctionSlot ()    ));
  connect(GraphHistogramQTD    ->GetPlotSettingsQPB(), SIGNAL(clicked()    ), this, SLOT(SettingsDataHistogramSlot()    ));
  connect(GraphGraph2DQTD      ->GetPlotSettingsQPB(), SIGNAL(clicked()    ), this, SLOT(SettingsDataGraph2DSlot  ()    ));
  connect(GraphFITSQTD         ->GetPlotSettingsQPB(), SIGNAL(clicked()    ), this, SLOT(SettingsDataFITSSlot     ()    ));
  connect(GraphFitQTD          ->GetPlotSettingsQPB(), SIGNAL(clicked()    ), this, SLOT(SettingsDataFunctionSlot ()    ));
  connect(ComputationComputeQTD->GetVariablesQPB   (), SIGNAL(clicked()    ), this, SLOT(ComputationVariablesSlot ()    ));
  
  /* Default settings */
  
  SetGlobalDefaultSettings();
  SettingsGlobalCancelSlot();
  SetFrameDefaultSettings ();
  SettingsFrameCancelSlot ();
  SetGraphDefaultSettings ();
  SettingsGraphCancelSlot ();
  SetDataDefaultSettings  ();
  SettingsDataCancelSlot  ();
  
  ApplyGlobalDefaultSettings();
  DisableFrameRelatedMenus  ();
  
  QDir  Dir;
  QFile HistoryFile(Dir.homeDirPath() + QString(BAYANI_DIR) + QString(BAYANI_HISFILE) + QString(BAYANI_EXT));
  
  if(HistoryFile.open(IO_ReadOnly))
    {
      QTextStream HistoryStream(&HistoryFile);
      vector      <QString> History;
      QString     Line = "";
      
      while(!Line.isNull())
	{
	  Line = HistoryStream.readLine();
	  
	  if(Line != "")
	    History.push_back(Line);
	}
      
      HistoryFile.close();
      
      ScriptingWindow->SetHistory(History);
    }
  
  /* Initialization of the status bar */
  
  PrintStatus(trUtf8("Ready."));
}

GUI::~GUI()
{
}

void GUI::SetR2L(bool NewOptionGlobalAppearanceR2LFlag)
{
  OptionGlobalAppearanceR2LFlag = NewOptionGlobalAppearanceR2LFlag;
  
  SetLayout();
}

void GUI::showEvent(QShowEvent *)
{
  if(FirstShow)
    {
      /* Why is it needed to call parentWidget() here? */
      
      QRect CurrentGeometry                = geometry();
      QRect CurrentDataTableGeometry       = DataTable      ->parentWidget()->geometry();
      QRect CurrentScriptingWindowGeometry = ScriptingWindow->parentWidget()->geometry();
      
      CurrentGeometry               .setSize(0.8*CurrentGeometry.size()     );
      CurrentDataTableGeometry      .setSize(0.5*WorkSpaceQWS->size()       );
      CurrentScriptingWindowGeometry.setSize(CurrentDataTableGeometry.size());
      
      CurrentGeometry               .moveCenter(geometry().center()                   );
      CurrentScriptingWindowGeometry.moveTopLeft(CurrentDataTableGeometry.bottomLeft());
      
      DataTable      ->parentWidget()->setGeometry(CurrentDataTableGeometry      );
      ScriptingWindow->parentWidget()->setGeometry(CurrentScriptingWindowGeometry);
      
      if(OptionDataTableShowFlag)
	DataTable->show();
      
      if(OptionScriptingWindowShowFlag)
	ScriptingWindow->show();
      
      HelpContentsBHB->setGeometry(CurrentGeometry);
      
      QFile TestFile(QString("%1/user/bayani.xml") .arg(DOC_DIR));
      
      if(!TestFile.exists())
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 could not be found.<br />Online help will not be available.") .arg(TestFile.name()));
      else
	{
	  HelpContentsBHB->SetFilePath(QString("%1/user/") .arg(DOC_DIR));
	  HelpContentsBHB->SetIndex(TestFile.name());
	}
      
      FirstShow = false;
    }
}

void GUI::closeEvent(QCloseEvent * Event)
{
  Event->ignore();
  
  FileQuitSlot();
}

int GUI::PrintMessage(QWidget * Parent, int Type, QString Message, QString Title, QTextEdit * OutputQTE)
{
  int ReturnCode = QMessageBox::Ok;
  
  if(Type == MESSAGE_INFORMATION)
    {
      if(Title)
	Title = trUtf8("Information: %1") .arg(Title);
      else
	Title = trUtf8("Information:");
      
      if(TerminalSenderFlag)
	ScriptingWindow->PrintInformation(QString("%1<br />%2") .arg(Title) .arg(Message));
      else
	{
	  if(OutputQTE)
	    OutputQTE->append(QString("<font color=%1>%2<br />%3</font>") .arg(OptionGlobalTerminalInformationColor.name()) .arg(Title) .arg(Message));
	  else
	    QMessageBox::information(Parent, Title,  QString("<nobr>%1</nobr>") .arg(Message), QMessageBox::Ok);
	}
    }
  else if(Type == MESSAGE_WARNING)
    {
      if(Title)
	Title = trUtf8("Warning: %1") .arg(Title);
      else
	Title = trUtf8("Warning:");
      
      //if(TerminalSenderFlag)
      //ReturnCode = ScriptingWindow->PrintWarning(QString("%1<br />%2") .arg(Title) .arg(Message));
      //else
      //{
      //if(OutputQTE)
      //ReturnCode = QMessageBox::warning(Parent, Title, QString("<nobr>%1</nobr>") .arg(Message), QMessageBox::Yes, QMessageBox::No);/* This is not supposed to happen anyway */
      //else
      ReturnCode = QMessageBox::warning(Parent, Title, QString("<nobr>%1</nobr>") .arg(Message), QMessageBox::Yes, QMessageBox::No);
      //}
    }
  else if(Type == MESSAGE_CRITICAL)
    {
      if(Title)
	Title = trUtf8("Error: %1") .arg(Title);
      else
	Title = trUtf8("Error:");
      
      if(TerminalSenderFlag)
	ScriptingWindow->PrintCritical(QString("%1<br />%2") .arg(Title) .arg(Message));
      else
	{
	  if(OutputQTE)
	    OutputQTE->append(QString("<font color=%1>%2<br />%3</font>") .arg(OptionGlobalTerminalCriticalColor.name()) .arg(Title) .arg(Message));
	  else
	    QMessageBox::critical(Parent, Title, QString("<nobr>%1</nobr>") .arg(Message), QMessageBox::Ok, QMessageBox::NoButton);
	}
    }
  
  return ReturnCode;
}

void GUI::SetLayout()
{
  int R2LAlignement = (OptionGlobalAppearanceR2LFlag) ? Qt::AlignRight : Qt::AlignLeft;
  
  DataTable->SetR2L(OptionGlobalAppearanceR2LFlag);
  ScriptingWindow->setAlignment(R2LAlignement);
  
  FrameZoneQTD           ->SetR2L(OptionGlobalAppearanceR2LFlag);
  FrameSetActivePlotQTD  ->SetR2L(OptionGlobalAppearanceR2LFlag);
  TableFillFromFileQTD   ->SetR2L(OptionGlobalAppearanceR2LFlag);
  TableWriteToFileQTD    ->SetR2L(OptionGlobalAppearanceR2LFlag);
  TableSwapQTD           ->SetR2L(OptionGlobalAppearanceR2LFlag);
  TableManageQTD         ->SetR2L(OptionGlobalAppearanceR2LFlag);
  TableEmptyQTD          ->SetR2L(OptionGlobalAppearanceR2LFlag);
  GraphFunctionQTD       ->SetR2L(OptionGlobalAppearanceR2LFlag);
  GraphHistogramQTD      ->SetR2L(OptionGlobalAppearanceR2LFlag);
  GraphGraph2DQTD        ->SetR2L(OptionGlobalAppearanceR2LFlag);
  GraphFITSQTD           ->SetR2L(OptionGlobalAppearanceR2LFlag);
  GraphBoundsQTD         ->SetR2L(OptionGlobalAppearanceR2LFlag);
  GraphAxesQTD           ->SetR2L(OptionGlobalAppearanceR2LFlag);
  GraphFitQTD            ->SetR2L(OptionGlobalAppearanceR2LFlag);
  ComputationVariablesQTD->SetR2L(OptionGlobalAppearanceR2LFlag);
  ComputationComputeQTD  ->SetR2L(OptionGlobalAppearanceR2LFlag);
  TextsFrameQTD          ->SetR2L(OptionGlobalAppearanceR2LFlag);
  TextsGraphQTD          ->SetR2L(OptionGlobalAppearanceR2LFlag);
  SettingsGlobalQTD      ->SetR2L(OptionGlobalAppearanceR2LFlag);
  SettingsFrameQTD       ->SetR2L(OptionGlobalAppearanceR2LFlag);
  SettingsGraphQTD       ->SetR2L(OptionGlobalAppearanceR2LFlag);
  SettingsDataQTD        ->SetR2L(OptionGlobalAppearanceR2LFlag);
  HelpContentsBHB        ->SetR2L(OptionGlobalAppearanceR2LFlag);
  HelpAboutQTD           ->SetR2L(OptionGlobalAppearanceR2LFlag);
}

void GUI::SetGlobalDefaultSettings()
{
  QDir Dir;
  QSettings SettingsFile(QSettings::Ini);/* Needs to be defined here, because the destructor needs to be called! */
  SettingsFile.insertSearchPath(QSettings::Unix, Dir.homeDirPath() + QString(BAYANI_DIR));
  
  SettingsFile.beginGroup("/Settings/GUI");
  SettingsFile.beginGroup("/Global");
  
  SettingsFile.beginGroup("/Appearance");
  OptionGlobalAppearanceR2LFlag = SettingsFile.readBoolEntry("/R2LFlag", false);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Terminal");
  OptionGlobalTerminalHistorySize =                  SettingsFile.readNumEntry("/HistorySize"     , 1000              );
  OptionGlobalTerminalTextColor       .setNamedColor(SettingsFile.readEntry   ("/TextColor"       , "#CCCCCC"        ));
  OptionGlobalTerminalInformationColor.setNamedColor(SettingsFile.readEntry   ("/InformationColor", "#55AA7F"        ));
  OptionGlobalTerminalWarningColor    .setNamedColor(SettingsFile.readEntry   ("/WarningColor"    , "#FFAA00"        ));
  OptionGlobalTerminalCriticalColor   .setNamedColor(SettingsFile.readEntry   ("/CriticalColor"   , "#FF0000"        ));
  OptionGlobalTerminalStatusColor     .setNamedColor(SettingsFile.readEntry   ("/StatusColor"     , "#55AAFF"        ));
  OptionGlobalTerminalBGColor         .setNamedColor(SettingsFile.readEntry   ("/BGColor"         , "#000000"        ));
  OptionGlobalTerminalTextFont        .fromString   (SettingsFile.readEntry   ("/TextFont"        , font().toString()));
  SettingsFile.endGroup();
  
  SettingsFile.endGroup();
  SettingsFile.endGroup();
  
  OptionDataTableShowFlag       = true;
  OptionDataTableRowsNumber     = 100;
  OptionDataTableColumnsNumber  = 4;
  OptionScriptingWindowShowFlag = true;
  
  OptionFrameZoneHorNumber = 2;
  OptionFrameZoneVerNumber = 2;
  
  OptionGraphHistogramColumnFlag     = true;
  OptionGraphHistogramBinsNumberFlag = false;
  OptionGraphHistogramXMinFlag       = false;
  OptionGraphHistogramXMaxFlag       = false;
  OptionGraphHistogramCutFlag        = false;
  
  GraphBoundsNewFlag = false;
  GraphAxesNewFlag   = false;
  
  OptionGlobalExternalErrorColor.setNamedColor("#AA00FF");
}

void GUI::ApplyGlobalDefaultSettings()
{
  /* Application's geometry */
  /* History */
  ScriptingWindow->SetTextColor       (OptionGlobalTerminalTextColor       );
  ScriptingWindow->SetInformationColor(OptionGlobalTerminalInformationColor);
  ScriptingWindow->SetWarningColor    (OptionGlobalTerminalWarningColor    );
  ScriptingWindow->SetCriticalColor   (OptionGlobalTerminalCriticalColor   );
  ScriptingWindow->SetStatusColor     (OptionGlobalTerminalStatusColor     );
  ScriptingWindow->SetBGColor         (OptionGlobalTerminalBGColor         );
  ScriptingWindow->SetTextFont        (OptionGlobalTerminalTextFont        );
  
  GraphFitQTD          ->GetOutputQTE()->setPaper(ScriptingWindow->paper());
  ComputationComputeQTD->GetOutputQTE()->setPaper(ScriptingWindow->paper());
  
  /* Return here if someflag */
  
  /* Show Table/Terminal and their geometries */
  /* Table's graphical attributes: font color and family and bg color */
  DataTable->setNumRows(OptionDataTableRowsNumber   );
  DataTable->setNumCols(OptionDataTableColumnsNumber);
  UpdateTableRelatedItems();
  
  /* New frames and their geometries */
  
  if(OptionFrameZoneHorNumber > 0)
    FrameZoneQTD->GetHorZoneQLE()->setText(QString("%1") .arg(OptionFrameZoneHorNumber));
  
  if(OptionFrameZoneVerNumber > 0)
    FrameZoneQTD->GetVerZoneQLE()->setText(QString("%1") .arg(OptionFrameZoneVerNumber));
  
  FrameZoneQTD->GetSecondaryQCKB()->setChecked(false);
  FrameZoneQTD->GetZoneQSB      ()->setValue  (1    );
  
  FrameSetActivePlotQTD->GetZoneQSB()->setValue(1);
  
  /* Save/SaveAs filenames */
  
  TableFillFromFileQTD->GetFileNameQFLS     ()->SetFileName(QString("")           );
  TableFillFromFileQTD->GetColumnsNumberQLE ()->setText    (QString("%1") .arg(4) );
  TableFillFromFileQTD->GetSeparatorQLE     ()->setText    (QString(" ")          );
  TableFillFromFileQTD->GetStartingRowQSB   ()->setValue   (1                     );
  TableFillFromFileQTD->GetStartingColumnQSB()->setValue   (1                     );
  TableFillFromFileQTD->GetAddRowsQCKB      ()->setChecked (true                  );
  TableFillFromFileQTD->GetAddColumnsQCKB   ()->setChecked (true                  );
  TableFillFromFileQTD->GetEmptyCellsQCKB   ()->setChecked (true                  );
  TableFillFromFileQTD->GetSupLinesQCKB     ()->setChecked (false                 );
  TableFillFromFileQTD->GetSubLinesQCKB     ()->setChecked (false                 );
  TableFillFromFileQTD->GetDefaultFillQLE   ()->setText    (QString("%1") .arg(-1));
  
  TableWriteToFileQTD->GetFileNameQFLS()->SetFileName(QString("")         );
  TableWriteToFileQTD->GetRow1QSB     ()->setValue   (1                   );
  TableWriteToFileQTD->GetColumn1QSB  ()->setValue   (1                   );
  TableWriteToFileQTD->GetRow2QSB     ()->setValue   (DataTable->numRows());/* Free or dependant of the table size? other QSBs concerned too */
  TableWriteToFileQTD->GetColumn2QSB  ()->setValue   (DataTable->numCols());
  TableWriteToFileQTD->GetSeparatorQLE()->setText    (QString(" ")        );
  TableWriteToFileQTD->GetAppendQCKB  ()->setChecked (false               );
  
  TableSwapQTD->GetRowsQRB    ()->setChecked(true );
  TableSwapQTD->GetColumnsQRB ()->setChecked(false);
  TableSwapQTD->GetElement1QSB()->setValue  (1    );
  TableSwapQTD->GetElement2QSB()->setValue  (2    );
  TableSwapQTD->GetCopyQCKB   ()->setChecked(false);
  
  TableManageQTD->GetInsertQRB ()->setChecked(true                 );
  TableManageQTD->GetRemoveQRB ()->setChecked(false                );
  TableManageQTD->GetRowsQRB   ()->setChecked(true                 );
  TableManageQTD->GetColumnsQRB()->setChecked(false                );
  TableManageQTD->GetNumberQLE ()->setText   (QString("%1") .arg(1));
  TableManageQTD->GetStartQSB  ()->setValue  (1                    );
  
  TableEmptyQTD->GetRow1QSB   ()->setValue(1                   );
  TableEmptyQTD->GetColumn1QSB()->setValue(1                   );
  TableEmptyQTD->GetRow2QSB   ()->setValue(DataTable->numRows());
  TableEmptyQTD->GetColumn2QSB()->setValue(DataTable->numCols());
  
  GraphFunctionQTD->GetExpressionQLE()->setText(trUtf8("x", "Variable"));
  GraphFunctionQTD->GetXMinQLE      ()->setText(QString("%1") .arg(-1) );
  GraphFunctionQTD->GetXMaxQLE      ()->setText(QString("%1") .arg(1)  );
  GraphFunctionQTD->GetSameQCKB     ()->setChecked(false               );
  
  GraphHistogramQTD->GetColumnQRB     ()->setChecked(OptionGraphHistogramColumnFlag    );
  GraphHistogramQTD->GetExpressionQRB ()->setChecked(!OptionGraphHistogramColumnFlag   );
  GraphHistogramQTD->GetColumnQSB     ()->setValue  (1                                 );
  GraphHistogramQTD->GetExpressionQLE ()->setText   (trUtf8("c1", "Parameter")         );
  GraphHistogramQTD->GetBinsNumberQLE ()->setText   (QString("%1") .arg(100)           );
  GraphHistogramQTD->GetBinsNumberQCKB()->setChecked(OptionGraphHistogramBinsNumberFlag);
  GraphHistogramQTD->GetXMinQLE       ()->setText   (QString("%1") .arg(0)             );
  GraphHistogramQTD->GetXMinQCKB      ()->setChecked(OptionGraphHistogramXMinFlag      );
  GraphHistogramQTD->GetXMaxQLE       ()->setText   (QString("%1") .arg(1)             );
  GraphHistogramQTD->GetXMaxQCKB      ()->setChecked(OptionGraphHistogramXMaxFlag      );
  GraphHistogramQTD->GetCutQCKB       ()->setChecked(OptionGraphHistogramCutFlag       );
  GraphHistogramQTD->GetCutQLE        ()->setText   (trUtf8("c1 > 0", "Cut")           );
  GraphHistogramQTD->GetSameQCKB      ()->setChecked(false                             );
  
  GraphGraph2DQTD->GetColumnXQRB        ()->setChecked(true                     );
  GraphGraph2DQTD->GetExpressionXQRB    ()->setChecked(false                    );
  GraphGraph2DQTD->GetColumnYQRB        ()->setChecked(true                     );
  GraphGraph2DQTD->GetExpressionYQRB    ()->setChecked(false                    );
  GraphGraph2DQTD->GetColumnXQSB        ()->setValue  (1                        );
  GraphGraph2DQTD->GetColumnYQSB        ()->setValue  (2                        );
  GraphGraph2DQTD->GetExpressionXQLE    ()->setText   (trUtf8("c1", "Parameter"));
  GraphGraph2DQTD->GetExpressionYQLE    ()->setText   (trUtf8("c2", "Parameter"));
  GraphGraph2DQTD->GetErrorsQCKB        ()->setChecked(false                    );
  GraphGraph2DQTD->GetCutQCKB           ()->setChecked(false                    );
  GraphGraph2DQTD->GetCutQLE            ()->setText   (trUtf8("c1 > 0", "Cut")  );
  GraphGraph2DQTD->GetSameQCKB          ()->setChecked(false                    );
  GraphGraph2DQTD->GetColumnDownXQRB    ()->setChecked(true                     );
  GraphGraph2DQTD->GetExpressionDownXQRB()->setChecked(false                    );
  GraphGraph2DQTD->GetColumnDownYQRB    ()->setChecked(true                     );
  GraphGraph2DQTD->GetExpressionDownYQRB()->setChecked(false                    );
  GraphGraph2DQTD->GetColumnDownXQSB    ()->setValue  (3                        );
  GraphGraph2DQTD->GetColumnDownYQSB    ()->setValue  (4                        );
  GraphGraph2DQTD->GetExpressionDownXQLE()->setText   (trUtf8("c3", "Parameter"));
  GraphGraph2DQTD->GetExpressionDownYQLE()->setText   (trUtf8("c4", "Parameter"));
  GraphGraph2DQTD->GetErrorsDownXQCKB   ()->setChecked(false                    );
  GraphGraph2DQTD->GetErrorsDownYQCKB   ()->setChecked(true                     );
  GraphGraph2DQTD->GetColumnUpXQRB      ()->setChecked(true                     );
  GraphGraph2DQTD->GetExpressionUpXQRB  ()->setChecked(false                    );
  GraphGraph2DQTD->GetColumnUpYQRB      ()->setChecked(true                     );
  GraphGraph2DQTD->GetExpressionUpYQRB  ()->setChecked(false                    );
  GraphGraph2DQTD->GetColumnUpXQSB      ()->setValue  (3                        );
  GraphGraph2DQTD->GetColumnUpYQSB      ()->setValue  (4                        );
  GraphGraph2DQTD->GetExpressionUpXQLE  ()->setText   (trUtf8("c3", "Parameter"));
  GraphGraph2DQTD->GetExpressionUpYQLE  ()->setText   (trUtf8("c4", "Parameter"));
  GraphGraph2DQTD->GetErrorsUpXQCKB     ()->setChecked(false                    );
  GraphGraph2DQTD->GetErrorsUpYQCKB     ()->setChecked(true                     );
  GraphGraph2DQTD->GetAsymmetricQCKB    ()->setChecked(false                    );
  
  GraphFITSQTD->GetFileNameQFLS()->SetFileName(QString(""));
  GraphFITSQTD->GetSameQCKB    ()->setChecked (false      );
  
  GraphBoundsQTD->GetXInfQLE() ->setText   (QString("%1") .arg(0));
  GraphBoundsQTD->GetXInfQCKB()->setChecked(false                );
  GraphBoundsQTD->GetXSupQLE() ->setText   (QString("%1") .arg(1));
  GraphBoundsQTD->GetXSupQCKB()->setChecked(false                );
  GraphBoundsQTD->GetYInfQLE() ->setText   (QString("%1") .arg(0));
  GraphBoundsQTD->GetYInfQCKB()->setChecked(false                );
  GraphBoundsQTD->GetYSupQLE() ->setText   (QString("%1") .arg(1));
  GraphBoundsQTD->GetYSupQCKB()->setChecked(false                );
  GraphBoundsQTD->GetNewQCKB() ->setChecked(true                 );
  
  GraphAxesQTD->GetXLogQCKB()->setChecked(false);
  GraphAxesQTD->GetYLogQCKB()->setChecked(false);
  GraphAxesQTD->GetXRevQCKB()->setChecked(false);
  GraphAxesQTD->GetYRevQCKB()->setChecked(false);
  GraphAxesQTD->GetNewQCKB() ->setChecked(true );
  
  GraphFitQTD->GetNumericalQCKB ()->setChecked    (false                       );
  GraphFitQTD->GetPlotQCKB      ()->setChecked    (true                        );
  GraphFitQTD->GetPlotNoConvQCKB()->setChecked    (false                       );
  GraphFitQTD->GetSameQCKB      ()->setChecked    (true                        );
  GraphFitQTD->GetDegreeQCB     ()->setCurrentItem(1                           );
  GraphFitQTD->GetParameter0QLE ()->setText       (QString("%1") .arg(1)       );
  GraphFitQTD->GetParameter1QLE ()->setText       (QString("%1") .arg(1)       );
  GraphFitQTD->GetParameter2QLE ()->setText       (QString("%1") .arg(1)       );
  GraphFitQTD->GetParameter3QLE ()->setText       (QString("%1") .arg(1)       );
  GraphFitQTD->GetMaxStepQLE    ()->setText       (QString("%1") .arg(1000)    );
  GraphFitQTD->GetChi2LimQLE    ()->setText       (QString("%1") .arg(0.000001));
  
  /* Computation */
  
  TextsFrameQTD->GetTitleQLE()->setText(QString(""));
  
  TextsGraphQTD->GetTitleQLE            ()->setText   (QString(""));
  TextsGraphQTD->GetXAxisLabelQLE       ()->setText   (QString(""));
  TextsGraphQTD->GetYAxisLabelQLE       ()->setText   (QString(""));
  TextsGraphQTD->GetXTicksLabelsFlagQCKB()->setChecked(true       );
  TextsGraphQTD->GetYTicksLabelsFlagQCKB()->setChecked(true       );
  
  /* Settings dialogs */
  /* Help dialogs */
}

void GUI::SaveGlobalDefaultSettings()
{
  QDir Dir;
  QSettings SettingsFile(QSettings::Ini);/* Cannot be defined as global, because the destructor needs to be called in order to save the settings! */
  SettingsFile.insertSearchPath(QSettings::Unix, Dir.homeDirPath() + QString(BAYANI_DIR));
  
  SettingsFile.beginGroup("/Settings/GUI");
  SettingsFile.beginGroup("/Global");
  
  SettingsFile.beginGroup("/Appearance");
  SettingsFile.writeEntry("/R2LFlag", OptionGlobalAppearanceR2LFlag);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Terminal");
  SettingsFile.writeEntry("/HistorySize"     , OptionGlobalTerminalHistorySize            );
  SettingsFile.writeEntry("/TextColor"       , OptionGlobalTerminalTextColor.name()       );
  SettingsFile.writeEntry("/InformationColor", OptionGlobalTerminalInformationColor.name());
  SettingsFile.writeEntry("/WarningColor"    , OptionGlobalTerminalWarningColor.name()    );
  SettingsFile.writeEntry("/CriticalColor"   , OptionGlobalTerminalCriticalColor.name()   );
  SettingsFile.writeEntry("/StatusColor"     , OptionGlobalTerminalStatusColor.name()     );
  SettingsFile.writeEntry("/BGColor"         , OptionGlobalTerminalBGColor.name()         );
  SettingsFile.writeEntry("/TextFont"        , OptionGlobalTerminalTextFont.toString()    );
  SettingsFile.endGroup();
  
  SettingsFile.endGroup();
  SettingsFile.endGroup();
}

void GUI::SetFrameDefaultSettings()
{
  QDir Dir;
  QSettings SettingsFile(QSettings::Ini);/* Needs to be defined here, because the destructor needs to be called! */
  SettingsFile.insertSearchPath(QSettings::Unix, Dir.homeDirPath() + QString(BAYANI_DIR));
  
  SettingsFile.beginGroup("/Settings/GUI");
  SettingsFile.beginGroup("/Frame");
  
  SettingsFile.beginGroup("/LineWidths");
  OptionFrameBoxLineWidth  = SettingsFile.readNumEntry("/Box" , 1);
  OptionFrameHSepLineWidth = SettingsFile.readNumEntry("/HSep", 1);
  OptionFrameVSepLineWidth = SettingsFile.readNumEntry("/VSep", 1);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Ratios");
  OptionFrameXRatio         = SettingsFile.readDoubleEntry("/X"        , 0.1);
  OptionFrameYRatio         = SettingsFile.readDoubleEntry("/Y"        , 0.1);
  OptionFrameWidthRatio     = SettingsFile.readDoubleEntry("/Width"    , 0.8);
  OptionFrameHeightRatio    = SettingsFile.readDoubleEntry("/Height"   , 0.8);
  OptionFrameBox2TitleRatio = SettingsFile.readDoubleEntry("/Box2Title", 0.05);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Colors");
  OptionFrameBGColor   .setNamedColor(SettingsFile.readEntry("/BG"   , "#FFFFFF"));
  OptionFrameTitleColor.setNamedColor(SettingsFile.readEntry("/Title", "#000000"));
  OptionFrameBoxColor  .setNamedColor(SettingsFile.readEntry("/Box"  , "#000000"));
  OptionFrameHSepColor .setNamedColor(SettingsFile.readEntry("/HSep" , "#000000"));
  OptionFrameVSepColor .setNamedColor(SettingsFile.readEntry("/VSep" , "#000000"));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Fonts");
  OptionFrameTitleFont.fromString(SettingsFile.readEntry("/Title", font().toString()));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/LineStyles");
  OptionFrameBoxLineStyle  = Qt::PenStyle(SettingsFile.readNumEntry("/Box" , int(Qt::SolidLine)));
  OptionFrameHSepLineStyle = Qt::PenStyle(SettingsFile.readNumEntry("/HSep", int(Qt::SolidLine)));
  OptionFrameVSepLineStyle = Qt::PenStyle(SettingsFile.readNumEntry("/VSep", int(Qt::SolidLine)));
  SettingsFile.endGroup();
  
  SettingsFile.endGroup();
  SettingsFile.endGroup();
}

void GUI::ApplyFrameDefaultSettings()
{
  TopFrame->SetR2L              (OptionGlobalAppearanceR2LFlag);
  TopFrame->SetBoxLineWidth     (OptionFrameBoxLineWidth      );
  TopFrame->SetHorLinesWidth    (OptionFrameHSepLineWidth     );
  TopFrame->SetVerLinesWidth    (OptionFrameVSepLineWidth     );
  TopFrame->SetXRatio           (OptionFrameXRatio            );
  TopFrame->SetYRatio           (OptionFrameYRatio            );
  TopFrame->SetWidthRatio       (OptionFrameWidthRatio        );
  TopFrame->SetHeightRatio      (OptionFrameHeightRatio       );
  TopFrame->SetBox2TitleRatio   (OptionFrameBox2TitleRatio    );
  TopFrame->SetBGColor          (OptionFrameBGColor           );
  TopFrame->SetBoxColor         (OptionFrameBoxColor          );
  TopFrame->SetTitleColor       (OptionFrameTitleColor        );
  TopFrame->SetHorLinesColor    (OptionFrameHSepColor         );
  TopFrame->SetVerLinesColor    (OptionFrameVSepColor         );
  TopFrame->SetTitleFont        (OptionFrameTitleFont         );
  TopFrame->SetBoxLineStyle     (OptionFrameBoxLineStyle      );
  TopFrame->SetHorLinesLineStyle(OptionFrameHSepLineStyle     );
  TopFrame->SetVerLinesLineStyle(OptionFrameVSepLineStyle     );
}

void GUI::SaveFrameDefaultSettings()
{
  QDir Dir;
  QSettings SettingsFile(QSettings::Ini);/* Cannot be defined as global, because the destructor needs to be called in order to save the settings! */
  SettingsFile.insertSearchPath(QSettings::Unix, Dir.homeDirPath() + QString(BAYANI_DIR));
  
  SettingsFile.beginGroup("/Settings/GUI");
  SettingsFile.beginGroup("/Frame");
  
  SettingsFile.beginGroup("/LineWidths");
  SettingsFile.writeEntry("/Box" , OptionFrameBoxLineWidth);
  SettingsFile.writeEntry("/HSep", OptionFrameHSepLineWidth);
  SettingsFile.writeEntry("/VSep", OptionFrameVSepLineWidth);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Ratios");
  SettingsFile.writeEntry("/X"        , OptionFrameXRatio);
  SettingsFile.writeEntry("/Y"        , OptionFrameYRatio);
  SettingsFile.writeEntry("/Width"    , OptionFrameWidthRatio);
  SettingsFile.writeEntry("/Height"   , OptionFrameHeightRatio);
  SettingsFile.writeEntry("/Box2Title", OptionFrameBox2TitleRatio);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Colors");
  SettingsFile.writeEntry("/BG"   , OptionFrameBGColor.name());
  SettingsFile.writeEntry("/Title", OptionFrameTitleColor.name());
  SettingsFile.writeEntry("/Box"  , OptionFrameBoxColor.name());
  SettingsFile.writeEntry("/HSep" , OptionFrameHSepColor.name());
  SettingsFile.writeEntry("/VSep" , OptionFrameVSepColor.name());
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Fonts");
  SettingsFile.writeEntry("/Title", OptionFrameTitleFont.toString());
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/LineStyles");
  SettingsFile.writeEntry("/Box" , int(OptionFrameBoxLineStyle));
  SettingsFile.writeEntry("/HSep", int(OptionFrameHSepLineStyle));
  SettingsFile.writeEntry("/VSep", int(OptionFrameVSepLineStyle));
  SettingsFile.endGroup();
  
  SettingsFile.endGroup();
  SettingsFile.endGroup();
}

void GUI::SetGraphDefaultSettings()
{
  QDir Dir;
  QSettings SettingsFile(QSettings::Ini);/* Needs to be defined here, because the destructor needs to be called! */
  SettingsFile.insertSearchPath(QSettings::Unix, Dir.homeDirPath() + QString(BAYANI_DIR));
  
  SettingsFile.beginGroup("/Settings/GUI");
  SettingsFile.beginGroup("/Graph");
  
  SettingsFile.beginGroup("/LineWidths");
  OptionGraphBoxLineWidth         = SettingsFile.readNumEntry("/Box"        , 1);
  OptionGraphBigXTicksLineWidth   = SettingsFile.readNumEntry("/BigXTicks"  , 1);
  OptionGraphBigYTicksLineWidth   = SettingsFile.readNumEntry("/BigYTicks"  , 1);
  OptionGraphSmallXTicksLineWidth = SettingsFile.readNumEntry("/SmallXTicks", 1);
  OptionGraphSmallYTicksLineWidth = SettingsFile.readNumEntry("/SmallYTicks", 1);
  OptionGraphXGridLineWidth       = SettingsFile.readNumEntry("/XGrid"      , 1);
  OptionGraphYGridLineWidth       = SettingsFile.readNumEntry("/YGrid"      , 1);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Ratios");
  OptionGraphXRatio                 = SettingsFile.readDoubleEntry("/X"                , 0.1);
  OptionGraphYRatio                 = SettingsFile.readDoubleEntry("/Y"                , 0.05);
  OptionGraphWidthRatio             = SettingsFile.readDoubleEntry("/Width"            , 0.85);
  OptionGraphHeightRatio            = SettingsFile.readDoubleEntry("/Height"           , 0.8);
  OptionGraphAxis2TitleRatio        = SettingsFile.readDoubleEntry("/Axis2Title"       , 0.075);
  OptionGraphBigXTicksRatio         = SettingsFile.readDoubleEntry("/BigXTicks"        , 0.02);
  OptionGraphBigYTicksRatio         = SettingsFile.readDoubleEntry("/BigYTicks"        , 0.02);
  OptionGraphSmallXTicksRatio       = SettingsFile.readDoubleEntry("/SmallXTicks"      , 0.01);
  OptionGraphSmallYTicksRatio       = SettingsFile.readDoubleEntry("/SmallYTicks"      , 0.01);
  OptionGraphXAxis2TicksLabelsRatio = SettingsFile.readDoubleEntry("/XAxis2TicksLabels", 0.04);
  OptionGraphYAxis2TicksLabelsRatio = SettingsFile.readDoubleEntry("/YAxis2TicksLabels", 0.02);
  OptionGraphXAxis2AxisLabelRatio   = SettingsFile.readDoubleEntry("/XAxis2AxisLabel"  , 0.035);
  OptionGraphYAxis2AxisLabelRatio   = SettingsFile.readDoubleEntry("/YAxis2AxisLabel"  , 0.02);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Colors");
  OptionGraphBoxColor.setNamedColor         (SettingsFile.readEntry("/Box"         , "#000000"));
  OptionGraphBGColor.setNamedColor          (SettingsFile.readEntry("/BG"          , "#FFFFFF"));
  OptionGraphBigXTicksColor.setNamedColor   (SettingsFile.readEntry("/BigXTicks"   , "#000000"));
  OptionGraphBigYTicksColor.setNamedColor   (SettingsFile.readEntry("/BigYTicks"   , "#000000"));
  OptionGraphSmallXTicksColor.setNamedColor (SettingsFile.readEntry("/SmallXTicks" , "#000000"));
  OptionGraphSmallYTicksColor.setNamedColor (SettingsFile.readEntry("/SmallYTicks" , "#000000"));
  OptionGraphXTicksLabelsColor.setNamedColor(SettingsFile.readEntry("/XTicksLabels", "#000000"));
  OptionGraphYTicksLabelsColor.setNamedColor(SettingsFile.readEntry("/YTicksLabels", "#000000"));
  OptionGraphXAxisLabelColor.setNamedColor  (SettingsFile.readEntry("/XAxisLabel"  , "#000000"));
  OptionGraphYAxisLabelColor.setNamedColor  (SettingsFile.readEntry("/YAxisLabel"  , "#000000"));
  OptionGraphTitleColor.setNamedColor       (SettingsFile.readEntry("/Title"       , "#000000"));
  OptionGraphXGridColor.setNamedColor       (SettingsFile.readEntry("/XGrid"       , "#000000"));
  OptionGraphYGridColor.setNamedColor       (SettingsFile.readEntry("/YGrid"       , "#000000"));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Fonts");
  OptionGraphXAxisLabelFont.fromString  (SettingsFile.readEntry("/XAxisLabel"  , font().toString()));
  OptionGraphYAxisLabelFont.fromString  (SettingsFile.readEntry("/YAxisLabel"  , font().toString()));
  OptionGraphXTicksLabelsFont.fromString(SettingsFile.readEntry("/XTicksLabels", font().toString()));
  OptionGraphYTicksLabelsFont.fromString(SettingsFile.readEntry("/YTicksLabels", font().toString()));
  OptionGraphTitleFont.fromString       (SettingsFile.readEntry("/Title"       , font().toString()));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/LineStyles");
  OptionGraphBoxLineStyle         = Qt::PenStyle(SettingsFile.readNumEntry("/Box"        , int(Qt::SolidLine)));
  OptionGraphBigXTicksLineStyle   = Qt::PenStyle(SettingsFile.readNumEntry("/BigXTicks"  , int(Qt::SolidLine)));
  OptionGraphBigYTicksLineStyle   = Qt::PenStyle(SettingsFile.readNumEntry("/BigYTicks"  , int(Qt::SolidLine)));
  OptionGraphSmallXTicksLineStyle = Qt::PenStyle(SettingsFile.readNumEntry("/SmallXTicks", int(Qt::SolidLine)));
  OptionGraphSmallYTicksLineStyle = Qt::PenStyle(SettingsFile.readNumEntry("/SmallYTicks", int(Qt::SolidLine)));
  OptionGraphXGridLineStyle       = Qt::PenStyle(SettingsFile.readNumEntry("/XGrid"      , int(Qt::DotLine)));
  OptionGraphYGridLineStyle       = Qt::PenStyle(SettingsFile.readNumEntry("/YGrid"      , int(Qt::DotLine)));
  SettingsFile.endGroup();
  
  SettingsFile.endGroup();
  SettingsFile.endGroup();
}

void GUI::ApplyGraphDefaultSettings()
{
  Graph * ActiveGraph = TopFrame->GetActivePlot();
  
  ActiveGraph->SetBoxLineWidth          (OptionGraphBoxLineWidth);
  ActiveGraph->SetXGridLineWidth        (OptionGraphXGridLineWidth);
  ActiveGraph->SetYGridLineWidth        (OptionGraphYGridLineWidth);
  ActiveGraph->SetBigXTicksLineWidth    (OptionGraphBigXTicksLineWidth);
  ActiveGraph->SetBigYTicksLineWidth    (OptionGraphBigYTicksLineWidth);
  ActiveGraph->SetSmallXTicksLineWidth  (OptionGraphSmallXTicksLineWidth);
  ActiveGraph->SetSmallYTicksLineWidth  (OptionGraphSmallYTicksLineWidth);
  ActiveGraph->SetXRatio                (OptionGraphXRatio);
  ActiveGraph->SetYRatio                (OptionGraphYRatio);
  ActiveGraph->SetWidthRatio            (OptionGraphWidthRatio);
  ActiveGraph->SetHeightRatio           (OptionGraphHeightRatio);
  ActiveGraph->SetAxis2TitleRatio       (OptionGraphAxis2TitleRatio);
  ActiveGraph->SetXAxis2AxisLabelRatio  (OptionGraphXAxis2AxisLabelRatio);
  ActiveGraph->SetYAxis2AxisLabelRatio  (OptionGraphYAxis2AxisLabelRatio);
  ActiveGraph->SetXAxis2TicksLabelsRatio(OptionGraphXAxis2TicksLabelsRatio);
  ActiveGraph->SetYAxis2TicksLabelsRatio(OptionGraphYAxis2TicksLabelsRatio);  
  ActiveGraph->SetBigXTicksRatio        (OptionGraphBigXTicksRatio);
  ActiveGraph->SetBigYTicksRatio        (OptionGraphBigYTicksRatio);
  ActiveGraph->SetSmallXTicksRatio      (OptionGraphSmallXTicksRatio);
  ActiveGraph->SetSmallYTicksRatio      (OptionGraphSmallYTicksRatio);
  ActiveGraph->SetBGColor               (OptionGraphBGColor);
  ActiveGraph->SetBoxColor              (OptionGraphBoxColor);
  ActiveGraph->SetTitleColor            (OptionGraphTitleColor);
  ActiveGraph->SetXGridColor            (OptionGraphXGridColor);
  ActiveGraph->SetYGridColor            (OptionGraphYGridColor);
  ActiveGraph->SetBigXTicksColor        (OptionGraphBigXTicksColor);
  ActiveGraph->SetBigYTicksColor        (OptionGraphBigYTicksColor);
  ActiveGraph->SetSmallXTicksColor      (OptionGraphSmallXTicksColor);
  ActiveGraph->SetSmallYTicksColor      (OptionGraphSmallYTicksColor);
  ActiveGraph->SetXAxisLabelColor       (OptionGraphXAxisLabelColor);
  ActiveGraph->SetYAxisLabelColor       (OptionGraphYAxisLabelColor);
  ActiveGraph->SetXTicksLabelsColor     (OptionGraphXTicksLabelsColor);
  ActiveGraph->SetYTicksLabelsColor     (OptionGraphYTicksLabelsColor);
  ActiveGraph->SetTitleFont             (OptionGraphTitleFont);
  ActiveGraph->SetXAxisLabelFont        (OptionGraphXAxisLabelFont);
  ActiveGraph->SetYAxisLabelFont        (OptionGraphYAxisLabelFont);
  ActiveGraph->SetXTicksLabelsFont      (OptionGraphXTicksLabelsFont);
  ActiveGraph->SetYTicksLabelsFont      (OptionGraphYTicksLabelsFont);
  ActiveGraph->SetBoxLineStyle          (OptionGraphBoxLineStyle);
  ActiveGraph->SetXGridLineStyle        (OptionGraphXGridLineStyle);
  ActiveGraph->SetYGridLineStyle        (OptionGraphYGridLineStyle);
  ActiveGraph->SetBigXTicksLineStyle    (OptionGraphBigXTicksLineStyle);
  ActiveGraph->SetBigYTicksLineStyle    (OptionGraphBigYTicksLineStyle);
  ActiveGraph->SetSmallXTicksLineStyle  (OptionGraphSmallXTicksLineStyle);
  ActiveGraph->SetSmallYTicksLineStyle  (OptionGraphSmallYTicksLineStyle);
}

void GUI::SaveGraphDefaultSettings()
{
  QDir Dir;
  QSettings SettingsFile(QSettings::Ini);/* Cannot be defined as global, because the destructor needs to be called in order to save the settings! */
  SettingsFile.insertSearchPath(QSettings::Unix, Dir.homeDirPath() + QString(BAYANI_DIR));
  
  SettingsFile.beginGroup("/Settings/GUI");
  SettingsFile.beginGroup("/Graph");
  
  SettingsFile.beginGroup("/LineWidths");
  SettingsFile.writeEntry("/Box"        , OptionGraphBoxLineWidth);
  SettingsFile.writeEntry("/BigXTicks"  , OptionGraphBigXTicksLineWidth);
  SettingsFile.writeEntry("/BigYTicks"  , OptionGraphBigYTicksLineWidth);
  SettingsFile.writeEntry("/SmallXTicks", OptionGraphSmallXTicksLineWidth);
  SettingsFile.writeEntry("/SmallYTicks", OptionGraphSmallYTicksLineWidth);
  SettingsFile.writeEntry("/XGrid"      , OptionGraphXGridLineWidth);
  SettingsFile.writeEntry("/YGrid"      , OptionGraphYGridLineWidth);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Ratios");
  SettingsFile.writeEntry("/X"                , OptionGraphXRatio);
  SettingsFile.writeEntry("/Y"                , OptionGraphYRatio);
  SettingsFile.writeEntry("/Width"            , OptionGraphWidthRatio);
  SettingsFile.writeEntry("/Height"           , OptionGraphHeightRatio);
  SettingsFile.writeEntry("/Axis2Title"       , OptionGraphAxis2TitleRatio);
  SettingsFile.writeEntry("/BigXTicks"        , OptionGraphBigXTicksRatio);
  SettingsFile.writeEntry("/BigYTicks"        , OptionGraphBigYTicksRatio);
  SettingsFile.writeEntry("/SmallXTicks"      , OptionGraphSmallXTicksRatio);
  SettingsFile.writeEntry("/SmallYTicks"      , OptionGraphSmallYTicksRatio);
  SettingsFile.writeEntry("/XAxis2TicksLabels", OptionGraphXAxis2TicksLabelsRatio);
  SettingsFile.writeEntry("/YAxis2TicksLabels", OptionGraphYAxis2TicksLabelsRatio);
  SettingsFile.writeEntry("/XAxis2AxisLabel"  , OptionGraphXAxis2AxisLabelRatio);
  SettingsFile.writeEntry("/YAxis2AxisLabel"  , OptionGraphYAxis2AxisLabelRatio);
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Colors");
  SettingsFile.writeEntry("/Box"         , OptionGraphBoxColor.name());
  SettingsFile.writeEntry("/BG"          , OptionGraphBGColor.name());
  SettingsFile.writeEntry("/BigXTicks"   , OptionGraphBigXTicksColor.name());
  SettingsFile.writeEntry("/BigYTicks"   , OptionGraphBigYTicksColor.name());
  SettingsFile.writeEntry("/SmallXTicks" , OptionGraphSmallXTicksColor.name());
  SettingsFile.writeEntry("/SmallYTicks" , OptionGraphSmallYTicksColor.name());
  SettingsFile.writeEntry("/XTicksLabels", OptionGraphXTicksLabelsColor.name());
  SettingsFile.writeEntry("/YTicksLabels", OptionGraphYTicksLabelsColor.name());
  SettingsFile.writeEntry("/XAxisLabel"  , OptionGraphXAxisLabelColor.name());
  SettingsFile.writeEntry("/YAxisLabel"  , OptionGraphYAxisLabelColor.name());
  SettingsFile.writeEntry("/Title"       , OptionGraphTitleColor.name());
  SettingsFile.writeEntry("/XGrid"       , OptionGraphXGridColor.name());
  SettingsFile.writeEntry("/YGrid"       , OptionGraphYGridColor.name());
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Fonts");
  SettingsFile.writeEntry("/XAxisLabel"  , OptionGraphXAxisLabelFont.toString());
  SettingsFile.writeEntry("/YAxisLabel"  , OptionGraphYAxisLabelFont.toString());
  SettingsFile.writeEntry("/XTicksLabels", OptionGraphXTicksLabelsFont.toString());
  SettingsFile.writeEntry("/YTicksLabels", OptionGraphYTicksLabelsFont.toString());
  SettingsFile.writeEntry("/Title"       , OptionGraphTitleFont.toString());
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/LineStyles");
  SettingsFile.writeEntry("/Box"        , int(OptionGraphBoxLineStyle));
  SettingsFile.writeEntry("/BigXTicks"  , int(OptionGraphBigXTicksLineStyle));
  SettingsFile.writeEntry("/BigYTicks"  , int(OptionGraphBigYTicksLineStyle));
  SettingsFile.writeEntry("/SmallXTicks", int(OptionGraphSmallXTicksLineStyle));
  SettingsFile.writeEntry("/SmallYTicks", int(OptionGraphSmallYTicksLineStyle));
  SettingsFile.writeEntry("/XGrid"      , int(OptionGraphXGridLineStyle));
  SettingsFile.writeEntry("/YGrid"      , int(OptionGraphYGridLineStyle));
  SettingsFile.endGroup();
  
  SettingsFile.endGroup();
  SettingsFile.endGroup();
}

void GUI::SetDataDefaultSettings()
{
  QDir Dir;
  QSettings SettingsFile(QSettings::Ini);/* Needs to be defined here, because the destructor needs to be called! */
  SettingsFile.insertSearchPath(QSettings::Unix, Dir.homeDirPath() + QString(BAYANI_DIR));
  
  SettingsFile.beginGroup("/Settings/GUI");
  SettingsFile.beginGroup("/Data");
  
  SettingsFile.beginGroup("/Function");
  OptionDataFunctionLineWidth = SettingsFile.readNumEntry             ("/LineWidth", 1);
  OptionDataFunctionLineColor.setNamedColor(SettingsFile.readEntry    ("/LineColor", "#000000"));
  OptionDataFunctionLineStyle = Qt::PenStyle(SettingsFile.readNumEntry("/LineStyle", int(Qt::SolidLine)));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Histogram");
  OptionDataHistogramLineWidth = SettingsFile.readNumEntry               ("/LineWidth", 1);
  OptionDataHistogramLineColor.setNamedColor(SettingsFile.readEntry      ("/LineColor", "#000000"));
  OptionDataHistogramFillColor.setNamedColor(SettingsFile.readEntry      ("/FillColor", "#000000"));
  OptionDataHistogramLineStyle = Qt::PenStyle(SettingsFile.readNumEntry  ("/LineStyle", int(Qt::SolidLine)));
  OptionDataHistogramFillStyle = Qt::BrushStyle(SettingsFile.readNumEntry("/FillStyle", int(Qt::NoBrush)));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Graph2D");
  OptionDataGraphShapeType       = SettingsFile.readNumEntry             ("/ShapeType"      , FILCIRCLE);
  OptionDataGraphErrorsLineWidth = SettingsFile.readNumEntry             ("/ErrorsLineWidth", 1);
  OptionDataGraphShapeRatio      = SettingsFile.readDoubleEntry          ("/ShapeRatio"     , 0.01);
  OptionDataGraphShapeColor.setNamedColor(SettingsFile.readEntry         ("/ShapeColor"     , "#000000"));
  OptionDataGraphErrorsLineColor.setNamedColor(SettingsFile.readEntry    ("/ErrorsLineColor", "#000000")); 
  OptionDataGraphErrorsLineStyle = Qt::PenStyle(SettingsFile.readNumEntry("/ErrorsLineStyle", int(Qt::SolidLine)));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/FITS");
  OptionDataFITSPalette     = SettingsFile.readNumEntry ("/Palette"    , PALETTE_BRYW);
  OptionDataFITSMinDynFlag  = SettingsFile.readBoolEntry("/MinDynFlag" , false);
  OptionDataFITSMinDynValue = SettingsFile.readNumEntry ("/MinDynValue", 0);
  OptionDataFITSMaxDynFlag  = SettingsFile.readBoolEntry("/MaxDynFlag" , false);
  OptionDataFITSMaxDynValue = SettingsFile.readNumEntry ("/MaxDynValue", 65535);
  SettingsFile.endGroup();
  
  SettingsFile.endGroup();
  SettingsFile.endGroup();
}

void GUI::ApplyDataDefaultSettings()
{
  Graph * ActiveGraph          = TopFrame->GetActivePlot();
  vector  <BBDO *> ActiveDatas = ActiveGraph->GetData();
  
  /* We update only the top member of each object class */
  bool Func1DFlag        = false;
  bool Histo1DFlag       = false;
  bool Graph2DFlag       = false;
  bool FITSContainerFlag = false;
  
  for(int i = ActiveDatas.size()-1; i >= 0; i--)
    {
      Func1D * NewFunc1D = dynamic_cast<Func1D *>(ActiveDatas[i]);
      
      if(NewFunc1D && !Func1DFlag)
	{
	  NewFunc1D->SetLineWidth(OptionDataFunctionLineWidth);
	  NewFunc1D->SetLineColor(OptionDataFunctionLineColor);
	  NewFunc1D->SetLineStyle(OptionDataFunctionLineStyle);
	  
	  Func1DFlag = true;
	  
	  continue;
	}
      
      Histo1D * NewHisto1D = dynamic_cast<Histo1D *>(ActiveDatas[i]);
      
      if(NewHisto1D && !Histo1DFlag)
	{
	  NewHisto1D->SetLineWidth(OptionDataHistogramLineWidth);
	  NewHisto1D->SetLineColor(OptionDataHistogramLineColor);
	  NewHisto1D->SetFillColor(OptionDataHistogramFillColor);
	  NewHisto1D->SetLineStyle(OptionDataHistogramLineStyle);
	  NewHisto1D->SetFillStyle(OptionDataHistogramFillStyle);
	  
	  Histo1DFlag = true;
	  
	  continue;
	}
      
      Graph2D * NewGraph2D = dynamic_cast<Graph2D *>(ActiveDatas[i]);
      
      if(NewGraph2D && !Graph2DFlag)
	{
	  NewGraph2D->SetDataType(OptionDataGraphShapeType);
	  NewGraph2D->SetErrorsLineWidth(OptionDataGraphErrorsLineWidth);
	  NewGraph2D->SetDataRatio(OptionDataGraphShapeRatio);
	  NewGraph2D->SetDataColor(OptionDataGraphShapeColor);
	  NewGraph2D->SetErrorsLineColor(OptionDataGraphErrorsLineColor);
	  NewGraph2D->SetErrorsLineStyle(OptionDataGraphErrorsLineStyle);
	  
	  Graph2DFlag = true;
	  
	  continue;
	}
      
      FITSContainer * NewFITSContainer = dynamic_cast<FITSContainer *>(ActiveDatas[i]);
      
      if(NewFITSContainer && !FITSContainerFlag)
	{
	  NewFITSContainer->SetPalette(OptionDataFITSPalette);
	  NewFITSContainer->SetMinDyn (OptionDataFITSMinDynFlag, double(OptionDataFITSMinDynValue));
	  NewFITSContainer->SetMaxDyn (OptionDataFITSMaxDynFlag, double(OptionDataFITSMaxDynValue));
	  
	  FITSContainerFlag = true;
	  
	  continue;
	}
    }
}

void GUI::SaveDataDefaultSettings()
{
  QDir Dir;
  QSettings SettingsFile(QSettings::Ini);/* Cannot be defined as global, because the destructor needs to be called in order to save the settings! */
  SettingsFile.insertSearchPath(QSettings::Unix, Dir.homeDirPath() + QString(BAYANI_DIR));
  
  SettingsFile.beginGroup("/Settings/GUI");
  SettingsFile.beginGroup("/Data");
  
  SettingsFile.beginGroup("/Function");
  SettingsFile.writeEntry("/LineWidth", OptionDataFunctionLineWidth);
  SettingsFile.writeEntry("/LineColor", OptionDataFunctionLineColor.name());
  SettingsFile.writeEntry("/LineStyle", int(OptionDataFunctionLineStyle));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Histogram");
  SettingsFile.writeEntry("/LineWidth", OptionDataHistogramLineWidth);
  SettingsFile.writeEntry("/LineColor", OptionDataHistogramLineColor.name());
  SettingsFile.writeEntry("/FillColor", OptionDataHistogramFillColor.name());
  SettingsFile.writeEntry("/LineStyle", int(OptionDataHistogramLineStyle));
  SettingsFile.writeEntry("/FillStyle", int(OptionDataHistogramFillStyle));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/Graph2D");
  SettingsFile.writeEntry("/ShapeType"      , OptionDataGraphShapeType);
  SettingsFile.writeEntry("/ErrorsLineWidth", OptionDataGraphErrorsLineWidth);
  SettingsFile.writeEntry("/ShapeRatio"     , OptionDataGraphShapeRatio);
  SettingsFile.writeEntry("/ShapeColor"     , OptionDataGraphShapeColor.name());
  SettingsFile.writeEntry("/ErrorsLineColor", OptionDataGraphErrorsLineColor.name());
  SettingsFile.writeEntry("/ErrorsLineStyle", int(OptionDataGraphErrorsLineStyle));
  SettingsFile.endGroup();
  
  SettingsFile.beginGroup("/FITS");
  SettingsFile.writeEntry("/Palette"    , OptionDataFITSPalette);
  SettingsFile.writeEntry("/MinDynFlag" , OptionDataFITSMinDynFlag);
  SettingsFile.writeEntry("/MinDynValue", OptionDataFITSMinDynValue);
  SettingsFile.writeEntry("/MaxDynFlag" , OptionDataFITSMaxDynFlag);
  SettingsFile.writeEntry("/MaxDynValue", OptionDataFITSMaxDynValue);
  SettingsFile.endGroup();
  
  SettingsFile.endGroup();
  SettingsFile.endGroup();
}

/* Find a better solution */
void GUI::EnableFrameRelatedMenus()
{
  FrameZoneQA     ->setEnabled(true);
  if(TopFrame->IsModified())
    FrameSaveQA   ->setEnabled(true);
  else
    FrameSaveQA   ->setEnabled(false);  
  FrameSaveAsQA   ->setEnabled(true);
  FrameCloseQA    ->setEnabled(true);
  menuBar()->setItemEnabled(GraphMenuID     , true);
  GraphFunctionQA ->setEnabled(true);
  GraphHistogramQA->setEnabled(true);
  GraphGraph2DQA  ->setEnabled(true);
  GraphFITSQA     ->setEnabled(true);
  menuBar()->setItemEnabled(TextsMenuID     , true);
  TextsFrameQA    ->setEnabled(true);
  
  FrameZoneQTD     ->setEnabled(true);
  GraphFunctionQTD ->setEnabled(true);
  GraphHistogramQTD->setEnabled(true);
  GraphGraph2DQTD  ->setEnabled(true);
  GraphFITSQTD     ->setEnabled(true);
  TextsFrameQTD    ->setEnabled(true);
  
  FrameZoneQTD->GetZoneQSB()->setMaxValue(TopFrame->PlotsNumber());
  
  DisableGraphRelatedMenus();
}

void GUI::DisableFrameRelatedMenus()
{
  FrameZoneQA     ->setEnabled(false);
  FrameSaveQA     ->setEnabled(false);
  FrameSaveAsQA   ->setEnabled(false);
  FrameCloseQA    ->setEnabled(false);
  menuBar()->setItemEnabled(GraphMenuID     , false);
  GraphFunctionQA ->setEnabled(false);
  GraphHistogramQA->setEnabled(false);
  GraphGraph2DQA  ->setEnabled(false);
  GraphFITSQA     ->setEnabled(false);
  menuBar()->setItemEnabled(TextsMenuID     , false);
  TextsFrameQA ->setEnabled(false);
  
  FrameZoneQTD     ->setEnabled(false);
  GraphFunctionQTD ->setEnabled(false);
  GraphHistogramQTD->setEnabled(false);
  GraphGraph2DQTD  ->setEnabled(false);
  GraphFITSQTD     ->setEnabled(false);
  TextsFrameQTD    ->setEnabled(false);
  
  FrameZoneQTD         ->GetZoneQSB()->setMaxValue(1);
  FrameSetActivePlotQTD->GetZoneQSB()->setMaxValue(1);
  
  DisableGraphRelatedMenus();
}

void GUI::UpdateTableRelatedItems()
{
  TableFillFromFileQTD->GetStartingRowQSB()->setMaxValue(DataTable->numRows());
  TableFillFromFileQTD->GetStartingColumnQSB()->setMaxValue(DataTable->numCols());
  
  TableWriteToFileQTD->GetRow1QSB()->setMaxValue(DataTable->numRows());
  TableWriteToFileQTD->GetColumn1QSB()->setMaxValue(DataTable->numCols());
  TableWriteToFileQTD->GetRow2QSB()->setMaxValue(DataTable->numRows());
  TableWriteToFileQTD->GetColumn2QSB()->setMaxValue(DataTable->numCols());
  
  TableSwapRowsQRBSlot(TableSwapQTD->GetRowsQRB()->isChecked());
  
  TableManageRowsQRBSlot(TableManageQTD->GetRowsQRB()->isChecked());
  
  TableEmptyQTD->GetRow1QSB()->setMaxValue(DataTable->numRows());
  TableEmptyQTD->GetColumn1QSB()->setMaxValue(DataTable->numCols());
  TableEmptyQTD->GetRow2QSB()->setMaxValue(DataTable->numRows());
  TableEmptyQTD->GetColumn2QSB()->setMaxValue(DataTable->numCols());
  
  GraphHistogramQTD->GetColumnQSB()->setMaxValue(DataTable->numCols());
  GraphGraph2DQTD->GetColumnYQSB()->setMaxValue(DataTable->numCols());
  GraphGraph2DQTD->GetColumnXQSB()->setMaxValue(DataTable->numCols());
  GraphGraph2DQTD->GetColumnDownXQSB()->setMaxValue(DataTable->numCols());
  GraphGraph2DQTD->GetColumnDownYQSB()->setMaxValue(DataTable->numCols());
  GraphGraph2DQTD->GetColumnUpXQSB()->setMaxValue(DataTable->numCols());
  GraphGraph2DQTD->GetColumnUpYQSB()->setMaxValue(DataTable->numCols());
}

void GUI::EnableGraphRelatedMenus()
{
  GraphBoundsQA->setEnabled(true);
  GraphAxesQA  ->setEnabled(true);
  GraphFitQA   ->setEnabled(true);
  GraphCloseQA ->setEnabled(true);
  TextsGraphQA ->setEnabled(true);
  
  GraphBoundsQTD->setEnabled(true);
  GraphAxesQTD  ->setEnabled(true);
  GraphFitQTD   ->setEnabled(true);
  TextsGraphQTD ->setEnabled(true);
  
  GraphFunctionQTD ->GetSameQCKB()->setEnabled(true);
  GraphHistogramQTD->GetSameQCKB()->setEnabled(true);
  GraphGraph2DQTD  ->GetSameQCKB()->setEnabled(true);
  GraphFITSQTD     ->GetSameQCKB()->setEnabled(true);
  
  if(TopFrame && TopFrame->ExistingPlotsNumber() > 1)
    {
      FrameSetActiveGraphQA->setEnabled(true);
      
      FrameSetActivePlotQTD->GetZoneQSB()->setMaxValue(TopFrame->ExistingPlotsNumber()    );
      FrameSetActivePlotQTD->GetZoneQSB()->setValue   (TopFrame->activePlotId       () + 1);
      
      FrameSetActivePlotQTD->setEnabled(true);
    }
  else
    FrameSetActivePlotQTD->GetZoneQSB()->setMaxValue(1);
}

void GUI::DisableGraphRelatedMenus()
{
  FrameSetActiveGraphQA->setEnabled(false);
  GraphBoundsQA        ->setEnabled(false);
  GraphAxesQA          ->setEnabled(false);
  GraphFitQA           ->setEnabled(false);
  GraphCloseQA         ->setEnabled(false);
  TextsGraphQA         ->setEnabled(false);
  
  FrameSetActivePlotQTD->setEnabled(false);
  GraphBoundsQTD       ->setEnabled(false);
  GraphAxesQTD         ->setEnabled(false);
  GraphFitQTD          ->setEnabled(false);
  TextsGraphQTD        ->setEnabled(false);
  
  GraphFunctionQTD ->GetSameQCKB()->setEnabled(false);
  GraphHistogramQTD->GetSameQCKB()->setEnabled(false);
  GraphGraph2DQTD  ->GetSameQCKB()->setEnabled(false);
  GraphFITSQTD     ->GetSameQCKB()->setEnabled(false);
  
  if(TopFrame && !TopFrame->IsEmpty())
    EnableGraphRelatedMenus();
}

void GUI::CreateNewGraph()
{
  Graph * NewGraph = new Graph(TopFrame);
  
  TopFrame->AddPlot(NewGraph);
  
  ApplyGraphDefaultSettings();
  
  if(GraphBoundsNewFlag)
    GraphBoundsSettings();
  
  if(GraphAxesNewFlag)
    GraphAxesSettings();
  
  connect(NewGraph, SIGNAL(DrawingProblem         (QString)               ), this, SLOT(DrawingProblemSlot (QString)               ));
  connect(NewGraph, SIGNAL(SendCoordinates        (double, double)        ), this, SLOT(ShowCoordinates    (double, double)        ));
  connect(NewGraph, SIGNAL(SendCoordinatesAndValue(double, double, double)), this, SLOT(ShowFITSCoordinates(double, double, double)));
  connect(NewGraph, SIGNAL(UnSendCoordinates      ()                      ), this, SLOT(HideCoordinates    ()                      ));
  
  EnableFrameRelatedMenus();
}

void GUI::GraphBoundsSettings()
{
  if(GraphBoundsQTD->GetXInfQCKB()->isChecked() && GraphBoundsQTD->GetXInfQLE()->text() == "")
    {
      PrintMessage(GraphBoundsQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the x axis inferior bound."));
      
      return;
    }
  
  if(GraphBoundsQTD->GetXSupQCKB()->isChecked() && GraphBoundsQTD->GetXSupQLE()->text() == "")
    {
      PrintMessage(GraphBoundsQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the x axis superior bound."));
      
      return;
    }
  
  if(GraphBoundsQTD->GetYInfQCKB()->isChecked() && GraphBoundsQTD->GetYInfQLE()->text() == "")
    {
      PrintMessage(GraphBoundsQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the y axis inferior bound."));
      
      return;
    }
  
  if(GraphBoundsQTD->GetYSupQCKB()->isChecked() && GraphBoundsQTD->GetYSupQLE()->text() == "")
    {
      PrintMessage(GraphBoundsQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the y axis superior bound."));
      
      return;
    }
  
  if(
     GraphBoundsQTD->GetXInfQCKB()->isChecked() && GraphBoundsQTD->GetXSupQCKB()->isChecked()
     &&
     GraphBoundsQTD->GetXInfQLE()->text().toDouble() >= GraphBoundsQTD->GetXSupQLE()->text().toDouble()
     )
    {
      PrintMessage(GraphBoundsQTD, MESSAGE_CRITICAL, trUtf8("The x axis inferior value is greater than or equal to the superior one."));
      
      return;
    }
  
  if(
     GraphBoundsQTD->GetYInfQCKB()->isChecked() && GraphBoundsQTD->GetYSupQCKB()->isChecked()
     &&
     GraphBoundsQTD->GetYInfQLE()->text().toDouble() >= GraphBoundsQTD->GetYSupQLE()->text().toDouble()
     )
    {
      PrintMessage(GraphBoundsQTD, MESSAGE_CRITICAL, trUtf8("The y axis inferior value is greater than or equal to the superior one."));
      
      return;
    }
  
  if(GraphBoundsQTD->GetXInfQCKB()->isChecked())
    TopFrame->GetActivePlot()->SetXInfBound(GraphBoundsQTD->GetXInfQLE()->text().toDouble());
  else
    TopFrame->GetActivePlot()->UnSetXInfBound();
  
  if(GraphBoundsQTD->GetXSupQCKB()->isChecked())
    TopFrame->GetActivePlot()->SetXSupBound(GraphBoundsQTD->GetXSupQLE()->text().toDouble());
  else
    TopFrame->GetActivePlot()->UnSetXSupBound();
  
  if(GraphBoundsQTD->GetYInfQCKB()->isChecked())
    TopFrame->GetActivePlot()->SetYInfBound(GraphBoundsQTD->GetYInfQLE()->text().toDouble());
  else
    TopFrame->GetActivePlot()->UnSetYInfBound();
  
  if(GraphBoundsQTD->GetYSupQCKB()->isChecked())
    TopFrame->GetActivePlot()->SetYSupBound(GraphBoundsQTD->GetYSupQLE()->text().toDouble());
  else
    TopFrame->GetActivePlot()->UnSetYSupBound();
}

void GUI::GraphAxesSettings()
{
  FITSContainer * OldFITSContainer = dynamic_cast<FITSContainer *>(TopFrame->GetActivePlot()->GetData(0));
  
  /*
  if(OldFITSContainer && (GraphAxesQTD->GetXLogQCKB()->isChecked() || GraphAxesQTD->GetYLogQCKB()->isChecked()))
    {
      GraphAxesQTD->GetXLogQCKB()->setChecked(false);
      GraphAxesQTD->GetYLogQCKB()->setChecked(false);
      
      PrintMessage(GraphAxesQTD, MESSAGE_CRITICAL, trUtf8("FITS images can't be displayed in logarithmic scale."));
    }
  */
  
  TopFrame->GetActivePlot()->SetLogarithmicXAxis(GraphAxesQTD->GetXLogQCKB()->isChecked());
  TopFrame->GetActivePlot()->SetLogarithmicYAxis(GraphAxesQTD->GetYLogQCKB()->isChecked());
  TopFrame->GetActivePlot()->SetReverseXAxis(GraphAxesQTD->GetXRevQCKB()->isChecked());
  TopFrame->GetActivePlot()->SetReverseYAxis(GraphAxesQTD->GetYRevQCKB()->isChecked());
}

void GUI::PrintStatus(QString Status, int Duration)
{
  if(TerminalSenderFlag)
    {
      ScriptingWindow->PrintStatus(Status);
      
      statusBar()->clear();
    }
  else
    {
      if(Duration >= 0)
	statusBar()->message(Status, Duration);
      else
	statusBar()->message(Status);
    }
}

void GUI::FileQuitSlot()
{
  if(DataTable->IsModified() && PrintMessage(this, MESSAGE_WARNING, trUtf8("The table has been modified.<br />Are you sure you want to quit without saving it?"), trUtf8("Table modified")) == QMessageBox::No)
    {
      PrintStatus(trUtf8("Quit canceled: the table has been modified."));
      
      return;
    }
  
  QWidgetList WL = WorkSpaceQWS->windowList(QWorkspace::StackingOrder);
  
  WL.remove(DataTable);
  WL.remove(ScriptingWindow);
  
  for(int i = WL.count()-1; i >= 0; i--)
    {
      Frame * TmpFrame = dynamic_cast<Frame *>(WL.at(i));
      
      if(TmpFrame && TmpFrame->IsModified())
	{
	  if(PrintMessage(this, MESSAGE_WARNING, trUtf8("There are modified frames.<br />Are you sure you want to quit without saving them?"), trUtf8("Modified frames")) == QMessageBox::No)
	    {
	      PrintStatus(trUtf8("Quit canceled: there are modified frames."));
	      
	      return;
	    }
	  else
	    break;
	}
    }
  
  vector <QString> History = ScriptingWindow->GetHistory();
  
  if(OptionGlobalTerminalHistorySize != 0 && !History.empty())
    {
      QDir  Dir;
      QFile HistoryFile(Dir.homeDirPath() + QString(BAYANI_DIR) + QString(BAYANI_HISFILE) + QString(BAYANI_EXT));
      
      Dir.setPath(Dir.homeDirPath() + QString(BAYANI_DIR));
      
      if(!Dir.exists())
	if(Dir.mkdir(Dir.path()))
	  QMessageBox::information(this, trUtf8("Information: New directory"), trUtf8("Created directory %1 in order to store the commands history.") .arg(Dir.path()), QMessageBox::Ok);/* This one stays as a QMessageBox */
      
      if(HistoryFile.open(IO_WriteOnly))
	{
	  QTextStream HistoryStream(&HistoryFile);
	  
	  int Start = (History.size() > OptionGlobalTerminalHistorySize) ? (History.size()-OptionGlobalTerminalHistorySize): 0;
	  
	  if(OptionGlobalTerminalHistorySize < 0)
	    Start = 0;
	  
	  for(int i = Start; i < History.size(); i++)
	    HistoryStream << History[i] << "\n";
	  
	  HistoryFile.close();
	}
      else
	QMessageBox::critical(this, trUtf8("Error: File error"), trUtf8("A problem occured while attempting to open file %1 in write-only mode.") .arg(HistoryFile.name()), QMessageBox::Ok, QMessageBox::NoButton);/* This one stays as a QMessageBox */
    }
  
  PrintStatus(trUtf8("Bye!"));
  
  dynamic_cast<QApplication *>(parentWidget())->quit();
}

void GUI::ViewDataTableSlot()
{
  if(DataTable->isVisible()) 
    {
      DataTable->close();
      
      PrintStatus(trUtf8("Table hidden."));
    }
  else
    {
      DataTable->show();
      
      PrintStatus(trUtf8("Table shown."));
    }
}

void GUI::ViewScriptingWindowSlot()
{
  if(ScriptingWindow->isVisible()) 
    {
      ScriptingWindow->close();
      
      PrintStatus(trUtf8("Terminal hidden."));
    }
  else
    {
      ScriptingWindow->show();
      
      PrintStatus(trUtf8("Terminal shown."));
    }
}

void GUI::CheckViewDataTableMenuSlot()
{
  ViewMenuQPM->setItemChecked(ViewDataTableMenuID, true);
}

void GUI::CheckViewScriptingWindowMenuSlot()
{
  ViewMenuQPM->setItemChecked(ViewScriptingWindowMenuID, true);
}

void GUI::UnCheckViewDataTableMenuSlot()
{
  ViewMenuQPM->setItemChecked(ViewDataTableMenuID, false);
}

void GUI::UnCheckViewScriptingWindowMenuSlot()
{
  ViewMenuQPM->setItemChecked(ViewScriptingWindowMenuID, false);
}

void GUI::FrameNewSlot()
{
  FramesCounter++;
  
  TopFrame = new Frame(WorkSpaceQWS);
  
  QRect CurrentGeometry = TopFrame->geometry();
  QSize ExtraGeometry(/* Contains the size of the title bar (both dimensions) - Why is the +1 needed? */
		      DataTable->parentWidget()->geometry().width()-DataTable->geometry().width()+1,
		      DataTable->parentWidget()->geometry().height()-DataTable->geometry().height()+1
		      );
  CurrentGeometry.setSize((0.5*WorkSpaceQWS->size())-ExtraGeometry);
  
  TopFrame->setGeometry(CurrentGeometry);
  TopFrame->setCaption(trUtf8("Frame %1") .arg(FramesCounter));
  TopFrame->setIcon(QPixmap(BayaniLogoXPM));
  
  connect(TopFrame, SIGNAL(Shown        (bool   )), this       , SLOT(ChangeTopFrameSlot(bool   )));
  connect(TopFrame, SIGNAL(Closed       (bool   )), this       , SLOT(ChangeTopFrameSlot(bool   )));
  connect(TopFrame, SIGNAL(Modified     (bool   )), FrameSaveQA, SLOT(setEnabled        (bool   )));
  connect(TopFrame, SIGNAL(CanceledClose(QString)), this       , SLOT(PrintStatusSlot   (QString)));
  
  ApplyFrameDefaultSettings();
  
  TopFrame->show();
  
  PrintStatus(trUtf8("New frame, number %1.") .arg(FramesCounter));
}

void GUI::FrameZoneSlot()
{
  FrameZoneQTD->show();
  FrameZoneQTD->raise();
}

void GUI::FrameZoneApplySlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(FrameZoneQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  if(FrameZoneQTD->GetHorZoneQLE()->text() == "")
    {
      PrintMessage(FrameZoneQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the number of horizontal zones."));
      
      return;
    }
  
  if(FrameZoneQTD->GetVerZoneQLE()->text() == "")
    {
      PrintMessage(FrameZoneQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the number of vertical zones."));
      
      return;
    }
  
  if(!FrameZoneQTD->GetSecondaryQCKB()->isChecked())
    TopFrame->Zone(FrameZoneQTD->GetHorZoneQLE()->text().toInt(), FrameZoneQTD->GetVerZoneQLE()->text().toInt());
  else
    TopFrame->SubZone(FrameZoneQTD->GetZoneQSB()->value()-1, FrameZoneQTD->GetHorZoneQLE()->text().toInt(), FrameZoneQTD->GetVerZoneQLE()->text().toInt());
  
  TopFrame->ForceRepaint();
  TopFrame->repaint();
  
  EnableFrameRelatedMenus();
  
  PrintStatus(trUtf8("%1 zones are now available in the frame.") .arg(TopFrame->PlotsNumber()));
}

void GUI::FrameSetActivePlotSlot()
{
  FrameSetActivePlotQTD->show();
  FrameSetActivePlotQTD->raise();
}

void GUI::FrameSetActivePlotApplySlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(FrameSetActivePlotQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  if(TopFrame->ExistingPlotsNumber() <= 1)
    {
      PrintMessage(FrameSetActivePlotQTD, MESSAGE_CRITICAL, trUtf8("The number of existing graphs is less than or equal to 1."));
      
      return;
    }
  
  int PlotId = FrameSetActivePlotQTD->GetZoneQSB()->value()-1;
  
  if(PlotId == TopFrame->activePlotId())
    PrintMessage(FrameSetActivePlotQTD, MESSAGE_INFORMATION, trUtf8("Graph number %1 is already active.") .arg(FrameSetActivePlotQTD->GetZoneQSB()->value()));
  else
    {
      TopFrame->SetActivePlot(PlotId);
      
      PrintStatus(trUtf8("Graph number %1 is now active.") .arg(FrameSetActivePlotQTD->GetZoneQSB()->value()));
    }
}

void GUI::FrameSaveSlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(this, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  QString TestFileName = "";
  QString Init = TopFrame->caption() + ".png";
  Init.simplifyWhiteSpace();
  Init.replace(" ", "");
  Init = Init.lower();
  
  if(TopFrame->GetSaveFileName() == "")
    {
      TestFileName = QFileDialog::getSaveFileName(Init, trUtf8("PNG pictures (*.png);;All files (*)"));
      
      if(TestFileName == "")
	{
	  PrintStatus(trUtf8("Save canceled."));
	  
	  return;
	}
      else
	{
	  QFile SaveFile(TestFileName);
	  
	  if(SaveFile.exists())
	    if(PrintMessage(this, MESSAGE_WARNING, trUtf8("File %1 already exists.<br />Are you sure you want to overwrite it?") .arg(TestFileName), trUtf8("File exists")) == QMessageBox::No)
	      {
		PrintStatus(trUtf8("Save canceled: file already exists."));
		
		return;
	      }
	  
	  TopFrame->SetSaveFileName(TestFileName);
	}
    }
  
  QPixmap FramePixmap = QPixmap::grabWidget(TopFrame);
  
  if(!FramePixmap.save(TopFrame->GetSaveFileName(), "PNG"))
    PrintMessage(this, MESSAGE_CRITICAL, trUtf8("A problem occured while attempting to save frame in %1.") .arg(TopFrame->GetSaveFileName()));  
  else
    {
      TopFrame->setCaption(TopFrame->GetSaveFileName());
      
      TopFrame->SetModified(false);
      
      PrintStatus(trUtf8("Saved frame in %1.") .arg(TopFrame->GetSaveFileName()));
    }
}

void GUI::FrameSaveAsSlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(this, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  QString TestFileName = "";
  QString Init = TopFrame->GetSaveFileName();
  if(Init == "")
    {
      Init = TopFrame->caption() + ".png";
      Init.simplifyWhiteSpace();
      Init.replace(" ", "");
      Init = Init.lower();
    }
  
  TestFileName = QFileDialog::getSaveFileName(Init, trUtf8("PNG pictures (*.png);;All files (*)"));
  
  if(TestFileName == "")
    {
      PrintStatus(trUtf8("Save canceled."));
      
      return;
    }
  else
    {
      QFile SaveFile(TestFileName);
      
      if(SaveFile.exists() && TestFileName != TopFrame->GetSaveFileName())
	if(PrintMessage(this,  MESSAGE_WARNING, trUtf8("File %1 already exists.<br />Are you sure you want to overwrite it?") .arg(TestFileName), trUtf8("File exists")) == QMessageBox::No)
	  {
	    PrintStatus(trUtf8("Save canceled: file already exists."));
	    
	    return;
	  }
      
      TopFrame->SetSaveFileName(TestFileName);
    }
  
  QPixmap FramePixmap = QPixmap::grabWidget(TopFrame);
  
  if(!FramePixmap.save(TopFrame->GetSaveFileName(), "PNG"))
    PrintMessage(this, MESSAGE_CRITICAL, trUtf8("A problem occured while attempting to save frame in %1.") .arg(TopFrame->GetSaveFileName()));
  else
    {
      TopFrame->setCaption(TopFrame->GetSaveFileName());
      
      TopFrame->SetModified(false);
      
      PrintStatus(trUtf8("Saved frame in %1.") .arg(TopFrame->GetSaveFileName()));
    }
}

void GUI::FrameCloseSlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(this, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  TopFrame->close();
}

void GUI::ChangeTopFrameSlot(bool Flag)
{
  if(Flag)
    {
      TopFrame = dynamic_cast<Frame *>(WorkSpaceQWS->activeWindow());
      
      EnableFrameRelatedMenus();
    }
  else
    {
      TopFrame = NULL;
      
      DisableFrameRelatedMenus();
      
      PrintStatus(trUtf8("Frame closed."));
    }
}

void GUI::ChangeTopFrameSlot(QWidget *)
{
  if(dynamic_cast<QTableBayani *>(WorkSpaceQWS->activeWindow()) || dynamic_cast<QTextEditBayani *>(WorkSpaceQWS->activeWindow()))
    return;
  
  TopFrame = dynamic_cast<Frame *>(WorkSpaceQWS->activeWindow());
  
  if(TopFrame)
    EnableFrameRelatedMenus();
  else
    DisableFrameRelatedMenus();
}

void GUI::TableFillFromFileSlot()
{
  TableFillFromFileQTD->show();
  TableFillFromFileQTD->raise();
}

void GUI::TableFillFromFileApplySlot()
{
  if(TableFillFromFileQTD->GetFileNameQFLS()->GetFileName() == "")
    {
      PrintMessage(TableFillFromFileQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the file name."));
      
      return;
    }
  
  if(TableFillFromFileQTD->GetColumnsNumberQLE()->text() == "")
    {
      PrintMessage(TableFillFromFileQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the columns number."));
      
      return;
    }
  
  if(TableFillFromFileQTD->GetSeparatorQLE()->text() == "")
    {
      PrintMessage(TableFillFromFileQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the columns separator."));
      
      return;
    }
  
  if(TableFillFromFileQTD->GetSubLinesQCKB()->isChecked() && TableFillFromFileQTD->GetDefaultFillQLE()->text() == "")
    {
      PrintMessage(TableFillFromFileQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the default fill."));
      
      return;
    }
  
  QFile NewFile(TableFillFromFileQTD->GetFileNameQFLS()->GetFileName());
  
  if(NewFile.open(IO_ReadOnly))
    {
      if(PrintMessage(TableFillFromFileQTD, MESSAGE_WARNING, trUtf8("This will remove all the current content from the selected cells.<br />Are you sure you want to continue?"), trUtf8("Fill table?")) == QMessageBox::No)
	{
	  NewFile.close();
	  
	  PrintStatus(trUtf8("Fill canceled."));
	  
	  return;
	}
      
      if(TableFillFromFileQTD->GetEmptyCellsQCKB()->isChecked())
	if(PrintMessage(TableFillFromFileQTD, MESSAGE_WARNING, trUtf8("This will remove all the current content from the other cells.<br />Are you sure you want to continue?"), trUtf8("Empty other cells?")) == QMessageBox::Yes)
	  {
	    for(int i = 0; i < DataTable->numRows(); i++)
	      for(int j = 0; j < DataTable->numCols(); j++)
		DataTable->setText(i, j, "");
	  }
      
      int     ColumnsNumber  = TableFillFromFileQTD->GetColumnsNumberQLE()->text().toInt();
      int     StartingRow    = TableFillFromFileQTD->GetStartingRowQSB()->value()-1;
      int     StartingColumn = TableFillFromFileQTD->GetStartingColumnQSB()->value()-1;
      double  DefaultFill    = TableFillFromFileQTD->GetDefaultFillQLE()->text().toDouble();
      QString Separator      = TableFillFromFileQTD->GetSeparatorQLE()->text();
      
      vector      <double> FileXY[ColumnsNumber];
      QTextStream NewStream(&NewFile);
      QStringList NewLineList;
      int         LinesNumber = 0;
      
      while(!NewStream.atEnd())
	{
	  NewLineList = NewLineList.split(Separator, NewStream.readLine());
	  
	  if(NewLineList.size() == ColumnsNumber)
	    for(int i = 0; i < ColumnsNumber; i++)
	      FileXY[i].push_back(NewLineList[i].toDouble());
	  
	  if((NewLineList.size() > ColumnsNumber) && TableFillFromFileQTD->GetSupLinesQCKB()->isChecked())
	    for(int i = 0; i < ColumnsNumber; i++)
	      FileXY[i].push_back(NewLineList[i].toDouble());
	  
	  if((NewLineList.size() < ColumnsNumber) && (NewLineList.size() > 0) && TableFillFromFileQTD->GetSubLinesQCKB()->isChecked())
	    {
	      for(int i = 0; i < NewLineList.size(); i++)
		FileXY[i].push_back(NewLineList[i].toDouble());
	      
	      for(int i = NewLineList.size(); i < ColumnsNumber; i++)
		FileXY[i].push_back(DefaultFill);
	    }
	  
	  LinesNumber++;
	}
      
      NewFile.close();
      
      if(((DataTable->numRows()-StartingRow) < FileXY[0].size()) && TableFillFromFileQTD->GetAddRowsQCKB()->isChecked())
	DataTable->insertRows(DataTable->numRows(), FileXY[0].size()-(DataTable->numRows()-StartingRow));
      
      if(((DataTable->numCols()-StartingColumn) < ColumnsNumber) && TableFillFromFileQTD->GetAddColumnsQCKB()->isChecked())
	DataTable->insertColumns(DataTable->numCols(), ColumnsNumber-(DataTable->numCols()-StartingColumn));
      
      int MaxRowIncrement    = (FileXY[0].size() > (DataTable->numRows()-StartingRow)) ? (DataTable->numRows()-StartingRow) : FileXY[0].size();
      int MaxColumnIncrement = (ColumnsNumber > (DataTable->numCols()-StartingColumn)) ? (DataTable->numCols()-StartingColumn) : ColumnsNumber;
      
      for(int i = 0; i < MaxRowIncrement; i++)
	for(int j = 0; j < MaxColumnIncrement; j++)
	  DataTable->setText(i+StartingRow, j+StartingColumn, QString("%1") .arg(FileXY[j][i]));
      
      UpdateTableRelatedItems();
      
      DataTable->SetModified(true);
      
      PrintStatus(trUtf8("Read %1 lines from %2. Considered %3 lines.") .arg(LinesNumber) .arg(TableFillFromFileQTD->GetFileNameQFLS()->GetFileName()) .arg(FileXY[0].size()));
    }
  else
    PrintMessage(TableFillFromFileQTD, MESSAGE_CRITICAL, trUtf8("A problem occured while attempting to open file %1 in read-only mode.") .arg(TableFillFromFileQTD->GetFileNameQFLS()->GetFileName()));
}

void GUI::TableWriteToFileSlot()
{
  TableWriteToFileQTD->show();
  TableWriteToFileQTD->raise();
}

void GUI::TableWriteToFileApplySlot()
{
  if(TableWriteToFileQTD->GetFileNameQFLS()->GetFileName() == "")
    {
      PrintMessage(TableWriteToFileQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the file name."));
      
      return;
    }
  
  if(TableWriteToFileQTD->GetSeparatorQLE()->text() == "")
    {
      PrintMessage(TableWriteToFileQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the columns separator."));
      
      return;
    }
  
  QFile NewFile(TableWriteToFileQTD->GetFileNameQFLS()->GetFileName());
  
  if(NewFile.exists() && !TableWriteToFileQTD->GetAppendQCKB()->isChecked())
    if(PrintMessage(TableWriteToFileQTD, MESSAGE_WARNING, trUtf8("File %1 already exists.<br />Are you sure you want to overwrite it?") .arg(TableWriteToFileQTD->GetFileNameQFLS()->GetFileName()), trUtf8("File exists")) == QMessageBox::No)
      {
	PrintStatus(trUtf8("Write canceled."));
	
	return;
      }
  
  int WriteMode = IO_WriteOnly;
  
  if(TableWriteToFileQTD->GetAppendQCKB()->isChecked())
    WriteMode = IO_WriteOnly | IO_Append;
  
  if(NewFile.open(WriteMode))
    {
      QTextStream NewStream(&NewFile);
      
      for(int i = TableWriteToFileQTD->GetRow1QSB()->value()-1; i <= TableWriteToFileQTD->GetRow2QSB()->value()-1; i++)
	for(int j = TableWriteToFileQTD->GetColumn1QSB()->value()-1; j <= TableWriteToFileQTD->GetColumn2QSB()->value()-1; j++)
	  {
	    if(j != TableWriteToFileQTD->GetColumn2QSB()->value()-1)
	      NewStream << DataTable->text(i, j) << TableWriteToFileQTD->GetSeparatorQLE()->text();
	    else
	      NewStream << DataTable->text(i, j) << "\n";
	  }
      
      NewFile.close();
      
      if(TableWriteToFileQTD->GetRow1QSB()->value() == 1 && TableWriteToFileQTD->GetRow2QSB()->value() == DataTable->numRows()
	 &&
	 TableWriteToFileQTD->GetColumn1QSB()->value() == 1 && TableWriteToFileQTD->GetColumn2QSB()->value() == DataTable->numCols())
	DataTable->SetModified(false);
      
      PrintStatus(trUtf8("Written %1 lines in %2.") .arg(TableWriteToFileQTD->GetRow2QSB()->value()-TableWriteToFileQTD->GetRow1QSB()->value()+1) .arg(TableWriteToFileQTD->GetFileNameQFLS()->GetFileName()));
    }
  else
    PrintMessage(TableWriteToFileQTD, MESSAGE_CRITICAL, trUtf8("A problem occured while attempting to open file %1 in write mode.") .arg(TableWriteToFileQTD->GetFileNameQFLS()->GetFileName()));
}

void GUI::TableSwapSlot()
{
  TableSwapQTD->show();
  TableSwapQTD->raise();
}

void GUI::TableSwapApplySlot()
{
  if(TableSwapQTD->GetElement1QSB()->value() == TableSwapQTD->GetElement2QSB()->value())
    {
      PrintMessage(TableSwapQTD, MESSAGE_CRITICAL, trUtf8("Elements are equal. There's nothing to do."));
      
      return;
    }
  
  if(TableSwapQTD->GetRowsQRB()->isChecked())
    {
      if(TableSwapQTD->GetCopyQCKB()->isChecked())
	{
	  if(PrintMessage(TableSwapQTD, MESSAGE_WARNING, trUtf8("This will remove all the current content from the destination element.<br />Are you sure you want to continue?"), trUtf8("Copy content?")) == QMessageBox::No)
	    {
	      PrintStatus(trUtf8("Copy canceled."));
	      
	      return;
	    }
	  
	  for(int i = 0; i < DataTable->numCols(); i++)
	    DataTable->setText(TableSwapQTD->GetElement2QSB()->value()-1, i, DataTable->text(TableSwapQTD->GetElement1QSB()->value()-1, i));
	}
      else
	{
	  DataTable->swapRows(TableSwapQTD->GetElement1QSB()->value()-1, TableSwapQTD->GetElement2QSB()->value()-1);
	  DataTable->updateContents();
	}
    }
  else
    {
      if(TableSwapQTD->GetCopyQCKB()->isChecked())
	{
	  if(PrintMessage(TableSwapQTD, MESSAGE_WARNING, trUtf8("This will remove all the current content from the destination element.<br />Are you sure you want to continue?"), trUtf8("Copy content?")) == QMessageBox::No)
	    {
	      PrintStatus(trUtf8("Copy canceled."));
	      
	      return;
	    }
	  
	  for(int i = 0; i < DataTable->numRows(); i++)
	    DataTable->setText(i, TableSwapQTD->GetElement2QSB()->value()-1, DataTable->text(i, TableSwapQTD->GetElement1QSB()->value()-1));
	}
      else
	DataTable->swapColumns(TableSwapQTD->GetElement1QSB()->value()-1, TableSwapQTD->GetElement2QSB()->value()-1);
    }
  
  DataTable->SetModified(true);
  
  PrintStatus(trUtf8("Updated table."));
}

void GUI::TableSwapRowsQRBSlot(bool State)
{
  if(State)
    {
      TableSwapQTD->GetElement1QSB()->setMaxValue(DataTable->numRows());
      TableSwapQTD->GetElement2QSB()->setMaxValue(DataTable->numRows());
    }
  else
    {
      TableSwapQTD->GetElement1QSB()->setMaxValue(DataTable->numCols());
      TableSwapQTD->GetElement2QSB()->setMaxValue(DataTable->numCols());
    }
}

void GUI::TableTransposeSlot()
{
  int Rows = DataTable->numRows();
  int Cols = DataTable->numCols();
  
  QString ** Data = new QString * [Rows];
  for(int i = 0; i < Rows; i++)
    Data[i] = new QString[Cols];
  
  for(int i = 0; i < Rows; i++)
    for(int j = 0; j < Cols; j++)
      Data[i][j] = DataTable->text(i, j);
  
  DataTable->setNumRows(Cols);
  DataTable->setNumCols(Rows);
  
  for(int i = 0; i < DataTable->numRows(); i++)
    for(int j = 0; j < DataTable->numCols(); j++)
      DataTable->setText(i, j, Data[j][i]);
  
  for(int i = 0; i < Rows; i++)
    delete[] Data[i];
  delete[] Data;
  
  UpdateTableRelatedItems();
  
  DataTable->SetModified(true);
  
  PrintStatus(trUtf8("Updated table: %1 rows, %2 columns.") .arg(DataTable->numRows()) .arg(DataTable->numCols()));
}

void GUI::TableManageSlot()
{
  TableManageQTD->show();
  TableManageQTD->raise();
}

void GUI::TableManageApplySlot()
{
  if(TableManageQTD->GetNumberQLE()->text() == "")
    {
      PrintMessage(TableManageQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the elements number."));
      
      return;
    }
  
  int ElementsNumber = TableManageQTD->GetNumberQLE()->text().toInt();
  int StartElement   = TableManageQTD->GetStartQSB()->value()-1;
  
  if(StartElement == -1)
    StartElement++;
  
  if(TableManageQTD->GetInsertQRB()->isChecked())
    {
      if(TableManageQTD->GetRowsQRB()->isChecked())
	DataTable->insertRows(StartElement, ElementsNumber);
      else
	DataTable->insertColumns(StartElement, ElementsNumber);
    }
  else
    {
      if(
	 (TableManageQTD->GetRowsQRB()->isChecked() && StartElement+ElementsNumber > DataTable->numRows())
	 ||
	 (TableManageQTD->GetColumnsQRB()->isChecked() && StartElement+ElementsNumber > DataTable->numCols())
	 )
	{
	  PrintMessage(TableManageQTD, MESSAGE_CRITICAL, trUtf8("You are attempting to remove inexisting elements."));
	  
	  return;
	}
      
      if(PrintMessage(TableManageQTD, MESSAGE_WARNING, trUtf8("This will remove all the selected elements from the table.<br />Are you sure you want to continue?"), trUtf8("Remove elements?")) == QMessageBox::No)
	{
	  PrintStatus(trUtf8("Remove canceled."));
	  
	  return;
	}
      
      QMemArray <int> Elements;
      
      int i = 0;
      
      while(i < ElementsNumber)
	{
	  Elements.resize(i+1);
	  
	  Elements[i] = i+StartElement;
	  
	  i++;
	}
      
      if(TableManageQTD->GetRowsQRB()->isChecked())
	DataTable->removeRows(Elements);
      else
	DataTable->removeColumns(Elements);
    }
  
  UpdateTableRelatedItems();
  
  DataTable->SetModified(true);
  
  PrintStatus(trUtf8("Updated table: %1 rows, %2 columns.") .arg(DataTable->numRows()) .arg(DataTable->numCols()));
}

void GUI::TableManageRowsQRBSlot(bool State)
{
  if(State)
    TableManageQTD->GetStartQSB()->setMaxValue(DataTable->numRows());
  else
    TableManageQTD->GetStartQSB()->setMaxValue(DataTable->numCols());
}

void GUI::TableEmptySlot()
{
  TableEmptyQTD->show();
  TableEmptyQTD->raise();
}

void GUI::TableEmptyApplySlot()
{
  if(PrintMessage(TableEmptyQTD, MESSAGE_WARNING, trUtf8("This will delete all the data from the selected cells.<br />Are you sure you want to continue?"), trUtf8("Empty cells?")) == QMessageBox::No)
    {
      PrintStatus(trUtf8("Empty canceled."));
      
      return;
    }
  
  for(int i = TableEmptyQTD->GetRow1QSB()->value()-1; i <= TableEmptyQTD->GetRow2QSB()->value()-1; i++)
    for(int j = TableEmptyQTD->GetColumn1QSB()->value()-1; j <= TableEmptyQTD->GetColumn2QSB()->value()-1; j++)
      DataTable->setText(i, j, "");
  
  UpdateTableRelatedItems();
  
  DataTable->SetModified(true);
  
  PrintStatus(trUtf8("Emptied %1 cells between [%2,%3] and [%4,%5].") .arg((TableEmptyQTD->GetRow2QSB()->value()-TableEmptyQTD->GetRow1QSB()->value()+1)*(TableEmptyQTD->GetColumn2QSB()->value()-TableEmptyQTD->GetColumn1QSB()->value()+1)) .arg(TableEmptyQTD->GetColumn1QSB()->value()) .arg(TableEmptyQTD->GetRow1QSB()->value()) .arg(TableEmptyQTD->GetColumn2QSB()->value()) .arg(TableEmptyQTD->GetRow2QSB()->value()));
}

void GUI::GraphFunctionSlot()
{
  GraphFunctionQTD->show();
  GraphFunctionQTD->raise();
}

void GUI::GraphFunctionApplySlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(GraphFunctionQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  if(GraphFunctionQTD->GetExpressionQLE()->text() == "")
    {
      PrintMessage(GraphFunctionQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the expression."));
      
      return;
    }
  
  if(GraphFunctionQTD->GetXMinQLE()->text() == "")
    {
      PrintMessage(GraphFunctionQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the inferior value."));
      
      return;
    }
  
  if(GraphFunctionQTD->GetXMaxQLE()->text() == "")
    {
      PrintMessage(GraphFunctionQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the superior value."));
      
      return;
    }
  
  double XMin = GraphFunctionQTD->GetXMinQLE()->text().toDouble();
  double XMax = GraphFunctionQTD->GetXMaxQLE()->text().toDouble();
  
  if(XMin >= XMax)
    {
      PrintMessage(GraphFunctionQTD, MESSAGE_CRITICAL, trUtf8("The inferior value is greater than or equal to the superior one."));
      
      return;
    }
  
  BExpression * NewExpression = new BExpression;
  NewExpression->setVariable(trUtf8("x", "Variable"));
  
  if(!NewExpression->setExpression(GraphFunctionQTD->GetExpressionQLE()->text()))
    {
      PrintMessage(GraphFunctionQTD, MESSAGE_CRITICAL, trUtf8("Error in expression: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression->error()));
      
      delete NewExpression;
      
      return;
    }
  
  Func1D * NewFunc1D = new Func1D;
  NewFunc1D->SetData(NewExpression);
  NewFunc1D->SetXMinXMax(XMin, XMax);/* Specific */
  
  NewFunc1D->SetLineWidth(OptionDataFunctionLineWidth);
  NewFunc1D->SetLineColor(OptionDataFunctionLineColor);
  NewFunc1D->SetLineStyle(OptionDataFunctionLineStyle);
  
  if(
     TopFrame->IsEmpty()
     ||
     (!TopFrame->IsEmpty() && !GraphFunctionQTD->GetSameQCKB()->isChecked())
     )
    {
      CreateNewGraph();
      
      Graph * ActiveGraph = TopFrame->GetActivePlot();
      ActiveGraph->AddData(NewFunc1D);
      
      ActiveGraph->show();
    }
  else
    {
      Graph * ActiveGraph = TopFrame->GetActivePlot();
      ActiveGraph->AddData(NewFunc1D);
      
      ActiveGraph->ForceRepaint();
      ActiveGraph->repaint();
    }
  
  PrintStatus(trUtf8("Represented %1 between %2 and %3.") .arg(GraphFunctionQTD->GetExpressionQLE()->text()) .arg(XMin) .arg(XMax));
}

void GUI::GraphHistogramSlot()
{
  GraphHistogramQTD->show();
  GraphHistogramQTD->raise();
}

void GUI::GraphHistogramApplySlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(GraphHistogramQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  Histogram NewHistogram;
  
  if(GraphHistogramQTD->GetBinsNumberQCKB()->isChecked())
    {
      if(GraphHistogramQTD->GetBinsNumberQLE()->text() == "")
	{
	  PrintMessage(GraphHistogramQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the number of bins."));
	  
	  return;
	}
      
      NewHistogram.SetBinsNumber(GraphHistogramQTD->GetBinsNumberQLE()->text().toInt());
    }
  
  if(GraphHistogramQTD->GetXMinQCKB()->isChecked())
    {
      if(GraphHistogramQTD->GetXMinQLE()->text() == "")
	{
	  PrintMessage(GraphHistogramQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the minimal value."));
	  
	  return;
	}
      
      NewHistogram.SetXMin(GraphHistogramQTD->GetXMinQLE()->text().toDouble());
    }
  
  if(GraphHistogramQTD->GetXMaxQCKB()->isChecked())
    {
      if(GraphHistogramQTD->GetXMaxQLE()->text() == "")
	{
	  PrintMessage(GraphHistogramQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the maximal value."));
	  
	  return;
	}
      
      NewHistogram.SetXMax(GraphHistogramQTD->GetXMaxQLE()->text().toDouble());
    }
  
  if(
     GraphHistogramQTD->GetXMinQCKB()->isChecked() && GraphHistogramQTD->GetXMaxQCKB()->isChecked()
     &&
     GraphHistogramQTD->GetXMinQLE()->text().toDouble() >= GraphHistogramQTD->GetXMaxQLE()->text().toDouble()
     )
    {
      PrintMessage(GraphHistogramQTD, MESSAGE_CRITICAL, trUtf8("The minimal value is greater than or equal to the maximal one."));
      
      return;
    }
  
  BExpression NewExpression1;
  BExpression NewExpression2;
  
  vector <Variable> Variables;
  Variable NewVariable;
  
  for(int j = 0; j < DataTable->numCols(); j++)
    {
      NewVariable.Name  = trUtf8("c%1", "Parameter") .arg(j+1);
      NewVariable.Value = 0.;
      
      Variables.push_back(NewVariable);
    }
  
  NewExpression1.setVariables(Variables);
  NewExpression2.setVariables(Variables);
  
  NewExpression1.setExpression(GraphHistogramQTD->GetCutQLE       ()->text());
  NewExpression2.setExpression(GraphHistogramQTD->GetExpressionQLE()->text());
  
  for(int i = 0; i < DataTable->numRows(); i++)
    {
      for(int j = 0; j < DataTable->numCols(); j++)
	Variables[j].Value = DataTable->text(i, j).toDouble(); 
      
      NewExpression1.setVariables(Variables);
      NewExpression2.setVariables(Variables);
      
      if(GraphHistogramQTD->GetCutQCKB()->isChecked())
	{
	  if(NewExpression1.evaluate())
	    {
	      if(!NewExpression1.value())
		continue;
	    }
	  else
	    {
	      PrintMessage(GraphHistogramQTD, MESSAGE_CRITICAL, trUtf8("Error in cut: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression1.error()));
	      
	      return;
	    }
	}
      
      if(GraphHistogramQTD->GetColumnQRB()->isChecked())
	{
	  QString X = DataTable->text(i, GraphHistogramQTD->GetColumnQSB()->value()-1);//out
	  
	  if(X != NULL)
	    NewHistogram.AddValue(X.toDouble());
	}
      else
	{
	  if(NewExpression2.evaluate())
	    NewHistogram.AddValue(NewExpression2.value());
	  else
	    {
	      PrintMessage(GraphHistogramQTD, MESSAGE_CRITICAL, trUtf8("Error in expression: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression2.error()));
	      
	      return;
	    }
	}
    }
  
  if(NewHistogram.GetData().empty())
    {
      PrintMessage(GraphHistogramQTD, MESSAGE_CRITICAL, trUtf8("There is nothing to represent."));
      
      return;
    }
  
  NewHistogram.Compute();
  
  Histo1D * NewHisto1D = new Histo1D;
  NewHisto1D->SetData(NewHistogram);
  
  NewHisto1D->SetLineWidth(OptionDataHistogramLineWidth);
  NewHisto1D->SetLineColor(OptionDataHistogramLineColor);
  NewHisto1D->SetFillColor(OptionDataHistogramFillColor);
  NewHisto1D->SetLineStyle(OptionDataHistogramLineStyle);
  NewHisto1D->SetFillStyle(OptionDataHistogramFillStyle);
  
  if(
     TopFrame->IsEmpty()
     ||
     (!TopFrame->IsEmpty() && !GraphHistogramQTD->GetSameQCKB()->isChecked())
     )
    {
      CreateNewGraph();
      
      Graph * ActiveGraph = TopFrame->GetActivePlot();
      ActiveGraph->AddData(NewHisto1D);
      
      ActiveGraph->show();
    }
  else
    {
      Graph * ActiveGraph = TopFrame->GetActivePlot();
      ActiveGraph->AddData(NewHisto1D);
      
      ActiveGraph->ForceRepaint();
      ActiveGraph->repaint();
    }
  
  PrintStatus(trUtf8("%1 entries in histogram.") .arg(NewHistogram.GetData().size()));
}

void GUI::GraphGraph2DSlot()
{
  GraphGraph2DQTD->show();
  GraphGraph2DQTD->raise();
}

void GUI::GraphGraph2DApplySlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(GraphGraph2DQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  DataArray NewDataArray;
  
  BExpression NewExpression1;
  BExpression NewExpression2;
  BExpression NewExpression3;
  BExpression NewExpression4;
  BExpression NewExpression5;
  BExpression NewExpression6;
  BExpression NewExpression7;
  
  vector <Variable> Variables;
  Variable NewVariable;
  
  for(int j = 0; j < DataTable->numCols(); j++)
    {
      NewVariable.Name  = trUtf8("c%1", "Parameter") .arg(j+1);
      NewVariable.Value = 0.;
      
      Variables.push_back(NewVariable);
    }
  
  NewExpression1.setVariables(Variables);
  NewExpression2.setVariables(Variables);
  NewExpression3.setVariables(Variables);
  NewExpression4.setVariables(Variables);
  NewExpression5.setVariables(Variables);
  NewExpression6.setVariables(Variables);
  NewExpression7.setVariables(Variables);
  
  NewExpression1.setExpression(GraphGraph2DQTD->GetCutQLE            ()->text());
  NewExpression2.setExpression(GraphGraph2DQTD->GetExpressionXQLE    ()->text());
  NewExpression3.setExpression(GraphGraph2DQTD->GetExpressionYQLE    ()->text());
  NewExpression4.setExpression(GraphGraph2DQTD->GetExpressionDownXQLE()->text());
  NewExpression5.setExpression(GraphGraph2DQTD->GetExpressionDownYQLE()->text());
  NewExpression6.setExpression(GraphGraph2DQTD->GetExpressionUpXQLE  ()->text());
  NewExpression7.setExpression(GraphGraph2DQTD->GetExpressionUpYQLE  ()->text());
  
  for(int i = 0; i < DataTable->numRows(); i++)
    {
      for(int j = 0; j < DataTable->numCols(); j++)
	Variables[j].Value = DataTable->text(i, j).toDouble(); 
      
      NewExpression1.setVariables(Variables);
      NewExpression2.setVariables(Variables);
      NewExpression3.setVariables(Variables);
      NewExpression4.setVariables(Variables);
      NewExpression5.setVariables(Variables);
      NewExpression6.setVariables(Variables);
      NewExpression7.setVariables(Variables);
      
      if(GraphGraph2DQTD->GetCutQCKB()->isChecked())
	{
	  if(NewExpression1.evaluate())
	    {
	      if(!NewExpression1.value())
		continue;
	    }
	  else
	    {
	      PrintMessage(GraphGraph2DQTD, MESSAGE_CRITICAL, trUtf8("Error in cut: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression1.error()));
	      
	      return;
	    }
	}
      
      double XValue     = -1.;
      double YValue     = -1.;
      double XErrorDown = -1.;
      double YErrorDown = -1.;
      double XErrorUp   = -1.;
      double YErrorUp   = -1.;
      
      if(GraphGraph2DQTD->GetColumnXQRB()->isChecked())
	{
	  QString X = DataTable->text(i, GraphGraph2DQTD->GetColumnXQSB()->value()-1);
	  
	  if(X == NULL)
	    continue;
	  
	  XValue = X.toDouble();
	}
      else
	{
	  if(NewExpression2.evaluate())
	    XValue = NewExpression2.value();
	  else
	    {
	      PrintMessage(GraphGraph2DQTD, MESSAGE_CRITICAL, trUtf8("Error in x axis expression: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression2.error()));
	      
	      return;
	    }
	}
      
      if(GraphGraph2DQTD->GetColumnYQRB()->isChecked())
	{
	  QString Y = DataTable->text(i, GraphGraph2DQTD->GetColumnYQSB()->value()-1);
	  
	  if(Y == NULL)
	    continue;
	  
	  YValue = Y.toDouble();
	}
      else
	{
	  if(NewExpression3.evaluate())
	    YValue = NewExpression3.value();
	  else
	    {
	      PrintMessage(GraphGraph2DQTD, MESSAGE_CRITICAL, trUtf8("Error in y axis expression: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression3.error()));
	      
	      return;
	    }
	}
      
      if(GraphGraph2DQTD->GetErrorsQCKB()->isChecked())
	{
	  if(GraphGraph2DQTD->GetErrorsDownXQCKB()->isChecked())
	    {
	      if(GraphGraph2DQTD->GetColumnDownXQRB()->isChecked())
		{
		  QString X = DataTable->text(i, GraphGraph2DQTD->GetColumnDownXQSB()->value()-1);
		  
		  if(X != NULL)
		    XErrorDown = X.toDouble();
		}
	      else
		{
		  if(NewExpression4.evaluate())
		    XErrorDown = NewExpression4.value();
		  else
		    {
		      PrintMessage(GraphGraph2DQTD, MESSAGE_CRITICAL, trUtf8("Error in x axis (inferior) errors expression: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression4.error()));
		      
		      return;
		    }
		}
	      
	      if(!GraphGraph2DQTD->GetAsymmetricQCKB()->isChecked())
		XErrorUp = XErrorDown;
	    }
	  
	  if(GraphGraph2DQTD->GetErrorsDownYQCKB()->isChecked())
	    {
	      if(GraphGraph2DQTD->GetColumnDownYQRB()->isChecked())
		{
		  QString Y = DataTable->text(i, GraphGraph2DQTD->GetColumnDownYQSB()->value()-1);
		  
		  if(Y != NULL)
		    YErrorDown = Y.toDouble();
		}
	      else
		{
		  if(NewExpression5.evaluate())
		    YErrorDown = NewExpression5.value();
		  else
		    {
		      PrintMessage(GraphGraph2DQTD, MESSAGE_CRITICAL, trUtf8("Error in y axis (inferior) errors expression: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression5.error()));
		      
		      return;
		    }
		}
	      
	      if(!GraphGraph2DQTD->GetAsymmetricQCKB()->isChecked())
		YErrorUp = YErrorDown;
	    }
	  
	  if(GraphGraph2DQTD->GetAsymmetricQCKB()->isChecked())
	    {
	      if(GraphGraph2DQTD->GetErrorsUpXQCKB()->isChecked())
		{
		  if(GraphGraph2DQTD->GetColumnUpXQRB()->isChecked())
		    {
		      QString X = DataTable->text(i, GraphGraph2DQTD->GetColumnUpXQSB()->value()-1);
		      
		      if(X != NULL)
			XErrorUp = X.toDouble();
		    }
		  else
		    {
		      if(NewExpression6.evaluate())
			XErrorUp = NewExpression6.value();
		      else
			{
			  PrintMessage(GraphGraph2DQTD, MESSAGE_CRITICAL, trUtf8("Error in x axis (superior) errors expression: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression6.error()));
			  
			  return;
			}
		    }
		}
	      
	      if(GraphGraph2DQTD->GetErrorsUpYQCKB()->isChecked())
		{
		  if(GraphGraph2DQTD->GetColumnUpYQRB()->isChecked())
		    {
		      QString Y = DataTable->text(i, GraphGraph2DQTD->GetColumnUpYQSB()->value()-1);
		      
		      if(Y != NULL)
			YErrorUp = Y.toDouble();
		    }
		  else
		    {
		      if(NewExpression7.evaluate())
			YErrorUp = NewExpression7.value();
		      else
			{
			  PrintMessage(GraphGraph2DQTD, MESSAGE_CRITICAL, trUtf8("Error in y axis (superior) errors expression: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression7.error()));
			  
			  return;
			}
		    }
		}
	    }
	}
      
      NewDataArray.AddData(XValue, YValue, XErrorDown, YErrorDown, XErrorUp, YErrorUp);
    }
  
  if(!NewDataArray.GetSize())
    {
      PrintMessage(GraphGraph2DQTD, MESSAGE_CRITICAL, trUtf8("There is nothing to represent."));
      
      return;
    }
  
  Graph2D * NewGraph2D = new Graph2D;
  NewGraph2D->SetData(NewDataArray);
  
  NewGraph2D->SetDataType(OptionDataGraphShapeType);
  NewGraph2D->SetErrorsLineWidth(OptionDataGraphErrorsLineWidth);
  NewGraph2D->SetDataRatio(OptionDataGraphShapeRatio);
  NewGraph2D->SetDataColor(OptionDataGraphShapeColor);
  NewGraph2D->SetErrorsLineColor(OptionDataGraphErrorsLineColor);
  NewGraph2D->SetErrorsLineStyle(OptionDataGraphErrorsLineStyle);
  
  if(
     TopFrame->IsEmpty()
     ||
     (!TopFrame->IsEmpty() && !GraphGraph2DQTD->GetSameQCKB()->isChecked())
     )
    {
      CreateNewGraph();
      
      Graph * ActiveGraph = TopFrame->GetActivePlot();
      ActiveGraph->AddData(NewGraph2D);
      
      ActiveGraph->show();
    }
  else
    {
      Graph * ActiveGraph = TopFrame->GetActivePlot();
      ActiveGraph->AddData(NewGraph2D);
      
      ActiveGraph->ForceRepaint();
      ActiveGraph->repaint();
    }
  
  PrintStatus(trUtf8("%1 entries in graph.") .arg(NewDataArray.GetSize()));
}

void GUI::GraphFITSSlot()
{
  GraphFITSQTD->show();
  GraphFITSQTD->raise();
}

void GUI::GraphFITSApplySlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(GraphFITSQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  if(GraphFITSQTD->GetFileNameQFLS()->GetFileName() == "")
    {
      PrintMessage(GraphFITSQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the file name."));
      
      return;
    }
  
  /*
    if(GraphAxesNewFlag && (GraphAxesQTD->GetXLogQCKB()->isChecked() || GraphAxesQTD->GetYLogQCKB()->isChecked()))
    {
    PrintMessage(GraphFITSQTD, MESSAGE_CRITICAL, trUtf8("FITS images can't be displayed in logarithmic scale."));
    
    return;
    }
  */
  
  FITSImage NewFITSImage;
  NewFITSImage.ReadFile(GraphFITSQTD->GetFileNameQFLS()->GetFileName());
  
  if(NewFITSImage.GetXSize() < 1 || NewFITSImage.GetYSize() < 1)
    {
      PrintMessage(GraphFITSQTD, MESSAGE_CRITICAL, trUtf8("A problem occured while attempting to represent file %1.") .arg(GraphFITSQTD->GetFileNameQFLS()->GetFileName()));
      
      return;
    }
  
  FITSContainer * NewFITSContainer = new FITSContainer;
  NewFITSContainer->SetData(NewFITSImage);
  
  NewFITSContainer->SetPalette(OptionDataFITSPalette);
  NewFITSContainer->SetMinDyn (OptionDataFITSMinDynFlag, double(OptionDataFITSMinDynValue));
  NewFITSContainer->SetMaxDyn (OptionDataFITSMaxDynFlag, double(OptionDataFITSMaxDynValue));
  
  if(!OptionDataFITSMinDynFlag && !OptionDataFITSMaxDynFlag)
    {
      SettingsDataQTD->GetMinDynQSD()->setValue(int(NewFITSImage.GetMinDyn()));
      SettingsDataQTD->GetMaxDynQSD()->setValue(int(NewFITSImage.GetMaxDyn()));
    }
  
  if(
     TopFrame->IsEmpty()
     ||
     (!TopFrame->IsEmpty() && !GraphFITSQTD->GetSameQCKB()->isChecked())
     )
    {
      CreateNewGraph();
      
      Graph * ActiveGraph = TopFrame->GetActivePlot();
      ActiveGraph->AddData(NewFITSContainer);
      
      ActiveGraph->show();
    }
  else
    {
      Graph * ActiveGraph = TopFrame->GetActivePlot();
      ActiveGraph->AddData(NewFITSContainer);
      
      ActiveGraph->ForceRepaint();
      ActiveGraph->repaint();
    }
  
  PrintStatus(trUtf8("Represented %1 (%2*%3 square pixels).") .arg(GraphFITSQTD->GetFileNameQFLS()->GetFileName()) .arg(NewFITSImage.GetXSize()) .arg(NewFITSImage.GetYSize()));
}

void GUI::GraphBoundsSlot()
{
  GraphBoundsQTD->show ();
  GraphBoundsQTD->raise();
}

void GUI::GraphBoundsApplySlot()
{
  GraphBoundsNewFlag = GraphBoundsQTD->GetNewQCKB()->isChecked();
  
  if(TopFrame == NULL)
    {
      PrintMessage(GraphBoundsQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  if(TopFrame->IsEmpty())
    {
      PrintMessage(GraphBoundsQTD, MESSAGE_CRITICAL, trUtf8("There's no available graph."));
      
      return;
    }
  
  GraphBoundsSettings();
  
  TopFrame->GetActivePlot()->ForceRepaint();
  TopFrame->GetActivePlot()->repaint();
  
  PrintStatus(trUtf8("Updated the graph's bounds."));
}

void GUI::GraphAxesSlot()
{
  GraphAxesQTD->show();
  GraphAxesQTD->raise();
}

void GUI::GraphAxesApplySlot()
{
  GraphAxesNewFlag = GraphAxesQTD->GetNewQCKB()->isChecked();
  
  if(TopFrame == NULL)
    {
      PrintMessage(GraphAxesQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  if(TopFrame->IsEmpty())
    {
      PrintMessage(GraphAxesQTD, MESSAGE_CRITICAL, trUtf8("There's no available graph."));
      
      return;
    }
  
  GraphAxesSettings();
  
  TopFrame->GetActivePlot()->ForceRepaint();
  TopFrame->GetActivePlot()->repaint();
  
  PrintStatus(trUtf8("Updated the graph's axes."));
}

void GUI::GraphFitSlot()
{
  GraphFitQTD->show();
  GraphFitQTD->raise();
}

void GUI::GraphFitApplySlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(GraphFitQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  if(TopFrame->IsEmpty())
    {
      PrintMessage(GraphFitQTD, MESSAGE_CRITICAL, trUtf8("There's no available graph."));
      
      return;
    }
  
  Graph * ActiveGraph = TopFrame->GetActivePlot();
  
  Histo1D * ActiveHisto1D = dynamic_cast<Histo1D *>(ActiveGraph->GetData(0));
  Graph2D * ActiveGraph2D = dynamic_cast<Graph2D *>(ActiveGraph->GetData(0));
  
  if(!ActiveHisto1D && !ActiveGraph2D)
    {
      PrintMessage(GraphFitQTD, MESSAGE_CRITICAL, trUtf8("The active graph is not fittable."));
      
      return;
    }
  
  GraphFitQTD->SetCurrentTab(0);
  
  bool    ProblemFlag = false;
  QString Message     = "";
  Fit     NewFit;
  
  if(ActiveHisto1D)
    NewFit.SetData(ActiveHisto1D->GetData().GetDataArray());
  else
    NewFit.SetData(ActiveGraph2D->GetData());
  
  int Degree = GraphFitQTD->GetDegreeQCB()->currentItem();
  
  if(!GraphFitQTD->GetNumericalQCKB()->isChecked())
    {
      NewFit.PolynomialFit(Degree);
      
      if(Degree == 0)
	Message = trUtf8("p<sub>0</sub> = %1") .arg(NewFit.GetParam(0));
      else if(Degree == 1)
	Message = trUtf8("p<sub>0</sub> = %1<br />p<sub>1</sub> = %2") .arg(NewFit.GetParam(0)) .arg(NewFit.GetParam(1));
      else
	Message = trUtf8("p<sub>0</sub> = %1<br />p<sub>1</sub> = %2<br />p<sub>2</sub> = %3") .arg(NewFit.GetParam(0)) .arg(NewFit.GetParam(1)) .arg(NewFit.GetParam(2));
    }
  else
    {
      if(GraphFitQTD->GetParameter0QLE()->text() == "")
	{
	  PrintMessage(GraphFitQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the initial value of p<sub>0</sub>."));
	  
	  return;
	}
      
      if(GraphFitQTD->GetParameter1QLE()->text() == "")
	{
	  PrintMessage(GraphFitQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the initial value of p<sub>1</sub>."));
	  
	  return;
	}
      
      if(GraphFitQTD->GetParameter2QLE()->text() == "")
	{
	  PrintMessage(GraphFitQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the initial value of p<sub>2</sub>."));
	  
	  return;
	}
      
      if(GraphFitQTD->GetParameter3QLE()->text() == "")
	{
	  PrintMessage(GraphFitQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the initial value of p<sub>3</sub>."));
	  
	  return;
	}
      
      if(GraphFitQTD->GetMaxStepQLE()->text() == "")
	{
	  PrintMessage(GraphFitQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the maximal iterations number."));
	  
	  return;
	}
      
      if(GraphFitQTD->GetChi2LimQLE()->text() == "")
	{
	  PrintMessage(GraphFitQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the &chi;<sup>2</sup> convergence criterion."));
	  
	  return;
	}
      
      vector <double> Params;
      Params.push_back(GraphFitQTD->GetParameter0QLE()->text().toDouble());
      Params.push_back(GraphFitQTD->GetParameter1QLE()->text().toDouble());
      Params.push_back(GraphFitQTD->GetParameter2QLE()->text().toDouble());
      Params.push_back(GraphFitQTD->GetParameter3QLE()->text().toDouble());
      
      NewFit.SetParams(Params);
      NewFit.SetMaxStep(GraphFitQTD->GetMaxStepQLE()->text().toInt());
      NewFit.SetChi2Lim(GraphFitQTD->GetChi2LimQLE()->text().toDouble());
      NewFit.FIT();
      
      Message = trUtf8("p<sub>0</sub> = (%1)&plusmn;(%2)<br />p<sub>1</sub> = (%3)&plusmn;(%4)<br />p<sub>2</sub> = (%5)&plusmn;(%6)<br />p<sub>3</sub> = (%7)&plusmn;(%8)<br />") .arg(NewFit.GetParam(0)) .arg(NewFit.GetErrParam(0)) .arg(NewFit.GetParam(1)) .arg(NewFit.GetErrParam(1)) .arg(NewFit.GetParam(2)) .arg(NewFit.GetErrParam(2)) .arg(NewFit.GetParam(3)) .arg(NewFit.GetErrParam(3));
      
      Message += trUtf8("&chi;<sup>2</sup> = %1<br />Iteration = %2<br />Number of degrees of freedom = %3") .arg(NewFit.GetChi2()) .arg(NewFit.GetStep()) .arg(NewFit.GetNDF());/* Need to split the string because QString::arg() does not accept more than 9 arguments */
    }
  
  if(NewFit.GetNDF() < 0)
    {
      ProblemFlag = true;
      
      Message = trUtf8("The number of degrees of freedom (%1) is negative.") .arg(NewFit.GetNDF());
    }
  
  if(NewFit.NoSolution())
    {
      ProblemFlag = true;
      
      Message = trUtf8("There is no fitting solution.");
    }
  
  if(NewFit.NoConvergence())
    {
      ProblemFlag = true;
      
      Message += trUtf8("<br />The fit did not converge.");
    }
  
  if(
     GraphFitQTD->GetPlotQCKB()->isChecked()
     &&
     !NewFit.NoSolution()
     &&
     NewFit.GetNDF() >= 0
     &&
     (!NewFit.NoConvergence() || (NewFit.NoConvergence() && GraphFitQTD->GetPlotNoConvQCKB()->isChecked()))
     )
    {
      BExpression * NewExpression = new BExpression;
      NewExpression->setVariable(trUtf8("x", "Variable"));
      
      if(!GraphFitQTD->GetNumericalQCKB()->isChecked())
	{
	  if(Degree == 0)
	    NewExpression->setExpression(trUtf8("%1", "Fit expression") .arg(NewFit.GetParam(0)));
	  else if(Degree == 1)
	    NewExpression->setExpression(trUtf8("%1*x+(%2)", "Fit expression") .arg(NewFit.GetParam(0)) .arg(NewFit.GetParam(1)));
	  else
	    NewExpression->setExpression(trUtf8("(%1)*x^2+(%2)*x+(%3)", "Fit expression") .arg(NewFit.GetParam(0)) .arg(NewFit.GetParam(1)) .arg(NewFit.GetParam(2)));
	}
      else
	NewExpression->setExpression(trUtf8("(%1)*x^3+(%2)*x^2+(%3)*x+(%4)", "Fit expression") .arg(NewFit.GetParam(0)) .arg(NewFit.GetParam(1)) .arg(NewFit.GetParam(2)) .arg(NewFit.GetParam(3)));
      
      Func1D * NewFunc1D = new Func1D;
      NewFunc1D->SetData(NewExpression);
      NewFunc1D->SetXMinXMax(ActiveGraph->GetXInfLimit(), ActiveGraph->GetXSupLimit());/* Specific */
      
      NewFunc1D->SetLineWidth(OptionDataFunctionLineWidth);
      NewFunc1D->SetLineColor(OptionDataFunctionLineColor);
      NewFunc1D->SetLineStyle(OptionDataFunctionLineStyle);
      
      if(!GraphFitQTD->GetSameQCKB()->isChecked())
	{
	  CreateNewGraph();
	  
	  ActiveGraph = TopFrame->GetActivePlot();
	  ActiveGraph->AddData(NewFunc1D);
	  
	  ActiveGraph->show();
	}
      else
	{
	  ActiveGraph->AddData(NewFunc1D);
	  
	  ActiveGraph->ForceRepaint();
	  ActiveGraph->repaint();
	}
      
      PrintStatus(trUtf8("Represented %1 between %2 and %3.") .arg(NewExpression->expression()) .arg(ActiveGraph->GetXInfLimit()) .arg(ActiveGraph->GetXSupLimit()));
    }
  
  if(ProblemFlag)
    PrintMessage(GraphFitQTD, MESSAGE_CRITICAL   , Message, trUtf8("Fit result"), GraphFitQTD->GetOutputQTE());
  else
    PrintMessage(GraphFitQTD, MESSAGE_INFORMATION, Message, trUtf8("Fit result"), GraphFitQTD->GetOutputQTE());
}

void GUI::GraphCloseSlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(this, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  if(TopFrame->IsEmpty())
    {
      PrintMessage(this, MESSAGE_CRITICAL, trUtf8("There's no available graph."));
      
      return;
    }
  
  TopFrame->DeleteActivePlot();
  
  TopFrame->ForceRepaint();
  TopFrame->repaint();
  
  DisableGraphRelatedMenus();
  
  PrintStatus(trUtf8("Graph closed."));
}

void GUI::ComputationVariablesSlot()
{
  ComputationVariablesQTD->show();
  ComputationVariablesQTD->raise();
}

void GUI::ComputationVariablesApplySlot()
{
  VariablesList = ComputationVariablesQTD->GetVariables();
  
  PrintStatus(trUtf8("%1 defined variables.") .arg(VariablesList.size()));
}

void GUI::ComputationVariablesHelpSlot()
{
  HelpContentsBHB->ShowHelpPage("computation");
}

void GUI::ComputationComputeSlot()
{
  ComputationComputeQTD->show();
  ComputationComputeQTD->raise();
}

void GUI::ComputationComputeApplySlot()
{
  if(ComputationComputeQTD->GetExpressionQLE()->text() == "")
    {
      PrintMessage(ComputationComputeQTD, MESSAGE_CRITICAL, trUtf8("Please indicate the expression."));
      
      return;
    }
  
  BExpression NewExpression;
  NewExpression.setVariables (VariablesList                                    );
  NewExpression.setExpression(ComputationComputeQTD->GetExpressionQLE()->text());
  
  if(ComputationComputeQTD->GetSimplifyQRB()->isChecked())
    {
      if(NewExpression.simplify())
	PrintMessage(ComputationComputeQTD, MESSAGE_INFORMATION, trUtf8("Expression simplification: %1") .arg(NewExpression.expression())                                                                                                                   , NULL, ComputationComputeQTD->GetOutputQTE());
      else
	PrintMessage(ComputationComputeQTD, MESSAGE_CRITICAL   , trUtf8("Error in expression: <font color=%1>%2</font><br />Intermediate result: %3") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression.error()) .arg(NewExpression.expression()), NULL, ComputationComputeQTD->GetOutputQTE());
    }
  
  else if(ComputationComputeQTD->GetEvaluateQRB()->isChecked())
    {
      if(NewExpression.evaluate())
	PrintMessage(ComputationComputeQTD, MESSAGE_INFORMATION, trUtf8("Expression evaluation: %1") .arg(NewExpression.value())                                                                , NULL, ComputationComputeQTD->GetOutputQTE());
      else
	PrintMessage(ComputationComputeQTD, MESSAGE_CRITICAL   , trUtf8("Error in expression: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression.error()), NULL, ComputationComputeQTD->GetOutputQTE());
    }
  
  else if(ComputationComputeQTD->GetDeriveQRB()->isChecked())
    {
      if(NewExpression.derive())
	PrintMessage(ComputationComputeQTD, MESSAGE_INFORMATION, trUtf8("Expression derivation: %1") .arg(NewExpression.expression())                                                                                                                       , NULL, ComputationComputeQTD->GetOutputQTE());
      else
	PrintMessage(ComputationComputeQTD, MESSAGE_CRITICAL   , trUtf8("Error in expression: <font color=%1>%2</font><br />Intermediate result: %3") .arg(OptionGlobalExternalErrorColor.name()) .arg(NewExpression.error()) .arg(NewExpression.expression()), NULL, ComputationComputeQTD->GetOutputQTE());
    }
}

void GUI::ComputationComputeHelpSlot()
{
  HelpContentsBHB->ShowHelpPage("functions");
}

void GUI::TextsFrameSlot()
{
  TextsFrameQTD->show();
  TextsFrameQTD->raise();
}

void GUI::TextsFrameApplySlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(TextsFrameQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  TopFrame->SetTitle(TextsFrameQTD->GetTitleQLE()->text());
  
  TopFrame->ForceRepaint();
  TopFrame->repaint();
  
  PrintStatus(trUtf8("Updated the frame's texts."));
}

void GUI::TextsGraphSlot()
{
  TextsGraphQTD->show();
  TextsGraphQTD->raise();
}

void GUI::TextsGraphApplySlot()
{
  if(TopFrame == NULL)
    {
      PrintMessage(TextsGraphQTD, MESSAGE_CRITICAL, trUtf8("There's no available frame."));
      
      return;
    }
  
  if(TopFrame->IsEmpty())
    {
      PrintMessage(TextsGraphQTD, MESSAGE_CRITICAL, trUtf8("There's no available graph."));
      
      return;
    }
  
  TopFrame->GetActivePlot()->SetTitle(TextsGraphQTD->GetTitleQLE()->text());
  TopFrame->GetActivePlot()->SetXAxisLabel(TextsGraphQTD->GetXAxisLabelQLE()->text());
  TopFrame->GetActivePlot()->SetYAxisLabel(TextsGraphQTD->GetYAxisLabelQLE()->text());
  TopFrame->GetActivePlot()->SetXTicksLabels(TextsGraphQTD->GetXTicksLabelsFlagQCKB()->isChecked());
  TopFrame->GetActivePlot()->SetYTicksLabels(TextsGraphQTD->GetYTicksLabelsFlagQCKB()->isChecked());
  
  TopFrame->GetActivePlot()->ForceRepaint();
  TopFrame->GetActivePlot()->repaint();
  
  PrintStatus(trUtf8("Updated the graph's texts."));
}

void GUI::SettingsGlobalSlot()
{
  SettingsGlobalQTD->show();
  SettingsGlobalQTD->raise();
}

void GUI::SettingsGlobalApplySlot()
{
  /* R2L */
  OptionGlobalTerminalHistorySize      = SettingsGlobalQTD->GetHistorySizeQLE       ()->text    ().toInt();
  OptionGlobalTerminalTextColor        = SettingsGlobalQTD->GetTextColorQPBCS       ()->GetColor();
  OptionGlobalTerminalInformationColor = SettingsGlobalQTD->GetInformationColorQPBCS()->GetColor();
  OptionGlobalTerminalWarningColor     = SettingsGlobalQTD->GetWarningColorQPBCS    ()->GetColor();
  OptionGlobalTerminalCriticalColor    = SettingsGlobalQTD->GetCriticalColorQPBCS   ()->GetColor();
  OptionGlobalTerminalStatusColor      = SettingsGlobalQTD->GetStatusColorQPBCS     ()->GetColor();
  OptionGlobalTerminalBGColor          = SettingsGlobalQTD->GetBGColorQPBCS         ()->GetColor();
  OptionGlobalTerminalTextFont         = SettingsGlobalQTD->GetTextFontQFS          ()->GetFont ();
  
  ApplyGlobalDefaultSettings();
  
  PrintStatus(trUtf8("Applied the new global settings."));
}

void GUI::SettingsGlobalDefaultsSlot()
{
  if(PrintMessage(SettingsGlobalQTD, MESSAGE_WARNING, trUtf8("This will unset the current global settings and set the default ones as current.<br />Are you sure you want to continue?"), trUtf8("Revert to defaults?")) == QMessageBox::No)
    {
      PrintStatus(trUtf8("Back to the default global settings canceled."));
      
      return;
    }
  
  SetGlobalDefaultSettings();
  
  SettingsGlobalCancelSlot();
  
  PrintStatus(trUtf8("Back to the default global settings."));
}

void GUI::SettingsGlobalCancelSlot()
{
  /* R2L */
  SettingsGlobalQTD->GetHistorySizeQLE()       ->setText (QString("%1") .arg(OptionGlobalTerminalHistorySize));
  SettingsGlobalQTD->GetTextColorQPBCS()       ->SetColor(OptionGlobalTerminalTextColor                      );
  SettingsGlobalQTD->GetInformationColorQPBCS()->SetColor(OptionGlobalTerminalInformationColor               );
  SettingsGlobalQTD->GetWarningColorQPBCS()    ->SetColor(OptionGlobalTerminalWarningColor                   );
  SettingsGlobalQTD->GetCriticalColorQPBCS()   ->SetColor(OptionGlobalTerminalCriticalColor                  );
  SettingsGlobalQTD->GetStatusColorQPBCS()     ->SetColor(OptionGlobalTerminalStatusColor                    );
  SettingsGlobalQTD->GetBGColorQPBCS()         ->SetColor(OptionGlobalTerminalBGColor                        );
  SettingsGlobalQTD->GetTextFontQFS()          ->SetFont (OptionGlobalTerminalTextFont                       );
}

void GUI::SettingsFrameSlot()
{
  SettingsFrameQTD->show();
  SettingsFrameQTD->raise();
}

void GUI::SettingsFrameApplySlot()
{
  OptionFrameBoxLineWidth   = SettingsFrameQTD->GetBoxLineWidthQLC()->GetCurrentWidth();
  OptionFrameHSepLineWidth  = SettingsFrameQTD->GetHSepLineWidthQLC()->GetCurrentWidth();
  OptionFrameVSepLineWidth  = SettingsFrameQTD->GetVSepLineWidthQLC()->GetCurrentWidth();
  OptionFrameXRatio         = SettingsFrameQTD->GetXRatioQLE()->text().toDouble();
  OptionFrameYRatio         = SettingsFrameQTD->GetYRatioQLE()->text().toDouble();
  OptionFrameWidthRatio     = SettingsFrameQTD->GetWidthRatioQLE()->text().toDouble();
  OptionFrameHeightRatio    = SettingsFrameQTD->GetHeightRatioQLE()->text().toDouble();
  OptionFrameBox2TitleRatio = SettingsFrameQTD->GetBox2TitleRatioQLE()->text().toDouble();
  OptionFrameBGColor        = SettingsFrameQTD->GetBGColorQPBCS()->GetColor();
  OptionFrameTitleColor     = SettingsFrameQTD->GetTitleColorQPBCS()->GetColor();
  OptionFrameBoxColor       = SettingsFrameQTD->GetBoxColorQPBCS()->GetColor();
  OptionFrameHSepColor      = SettingsFrameQTD->GetHSepColorQPBCS()->GetColor();
  OptionFrameVSepColor      = SettingsFrameQTD->GetVSepColorQPBCS()->GetColor();
  OptionFrameTitleFont      = SettingsFrameQTD->GetTitleFontQFS()->GetFont();
  OptionFrameBoxLineStyle   = SettingsFrameQTD->GetBoxLineStyleQPC()->GetCurrentStyle();
  OptionFrameHSepLineStyle  = SettingsFrameQTD->GetHSepLineStyleQPC()->GetCurrentStyle();
  OptionFrameVSepLineStyle  = SettingsFrameQTD->GetVSepLineStyleQPC()->GetCurrentStyle();
  
  if(TopFrame)
    {
      ApplyFrameDefaultSettings();
      
      TopFrame->ForceRepaint();
      TopFrame->repaint();
    }
  
  PrintStatus(trUtf8("Applied the new frame settings."));
}

void GUI::SettingsFrameDefaultsSlot()
{
  if(PrintMessage(SettingsFrameQTD, MESSAGE_WARNING, trUtf8("This will unset the current frame settings and set the default ones as current.<br />Are you sure you want to continue?"), trUtf8("Revert to defaults?")) == QMessageBox::No)
    {
      PrintStatus(trUtf8("Back to the default frame settings canceled."));
      
      return;
    }
  
  SetFrameDefaultSettings();
  
  SettingsFrameCancelSlot();
  
  PrintStatus(trUtf8("Back to the default frame settings."));
}

void GUI::SettingsFrameCancelSlot()
{
  SettingsFrameQTD->GetBoxLineWidthQLC()->SetCurrentWidth(OptionFrameBoxLineWidth);
  SettingsFrameQTD->GetHSepLineWidthQLC()->SetCurrentWidth(OptionFrameHSepLineWidth);
  SettingsFrameQTD->GetVSepLineWidthQLC()->SetCurrentWidth(OptionFrameVSepLineWidth);
  SettingsFrameQTD->GetXRatioQLE()->setText(QString("%1") .arg(OptionFrameXRatio));
  SettingsFrameQTD->GetYRatioQLE()->setText(QString("%1") .arg(OptionFrameYRatio));
  SettingsFrameQTD->GetWidthRatioQLE()->setText(QString("%1") .arg(OptionFrameWidthRatio));
  SettingsFrameQTD->GetHeightRatioQLE()->setText(QString("%1") .arg(OptionFrameHeightRatio));
  SettingsFrameQTD->GetBox2TitleRatioQLE()->setText(QString("%1") .arg(OptionFrameBox2TitleRatio));
  SettingsFrameQTD->GetBGColorQPBCS()->SetColor(OptionFrameBGColor);
  SettingsFrameQTD->GetTitleColorQPBCS()->SetColor(OptionFrameTitleColor);
  SettingsFrameQTD->GetBoxColorQPBCS()->SetColor(OptionFrameBoxColor);
  SettingsFrameQTD->GetHSepColorQPBCS()->SetColor(OptionFrameHSepColor);
  SettingsFrameQTD->GetVSepColorQPBCS()->SetColor(OptionFrameVSepColor);
  SettingsFrameQTD->GetTitleFontQFS()->SetFont(OptionFrameTitleFont);
  SettingsFrameQTD->GetBoxLineStyleQPC()->SetCurrentStyle(OptionFrameBoxLineStyle);
  SettingsFrameQTD->GetHSepLineStyleQPC()->SetCurrentStyle(OptionFrameHSepLineStyle);
  SettingsFrameQTD->GetVSepLineStyleQPC()->SetCurrentStyle(OptionFrameVSepLineStyle);
}

void GUI::SettingsGraphSlot()
{
  SettingsGraphQTD->show();
  SettingsGraphQTD->raise();
}

void GUI::SettingsGraphApplySlot()
{
  OptionGraphBoxLineWidth           = SettingsGraphQTD->GetBoxLineWidthQLC()->GetCurrentWidth();
  OptionGraphBigXTicksLineWidth     = SettingsGraphQTD->GetBigXTicksLineWidthQLC()->GetCurrentWidth();
  OptionGraphBigYTicksLineWidth     = SettingsGraphQTD->GetBigYTicksLineWidthQLC()->GetCurrentWidth();
  OptionGraphSmallXTicksLineWidth   = SettingsGraphQTD->GetSmallXTicksLineWidthQLC()->GetCurrentWidth();
  OptionGraphSmallYTicksLineWidth   = SettingsGraphQTD->GetSmallYTicksLineWidthQLC()->GetCurrentWidth();
  OptionGraphXGridLineWidth         = SettingsGraphQTD->GetXGridLineWidthQLC()->GetCurrentWidth();
  OptionGraphYGridLineWidth         = SettingsGraphQTD->GetYGridLineWidthQLC()->GetCurrentWidth();
  OptionGraphXRatio                 = SettingsGraphQTD->GetXRatioQLE()->text().toDouble();
  OptionGraphYRatio                 = SettingsGraphQTD->GetYRatioQLE()->text().toDouble();
  OptionGraphWidthRatio             = SettingsGraphQTD->GetWidthRatioQLE()->text().toDouble();
  OptionGraphHeightRatio            = SettingsGraphQTD->GetHeightRatioQLE()->text().toDouble();
  OptionGraphAxis2TitleRatio        = SettingsGraphQTD->GetAxis2TitleRatioQLE()->text().toDouble();
  OptionGraphBigXTicksRatio         = SettingsGraphQTD->GetBigXTicksRatioQLE()->text().toDouble();
  OptionGraphBigYTicksRatio         = SettingsGraphQTD->GetBigYTicksRatioQLE()->text().toDouble();
  OptionGraphSmallXTicksRatio       = SettingsGraphQTD->GetSmallXTicksRatioQLE()->text().toDouble();
  OptionGraphSmallYTicksRatio       = SettingsGraphQTD->GetSmallYTicksRatioQLE()->text().toDouble();
  OptionGraphXAxis2TicksLabelsRatio = SettingsGraphQTD->GetXAxis2TicksLabelsRatioQLE()->text().toDouble();
  OptionGraphYAxis2TicksLabelsRatio = SettingsGraphQTD->GetYAxis2TicksLabelsRatioQLE()->text().toDouble();
  OptionGraphXAxis2AxisLabelRatio   = SettingsGraphQTD->GetXAxis2AxisLabelRatioQLE()->text().toDouble();
  OptionGraphYAxis2AxisLabelRatio   = SettingsGraphQTD->GetYAxis2AxisLabelRatioQLE()->text().toDouble();
  OptionGraphBoxColor               = SettingsGraphQTD->GetBoxColorQPBCS()->GetColor();
  OptionGraphBGColor                = SettingsGraphQTD->GetBGColorQPBCS()->GetColor();
  OptionGraphBigXTicksColor         = SettingsGraphQTD->GetBigXTicksColorQPBCS()->GetColor();
  OptionGraphBigYTicksColor         = SettingsGraphQTD->GetBigYTicksColorQPBCS()->GetColor();
  OptionGraphSmallXTicksColor       = SettingsGraphQTD->GetSmallXTicksColorQPBCS()->GetColor();
  OptionGraphSmallYTicksColor       = SettingsGraphQTD->GetSmallYTicksColorQPBCS()->GetColor();
  OptionGraphXTicksLabelsColor      = SettingsGraphQTD->GetXTicksLabelsColorQPBCS()->GetColor();
  OptionGraphYTicksLabelsColor      = SettingsGraphQTD->GetYTicksLabelsColorQPBCS()->GetColor();
  OptionGraphXAxisLabelColor        = SettingsGraphQTD->GetXAxisLabelColorQPBCS()->GetColor();
  OptionGraphYAxisLabelColor        = SettingsGraphQTD->GetYAxisLabelColorQPBCS()->GetColor();
  OptionGraphTitleColor             = SettingsGraphQTD->GetTitleColorQPBCS()->GetColor();
  OptionGraphXGridColor             = SettingsGraphQTD->GetXGridColorQPBCS()->GetColor();
  OptionGraphYGridColor             = SettingsGraphQTD->GetYGridColorQPBCS()->GetColor();
  OptionGraphXAxisLabelFont         = SettingsGraphQTD->GetXAxisLabelFontQFS()->GetFont();
  OptionGraphYAxisLabelFont         = SettingsGraphQTD->GetYAxisLabelFontQFS()->GetFont();
  OptionGraphXTicksLabelsFont       = SettingsGraphQTD->GetXTicksLabelsFontQFS()->GetFont();
  OptionGraphYTicksLabelsFont       = SettingsGraphQTD->GetYTicksLabelsFontQFS()->GetFont();
  OptionGraphTitleFont              = SettingsGraphQTD->GetTitleFontQFS()->GetFont();
  OptionGraphBoxLineStyle           = SettingsGraphQTD->GetBoxLineStyleQPC()->GetCurrentStyle();
  OptionGraphBigXTicksLineStyle     = SettingsGraphQTD->GetBigXTicksLineStyleQPC()->GetCurrentStyle();
  OptionGraphBigYTicksLineStyle     = SettingsGraphQTD->GetBigYTicksLineStyleQPC()->GetCurrentStyle();
  OptionGraphSmallXTicksLineStyle   = SettingsGraphQTD->GetSmallXTicksLineStyleQPC()->GetCurrentStyle();
  OptionGraphSmallYTicksLineStyle   = SettingsGraphQTD->GetSmallYTicksLineStyleQPC()->GetCurrentStyle();
  OptionGraphXGridLineStyle         = SettingsGraphQTD->GetXGridLineStyleQPC()->GetCurrentStyle();
  OptionGraphYGridLineStyle         = SettingsGraphQTD->GetYGridLineStyleQPC()->GetCurrentStyle();
  
  if(TopFrame)
    if(!TopFrame->IsEmpty())
      {
	ApplyGraphDefaultSettings();
	
	TopFrame->GetActivePlot()->ForceRepaint();
	TopFrame->GetActivePlot()->repaint();
      }
  
  PrintStatus(trUtf8("Applied the new graph settings."));
}

void GUI::SettingsGraphDefaultsSlot()
{
  if(PrintMessage(SettingsGraphQTD, MESSAGE_WARNING, trUtf8("This will unset the current graph settings and set the default ones as current.<br />Are you sure you want to continue?"), trUtf8("Revert to defaults?")) == QMessageBox::No)
    {
      PrintStatus(trUtf8("Back to the default graph settings canceled."));
      
      return;
    }
  
  SetGraphDefaultSettings();
  
  SettingsGraphCancelSlot();
  
  PrintStatus(trUtf8("Back to the default graph settings."));
}

void GUI::SettingsGraphCancelSlot()
{
  SettingsGraphQTD->GetBoxLineWidthQLC()->SetCurrentWidth(OptionGraphBoxLineWidth);
  SettingsGraphQTD->GetBigXTicksLineWidthQLC()->SetCurrentWidth(OptionGraphBigXTicksLineWidth);
  SettingsGraphQTD->GetBigYTicksLineWidthQLC()->SetCurrentWidth(OptionGraphBigYTicksLineWidth);
  SettingsGraphQTD->GetSmallXTicksLineWidthQLC()->SetCurrentWidth(OptionGraphSmallXTicksLineWidth);
  SettingsGraphQTD->GetSmallYTicksLineWidthQLC()->SetCurrentWidth(OptionGraphSmallYTicksLineWidth);
  SettingsGraphQTD->GetXGridLineWidthQLC()->SetCurrentWidth(OptionGraphXGridLineWidth);
  SettingsGraphQTD->GetYGridLineWidthQLC()->SetCurrentWidth(OptionGraphYGridLineWidth);
  SettingsGraphQTD->GetXRatioQLE()->setText(QString("%1") .arg(OptionGraphXRatio));
  SettingsGraphQTD->GetYRatioQLE()->setText(QString("%1") .arg(OptionGraphYRatio));
  SettingsGraphQTD->GetWidthRatioQLE()->setText(QString("%1") .arg(OptionGraphWidthRatio));
  SettingsGraphQTD->GetHeightRatioQLE()->setText(QString("%1") .arg(OptionGraphHeightRatio));
  SettingsGraphQTD->GetAxis2TitleRatioQLE()->setText(QString("%1") .arg(OptionGraphAxis2TitleRatio));
  SettingsGraphQTD->GetBigXTicksRatioQLE()->setText(QString("%1") .arg(OptionGraphBigXTicksRatio));
  SettingsGraphQTD->GetBigYTicksRatioQLE()->setText(QString("%1") .arg(OptionGraphBigYTicksRatio));
  SettingsGraphQTD->GetSmallXTicksRatioQLE()->setText(QString("%1") .arg(OptionGraphSmallXTicksRatio));
  SettingsGraphQTD->GetSmallYTicksRatioQLE()->setText(QString("%1") .arg(OptionGraphSmallYTicksRatio));
  SettingsGraphQTD->GetXAxis2TicksLabelsRatioQLE()->setText(QString("%1") .arg(OptionGraphXAxis2TicksLabelsRatio));
  SettingsGraphQTD->GetYAxis2TicksLabelsRatioQLE()->setText(QString("%1") .arg(OptionGraphYAxis2TicksLabelsRatio));
  SettingsGraphQTD->GetXAxis2AxisLabelRatioQLE()->setText(QString("%1") .arg(OptionGraphXAxis2AxisLabelRatio));
  SettingsGraphQTD->GetYAxis2AxisLabelRatioQLE()->setText(QString("%1") .arg(OptionGraphYAxis2AxisLabelRatio));
  SettingsGraphQTD->GetBoxColorQPBCS()->SetColor(OptionGraphBoxColor);
  SettingsGraphQTD->GetBGColorQPBCS()->SetColor(OptionGraphBGColor);
  SettingsGraphQTD->GetBigXTicksColorQPBCS()->SetColor(OptionGraphBigXTicksColor);
  SettingsGraphQTD->GetBigYTicksColorQPBCS()->SetColor(OptionGraphBigYTicksColor);
  SettingsGraphQTD->GetSmallXTicksColorQPBCS()->SetColor(OptionGraphSmallXTicksColor);
  SettingsGraphQTD->GetSmallYTicksColorQPBCS()->SetColor(OptionGraphSmallYTicksColor);
  SettingsGraphQTD->GetXTicksLabelsColorQPBCS()->SetColor(OptionGraphXTicksLabelsColor);
  SettingsGraphQTD->GetYTicksLabelsColorQPBCS()->SetColor(OptionGraphYTicksLabelsColor);
  SettingsGraphQTD->GetXAxisLabelColorQPBCS()->SetColor(OptionGraphXAxisLabelColor);
  SettingsGraphQTD->GetYAxisLabelColorQPBCS()->SetColor(OptionGraphYAxisLabelColor);
  SettingsGraphQTD->GetTitleColorQPBCS()->SetColor(OptionGraphTitleColor);
  SettingsGraphQTD->GetXGridColorQPBCS()->SetColor(OptionGraphXGridColor);
  SettingsGraphQTD->GetYGridColorQPBCS()->SetColor(OptionGraphYGridColor);
  SettingsGraphQTD->GetXAxisLabelFontQFS()->SetFont(OptionGraphXAxisLabelFont);
  SettingsGraphQTD->GetYAxisLabelFontQFS()->SetFont(OptionGraphYAxisLabelFont);
  SettingsGraphQTD->GetXTicksLabelsFontQFS()->SetFont(OptionGraphXTicksLabelsFont);
  SettingsGraphQTD->GetYTicksLabelsFontQFS()->SetFont(OptionGraphYTicksLabelsFont);
  SettingsGraphQTD->GetTitleFontQFS()->SetFont(OptionGraphTitleFont);
  SettingsGraphQTD->GetBoxLineStyleQPC()->SetCurrentStyle(OptionGraphBoxLineStyle);
  SettingsGraphQTD->GetBigXTicksLineStyleQPC()->SetCurrentStyle(OptionGraphBigXTicksLineStyle);
  SettingsGraphQTD->GetBigYTicksLineStyleQPC()->SetCurrentStyle(OptionGraphBigYTicksLineStyle);
  SettingsGraphQTD->GetSmallXTicksLineStyleQPC()->SetCurrentStyle(OptionGraphSmallXTicksLineStyle);
  SettingsGraphQTD->GetSmallYTicksLineStyleQPC()->SetCurrentStyle(OptionGraphSmallYTicksLineStyle);
  SettingsGraphQTD->GetXGridLineStyleQPC()->SetCurrentStyle(OptionGraphXGridLineStyle);
  SettingsGraphQTD->GetYGridLineStyleQPC()->SetCurrentStyle(OptionGraphYGridLineStyle);
}

void GUI::SettingsDataSlot()
{
  if(SettingsDataCurrentTabID != SettingsDataQTD->GetCurrentTabID())
    SettingsDataQTD->SetCurrentTab(SettingsDataCurrentTabID);
  
  SettingsDataQTD->show();
  SettingsDataQTD->raise();
}

void GUI::SettingsDataApplySlot()
{
  OptionDataFunctionLineWidth    = SettingsDataQTD->GetFunctionLineWidthQLC()->GetCurrentWidth();
  OptionDataFunctionLineColor    = SettingsDataQTD->GetFunctionLineColorQPBCS()->GetColor();
  OptionDataFunctionLineStyle    = SettingsDataQTD->GetFunctionLineStyleQPC()->GetCurrentStyle();
  OptionDataHistogramLineWidth   = SettingsDataQTD->GetHistogramLineWidthQLC()->GetCurrentWidth();
  OptionDataHistogramLineColor   = SettingsDataQTD->GetHistogramLineColorQPBCS()->GetColor();
  OptionDataHistogramFillColor   = SettingsDataQTD->GetHistogramFillColorQPBCS()->GetColor();
  OptionDataHistogramLineStyle   = SettingsDataQTD->GetHistogramLineStyleQPC()->GetCurrentStyle();
  OptionDataHistogramFillStyle   = SettingsDataQTD->GetHistogramFillStyleQBC()->GetCurrentStyle();
  OptionDataGraphShapeType       = SettingsDataQTD->GetDataTypeQCB()->currentItem();
  OptionDataGraphErrorsLineWidth = SettingsDataQTD->GetErrorsLineWidthQLC()->GetCurrentWidth();
  OptionDataGraphShapeRatio      = SettingsDataQTD->GetDataRatioQLE()->text().toDouble();
  OptionDataGraphShapeColor      = SettingsDataQTD->GetDataColorQPBCS()->GetColor();
  OptionDataGraphErrorsLineColor = SettingsDataQTD->GetErrorsLineColorQPBCS()->GetColor();
  OptionDataGraphErrorsLineStyle = SettingsDataQTD->GetErrorsLineStyleQPC()->GetCurrentStyle();
  OptionDataFITSPalette          = SettingsDataQTD->GetPaletteQCB()->currentItem();
  OptionDataFITSMinDynFlag       = SettingsDataQTD->GetMinDynQCKB()->isChecked  ();
  OptionDataFITSMinDynValue      = SettingsDataQTD->GetMinDynQSD()->value       ();
  OptionDataFITSMaxDynFlag       = SettingsDataQTD->GetMaxDynQCKB()->isChecked  ();
  OptionDataFITSMaxDynValue      = SettingsDataQTD->GetMaxDynQSD()->value       ();
  
  if(TopFrame)
    if(!TopFrame->IsEmpty())
      if(!TopFrame->GetActivePlot()->GetData().empty())
	{
	  ApplyDataDefaultSettings();
	  
	  TopFrame->GetActivePlot()->ForceRepaint();
	  TopFrame->GetActivePlot()->repaint();
	}
  
  PrintStatus(trUtf8("Applied the new data settings."));
}

void GUI::SettingsDataDefaultsSlot()
{
  if(PrintMessage(SettingsDataQTD, MESSAGE_WARNING, trUtf8("This will unset the current data settings and set the default ones as current.<br />Are you sure you want to continue?"), trUtf8("Revert to defaults?")) == QMessageBox::No)
    {
      PrintStatus(trUtf8("Back to the default data settings canceled."));
      
      return;
    }
  
  SetDataDefaultSettings();
  
  SettingsDataCancelSlot();
  
  PrintStatus(trUtf8("Back to the default data settings."));
}

void GUI::SettingsDataCancelSlot()
{
  SettingsDataQTD->GetFunctionLineWidthQLC()->SetCurrentWidth(OptionDataFunctionLineWidth);
  SettingsDataQTD->GetFunctionLineColorQPBCS()->SetColor(OptionDataFunctionLineColor);
  SettingsDataQTD->GetFunctionLineStyleQPC()->SetCurrentStyle(OptionDataFunctionLineStyle);
  SettingsDataQTD->GetHistogramLineWidthQLC()->SetCurrentWidth(OptionDataHistogramLineWidth);
  SettingsDataQTD->GetHistogramLineColorQPBCS()->SetColor(OptionDataHistogramLineColor);
  SettingsDataQTD->GetHistogramFillColorQPBCS()->SetColor(OptionDataHistogramFillColor);
  SettingsDataQTD->GetHistogramLineStyleQPC()->SetCurrentStyle(OptionDataHistogramLineStyle);
  SettingsDataQTD->GetHistogramFillStyleQBC()->SetCurrentStyle(OptionDataHistogramFillStyle);
  SettingsDataQTD->GetDataTypeQCB()->setCurrentItem(OptionDataGraphShapeType);
  SettingsDataQTD->GetErrorsLineWidthQLC()->SetCurrentWidth(OptionDataGraphErrorsLineWidth);
  SettingsDataQTD->GetDataRatioQLE()->setText(QString("%1") .arg(OptionDataGraphShapeRatio));
  SettingsDataQTD->GetDataColorQPBCS()->SetColor(OptionDataGraphShapeColor);
  SettingsDataQTD->GetErrorsLineColorQPBCS()->SetColor(OptionDataGraphErrorsLineColor);
  SettingsDataQTD->GetErrorsLineStyleQPC()->SetCurrentStyle(OptionDataGraphErrorsLineStyle);
  SettingsDataQTD->GetPaletteQCB()->setCurrentItem(OptionDataFITSPalette);
  SettingsDataQTD->GetMinDynQCKB()->setChecked(OptionDataFITSMinDynFlag);
  if(OptionDataFITSMinDynFlag)
    SettingsDataQTD->GetMinDynQSD()->setValue(OptionDataFITSMinDynValue);
  SettingsDataQTD->GetMaxDynQCKB()->setChecked(OptionDataFITSMaxDynFlag);
  if(OptionDataFITSMaxDynFlag)
    SettingsDataQTD->GetMaxDynQSD()->setValue(OptionDataFITSMaxDynValue);
}

void GUI::SettingsDataChangedSlot(QWidget *)
{
  SettingsDataCurrentTabID = SettingsDataQTD->GetCurrentTabID();
}

void GUI::SettingsDataFunctionSlot()
{
  SettingsDataCurrentTabID = 0;
  
  SettingsDataSlot();
}

void GUI::SettingsDataHistogramSlot()
{
  SettingsDataCurrentTabID = 1;
  
  SettingsDataSlot();
}

void GUI::SettingsDataGraph2DSlot()
{
  SettingsDataCurrentTabID = 2;
  
  SettingsDataSlot();
}

void GUI::SettingsDataFITSSlot()
{
  SettingsDataCurrentTabID = 3;
  
  SettingsDataSlot();
}

void GUI::SettingsSaveSlot()
{
  if(PrintMessage(this, MESSAGE_WARNING, trUtf8("This will overwrite the existing default settings and set the current ones as default.<br />Are you sure you want to continue?"), trUtf8("Overwrite?")) == QMessageBox::No)
    {
      PrintStatus(trUtf8("Save current settings canceled."));
      
      return;
    }
  
  QDir Dir;
  Dir.setPath(Dir.homeDirPath() + QString(BAYANI_DIR));
  
  if(!Dir.exists())
    if(Dir.mkdir(Dir.path()))
      PrintMessage(this, MESSAGE_INFORMATION, trUtf8("Created directory %1 in order to store the default settings.") .arg(Dir.path()), trUtf8("New directory"));
  
  SaveGlobalDefaultSettings();
  SaveFrameDefaultSettings ();
  SaveGraphDefaultSettings ();
  SaveDataDefaultSettings  ();
  
  PrintStatus(trUtf8("Saved current settings."));
}

void GUI::HelpContentsSlot()
{
  HelpContentsBHB->show();
  HelpContentsBHB->raise();
}

void GUI::HelpAboutSlot()
{
  HelpAboutQTD->show();
  HelpAboutQTD->raise();
}

void GUI::PrintStatusSlot(QString Status)
{
  PrintStatus(Status);
}

void GUI::ParseCommandSlot(QString Cmd)
{
  Cmd = Cmd.stripWhiteSpace();
  
  QStringList CmdList;
  
  CmdList = CmdList.split(" ", Cmd);
  
  if(CmdList.size() == 0)
    return;
  
  if(CmdList[0].mid(0,1) == "#")
    return;
 
  TerminalSenderFlag = true;
  
  QString UsageString = trUtf8("Usage: %1") .arg(CmdList[0]);
  
  /* The following commands reproduce the GUI behaviour */
  
  if(CmdList[0] == trUtf8("quit", "Command"))
    {
      if(CmdList.size() > 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	FileQuitSlot();
    }
  
  else if(CmdList[0] == trUtf8("table", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Display flag&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  if(!CmdList[1].toInt())//does not use the Slot and thus does not print the corresponding status msg
	    DataTable->close();
	  else
	    DataTable->show();
	}
    }
  
  else if(CmdList[0] == trUtf8("terminal", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Display flag&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  if(!CmdList[1].toInt())//does not use the Slot and thus does not print the corresponding status msg
	    ScriptingWindow->close();
	  else
	    ScriptingWindow->show();
	}
    }
  
  else if(CmdList[0] == trUtf8("cascade", "Command"))
    {
      if(CmdList.size() > 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	WorkSpaceQWS->cascade();//does not use the Slot (which does not exist yet!) and thus does not print the corresponding status msg
    }
  
  else if(CmdList[0] == trUtf8("tile", "Command"))
    {
      if(CmdList.size() > 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	WorkSpaceQWS->tile();//does not use the Slot (which does not exist yet!) and thus does not print the corresponding status msg
    }
  
  else if(CmdList[0] == trUtf8("frame", "Command"))
    {
      if(CmdList.size() > 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	FrameNewSlot();
    }
  
  else if(CmdList[0] == trUtf8("zone", "Command"))
    {
      if(CmdList.size() != 3 && CmdList.size() != 4)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Horizontally&gt; &lt;Vertically&gt; [Secondary zone]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  if(CmdList.size() == 4)
	    {
	      FrameZoneQTD->GetSecondaryQCKB()->setChecked(true);
	      FrameZoneQTD->GetZoneQSB      ()->setValue  (CmdList[3].toInt());
	    }
	  else
	    FrameZoneQTD->GetSecondaryQCKB()->setChecked(false);
	  
	  FrameZoneQTD->GetHorZoneQLE()->clear (          );
	  FrameZoneQTD->GetHorZoneQLE()->insert(CmdList[1]);
	  FrameZoneQTD->GetVerZoneQLE()->clear (          );
	  FrameZoneQTD->GetVerZoneQLE()->insert(CmdList[2]);
	  
	  FrameZoneApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("activegraph", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Zone&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  FrameSetActivePlotQTD->GetZoneQSB()->setValue(CmdList[1].toInt());
	  
	  FrameSetActivePlotApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("saveframe", "Command"))
    {
      if(CmdList.size() > 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 [Frame's new filename]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  if(CmdList.size() == 1 && TopFrame && TopFrame->GetSaveFileName() == "")
	    PrintMessage(this, MESSAGE_CRITICAL, trUtf8("Please set a filename for the active frame first."));
	  else
	    {
	      if(CmdList.size() == 2 && TopFrame)
		TopFrame->SetSaveFileName(CmdList[1]);//This excludes checking for the file's prior existence in FrameSaveSlot()
	      
	      FrameSaveSlot();
	    }
	}
    }
  
  else if(CmdList[0] == trUtf8("closeframe", "Command"))
    {
      if(CmdList.size() > 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	FrameCloseSlot();
    }
  
  else if(CmdList[0] == trUtf8("fill", "Command"))/* Separator option needed */
    {
      if(CmdList.size() < 5 || CmdList.size() > 10)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Filename&gt; &lt;Columns number&gt; &lt;Starting row&gt; &lt;Starting column&gt; [Add rows flag] [Add columns flag] [Empty cells flag] [Superior columns number flag] [Default fill]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TableFillFromFileQTD->GetAddRowsQCKB   ()->setChecked(false);
	  TableFillFromFileQTD->GetAddColumnsQCKB()->setChecked(false);
	  TableFillFromFileQTD->GetEmptyCellsQCKB()->setChecked(false);
	  TableFillFromFileQTD->GetSupLinesQCKB  ()->setChecked(false);
	  TableFillFromFileQTD->GetSubLinesQCKB  ()->setChecked(false);
	  
	  TableFillFromFileQTD->GetFileNameQFLS()->SetFileName(CmdList[1]);
	  
	  TableFillFromFileQTD->GetColumnsNumberQLE()->clear (          );
	  TableFillFromFileQTD->GetColumnsNumberQLE()->insert(CmdList[2]);
	  
	  TableFillFromFileQTD->GetStartingRowQSB   ()->setValue(CmdList[3].toInt());
	  TableFillFromFileQTD->GetStartingColumnQSB()->setValue(CmdList[4].toInt());
	  
	  if(CmdList.size() > 5 && CmdList[5].toInt())
	    TableFillFromFileQTD->GetAddRowsQCKB()->setChecked(true);
	  
	  if(CmdList.size() > 6 && CmdList[6].toInt())
	    TableFillFromFileQTD->GetAddColumnsQCKB()->setChecked(true);
	  
	  if(CmdList.size() > 7 && CmdList[7].toInt())
	    TableFillFromFileQTD->GetEmptyCellsQCKB()->setChecked(true);
	  
	  if(CmdList.size() > 8 && CmdList[8].toInt())
	    TableFillFromFileQTD->GetSupLinesQCKB()->setChecked(true);
	  
	  if(CmdList.size() > 9)
	    {
	      TableFillFromFileQTD->GetSubLinesQCKB()->setChecked(true);
	      
	      TableFillFromFileQTD->GetDefaultFillQLE()->clear (          );
	      TableFillFromFileQTD->GetDefaultFillQLE()->insert(CmdList[9]);
	    }
	  
	  TableFillFromFileApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("write", "Command"))/* Separator option needed */
    {
      if(CmdList.size() < 6 || CmdList.size() > 7)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Filename&gt; &lt;Row 1&gt; &lt;Column 1&gt; &lt;Row 2&gt; &lt;Column 2&gt; [Append flag]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TableWriteToFileQTD->GetFileNameQFLS()->SetFileName(CmdList[1]);
	  
	  TableWriteToFileQTD->GetRow1QSB   ()->setValue(CmdList[2].toInt());
	  TableWriteToFileQTD->GetColumn1QSB()->setValue(CmdList[3].toInt());
	  TableWriteToFileQTD->GetRow2QSB   ()->setValue(CmdList[4].toInt());
	  TableWriteToFileQTD->GetColumn2QSB()->setValue(CmdList[5].toInt());
	  
	  if(CmdList.size() > 6 && CmdList[6].toInt())
	    TableWriteToFileQTD->GetAppendQCKB()->setChecked(true);
	  else
	    TableWriteToFileQTD->GetAppendQCKB()->setChecked(false);
	  
	  TableWriteToFileApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("swaprows", "Command"))
    {
      if(CmdList.size() < 3 || CmdList.size() > 4)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Row 1&gt; &lt;Row 2&gt; [Copy flag]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TableSwapQTD->GetRowsQRB()->setChecked(true);
	  
	  TableSwapQTD->GetElement1QSB()->setValue(CmdList[1].toInt());
	  TableSwapQTD->GetElement2QSB()->setValue(CmdList[2].toInt());
	  
	  if(CmdList.size() > 3 && CmdList[3].toInt())
	    TableSwapQTD->GetCopyQCKB()->setChecked(true);
	  else
	    TableSwapQTD->GetCopyQCKB()->setChecked(false);
	  
	  TableSwapApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("swapcolumns", "Command"))
    {
      if(CmdList.size() < 3 || CmdList.size() > 4)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Column 1&gt; &lt;Column 2&gt; [Copy flag]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TableSwapQTD->GetColumnsQRB()->setChecked(true);
	  
	  TableSwapQTD->GetElement1QSB()->setValue(CmdList[1].toInt());
	  TableSwapQTD->GetElement2QSB()->setValue(CmdList[2].toInt());
	  
	  if(CmdList.size() > 3 && CmdList[3].toInt())
	    TableSwapQTD->GetCopyQCKB()->setChecked(true);
	  else
	    TableSwapQTD->GetCopyQCKB()->setChecked(false);
	  
	  TableSwapApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("transpose", "Command"))
    {
      if(CmdList.size() != 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	TableTransposeSlot();
    }
  
  else if(CmdList[0] == trUtf8("insertrows", "Command"))
    {
      if(CmdList.size() != 3)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Rows number&gt; &lt;Starting row&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TableManageQTD->GetInsertQRB()->setChecked(true);
	  TableManageQTD->GetRowsQRB  ()->setChecked(true);
	  
	  TableManageQTD->GetNumberQLE()->clear (          );
	  TableManageQTD->GetNumberQLE()->insert(CmdList[1]);
	  
	  TableManageQTD->GetStartQSB()->setValue(CmdList[2].toInt());
	  
	  TableManageApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("removerows", "Command"))
    {
      if(CmdList.size() != 3)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Rows number&gt; &lt;Starting row&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TableManageQTD->GetRemoveQRB()->setChecked(true);
	  TableManageQTD->GetRowsQRB  ()->setChecked(true);
	  
	  TableManageQTD->GetNumberQLE()->clear (          );
	  TableManageQTD->GetNumberQLE()->insert(CmdList[1]);
	  
	  TableManageQTD->GetStartQSB()->setValue(CmdList[2].toInt());
	  
	  TableManageApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("insertcolumns", "Command"))
    {
      if(CmdList.size() != 3)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Columns number&gt; &lt;Starting column&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TableManageQTD->GetInsertQRB ()->setChecked(true);
	  TableManageQTD->GetColumnsQRB()->setChecked(true);
	  
	  TableManageQTD->GetNumberQLE()->clear (          );
	  TableManageQTD->GetNumberQLE()->insert(CmdList[1]);
	  
	  TableManageQTD->GetStartQSB()->setValue(CmdList[2].toInt());
	  
	  TableManageApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("removecolumns", "Command"))
    {
      if(CmdList.size() != 3)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Columns number&gt; &lt;Starting column&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TableManageQTD->GetRemoveQRB ()->setChecked(true);
	  TableManageQTD->GetColumnsQRB()->setChecked(true);
	  
	  TableManageQTD->GetNumberQLE()->clear (          );
	  TableManageQTD->GetNumberQLE()->insert(CmdList[1]);
	  
	  TableManageQTD->GetStartQSB()->setValue(CmdList[2].toInt());
	  
	  TableManageApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("empty", "Command"))
    {
      if(CmdList.size() != 5)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Row 1&gt; &lt;Column 1&gt; &lt;Row 2&gt; &lt;Column 2&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TableEmptyQTD->GetRow1QSB   ()->setValue(CmdList[1].toInt());
	  TableEmptyQTD->GetColumn1QSB()->setValue(CmdList[2].toInt());
	  TableEmptyQTD->GetRow2QSB   ()->setValue(CmdList[3].toInt());
	  TableEmptyQTD->GetColumn2QSB()->setValue(CmdList[4].toInt());
	  
	  TableEmptyApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("function", "Command"))
    {
      if(CmdList.size() != 4)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Expression&gt; &lt;x<sub>0</sub>&gt; &lt;x<sub>1</sub>&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  GraphFunctionQTD->GetExpressionQLE()->setText(CmdList[1]);
	  GraphFunctionQTD->GetXMinQLE      ()->clear  (          );
	  GraphFunctionQTD->GetXMinQLE      ()->insert (CmdList[2]);
	  GraphFunctionQTD->GetXMaxQLE      ()->clear  (          );
	  GraphFunctionQTD->GetXMaxQLE      ()->insert (CmdList[3]);
	  
	  GraphFunctionApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("histogram", "Command"))
    {
      if(CmdList.size() < 2 || CmdList.size() > 6)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Expression&gt; [Bins number] [Minimum] [Maximum] [Cut]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  GraphHistogramQTD->GetExpressionQRB()->setChecked(true      );
	  GraphHistogramQTD->GetExpressionQLE()->setText   (CmdList[1]);
	  
	  GraphHistogramQTD->GetBinsNumberQCKB()->setChecked(false);
	  GraphHistogramQTD->GetXMinQCKB      ()->setChecked(false);
	  GraphHistogramQTD->GetXMaxQCKB      ()->setChecked(false);
	  GraphHistogramQTD->GetCutQCKB       ()->setChecked(false);
	  
	  if(CmdList.size() > 2 && CmdList[2] != "!")
	    {
	      GraphHistogramQTD->GetBinsNumberQCKB()->setChecked(true      );
	      GraphHistogramQTD->GetBinsNumberQLE ()->clear     (          );
	      GraphHistogramQTD->GetBinsNumberQLE ()->insert    (CmdList[2]);
	    }
	  
	  if(CmdList.size() > 3 && CmdList[3] != "!")
	    {
	      GraphHistogramQTD->GetXMinQCKB()->setChecked(true      );
	      GraphHistogramQTD->GetXMinQLE ()->clear     (          );
	      GraphHistogramQTD->GetXMinQLE ()->insert    (CmdList[3]);
	    }
	  
	  if(CmdList.size() > 4 && CmdList[4] != "!")
	    {
	      GraphHistogramQTD->GetXMaxQCKB()->setChecked(true      );
	      GraphHistogramQTD->GetXMaxQLE ()->clear     (          );
	      GraphHistogramQTD->GetXMaxQLE ()->insert    (CmdList[4]);
	    }
	  
	  if(CmdList.size() > 5 && CmdList[5] != "!")
	    {
	      GraphHistogramQTD->GetCutQCKB()->setChecked(true      );
	      GraphHistogramQTD->GetCutQLE ()->setText   (CmdList[5]);
	    }
	  
	  GraphHistogramApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("graph2d", "Command"))
    {
      if(CmdList.size() < 3 || CmdList.size() > 8)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;X axis expression&gt; &lt;Y axis expression&gt; [Inferior x errors] [Inferior y errors] [Superior x errors] [Superior y errors] [Cut]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  GraphGraph2DQTD->GetExpressionXQRB()->setChecked(true      );
	  GraphGraph2DQTD->GetExpressionXQLE()->setText   (CmdList[1]);
	  GraphGraph2DQTD->GetExpressionYQRB()->setChecked(true      );
	  GraphGraph2DQTD->GetExpressionYQLE()->setText   (CmdList[2]);
	  
	  GraphGraph2DQTD->GetErrorsQCKB     ()->setChecked(false);
	  GraphGraph2DQTD->GetCutQCKB        ()->setChecked(false);
	  GraphGraph2DQTD->GetErrorsDownXQCKB()->setChecked(false);
	  GraphGraph2DQTD->GetErrorsDownYQCKB()->setChecked(false);
	  GraphGraph2DQTD->GetErrorsUpXQCKB  ()->setChecked(false);
	  GraphGraph2DQTD->GetErrorsUpYQCKB  ()->setChecked(false);
	  GraphGraph2DQTD->GetAsymmetricQCKB ()->setChecked(false);
	  
	  if(CmdList.size() > 3 && CmdList[3] != "!")
	    {
	      GraphGraph2DQTD->GetErrorsQCKB     ()->setChecked(true);
	      GraphGraph2DQTD->GetErrorsDownXQCKB()->setChecked(true);
	      GraphGraph2DQTD->GetErrorsUpXQCKB  ()->setChecked(true);
	      GraphGraph2DQTD->GetAsymmetricQCKB ()->setChecked(true);
	      
	      GraphGraph2DQTD->GetExpressionDownXQRB()->setChecked(true      );
	      GraphGraph2DQTD->GetExpressionDownXQLE()->setText   (CmdList[3]);
	      GraphGraph2DQTD->GetExpressionUpXQRB  ()->setChecked(true      );
	      GraphGraph2DQTD->GetExpressionUpXQLE  ()->setText   (CmdList[3]);
	    }
	  
	  if(CmdList.size() > 4 && CmdList[4] != "!")
	    {
	      GraphGraph2DQTD->GetErrorsQCKB     ()->setChecked(true);
	      GraphGraph2DQTD->GetErrorsDownYQCKB()->setChecked(true);
	      GraphGraph2DQTD->GetErrorsUpYQCKB  ()->setChecked(true);
	      GraphGraph2DQTD->GetAsymmetricQCKB ()->setChecked(true);
	      
	      GraphGraph2DQTD->GetExpressionDownYQRB()->setChecked(true      );
	      GraphGraph2DQTD->GetExpressionDownYQLE()->setText   (CmdList[4]);
	      GraphGraph2DQTD->GetExpressionUpYQRB  ()->setChecked(true      );
	      GraphGraph2DQTD->GetExpressionUpYQLE  ()->setText   (CmdList[4]);
	    }
	  
	  if(CmdList.size() > 5 && CmdList[5] != "!")
	    {
	      GraphGraph2DQTD->GetErrorsQCKB     ()->setChecked(true);
	      GraphGraph2DQTD->GetErrorsDownXQCKB()->setChecked(true);
	      GraphGraph2DQTD->GetErrorsUpXQCKB  ()->setChecked(true);
	      GraphGraph2DQTD->GetAsymmetricQCKB ()->setChecked(true);
	      
	      if(CmdList[3] == "!")
		{
		  GraphGraph2DQTD->GetExpressionDownXQRB()->setChecked(true      );
		  GraphGraph2DQTD->GetExpressionDownXQLE()->setText   (CmdList[5]);
		}
	      
	      GraphGraph2DQTD->GetExpressionUpXQRB()->setChecked(true      );
	      GraphGraph2DQTD->GetExpressionUpXQLE()->setText   (CmdList[5]);
	    }
	  
	  if(CmdList.size() > 6 && CmdList[6] != "!")
	    {
	      GraphGraph2DQTD->GetErrorsQCKB     ()->setChecked(true);
	      GraphGraph2DQTD->GetErrorsDownYQCKB()->setChecked(true);
	      GraphGraph2DQTD->GetErrorsUpYQCKB  ()->setChecked(true);
	      GraphGraph2DQTD->GetAsymmetricQCKB ()->setChecked(true);
	      
	      if(CmdList[4] == "!")
		{
		  GraphGraph2DQTD->GetExpressionDownYQRB()->setChecked(true      );
		  GraphGraph2DQTD->GetExpressionDownYQLE()->setText   (CmdList[6]);
		}
	      
	      GraphGraph2DQTD->GetExpressionUpYQRB()->setChecked(true      );
	      GraphGraph2DQTD->GetExpressionUpYQLE()->setText   (CmdList[6]);
	    }
	  
	  if(CmdList.size() > 7 && CmdList[7] != "!")
	    {
	      GraphGraph2DQTD->GetCutQCKB()->setChecked(true      );
	      GraphGraph2DQTD->GetCutQLE ()->setText   (CmdList[7]);
	    }
	  
	  GraphGraph2DApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("fitsimage", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Filename&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  GraphFITSQTD->GetFileNameQFLS()->SetFileName(CmdList[1]);
	  
	  GraphFITSApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("bounds", "Command"))
    {
      if(CmdList.size() > 6)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 [X inf] [X sup] [Y inf] [Y sup] [New graphs flag]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  GraphBoundsQTD->GetXInfQCKB()->setChecked(false);
	  GraphBoundsQTD->GetXSupQCKB()->setChecked(false);
	  GraphBoundsQTD->GetYInfQCKB()->setChecked(false);
	  GraphBoundsQTD->GetYSupQCKB()->setChecked(false);
	  GraphBoundsQTD->GetNewQCKB ()->setChecked(false);
	  
	  if(CmdList.size() > 1 && CmdList[1] != "!")
	    {
	      GraphBoundsQTD->GetXInfQCKB()->setChecked(true      );
	      GraphBoundsQTD->GetXInfQLE ()->clear     (          );
	      GraphBoundsQTD->GetXInfQLE ()->insert    (CmdList[1]);
	    }
	  
	  if(CmdList.size() > 2 && CmdList[2] != "!")
	    {
	      GraphBoundsQTD->GetXSupQCKB()->setChecked(true      );
	      GraphBoundsQTD->GetXSupQLE ()->clear     (          );
	      GraphBoundsQTD->GetXSupQLE ()->insert    (CmdList[2]);
	    }
	  
	  if(CmdList.size() > 3 && CmdList[3] != "!")
	    {
	      GraphBoundsQTD->GetYInfQCKB()->setChecked(true      );
	      GraphBoundsQTD->GetYInfQLE ()->clear     (          );
	      GraphBoundsQTD->GetYInfQLE ()->insert    (CmdList[3]);
	    }
	  
	  if(CmdList.size() > 4 && CmdList[4] != "!")
	    {
	      GraphBoundsQTD->GetYSupQCKB()->setChecked(true      );
	      GraphBoundsQTD->GetYSupQLE ()->clear     (          );
	      GraphBoundsQTD->GetYSupQLE ()->insert    (CmdList[4]);
	    }
	  
	  if(CmdList.size() > 5 && CmdList[5].toInt())
	    GraphBoundsQTD->GetNewQCKB()->setChecked(true);
	  
	  GraphBoundsApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("axes", "Command"))
    {
      if(CmdList.size() > 6)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 [X log flag] [Y log flag] [X reverse flag] [Y reverse flag] [New graphs flag]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  GraphAxesQTD->GetXLogQCKB()->setChecked(false);
	  GraphAxesQTD->GetYLogQCKB()->setChecked(false);
	  GraphAxesQTD->GetXRevQCKB()->setChecked(false);
	  GraphAxesQTD->GetYRevQCKB()->setChecked(false);
	  GraphAxesQTD->GetNewQCKB ()->setChecked(false);
	  
	  if(CmdList.size() > 1 && CmdList[1].toInt())
	    GraphAxesQTD->GetXLogQCKB()->setChecked(true);
	  
	  if(CmdList.size() > 2 && CmdList[2].toInt())
	    GraphAxesQTD->GetYLogQCKB()->setChecked(true);
	  
	  if(CmdList.size() > 3 && CmdList[3].toInt())
	    GraphAxesQTD->GetXRevQCKB()->setChecked(true);
	  
	  if(CmdList.size() > 4 && CmdList[4].toInt())
	    GraphAxesQTD->GetYRevQCKB()->setChecked(true);
	  
	  if(CmdList.size() > 5 && CmdList[5].toInt())
	    GraphAxesQTD->GetNewQCKB()->setChecked(true);
	  
	  GraphAxesApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("fit", "Command"))
    {
      if(CmdList.size() < 2 || CmdList.size() > 3)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Degree&gt; [Representation flag]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  GraphFitQTD->GetNumericalQCKB()->setChecked(false);
	  
	  int Item = CmdList[1].toInt();
	  
	  if(Item < GraphFitQTD->GetDegreeQCB()->count())
	    GraphFitQTD->GetDegreeQCB()->setCurrentItem(Item);
	  
	  if(CmdList.size() > 2 && CmdList[2].toInt())
	    GraphFitQTD->GetPlotQCKB()->setChecked(true);
	  else
	    GraphFitQTD->GetPlotQCKB()->setChecked(false);
	  
	  GraphFitApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("fitnum", "Command"))
    {
      if(CmdList.size() < 7 || CmdList.size() > 9)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;p<sub>0</sub>&gt; &lt;p<sub>1</sub>&gt; &lt;p<sub>2</sub>&gt; &lt;p<sub>3</sub>&gt; &lt;Iterations number&gt; &lt;&chi;<sup>2</sup> criterion&gt; [Representation flag] [No convergence flag]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  GraphFitQTD->GetNumericalQCKB()->setChecked(true);
	  
	  GraphFitQTD->GetParameter0QLE()->clear (          );
	  GraphFitQTD->GetParameter0QLE()->insert(CmdList[1]);
	  GraphFitQTD->GetParameter1QLE()->clear (          );
	  GraphFitQTD->GetParameter1QLE()->insert(CmdList[2]);
	  GraphFitQTD->GetParameter2QLE()->clear (          );
	  GraphFitQTD->GetParameter2QLE()->insert(CmdList[3]);
	  GraphFitQTD->GetParameter3QLE()->clear (          );
	  GraphFitQTD->GetParameter3QLE()->insert(CmdList[4]);
	  GraphFitQTD->GetMaxStepQLE   ()->clear (          );
	  GraphFitQTD->GetMaxStepQLE   ()->insert(CmdList[5]);
	  GraphFitQTD->GetChi2LimQLE   ()->clear (          );
	  GraphFitQTD->GetChi2LimQLE   ()->insert(CmdList[6]);
	  
	  if(CmdList.size() > 7 && CmdList[7].toInt())
	    GraphFitQTD->GetPlotQCKB()->setChecked(true);
	  else
	    GraphFitQTD->GetPlotQCKB()->setChecked(false);
	  
	  if(CmdList.size() > 8 && CmdList[8].toInt())
	    GraphFitQTD->GetPlotNoConvQCKB()->setChecked(true);
	  else
	    GraphFitQTD->GetPlotNoConvQCKB()->setChecked(false);
	  
	  GraphFitApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("same", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Display flag&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  if(CmdList[1].toInt())
	    {
	      GraphFunctionQTD ->GetSameQCKB()->setChecked(true);
	      GraphHistogramQTD->GetSameQCKB()->setChecked(true);
	      GraphGraph2DQTD  ->GetSameQCKB()->setChecked(true);
	      GraphFITSQTD     ->GetSameQCKB()->setChecked(true);
	      GraphFitQTD      ->GetSameQCKB()->setChecked(true);
	    }
	  else
	    {
	      GraphFunctionQTD ->GetSameQCKB()->setChecked(false);
	      GraphHistogramQTD->GetSameQCKB()->setChecked(false);
	      GraphGraph2DQTD  ->GetSameQCKB()->setChecked(false);
	      GraphFITSQTD     ->GetSameQCKB()->setChecked(false);
	      GraphFitQTD      ->GetSameQCKB()->setChecked(false);
	    }
	}
    }
  
  else if(CmdList[0] == trUtf8("closegraph", "Command"))
    {
      if(CmdList.size() > 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	GraphCloseSlot();
    }
  
  else if(CmdList[0] == trUtf8("variable", "Command"))
    {
      if(CmdList.size() < 2 || CmdList.size() > 3)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Name&gt; [Value]") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  //isOperator , isFunction etc need to be static (they are now) to be used in the VariablesDialog
	  
	  ComputationVariablesQTD->GetVariablesNameQLE ()->clear();
	  ComputationVariablesQTD->GetVariablesValueQLE()->clear();
	  
	  ComputationVariablesQTD->GetVariablesNameQLE()->insert(CmdList[1]);
	  
	  if(CmdList.size() > 2 && CmdList[2] != "!")
	    ComputationVariablesQTD->GetVariablesValueQLE()->insert(CmdList[2]);
	  
	  if(ComputationVariablesQTD->GetAddVariablesQPB()->isEnabled())
	    ComputationVariablesQTD->AddVariablesSlot();
	  else
	    PrintMessage(this, MESSAGE_CRITICAL, trUtf8("Name '%1' is not allowed.") .arg(CmdList[1]), trUtf8("Syntax error"));
	}
    }
  
  else if(CmdList[0] == trUtf8("variables", "Command"))
    {
      if(CmdList.size() != 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	{
	  QString Message;
	  
	  if(VariablesList.empty())
	    Message = trUtf8("The variables list is empty.");
	  else
	    for(int i = 0; i < VariablesList.size(); i++)
	      {
		Message += QString("%1 = %2") .arg(VariablesList[i].Name) .arg(VariablesList[i].Value);
		
		if(i != VariablesList.size()-1)
		  Message += "<br />";
	      }
	  
	  PrintMessage(this, MESSAGE_INFORMATION, Message);
	}
    }
  
  else if(CmdList[0] == trUtf8("remove_variable", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Name&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  ComputationVariablesQTD->GetVariablesQLV()->clearSelection();
	  
	  QListViewItem * OldItem = ComputationVariablesQTD->GetVariablesQLV()->findItem(CmdList[1], 0);
	  
	  if(!OldItem)
	    PrintMessage(this, MESSAGE_CRITICAL, trUtf8("Variable '%1' does not exist.") .arg(CmdList[1]));
	  else
	    {
	      ComputationVariablesQTD->GetVariablesQLV()->setSelected(OldItem, true);
	      
	      ComputationVariablesQTD->RemoveVariablesSlot();
	    }
	}
    }
  
  else if(CmdList[0] == trUtf8("remove_variables", "Command"))
    {
      if(CmdList.size() != 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	{
	  ComputationVariablesQTD->GetVariablesQLV()->clearSelection ();
	  ComputationVariablesQTD->GetVariablesQLV()->invertSelection();
	  
	  if(ComputationVariablesQTD->GetRemoveVariablesQPB()->isEnabled())
	    ComputationVariablesQTD->RemoveVariablesSlot();
	  else
	    PrintMessage(this, MESSAGE_CRITICAL, trUtf8("The variables list is empty."));
	}
    }
  
  else if(CmdList[0] == trUtf8("simplify", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Expression&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  ComputationComputeQTD->GetExpressionQLE()->setText   (Cmd.remove(0, CmdList[0].length()+1));
	  ComputationComputeQTD->GetSimplifyQRB  ()->setChecked(true                                );
	  
	  ComputationComputeApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("evaluate", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Expression&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  ComputationComputeQTD->GetExpressionQLE()->setText   (Cmd.remove(0, CmdList[0].length()+1));
	  ComputationComputeQTD->GetEvaluateQRB  ()->setChecked(true                                );
	  
	  ComputationComputeApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("resolve", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Expression&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  ComputationComputeQTD->GetExpressionQLE()->setText   (Cmd.remove(0, CmdList[0].length()+1));
	  ComputationComputeQTD->GetResolveQRB   ()->setChecked(true                                );
	  
	  ComputationComputeApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("derive", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Expression&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  ComputationComputeQTD->GetExpressionQLE()->setText   (Cmd.remove(0, CmdList[0].length()+1));
	  ComputationComputeQTD->GetDeriveQRB    ()->setChecked(true                                );
	  
	  ComputationComputeApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("integrate", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Expression&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  ComputationComputeQTD->GetExpressionQLE()->setText   (Cmd.remove(0, CmdList[0].length()+1));
	  ComputationComputeQTD->GetIntegrateQRB ()->setChecked(true                                );
	  
	  ComputationComputeApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("titleframe", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Frame title&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TextsFrameQTD->GetTitleQLE()->setText(Cmd.remove(0, CmdList[0].length()+1));
	  
	  TextsFrameApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("titlegraph", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Graph title&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TextsGraphQTD->GetTitleQLE()->setText(Cmd.remove(0, CmdList[0].length()+1));
	  
	  TextsGraphApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("titlex", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;X axis title&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TextsGraphQTD->GetXAxisLabelQLE()->setText(Cmd.remove(0, CmdList[0].length()+1));
	  
	  TextsGraphApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("titley", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Y axis title&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  TextsGraphQTD->GetYAxisLabelQLE()->setText(Cmd.remove(0, CmdList[0].length()+1));
	  
	  TextsGraphApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("digitsx", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Display flag&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  if(CmdList[1].toInt())
	    TextsGraphQTD->GetXTicksLabelsFlagQCKB()->setChecked(true);
	  else
	    TextsGraphQTD->GetXTicksLabelsFlagQCKB()->setChecked(false);
	  
	  TextsGraphApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("digitsy", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Display flag&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  if(CmdList[1].toInt())
	    TextsGraphQTD->GetYTicksLabelsFlagQCKB()->setChecked(true);
	  else
	    TextsGraphQTD->GetYTicksLabelsFlagQCKB()->setChecked(false);
	  
	  TextsGraphApplySlot();
	}
    }
  
  else if(CmdList[0] == trUtf8("optiondata", "Command"))
    {
      if(CmdList.size() != 3)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Option&gt; &lt;Value&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  SettingsDataCancelSlot();//need to do the same for the graph texts commands (global texts vars, put again in the QLEs/QCKBs when clicking on cancel), in order to avoid applying half typed texts// and for remove vars too?
	  
	  if(CmdList[1] == trUtf8("f_width", "Command - Option"))
	    SettingsDataQTD->GetFunctionLineWidthQLC()->SetCurrentWidth(CmdList[2].toInt());
	  else if(CmdList[1] == trUtf8("f_style", "Command - Option"))
	    SettingsDataQTD->GetFunctionLineStyleQPC()->SetCurrentStyle(Qt::PenStyle(CmdList[2].toInt()));
	  else if(CmdList[1] == trUtf8("f_color", "Command - Option"))
	    SettingsDataQTD->GetFunctionLineColorQPBCS()->SetColor(QColor(CmdList[2]));
	  else if(CmdList[1] == trUtf8("h_width", "Command - Option"))
	    SettingsDataQTD->GetHistogramLineWidthQLC()->SetCurrentWidth(CmdList[2].toInt());
	  else if(CmdList[1] == trUtf8("h_style", "Command - Option"))
	    SettingsDataQTD->GetHistogramLineStyleQPC()->SetCurrentStyle(Qt::PenStyle(CmdList[2].toInt()));
	  else if(CmdList[1] == trUtf8("h_color", "Command - Option"))
	    SettingsDataQTD->GetHistogramLineColorQPBCS()->SetColor(QColor(CmdList[2]));
	  else if(CmdList[1] == trUtf8("h_fstyle", "Command - Option"))
	    SettingsDataQTD->GetHistogramFillStyleQBC()->SetCurrentStyle(Qt::BrushStyle(CmdList[2].toInt()));
	  else if(CmdList[1] == trUtf8("h_fcolor", "Command - Option"))
	    SettingsDataQTD->GetHistogramFillColorQPBCS()->SetColor(QColor(CmdList[2]));
	  else if(CmdList[1] == trUtf8("g_size", "Command - Option"))
	    {
	      SettingsDataQTD->GetDataRatioQLE()->clear (          );
	      SettingsDataQTD->GetDataRatioQLE()->insert(CmdList[2]);
	    }
	  else if(CmdList[1] == trUtf8("g_marker", "Command - Option"))
	    {
	      int Item = CmdList[2].toInt();
	      
	      if(Item < SettingsDataQTD->GetDataTypeQCB()->count())
		SettingsDataQTD->GetDataTypeQCB()->setCurrentItem(Item);
	    }
	  else if(CmdList[1] == trUtf8("g_color", "Command - Option"))
	    SettingsDataQTD->GetDataColorQPBCS()->SetColor(QColor(CmdList[2]));
	  else if(CmdList[1] == trUtf8("g_ewidth", "Command - Option"))
	    SettingsDataQTD->GetErrorsLineWidthQLC()->SetCurrentWidth(CmdList[2].toInt());
	  else if(CmdList[1] == trUtf8("g_estyle", "Command - Option"))
	    SettingsDataQTD->GetErrorsLineStyleQPC()->SetCurrentStyle(Qt::PenStyle(CmdList[2].toInt()));
	  else if(CmdList[1] == trUtf8("g_ecolor", "Command - Option"))
	    SettingsDataQTD->GetErrorsLineColorQPBCS()->SetColor(QColor(CmdList[2]));
	  else if(CmdList[1] == trUtf8("fi_palette", "Command - Option"))
	    {
	      int Item = CmdList[2].toInt();
	      
	      if(Item < SettingsDataQTD->GetPaletteQCB()->count())
		SettingsDataQTD->GetPaletteQCB()->setCurrentItem(Item);
	    }
	  else if(CmdList[1] == trUtf8("fi_min", "Command - Option"))
	    {
	      int Value = CmdList[2].toInt();
	      
	      if(Value >= 0)
		{
		  SettingsDataQTD->GetMinDynQCKB()->setChecked(true );
		  SettingsDataQTD->GetMinDynQSD ()->setValue  (Value);
		}
	      else
		SettingsDataQTD->GetMinDynQCKB()->setChecked(false);
	    }
	  else if(CmdList[1] == trUtf8("fi_max", "Command - Option"))
	    {
	      int Value = CmdList[2].toInt();
	      
	      if(Value >= 0)
		{
		  SettingsDataQTD->GetMaxDynQCKB()->setChecked(true );
		  SettingsDataQTD->GetMaxDynQSD ()->setValue  (Value);
		}
	      else
		SettingsDataQTD->GetMaxDynQCKB()->setChecked(false);
	    }
	  else
	    PrintMessage(this, MESSAGE_CRITICAL, trUtf8("Unknown option: '%1'") .arg(CmdList[1]), trUtf8("Syntax error"));
	  
	  SettingsDataApplySlot ();
	}
    }
  
  else if(CmdList[0] == trUtf8("defaults", "Command"))//option to enforce it? must have the same option in the SettingsXXXDefaultsSlots - should not be really needed if the terminal would accpet 'answers'
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Type&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  if(CmdList[1] == trUtf8("global", "Command - Option"))
	    {
	      SettingsGlobalCancelSlot  ();
	      SettingsGlobalDefaultsSlot();
	      SettingsGlobalApplySlot   ();
	    }
	  else if(CmdList[1] == trUtf8("frame", "Command - Option"))
	    {
	      SettingsFrameCancelSlot  ();
	      SettingsFrameDefaultsSlot();
	      SettingsFrameApplySlot   ();
	    }
	  else if(CmdList[1] == trUtf8("graph", "Command - Option"))
	    {
	      SettingsGraphCancelSlot  ();
	      SettingsGraphDefaultsSlot();
	      SettingsGraphApplySlot   ();
	    }
	  else if(CmdList[1] == trUtf8("data", "Command - Option"))
	    {
	      SettingsDataCancelSlot  ();
	      SettingsDataDefaultsSlot();
	      SettingsDataApplySlot   ();
	    }
	  else
	    PrintMessage(this, MESSAGE_CRITICAL, trUtf8("Unknown type: '%1'") .arg(CmdList[1]), trUtf8("Syntax error"));
	}
    }
  
  else if(CmdList[0] == trUtf8("saveoptions", "Command"))
    {
      if(CmdList.size() > 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	SettingsSaveSlot();
    }
  
  else if(CmdList[0] == trUtf8("help", "Command"))
    {
      if(CmdList.size() > 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	HelpContentsSlot();
    }
  
  else if(CmdList[0] == trUtf8("about", "Command"))
    {
      if(CmdList.size() > 1)
	PrintMessage(this, MESSAGE_CRITICAL, UsageString, trUtf8("Syntax error"));
      else
	HelpAboutSlot();
    }
  
  /* Help (and related) in text mode */
  
  /* About in text mode */
  
  /* The following commands are specific to the script */
  
  else if(CmdList[0] == trUtf8("execute", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Filename&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  QFile ExecFile(CmdList[1]);
	  
	  QString Extension(BAYANI_EXT);
	  QString ErrorMsg (trUtf8("I could not open file %1 in read-only mode.") .arg(ExecFile.name()));
	  
	  if(!ExecFile.open(IO_ReadOnly) && CmdList[1].right(Extension.length()) != Extension)
	    {
	      ExecFile.setName(CmdList[1] + BAYANI_EXT);
	      
	      ExecFile.open(IO_ReadOnly);
	      
	      if(!ExecFile.isOpen())
		ErrorMsg += trUtf8("<br />I could not open file %1 either.") .arg(ExecFile.name());
	    }
	  
	  if(!ExecFile.isOpen())
	    PrintMessage(this, MESSAGE_CRITICAL, ErrorMsg);
	  else
	    {
	      QTextStream ExecStream(&ExecFile);
	      
	      while(!ExecStream.atEnd())
		ParseCommandSlot(ExecStream.readLine());
	      
	      ExecFile.close();
	    }
	}
    }
  
  else if(CmdList[0] == trUtf8("shell", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Command&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  QString ShCmd = Cmd;
	  ShCmd = ShCmd.remove(0, CmdList[0].length()+1) + " >& _tmp_bayani_shell_cmd_";
	  system(ShCmd.ascii());
	  
	  QFile ShFile("_tmp_bayani_shell_cmd_");
	  
	  if(ShFile.open(IO_ReadOnly))
	    {
	      QTextStream ShStream(&ShFile);
	      
	      bool    CRFlag  = false;/* To avoid adding a CR at the beginning */
	      QString Content = "";
	      
	      while(!ShStream.atEnd())
		{
		  if(CRFlag)
		    Content += "<br />";
		  else
		    CRFlag = true;
		  
		  Content += ShStream.readLine();
		}
	      
	      PrintMessage(this, MESSAGE_INFORMATION, Content);
	      
	      ShFile.close();
	    }
	  
	  if(ShFile.exists())
	    ShFile.remove();
	}
    }
  
  else if(CmdList[0] == trUtf8("sleep", "Command"))
    {
      if(CmdList.size() != 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Seconds&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  int NumberOfSeconds = CmdList[1].toInt();
	  
	  if(NumberOfSeconds < 0)
	    NumberOfSeconds = -NumberOfSeconds;
	  
	  PrintMessage(this, MESSAGE_INFORMATION, trUtf8("Sleeping %1 second(s)...") .arg(NumberOfSeconds));
	  
	  dynamic_cast<QApplication *>(parentWidget())->processEvents();
	  
	  sleep(NumberOfSeconds);
	}
    }
  
  else if(CmdList[0] == trUtf8("print", "Command"))
    {
      if(CmdList.size() < 2)
	PrintMessage(this, MESSAGE_CRITICAL, trUtf8("%1 &lt;Expression&gt;") .arg(UsageString), trUtf8("Syntax error"));
      else
	{
	  QString PrintCmd = Cmd;
	  
	  PrintCmd = PrintCmd.remove(0, CmdList[0].length()+1);
	  
	  PrintMessage(this, MESSAGE_INFORMATION, PrintCmd);
	}
    }
  
  else
    PrintMessage(this, MESSAGE_CRITICAL, trUtf8("Unknown command: '%1'") .arg(CmdList[0]), trUtf8("Syntax error"));
  
  TerminalSenderFlag = false;
  
  ScriptingWindow->setFocus();
}

void GUI::DrawingProblemSlot(QString ErrorMessage)
{
  PrintMessage(this, MESSAGE_CRITICAL, trUtf8("Error in draw: <font color=%1>%2</font>") .arg(OptionGlobalExternalErrorColor.name()) .arg(ErrorMessage));
}

void GUI::ShowCoordinates(double XValue, double YValue)
{
  statusBar()->message(trUtf8("x=%1 y=%2") .arg(XValue) .arg(YValue));/* This one stays as a  QStatusBar::message() */
}

void GUI::ShowFITSCoordinates(double XValue, double YValue, double Value)
{
  statusBar()->message(trUtf8("x=%1 y=%2 value=%3") .arg(XValue) .arg(YValue) .arg(Value));/* This one stays as a  QStatusBar::message() */
}

void GUI::HideCoordinates()
{
  statusBar()->clear();
}
