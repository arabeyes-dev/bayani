/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogSettingsGlobal Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGSETTINGSGLOBAL_H
#define BAYANI_QTABDIALOGSETTINGSGLOBAL_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qtabdialog.h>
#include <qvbox.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qvalidator.h>
/* Bayani headers */
#include <qpushbuttoncolorselect.h>
#include <qfontselector.h>

/*!
  \file qtabdialogsettingsglobal.h
  \brief Header file for the QTabDialogSettingsGlobal class.
  
  The QTabDialogSettingsGlobal class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set the global options.
*/
class QTabDialogSettingsGlobal: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QVBox * TerminalQVB;
  QVBox * Terminal11QVB;
  QVBox * Terminal12QVB;
  QVBox * Terminal21QVB;
  QVBox * Terminal22QVB;
  
  QGroupBox * Terminal1QGB;
  QGroupBox * Terminal2QGB;
  
  QLabel * HistorySizeQL;
  QLabel * TextFontQL;
  QLabel * TextColorQL;
  QLabel * InformationColorQL;
  QLabel * WarningColorQL;
  QLabel * CriticalColorQL;
  QLabel * StatusColorQL;
  QLabel * BGColorQL;
  
  QLineEdit * HistorySizeQLE;
  
  QPushButtonColorSelect * TextColorQPBCS;
  QPushButtonColorSelect * InformationColorQPBCS;
  QPushButtonColorSelect * WarningColorQPBCS;
  QPushButtonColorSelect * CriticalColorQPBCS;
  QPushButtonColorSelect * StatusColorQPBCS;
  QPushButtonColorSelect * BGColorQPBCS;
  
  QFontSelector * TextFontQFS;
  
 public:
                                   QTabDialogSettingsGlobal(QWidget * Parent = 0, const char * Name = 0);
  virtual                         ~QTabDialogSettingsGlobal();
          QLineEdit              * GetHistorySizeQLE       ();
	  QPushButtonColorSelect * GetTextColorQPBCS       ();
	  QPushButtonColorSelect * GetInformationColorQPBCS();
	  QPushButtonColorSelect * GetWarningColorQPBCS    ();
	  QPushButtonColorSelect * GetCriticalColorQPBCS   ();
	  QPushButtonColorSelect * GetStatusColorQPBCS     ();
	  QPushButtonColorSelect * GetBGColorQPBCS         ();
	  QFontSelector          * GetTextFontQFS          ();
	  void                     SetR2L                  (bool);
};

#endif
