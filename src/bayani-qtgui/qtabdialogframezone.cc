/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogFrameZone Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogframezone.h>

QTabDialogFrameZone::QTabDialogFrameZone(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB  = new QVBox(Parent);
  General1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Main Zones"     ), GeneralQVB);
  General2QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Secondary Zones"), GeneralQVB);
  
  Sub1QVB = new QVBox(General1QGB);
  Sub2QVB = new QVBox(General1QGB);
  
  HorZoneQL  = new QLabel(trUtf8("&Horizontally"), Sub1QVB);
  HorZoneQLE = new QLineEdit(Sub2QVB);
  
  VerZoneQL  = new QLabel(trUtf8("&Vertically"), Sub1QVB);
  VerZoneQLE = new QLineEdit(Sub2QVB);
  
  SecondaryQCKB = new QCheckBox(trUtf8("&Split the already existing main zone"), General2QGB);
  ZoneQSB       = new QSpinBoxBayani(1, 1, 1, General2QGB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  
  HorZoneQL->setBuddy(HorZoneQLE);
  VerZoneQL->setBuddy(VerZoneQLE);
  
  HorZoneQLE->setMaximumWidth(50);
  HorZoneQLE->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{1,2}"), HorZoneQLE));
  VerZoneQLE->setMaximumWidth(50);
  VerZoneQLE->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{1,2}"), VerZoneQLE));
  ZoneQSB->setMaximumWidth(50);
  
  ZoneQSB->setEnabled(false);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Frame - Define Zones"));
  
  HorZoneQLE->setFocus();
  
  connect(SecondaryQCKB, SIGNAL(toggled(bool)), ZoneQSB, SLOT(setEnabled(bool)));
  connect(SecondaryQCKB, SIGNAL(clicked()    ), ZoneQSB, SLOT(setFocus()      ));
  connect(SecondaryQCKB, SIGNAL(pressed()    ), ZoneQSB, SLOT(clearFocus()    ));
}

QTabDialogFrameZone::~QTabDialogFrameZone()
{
}

QLineEdit * QTabDialogFrameZone::GetHorZoneQLE()
{
  return HorZoneQLE;
}

QLineEdit * QTabDialogFrameZone::GetVerZoneQLE()
{
  return VerZoneQLE;
}

QCheckBox * QTabDialogFrameZone::GetSecondaryQCKB()
{
  return SecondaryQCKB;
}

QSpinBoxBayani * QTabDialogFrameZone::GetZoneQSB()
{
  return ZoneQSB;
}

void QTabDialogFrameZone::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      HorZoneQLE->setAlignment(Qt::AlignRight);
      VerZoneQLE->setAlignment(Qt::AlignRight);
      ZoneQSB->GetEditor()->setAlignment(Qt::AlignRight);
    }
  else
    {
      HorZoneQLE->setAlignment(Qt::AlignLeft);
      VerZoneQLE->setAlignment(Qt::AlignLeft);
      ZoneQSB->GetEditor()->setAlignment(Qt::AlignLeft);
    }
}
