/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogGraphFunction Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGGRAPHFUNCTION_H
#define BAYANI_QTABDIALOGGRAPHFUNCTION_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qvalidator.h>
#include <qregexp.h>
#include <qpushbutton.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialoggraphfunction.h
  \brief Header file for the QTabDialogGraphFunction class.
  
  The QTabDialogGraphFunction class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to represent functions.
*/
class QTabDialogGraphFunction: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel      * ExpressionQL;
  QLabel      * XMinQL;
  QLabel      * XMaxQL;
  QLineEdit   * ExpressionQLE;
  QLineEdit   * XMinQLE;
  QLineEdit   * XMaxQLE;
  QPushButton * PlotSettingsQPB;
  QCheckBox   * SameQCKB;
  QGroupBox   * General1QGB;
  QGroupBox   * General2QGB;
  QVBox       * GeneralQVB;
  QVBox       * Sub1QVB;
  QVBox       * Sub2QVB;
  
 public:
                        QTabDialogGraphFunction(QWidget * Parent = 0, const char * Name = 0);
  virtual              ~QTabDialogGraphFunction();
          QLineEdit   * GetExpressionQLE       ();
	  QLineEdit   * GetXMinQLE             ();
	  QLineEdit   * GetXMaxQLE             ();
	  QPushButton * GetPlotSettingsQPB     ();
	  QCheckBox   * GetSameQCKB            ();
	  void          SetR2L                 (bool);
};

#endif
