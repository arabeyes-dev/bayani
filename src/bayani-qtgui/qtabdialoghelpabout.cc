/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogHelpAbout Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialoghelpabout.h>

QTabDialogHelpAbout::QTabDialogHelpAbout(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  AboutBTB   = new BTextBrowser(Parent);
  AuthorsBTB = new BTextBrowser(Parent);
  ThanksBTB  = new BTextBrowser(Parent);
  LicenseBTB = new BTextBrowser(Parent);
  
  AboutBTB  ->mimeSourceFactory()->setFilePath(QString("%1/about/") .arg(DOC_DIR));
  AuthorsBTB->mimeSourceFactory()->setFilePath(QString("%1/about/") .arg(DOC_DIR));
  ThanksBTB ->mimeSourceFactory()->setFilePath(QString("%1/about/") .arg(DOC_DIR));
  LicenseBTB->mimeSourceFactory()->setFilePath(QString("%1/about/") .arg(DOC_DIR));
  
  AboutBTB  ->setSource("about.html"  );
  AuthorsBTB->setSource("authors.html");
  ThanksBTB ->setSource("thanks.html" );
  LicenseBTB->setSource("license.html");
  
  addTab(AboutBTB  , trUtf8("&About"  ));
  addTab(AuthorsBTB, trUtf8("A&uthors"));
  addTab(ThanksBTB , trUtf8("&Thanks" ));
  addTab(LicenseBTB, trUtf8("&License"));
  
  setOKButton(trUtf8("&OK"));
  
  setCaption(trUtf8("Help - About Bayani (Using QT %1)") .arg(QT_VERSION_STR));
  
  AboutBTB->setFocus();
}

QTabDialogHelpAbout::~QTabDialogHelpAbout()
{
}

void QTabDialogHelpAbout::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      AboutBTB  ->setAlignment(Qt::AlignRight);
      AuthorsBTB->setAlignment(Qt::AlignRight);
      ThanksBTB ->setAlignment(Qt::AlignRight);
      LicenseBTB->setAlignment(Qt::AlignRight);
    }
  else
    {
      AboutBTB  ->setAlignment(Qt::AlignLeft);
      AuthorsBTB->setAlignment(Qt::AlignLeft);
      ThanksBTB ->setAlignment(Qt::AlignLeft);
      LicenseBTB->setAlignment(Qt::AlignLeft);
    }
}

void QTabDialogHelpAbout::showEvent(QShowEvent *)
{
  QRect Geometry = geometry();
  
  Geometry.setSize(0.5*parentWidget()->geometry().size());
  
  setGeometry(Geometry);
}
