/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogFrameSetActivePlot Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGFRAMESETACTIVEPLOT_H
#define BAYANI_QTABDIALOGFRAMESETACTIVEPLOT_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qspinboxbayani.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialogframesetactiveplot.h
  \brief Header file for the QTabDialogFrameSetActivePlot class.
  
  The QTabDialogFrameSetActivePlot class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set the active plot.
*/
class QTabDialogFrameSetActivePlot: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel         * ZoneQL;
  QSpinBoxBayani * ZoneQSB;
  QGroupBox      * GeneralQGB;
  QVBox          * GeneralQVB;
  QVBox          * Sub1QVB;
  QVBox          * Sub2QVB;
  
 public:
                           QTabDialogFrameSetActivePlot(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogFrameSetActivePlot();
          QSpinBoxBayani * GetZoneQSB                  ();
	  void             SetR2L                      (bool);
};

#endif
