/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogTextsFrame Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogtextsframe.h>

QTabDialogTextsFrame::QTabDialogTextsFrame(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB = new QVBox(this);
  GeneralQGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Texts"), GeneralQVB);
  
  Sub1QVB = new QVBox(GeneralQGB);
  Sub2QVB = new QVBox(GeneralQGB);
  
  TitleQL  = new QLabel(trUtf8("&Title"), Sub1QVB);
  TitleQLE = new QLineEdit(Sub2QVB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  
  TitleQL->setBuddy(TitleQLE);
  
  TitleQLE->setMaximumWidth(200);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Texts - Frame"));
  
  TitleQLE->setFocus();
}

QTabDialogTextsFrame::~QTabDialogTextsFrame()
{
}

QLineEdit * QTabDialogTextsFrame::GetTitleQLE()
{
  return TitleQLE;
}

void QTabDialogTextsFrame::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      TitleQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      TitleQLE->setAlignment(Qt::AlignLeft);
    }
}
