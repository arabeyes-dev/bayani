/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogGraphGraph2D Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGGRAPHGRAPH2D_H
#define BAYANI_QTABDIALOGGRAPHGRAPH2D_H

/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <qtabdialog.h>
#include <qtabbar.h>
#include <qhbox.h>
#include <qvbox.h>
#include <qgroupbox.h>
#include <qbuttongroup.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qcheckbox.h>
#include <qradiobutton.h>
#include <qspinboxbayani.h>

/*!
  \file qtabdialoggraphgraph2d.h
  \brief Header file for the QTabDialogGraphGraph2D class.
  
  The QTabDialogGraphGraph2D class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to represent 2D graphs.
*/
class QTabDialogGraphGraph2D: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QHBox          * Data11QHB;
  QHBox          * Data12QHB;
  QHBox          * Data21QHB;
  QHBox          * Errors11QHB;
  QHBox          * Errors12QHB;
  QHBox          * Errors21QHB;
  QHBox          * Errors22QHB;
  QVBox          * DataQVB;
  QVBox          * Data111QVB;
  QVBox          * Data112QVB;
  QVBox          * Data121QVB;
  QVBox          * Data122QVB;
  QVBox          * Data211QVB;
  QVBox          * Data212QVB;
  QVBox          * ErrorsQVB;
  QVBox          * Errors111QVB;
  QVBox          * Errors112QVB;
  QVBox          * Errors121QVB;
  QVBox          * Errors122QVB;
  QVBox          * Errors211QVB;
  QVBox          * Errors212QVB;
  QVBox          * Errors221QVB;
  QVBox          * Errors222QVB;
  QGroupBox      * Data1QGB;
  QGroupBox      * Data2QGB;
  QGroupBox      * Errors1QGB;
  QGroupBox      * Errors2QGB;
  QButtonGroup   * Data11QBG;
  QButtonGroup   * Data12QBG;
  QButtonGroup   * Errors11QBG;
  QButtonGroup   * Errors12QBG;
  QButtonGroup   * Errors21QBG;
  QButtonGroup   * Errors22QBG;
  QLabel         * ColumnXQL;
  QLabel         * ExpressionXQL;
  QLabel         * ColumnYQL;
  QLabel         * ExpressionYQL;
  QLabel         * ColumnDownXQL;
  QLabel         * ExpressionDownXQL;
  QLabel         * ColumnDownYQL;
  QLabel         * ExpressionDownYQL;
  QLabel         * ColumnUpXQL;
  QLabel         * ExpressionUpXQL;
  QLabel         * ColumnUpYQL;
  QLabel         * ExpressionUpYQL;
  QLineEdit      * ExpressionXQLE;
  QLineEdit      * ExpressionYQLE;
  QLineEdit      * CutQLE;
  QLineEdit      * ExpressionDownXQLE;
  QLineEdit      * ExpressionDownYQLE;
  QLineEdit      * ExpressionUpXQLE;
  QLineEdit      * ExpressionUpYQLE;
  QPushButton    * PlotSettingsQPB;
  QCheckBox      * ErrorsQCKB;
  QCheckBox      * CutQCKB;
  QCheckBox      * SameQCKB;
  QCheckBox      * ErrorsDownXQCKB;
  QCheckBox      * ErrorsDownYQCKB;
  QCheckBox      * ErrorsUpXQCKB;
  QCheckBox      * ErrorsUpYQCKB;
  QCheckBox      * AsymmetricQCKB;
  QRadioButton   * ColumnXQRB;
  QRadioButton   * ExpressionXQRB;
  QRadioButton   * ColumnYQRB;
  QRadioButton   * ExpressionYQRB;
  QRadioButton   * ColumnDownXQRB;
  QRadioButton   * ExpressionDownXQRB;
  QRadioButton   * ColumnDownYQRB;
  QRadioButton   * ExpressionDownYQRB;
  QRadioButton   * ColumnUpXQRB;
  QRadioButton   * ExpressionUpXQRB;
  QRadioButton   * ColumnUpYQRB;
  QRadioButton   * ExpressionUpYQRB;
  QSpinBoxBayani * ColumnXQSB;
  QSpinBoxBayani * ColumnYQSB;
  QSpinBoxBayani * ColumnDownXQSB;
  QSpinBoxBayani * ColumnDownYQSB;
  QSpinBoxBayani * ColumnUpXQSB;
  QSpinBoxBayani * ColumnUpYQSB;
  
 public:
                           QTabDialogGraphGraph2D(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogGraphGraph2D();
          QLineEdit      * GetExpressionXQLE     ();
	  QLineEdit      * GetExpressionYQLE     ();
	  QLineEdit      * GetCutQLE             ();
	  QLineEdit      * GetExpressionDownXQLE ();
	  QLineEdit      * GetExpressionDownYQLE ();
	  QLineEdit      * GetExpressionUpXQLE   ();
	  QLineEdit      * GetExpressionUpYQLE   ();
	  QPushButton    * GetPlotSettingsQPB    ();
	  QCheckBox      * GetErrorsQCKB         ();
	  QCheckBox      * GetCutQCKB            ();
	  QCheckBox      * GetSameQCKB           ();
	  QCheckBox      * GetErrorsDownXQCKB    ();
	  QCheckBox      * GetErrorsDownYQCKB    ();
	  QCheckBox      * GetErrorsUpXQCKB      ();
	  QCheckBox      * GetErrorsUpYQCKB      ();
	  QCheckBox      * GetAsymmetricQCKB     ();
	  QRadioButton   * GetColumnXQRB         ();
	  QRadioButton   * GetExpressionXQRB     ();
	  QRadioButton   * GetColumnYQRB         ();
	  QRadioButton   * GetExpressionYQRB     ();
	  QRadioButton   * GetColumnDownXQRB     ();
	  QRadioButton   * GetExpressionDownXQRB ();
	  QRadioButton   * GetColumnDownYQRB     ();
	  QRadioButton   * GetExpressionDownYQRB ();
	  QRadioButton   * GetColumnUpXQRB       ();
	  QRadioButton   * GetExpressionUpXQRB   ();
	  QRadioButton   * GetColumnUpYQRB       ();
	  QRadioButton   * GetExpressionUpYQRB   ();
	  QSpinBoxBayani * GetColumnXQSB         ();
	  QSpinBoxBayani * GetColumnYQSB         ();
	  QSpinBoxBayani * GetColumnDownXQSB     ();
	  QSpinBoxBayani * GetColumnDownYQSB     ();
	  QSpinBoxBayani * GetColumnUpXQSB       ();
	  QSpinBoxBayani * GetColumnUpYQSB       ();
	  void             SetR2L                (bool);
	  
 public slots:
  void ErrorsChangedSlot    (bool);
  void AsymmetricChangedSlot(bool);
};

#endif
