/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogComputationCompute Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGCOMPUTATIONCOMPUTE_H
#define BAYANI_QTABDIALOGCOMPUTATIONCOMPUTE_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qtextedit.h>
#include <qradiobutton.h>
#include <qgroupbox.h>
#include <qbuttongroup.h>
#include <qpushbutton.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialogcomputationcompute.h
  \brief Header file for the QTabDialogComputationCompute class.
  
  The QTabDialogComputationCompute class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to perform symbolic computation.
*/
class QTabDialogComputationCompute: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QVBox        * GeneralQVB;
  QGroupBox    * General1QGB;
  QGroupBox    * General2QGB;
  QButtonGroup * GeneralQBG;
  QLabel       * ExpressionQL;
  QLineEdit    * ExpressionQLE;
  QRadioButton * SimplifyQRB;
  QRadioButton * EvaluateQRB;
  QRadioButton * ResolveQRB;
  QRadioButton * DeriveQRB;
  QRadioButton * IntegrateQRB;
  QTextEdit    * OutputQTE;
  QPushButton  * VariablesQPB;
  
 public:
                           QTabDialogComputationCompute(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogComputationCompute();
          QLineEdit      * GetExpressionQLE            ();
	  QRadioButton   * GetSimplifyQRB              ();
	  QRadioButton   * GetEvaluateQRB              ();
	  QRadioButton   * GetResolveQRB               ();
	  QRadioButton   * GetDeriveQRB                ();
	  QRadioButton   * GetIntegrateQRB             ();
	  QTextEdit      * GetOutputQTE                ();
	  QPushButton    * GetVariablesQPB             ();
	  void             SetR2L                      (bool);
};

#endif
