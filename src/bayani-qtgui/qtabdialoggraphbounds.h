/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogGraphBounds Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGGRAPHBOUNDS_H
#define BAYANI_QTABDIALOGGRAPHBOUNDS_H

/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <qlineedit.h>
#include <qvalidator.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialoggraphbounds.h
  \brief Header file for the QTabDialogGraphBounds class.
  
  The QTabDialogGraphBounds class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set graphs' bounds.
*/
class QTabDialogGraphBounds: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLineEdit * XInfQLE;
  QLineEdit * XSupQLE;
  QLineEdit * YInfQLE;
  QLineEdit * YSupQLE;
  QCheckBox * XInfQCKB;
  QCheckBox * XSupQCKB;
  QCheckBox * YInfQCKB;
  QCheckBox * YSupQCKB;
  QCheckBox * NewQCKB;
  QGroupBox * GeneralXQGB;
  QGroupBox * GeneralYQGB;
  QVBox     * GeneralQVB;
  QVBox     * Sub1QVB;
  QVBox     * Sub2QVB;
  QVBox     * Sub3QVB;
  QVBox     * Sub4QVB;
  
 public:
                      QTabDialogGraphBounds(QWidget * Parent = 0, const char * Name = 0);
  virtual            ~QTabDialogGraphBounds();
          QLineEdit * GetXInfQLE           ();
	  QLineEdit * GetXSupQLE           ();
	  QLineEdit * GetYInfQLE           ();
	  QLineEdit * GetYSupQLE           ();
	  QCheckBox * GetXInfQCKB          ();
	  QCheckBox * GetXSupQCKB          ();
	  QCheckBox * GetYInfQCKB          ();
	  QCheckBox * GetYSupQCKB          ();
	  QCheckBox * GetNewQCKB           ();
	  void        SetR2L               (bool);
};

#endif
