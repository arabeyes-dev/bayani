/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogTableWriteToFile Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogtablewritetofile.h>

QTabDialogTableWriteToFile::QTabDialogTableWriteToFile(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB  = new QVBox(Parent);
  General1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("File"), GeneralQVB);
  Sub1QHB     = new QHBox(GeneralQVB);
  General2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Options"), GeneralQVB);
  
  FileNameQL   = new QLabel(trUtf8("&File name"), General1QGB);
  FileNameQFLS = new QFileSelector(General1QGB);
  FileNameQFLS->AddFileType(trUtf8("Text files (*.txt)"));
  FileNameQFLS->SetOpenType(false);
  
  Sub1QGB    = new QGroupBox(2, Qt::Horizontal, trUtf8("Cell 1 (included)"), Sub1QHB);
  Sub2QGB    = new QGroupBox(2, Qt::Horizontal, trUtf8("Cell 2 (included)"), Sub1QHB);
  
  Sub1QVB  = new QVBox(Sub1QGB);
  Sub2QVB  = new QVBox(Sub1QGB);
  Sub3QVB  = new QVBox(Sub2QGB);
  Sub4QVB  = new QVBox(Sub2QGB);
  
  Row1QL  = new QLabel(trUtf8("&Row"), Sub1QVB);
  Row1QSB = new QSpinBoxBayani(1, 1, 1, Sub2QVB);
  
  Column1QL  = new QLabel(trUtf8("Co&lumn"), Sub1QVB);
  Column1QSB = new QSpinBoxBayani(1, 1, 1, Sub2QVB);
  
  Row2QL  = new QLabel(trUtf8("Ro&w"), Sub3QVB);
  Row2QSB = new QSpinBoxBayani(1, 1, 1, Sub4QVB);
  
  Column2QL  = new QLabel(trUtf8("Colu&mn"), Sub3QVB);
  Column2QSB = new QSpinBoxBayani(1, 1, 1, Sub4QVB);
  
  Sub2QHB = new QHBox(General2QGB);
  
  Sub5QVB  = new QVBox(Sub2QHB);
  Sub6QVB  = new QVBox(Sub2QHB);
  
  SeparatorQL  = new QLabel(trUtf8("Columns &separator"), Sub5QVB);
  SeparatorQLE = new QLineEdit(Sub6QVB);
  
  AppendQCKB = new QCheckBox(trUtf8("A&ppend existing file"), Sub5QVB);
  AppendQL   = new QLabel("", Sub6QVB);/* In order to make a nice layout only */
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub1QHB->setSpacing(5);
  Sub1QHB->setMargin(5);
  Sub2QHB->setSpacing(5);
  Sub2QHB->setMargin(5);
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  Sub3QVB->setSpacing(5);
  Sub3QVB->setMargin(5);
  Sub4QVB->setSpacing(5);
  Sub4QVB->setMargin(5);
  Sub5QVB->setSpacing(5);
  Sub5QVB->setMargin(5);
  Sub6QVB->setSpacing(5);
  Sub6QVB->setMargin(5);
  
  FileNameQL ->setBuddy(FileNameQFLS);
  Row1QL     ->setBuddy(Row1QSB     );
  Column1QL  ->setBuddy(Column1QSB  );
  Row2QL     ->setBuddy(Row2QSB     );
  Column2QL  ->setBuddy(Column2QSB  );
  SeparatorQL->setBuddy(SeparatorQLE);
  
  Row1QSB->setMaximumWidth(50);
  Column1QSB->setMaximumWidth(50);
  Row2QSB->setMaximumWidth(50);
  Column2QSB->setMaximumWidth(50);
  SeparatorQLE->setMaximumWidth(50);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Table - Write to File"));
  
  Row1QSB->setFocus();
  
  connect(Row1QSB   , SIGNAL(valueChanged(int)), this, SLOT(Row1QSBChangedSlot(int)));
  connect(Column1QSB, SIGNAL(valueChanged(int)), this, SLOT(Column1QSBChangedSlot(int)));
  connect(Row2QSB   , SIGNAL(valueChanged(int)), this, SLOT(Row2QSBChangedSlot(int)));
  connect(Column2QSB, SIGNAL(valueChanged(int)), this, SLOT(Column2QSBChangedSlot(int)));
}

QTabDialogTableWriteToFile::~QTabDialogTableWriteToFile()
{
}

QFileSelector * QTabDialogTableWriteToFile::GetFileNameQFLS()
{
  return FileNameQFLS;
}

QSpinBoxBayani * QTabDialogTableWriteToFile::GetRow1QSB()
{
  return Row1QSB;
}

QSpinBoxBayani * QTabDialogTableWriteToFile::GetColumn1QSB()
{
  return Column1QSB;
}

QSpinBoxBayani * QTabDialogTableWriteToFile::GetRow2QSB()
{
  return Row2QSB;
}

QSpinBoxBayani * QTabDialogTableWriteToFile::GetColumn2QSB()
{
  return Column2QSB;
}

QCheckBox * QTabDialogTableWriteToFile::GetAppendQCKB()
{
  return AppendQCKB;
}

QLineEdit * QTabDialogTableWriteToFile::GetSeparatorQLE()
{
  return SeparatorQLE;
}

void QTabDialogTableWriteToFile::SetR2L(bool R2LFlag)
{
  FileNameQFLS->SetR2L(R2LFlag);
  
  if(R2LFlag)
    {
      Row1QSB->GetEditor()->setAlignment(Qt::AlignRight);
      Column1QSB->GetEditor()->setAlignment(Qt::AlignRight);
      Row2QSB->GetEditor()->setAlignment(Qt::AlignRight);
      Column2QSB->GetEditor()->setAlignment(Qt::AlignRight);
      SeparatorQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      Row1QSB->GetEditor()->setAlignment(Qt::AlignLeft);
      Column1QSB->GetEditor()->setAlignment(Qt::AlignLeft);
      Row2QSB->GetEditor()->setAlignment(Qt::AlignLeft);
      Column2QSB->GetEditor()->setAlignment(Qt::AlignLeft);
      SeparatorQLE->setAlignment(Qt::AlignLeft);
    }
}

void QTabDialogTableWriteToFile::Row1QSBChangedSlot(int Value)
{
  if(Row2QSB->value() < Value)
    Row2QSB->setValue(Value);
}

void QTabDialogTableWriteToFile::Column1QSBChangedSlot(int Value)
{
  if(Column2QSB->value() < Value)
    Column2QSB->setValue(Value);
}

void QTabDialogTableWriteToFile::Row2QSBChangedSlot(int Value)
{
  if(Row1QSB->value() > Value)
    Row1QSB->setValue(Value);
}

void QTabDialogTableWriteToFile::Column2QSBChangedSlot(int Value)
{
  if(Column1QSB->value() > Value)
    Column1QSB->setValue(Value);
}
