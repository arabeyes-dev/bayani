/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogTextsFrame Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGTEXTSFRAME_H
#define BAYANI_QTABDIALOGTEXTSFRAME_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialogtextsframe.h
  \brief Header file for the QTabDialogTextsFrame class.
  
  The QTabDialogTextsFrame class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set the frames' texts.
*/
class QTabDialogTextsFrame: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel    * TitleQL;
  QLineEdit * TitleQLE;
  QGroupBox * GeneralQGB;
  QVBox     * GeneralQVB;
  QVBox     * Sub1QVB;
  QVBox     * Sub2QVB;
  
 public:
                      QTabDialogTextsFrame(QWidget * Parent = 0, const char * Name = 0);
  virtual            ~QTabDialogTextsFrame();
          QLineEdit * GetTitleQLE         ();
	  void        SetR2L              (bool);
};

#endif
