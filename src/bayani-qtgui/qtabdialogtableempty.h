/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogTableEmpty Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGTABLEEMPTY_H
#define BAYANI_QTABDIALOGTABLEEMPTY_H

/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qspinboxbayani.h>
#include <qgroupbox.h>
#include <qhbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialogtableempty.h
  \brief Header file for the QTabDialogTableEmpty class.
  
  The QTabDialogTableEmpty class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to empty table's cells.
*/
class QTabDialogTableEmpty: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel         * Row1QL;
  QLabel         * Column1QL;
  QLabel         * Row2QL;
  QLabel         * Column2QL;
  QSpinBoxBayani * Row1QSB;
  QSpinBoxBayani * Column1QSB;
  QSpinBoxBayani * Row2QSB;
  QSpinBoxBayani * Column2QSB;
  QGroupBox      * Sub1QGB;
  QGroupBox      * Sub2QGB;
  QVBox          * Sub1QVB;
  QVBox          * Sub2QVB;
  QVBox          * Sub3QVB;
  QVBox          * Sub4QVB;
  QHBox          * GeneralQHB;
  
 public:
                           QTabDialogTableEmpty(QWidget * Parent = 0, const char * Name = 0);
  virtual                 ~QTabDialogTableEmpty();
          QSpinBoxBayani * GetRow1QSB          ();
	  QSpinBoxBayani * GetColumn1QSB       ();
	  QSpinBoxBayani * GetRow2QSB          ();
	  QSpinBoxBayani * GetColumn2QSB       ();
	  void             SetR2L              (bool);
	  
 public slots:
  void Row1QSBChangedSlot   (int);
  void Column1QSBChangedSlot(int);
  void Row2QSBChangedSlot   (int);
  void Column2QSBChangedSlot(int);
};

#endif
