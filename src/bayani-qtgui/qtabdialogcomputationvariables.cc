/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogComputationVariables Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogcomputationvariables.h>
#include <gui.h>

QTabDialogComputationVariables::QTabDialogComputationVariables(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  setCaption(trUtf8("Computation - Variables"));
  
  //setOKButton    (trUtf8("&OK"    ));
  //setHelpButton  (trUtf8("&Help"  ));
  //setApplyButton (trUtf8("&Apply" ));
  //setCancelButton(trUtf8("&Cancel"));
  
  setOKButton    (trUtf8("&Close"));
  setHelpButton  (trUtf8("&Help" ));
  
  VariablesQVB = new QVBox(Parent);
  VariablesQVB->setSpacing(5);
  VariablesQVB->setMargin(5);
  
  addTab(VariablesQVB, trUtf8("&Variables"));
  
  Variables1QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Variables List"), VariablesQVB);
  
  VariablesQLV = new QListView(Variables1QGB);
  VariablesQLV->addColumn(trUtf8("Variable"));
  VariablesQLV->addColumn(trUtf8("Value"   ));
  VariablesQLV->setSelectionMode(QListView::Extended);
  VariablesQLV->setMinimumWidth (300);
  VariablesQLV->setMinimumHeight(200);
  
  Variables2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Add"), VariablesQVB);
  
  VariablesQHB = new QHBox(Variables2QGB);
  VariablesQHB->setSpacing(5);
  VariablesQHB->setMargin(5);
  
  Variables11QVB = new QVBox(VariablesQHB);
  Variables11QVB->setSpacing(5);
  Variables11QVB->setMargin(5);
  
  Variables12QVB = new QVBox(VariablesQHB);
  Variables12QVB->setSpacing(5);
  Variables12QVB->setMargin(5);
  
  VariablesNameQL  = new QLabel(trUtf8("&Name"), Variables11QVB);
  VariablesNameQLE = new QLineEdit(Variables12QVB);
  //VariablesNameQLE->setMaximumWidth(50);
  VariablesNameQLE->setValidator(new QRegExpValidator(QRegExp("\\w+"), VariablesNameQLE));
  
  VariablesValueQL  = new QLabel(trUtf8("Va&lue"), Variables11QVB);
  VariablesValueQLE = new QLineEdit(Variables12QVB);
  //VariablesValueQLE->setMaximumWidth(50);
  VariablesValueQLE->setValidator(new QDoubleValidator(VariablesValueQLE));
  
  VariablesNameQL ->setBuddy(VariablesNameQLE );
  VariablesValueQL->setBuddy(VariablesValueQLE);
  
  AddVariablesQPB = new QPushButton(trUtf8("A&dd to list"), Variables2QGB);
  
  Variables3QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Remove"), VariablesQVB);
  
  RemoveVariablesQPB = new QPushButton(trUtf8("&Remove selection"), Variables3QGB);
  
  AddVariablesQPB   ->setFixedSize(AddVariablesQPB   ->sizeHint());
  RemoveVariablesQPB->setFixedSize(RemoveVariablesQPB->sizeHint());
  
  VariablesNameChangedSlot("");
  RemoveVariablesQPB->setEnabled(false);
  
  setFixedSize(sizeHint());
  
  VariablesQLV->setFocus();
  
  connect(VariablesQLV      , SIGNAL(selectionChanged(               )), this, SLOT(VariablesSelectionChangedSlot(               )));
  connect(VariablesNameQLE  , SIGNAL(textChanged     (const QString &)), this, SLOT(VariablesNameChangedSlot     (const QString &)));
  connect(AddVariablesQPB   , SIGNAL(clicked         (               )), this, SLOT(AddVariablesSlot             (               )));
  connect(RemoveVariablesQPB, SIGNAL(clicked         (               )), this, SLOT(RemoveVariablesSlot          (               )));
}

QTabDialogComputationVariables::~QTabDialogComputationVariables()
{
}

QListView * QTabDialogComputationVariables::GetVariablesQLV()
{
  return VariablesQLV;
}

QLineEdit * QTabDialogComputationVariables::GetVariablesNameQLE()
{
  return VariablesNameQLE;
}

QLineEdit * QTabDialogComputationVariables::GetVariablesValueQLE()
{
  return VariablesValueQLE;
}

QPushButton * QTabDialogComputationVariables::GetAddVariablesQPB()
{
  return AddVariablesQPB;
}

QPushButton * QTabDialogComputationVariables::GetRemoveVariablesQPB()
{
  return RemoveVariablesQPB;
}

vector <Variable> QTabDialogComputationVariables::GetVariables()
{
  return Variables;
}

void QTabDialogComputationVariables::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      VariablesNameQLE ->setAlignment(Qt::AlignRight);
      VariablesValueQLE->setAlignment(Qt::AlignRight);
    }
  else
    {
      VariablesNameQLE ->setAlignment(Qt::AlignLeft);
      VariablesValueQLE->setAlignment(Qt::AlignLeft);
    }
}

void QTabDialogComputationVariables::VariablesSelectionChangedSlot()
{
  QListViewItemIterator Iterator(VariablesQLV);
  
  while(Iterator.current())
    {
      if(Iterator.current()->isSelected())
	{
	  VariablesNameQLE ->setText (Iterator.current()->text(0));
	  VariablesValueQLE->setText (Iterator.current()->text(1));
	  VariablesValueQLE->setFocus(                           );
	  
	  RemoveVariablesQPB->setEnabled(true);
	  
	  return;
	}
      
      Iterator++;
    }
  
  RemoveVariablesQPB->setEnabled(false);
}

void  QTabDialogComputationVariables::VariablesNameChangedSlot(const QString & NewText)
{
  if(NewText == "")
    {
      VariablesValueQL ->setEnabled(false);
      VariablesValueQLE->setEnabled(false);
      AddVariablesQPB  ->setEnabled(false);
    }
  else
    {
      VariablesValueQL ->setEnabled(true);
      VariablesValueQLE->setEnabled(true);
      AddVariablesQPB  ->setEnabled(true);
    }
}

void QTabDialogComputationVariables::AddVariablesSlot()
{
  bool FoundFlag = false;
  
  QListViewItemIterator Iterator(VariablesQLV);
  
  while(Iterator.current())
    {
      if(Iterator.current()->text(0) == VariablesNameQLE->text())
	{
	  Iterator.current()->setText(1, VariablesValueQLE->text());
	  
	  FoundFlag = true;
	  
	  break;
	}
      
      Iterator++;
    }
  
  if(FoundFlag)
    for(int i = 0; i < Variables.size(); i++)
      if(Variables[i].Name == VariablesNameQLE->text())
	{
	  Variables[i].Value = VariablesValueQLE->text().toDouble();
	  
	  emit applyButtonPressed();
	  
	  return;
	}
  
  Variable NewVariable;
  NewVariable.Name  = VariablesNameQLE ->text();
  NewVariable.Value = VariablesValueQLE->text().toDouble();
  Variables.push_back(NewVariable);
  
  new QListViewItem(VariablesQLV, VariablesNameQLE->text(), VariablesValueQLE->text());
  
  emit applyButtonPressed();
}

void QTabDialogComputationVariables::RemoveVariablesSlot()
{
  if(GUI::PrintMessage(this, MESSAGE_WARNING, trUtf8("Are you sure you want to remove the selected items?")) == QMessageBox::No)
    return;
  
  Variables.clear();
  
  QListViewItemIterator Iterator(VariablesQLV);
  
  vector <QListViewItem *> ItemsToDelete;
  
  while(Iterator.current())
    {
      if(Iterator.current()->isSelected())
	ItemsToDelete.push_back(Iterator.current());
      else
	{
	  Variable NewVariable;
	  NewVariable.Name  = Iterator.current()->text(0);
	  NewVariable.Value = Iterator.current()->text(1).toDouble();
	  Variables.push_back(NewVariable);
    	}
      
      Iterator++;
    }
  
  for(int i = 0; i < ItemsToDelete.size(); i++)
    VariablesQLV->takeItem(ItemsToDelete[i]);
  
  RemoveVariablesQPB->setEnabled(false);
  
  emit applyButtonPressed();
}
