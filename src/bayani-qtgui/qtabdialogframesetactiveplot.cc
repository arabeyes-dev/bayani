/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogFrameSetActivePlot Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogframesetactiveplot.h>

QTabDialogFrameSetActivePlot::QTabDialogFrameSetActivePlot(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB = new QVBox(Parent);
  GeneralQGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Graph"), GeneralQVB);
  
  Sub1QVB = new QVBox(GeneralQGB);
  Sub2QVB = new QVBox(GeneralQGB);
  
  ZoneQL  = new QLabel(trUtf8("&Zone"), Sub1QVB);
  ZoneQSB = new QSpinBoxBayani(1, 1, 1, Sub2QVB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  
  ZoneQL->setBuddy(ZoneQSB);
  
  ZoneQSB->setMaximumWidth(50);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Frame - Set Active Graph"));
  
  ZoneQSB->setFocus();
}

QTabDialogFrameSetActivePlot::~QTabDialogFrameSetActivePlot()
{
}

QSpinBoxBayani * QTabDialogFrameSetActivePlot::GetZoneQSB()
{
  return ZoneQSB;
}

void QTabDialogFrameSetActivePlot::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      ZoneQSB->GetEditor()->setAlignment(Qt::AlignRight);
    }
  else
    {
      ZoneQSB->GetEditor()->setAlignment(Qt::AlignLeft);
    }
}
