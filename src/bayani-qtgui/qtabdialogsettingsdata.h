/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogSettingsData Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGSETTINGSDATA_H
#define BAYANI_QTABDIALOGSETTINGSDATA_H

using namespace std;

/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <qtabdialog.h>
#include <qtabbar.h>
#include <qvbox.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qvalidator.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qslider.h>
/* Bayani headers */
#include <bbgo.h>
#include <fitscontainer.h>
#include <qspinboxbayani.h>
#include <qlinecombo.h>
#include <qpushbuttoncolorselect.h>
#include <qpencombo.h>
#include <qbrushcombo.h>

/*!
  \file qtabdialogsettingsdata.h
  \brief Header file for the QTabDialogSettingsData class.
  
  The QTabDialogSettingsData class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set data's options.
*/
class QTabDialogSettingsData: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QVBox * FunctionQVB;
  QVBox * Function11QVB;
  QVBox * Function12QVB;
  QVBox * HistogramQVB;
  QVBox * Histogram11QVB;
  QVBox * Histogram12QVB;
  QVBox * Histogram21QVB;
  QVBox * Histogram22QVB;
  QVBox * Plot2DQVB;
  QVBox * Plot2D11QVB;
  QVBox * Plot2D12QVB;
  QVBox * Plot2D21QVB;
  QVBox * Plot2D22QVB;
  QVBox * FITSQVB;
  QVBox * FITS11QVB;
  QVBox * FITS12QVB;
  QVBox * FITS21QVB;
  QVBox * FITS22QVB;
  QVBox * FITS23QVB;
  
  QGroupBox * Function1QGB;
  QGroupBox * Histogram1QGB;
  QGroupBox * Histogram2QGB;
  QGroupBox * Plot2D1QGB;
  QGroupBox * Plot2D2QGB;
  QGroupBox * FITS1QGB;
  QGroupBox * FITS2QGB;
  
  QLabel * FunctionLineWidthQL;
  QLabel * FunctionLineStyleQL;
  QLabel * FunctionLineColorQL;
  QLabel * HistogramLineWidthQL;
  QLabel * HistogramLineStyleQL;
  QLabel * HistogramLineColorQL;
  QLabel * HistogramFillStyleQL;
  QLabel * HistogramFillColorQL;
  QLabel * DataRatioQL;
  QLabel * DataColorQL;
  QLabel * DataTypeQL;
  QLabel * ErrorsLineWidthQL;
  QLabel * ErrorsLineStyleQL;
  QLabel * ErrorsLineColorQL;
  QLabel * PaletteQL;
  
  QLineEdit * DataRatioQLE;
  
  QDoubleValidator * DataRatioQV;
  
  QCheckBox * MinDynQCKB;
  QCheckBox * MaxDynQCKB;
  
  QComboBox * DataTypeQCB;
  QComboBox * PaletteQCB;
  
  QSlider * MinDynQSD;
  QSlider * MaxDynQSD;
  
  QSpinBoxBayani * MinDynQSB;
  QSpinBoxBayani * MaxDynQSB;
  
  QLineCombo * FunctionLineWidthQLC;
  QLineCombo * HistogramLineWidthQLC;
  QLineCombo * ErrorsLineWidthQLC;
  
  QPushButtonColorSelect * FunctionLineColorQPBCS;
  QPushButtonColorSelect * HistogramLineColorQPBCS;
  QPushButtonColorSelect * HistogramFillColorQPBCS;
  QPushButtonColorSelect * DataColorQPBCS;
  QPushButtonColorSelect * ErrorsLineColorQPBCS;
  
  QPenCombo * FunctionLineStyleQPC;
  QPenCombo * HistogramLineStyleQPC;
  QPenCombo * ErrorsLineStyleQPC;
  
  QBrushCombo * HistogramFillStyleQBC;
  
 public:
                                   QTabDialogSettingsData    (QWidget * Parent = 0, const char * Name = 0);
  virtual                         ~QTabDialogSettingsData    ();
          QLineEdit              * GetDataRatioQLE           ();
	  QCheckBox              * GetMinDynQCKB             ();
	  QCheckBox              * GetMaxDynQCKB             ();
	  QComboBox              * GetDataTypeQCB            ();
	  QComboBox              * GetPaletteQCB             ();
	  QSlider                * GetMinDynQSD              ();
	  QSlider                * GetMaxDynQSD              ();
	  QLineCombo             * GetFunctionLineWidthQLC   ();
	  QLineCombo             * GetHistogramLineWidthQLC  ();
	  QLineCombo             * GetErrorsLineWidthQLC     ();
	  QPushButtonColorSelect * GetFunctionLineColorQPBCS ();
	  QPushButtonColorSelect * GetHistogramLineColorQPBCS();
	  QPushButtonColorSelect * GetHistogramFillColorQPBCS();
	  QPushButtonColorSelect * GetDataColorQPBCS         ();
	  QPushButtonColorSelect * GetErrorsLineColorQPBCS   ();
	  QPenCombo              * GetFunctionLineStyleQPC   ();
	  QPenCombo              * GetHistogramLineStyleQPC  ();
	  QPenCombo              * GetErrorsLineStyleQPC     ();
	  QBrushCombo            * GetHistogramFillStyleQBC  ();
	  void                     SetR2L                    (bool);
	  void                     SetCurrentTab             (int);
	  int                      GetCurrentTabID           ();
	  
 public slots:
  void MinDynQSDChangedSlot(int);
  void MaxDynQSDChangedSlot(int);
};

#endif
