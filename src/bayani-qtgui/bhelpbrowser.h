/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the BHelpBrowser Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_BHELPBROWSER_H
#define BAYANI_BHELPBROWSER_H

/* QT headers */
#include <qnamespace.h>
#include <qstring.h>
#include <qmime.h>
#include <qwidget.h>
#include <qmainwindow.h>
#include <qdockwindow.h>
#include <qtabwidget.h>
#include <qlistview.h>
#include <qstatusbar.h>
#include <qpixmap.h>
#include <qaction.h>
#include <qpopupmenu.h>
#include <qtoolbar.h>
#include <qmenubar.h>
#include <qiconset.h>
#include <qkeysequence.h>
#include <qfile.h>
#include <qxml.h>
/* Bayani headers */
#include <btextbrowser.h>
#include <blistviewitem.h>
#include <bxmltoqlvhandler.h>

/*!
  \file bhelpbrowser.h
  \brief Header file for the BHelpBrowser class.
  
  The BHelpBrowser class is defined in this file.
*/

//! A help browser for Bayani's GUI.
/*!
  This class inherits QMainWindow. It defines a basic help browser.
*/
class BHelpBrowser: public QMainWindow
{
  Q_OBJECT
  
 protected:
  QAction * FileQuitQA;
  QAction * GoBackwardQA;
  QAction * GoForwardQA;
  QAction * GoHomeQA;
  
  QPopupMenu * FileMenuQPM;
  QPopupMenu * GoMenuQPM;
  
  QToolBar * ToolBarQTB;
  
  BTextBrowser * TextBrowserBTB;
  QDockWindow  * DockWindowQDW;
  QTabWidget   * TabWidgetQTW;
  QListView    * ContentsQLV;
  QListView    * GlossaryQLV;
  
 public:
               BHelpBrowser(QWidget * Parent = 0, const char * Name = 0);
  virtual     ~BHelpBrowser();
          void SetIndex    (QString);
	  void SetFilePath (QString);
	  void ShowHelpPage(QString);
	  void SetR2L      (bool);
	  
 public slots:
  void ListViewItemSetCurrentSlot(const QString &);
  void ListViewItemPressedSlot   (QListViewItem *);
  void ListViewItemChangedSlot   (QListViewItem *);
  
 signals:
  void ListViewItemSetSource(const QString &);
};

#endif
