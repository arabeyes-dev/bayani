/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogSettingsData Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogsettingsdata.h>

QTabDialogSettingsData::QTabDialogSettingsData(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  setCaption(trUtf8("Settings - Data"));
  
  setOKButton     (trUtf8("&OK"      ));
  setApplyButton  (trUtf8("&Apply"   ));
  setDefaultButton(trUtf8("&Defaults"));
  setCancelButton (trUtf8("&Cancel"  ));
  
  /* Function */
  
  FunctionQVB = new QVBox(Parent);
  FunctionQVB->setSpacing(5);
  FunctionQVB->setMargin(5);
  
  addTab(FunctionQVB, trUtf8("&Function"));
  
  Function1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Properties"), FunctionQVB);
  
  Function11QVB = new QVBox(Function1QGB);
  Function11QVB->setSpacing(5);
  Function11QVB->setMargin(5);
  
  FunctionLineWidthQL = new QLabel(trUtf8("Line &width"), Function11QVB);
  FunctionLineStyleQL = new QLabel(trUtf8("Line &style"), Function11QVB);
  FunctionLineColorQL = new QLabel(trUtf8("Line colo&r"), Function11QVB);
  
  Function12QVB = new QVBox(Function1QGB);
  Function12QVB->setSpacing(5);
  Function12QVB->setMargin(5);
  
  FunctionLineWidthQLC   = new QLineCombo(Function12QVB);
  FunctionLineStyleQPC   = new QPenCombo(Function12QVB);
  FunctionLineColorQPBCS = new QPushButtonColorSelect(Function12QVB);
  
  FunctionLineWidthQL->setBuddy(FunctionLineWidthQLC  );
  FunctionLineStyleQL->setBuddy(FunctionLineStyleQPC  );
  FunctionLineColorQL->setBuddy(FunctionLineColorQPBCS);
  
  /* Histogram */
  
  HistogramQVB  = new QVBox(Parent);
  HistogramQVB->setSpacing(5);
  HistogramQVB->setMargin(5);
  
  addTab(HistogramQVB, trUtf8("&Histogram"));
  
  Histogram1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Outline"), HistogramQVB);
  
  Histogram11QVB = new QVBox(Histogram1QGB);
  Histogram11QVB->setSpacing(5);
  Histogram11QVB->setMargin(5);
  
  HistogramLineWidthQL  = new QLabel(trUtf8("&Width"), Histogram11QVB);
  HistogramLineStyleQL  = new QLabel(trUtf8("&Style"), Histogram11QVB);
  HistogramLineColorQL  = new QLabel(trUtf8("Colo&r"), Histogram11QVB);
  
  Histogram12QVB = new QVBox(Histogram1QGB);
  Histogram12QVB->setSpacing(5);
  Histogram12QVB->setMargin(5);
  
  HistogramLineWidthQLC   = new QLineCombo(Histogram12QVB);
  HistogramLineStyleQPC   = new QPenCombo(Histogram12QVB);
  HistogramLineColorQPBCS = new QPushButtonColorSelect(Histogram12QVB);
  
  Histogram2QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Fill"), HistogramQVB);
  
  Histogram21QVB = new QVBox(Histogram2QGB);
  Histogram21QVB->setSpacing(5);
  Histogram21QVB->setMargin(5);
  
  HistogramFillStyleQL = new QLabel(trUtf8("St&yle"), Histogram21QVB);
  HistogramFillColorQL = new QLabel(trUtf8("Co&lor"), Histogram21QVB);
  
  Histogram22QVB = new QVBox(Histogram2QGB);
  Histogram22QVB->setSpacing(5);
  Histogram22QVB->setMargin(5);
  
  HistogramFillStyleQBC   = new QBrushCombo(Histogram22QVB);
  HistogramFillColorQPBCS = new QPushButtonColorSelect(Histogram22QVB);
  
  HistogramLineWidthQL->setBuddy(HistogramLineWidthQLC  );
  HistogramLineStyleQL->setBuddy(HistogramLineStyleQPC  );
  HistogramLineColorQL->setBuddy(HistogramLineColorQPBCS);
  HistogramFillStyleQL->setBuddy(HistogramFillStyleQBC  );
  HistogramFillColorQL->setBuddy(HistogramFillColorQPBCS);
  
  /* 2D graph */
  
  Plot2DQVB = new QVBox(Parent);
  Plot2DQVB->setSpacing(5);
  Plot2DQVB->setMargin(5);
  
  addTab(Plot2DQVB, trUtf8("2D &Graph"));
  
  Plot2D1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Data Points"), Plot2DQVB);
  
  Plot2D11QVB = new QVBox(Plot2D1QGB);
  Plot2D11QVB->setSpacing(5);
  Plot2D11QVB->setMargin(5);
  
  DataRatioQL = new QLabel(trUtf8("Relative si&ze"), Plot2D11QVB);
  DataTypeQL  = new QLabel(trUtf8("&Marker"), Plot2D11QVB);
  DataColorQL = new QLabel(trUtf8("Colo&r"), Plot2D11QVB);
  
  Plot2D12QVB = new QVBox(Plot2D1QGB);
  Plot2D12QVB->setSpacing(5);
  Plot2D12QVB->setMargin(5);
  
  DataRatioQLE = new QLineEdit("0.01", Plot2D12QVB);
  DataRatioQLE->setMaximumWidth(50);
  
  DataRatioQV = new QDoubleValidator(DataRatioQLE);
  DataRatioQV->setBottom(0.);
  
  DataRatioQLE->setValidator(DataRatioQV);
  
  DataTypeQCB = new QComboBox(false, Plot2D12QVB);
  DataTypeQCB->insertItem(trUtf8("Hollow circle"), HOLCIRCLE);
  DataTypeQCB->insertItem(trUtf8("Filled circle"), FILCIRCLE);
  DataTypeQCB->insertItem(trUtf8("Hollow square"), HOLSQUARE);
  DataTypeQCB->insertItem(trUtf8("Filled square"), FILSQUARE);
  DataTypeQCB->setFixedSize(DataTypeQCB->sizeHint());
  
  DataColorQPBCS = new QPushButtonColorSelect(Plot2D12QVB);
  
  Plot2D2QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Error Bars"), Plot2DQVB);
  
  Plot2D21QVB = new QVBox(Plot2D2QGB);
  Plot2D21QVB->setSpacing(5);
  Plot2D21QVB->setMargin(5);
  
  ErrorsLineWidthQL = new QLabel(trUtf8("Line &width"), Plot2D21QVB);
  ErrorsLineStyleQL = new QLabel(trUtf8("Line &style"), Plot2D21QVB);
  ErrorsLineColorQL = new QLabel(trUtf8("Line co&lor"), Plot2D21QVB);
  
  Plot2D22QVB = new QVBox(Plot2D2QGB);
  Plot2D22QVB->setSpacing(5);
  Plot2D22QVB->setMargin(5);
  
  ErrorsLineWidthQLC   = new QLineCombo(Plot2D22QVB);
  ErrorsLineStyleQPC   = new QPenCombo(Plot2D22QVB);
  ErrorsLineColorQPBCS = new QPushButtonColorSelect(Plot2D22QVB);
  
  DataRatioQL      ->setBuddy(DataRatioQLE        );
  DataTypeQL       ->setBuddy(DataTypeQCB         );
  DataColorQL      ->setBuddy(DataColorQPBCS      );
  ErrorsLineWidthQL->setBuddy(ErrorsLineWidthQLC  );
  ErrorsLineStyleQL->setBuddy(ErrorsLineStyleQPC  );
  ErrorsLineColorQL->setBuddy(ErrorsLineColorQPBCS);
  
  /* FITS image */
  
  FITSQVB = new QVBox(Parent);
  FITSQVB->setSpacing(5);
  FITSQVB->setMargin(5);
  
  addTab(FITSQVB, trUtf8("FITS &Image"));
  
  FITS1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Properties"), FITSQVB);
  
  FITS11QVB = new QVBox(FITS1QGB);
  FITS11QVB->setSpacing(5);
  FITS11QVB->setMargin(5);
  
  PaletteQL = new QLabel(trUtf8("&Palette"), FITS11QVB);
  
  FITS12QVB = new QVBox(FITS1QGB);
  FITS12QVB->setSpacing(5);
  FITS12QVB->setMargin(5);
  
  PaletteQCB = new QComboBox(false, FITS12QVB);
  PaletteQCB->insertItem(trUtf8("Monochrome") , PALETTE_MC);
  PaletteQCB->insertItem(trUtf8("Four colors"), PALETTE_BRYW);
  PaletteQCB->insertItem(trUtf8("Six colors") , PALETTE_BBGRYW);
  PaletteQCB->setFixedSize(PaletteQCB->sizeHint());
  
  FITS2QGB = new QGroupBox(4, Qt::Horizontal, trUtf8("Dynamics"), FITSQVB);
  
  FITS21QVB = new QVBox(FITS2QGB);
  FITS21QVB->setSpacing(5);
  FITS21QVB->setMargin(5);
  
  MinDynQCKB = new QCheckBox(trUtf8("Set minim&um"), FITS21QVB);
  MaxDynQCKB = new QCheckBox(trUtf8("Set ma&ximum"), FITS21QVB);
  
  FITS22QVB = new QVBox(FITS2QGB);
  FITS22QVB->setSpacing(5);
  FITS22QVB->setMargin(5);
  
  MinDynQSD = new QSlider(Qt::Horizontal, FITS22QVB);
  MaxDynQSD = new QSlider(Qt::Horizontal, FITS22QVB);
  
  FITS23QVB = new QVBox(FITS2QGB);
  FITS23QVB->setSpacing(5);
  FITS23QVB->setMargin(5);
  
  MinDynQSB = new QSpinBoxBayani(0, 65534, 1, FITS23QVB);
  MaxDynQSB = new QSpinBoxBayani(1, 65535, 1, FITS23QVB);
  
  MinDynQSD->setRange(0, 65534);
  MaxDynQSD->setRange(1, 65535);
  
  MinDynQSD->setFixedWidth(100);
  MaxDynQSD->setFixedWidth(100);
  
  MinDynQSB->setFixedWidth(75);
  MaxDynQSB->setFixedWidth(75);
  
  PaletteQL->setBuddy(PaletteQCB);
  
  /* General */
  
  FunctionLineWidthQLC->setFocus();
  
  setFixedSize(sizeHint());
  
  MinDynQSD->setEnabled(false);
  MinDynQSB->setEnabled(false);
  MaxDynQSD->setEnabled(false);
  MaxDynQSB->setEnabled(false);
  
  connect(MinDynQSD , SIGNAL(valueChanged(int )), MinDynQSB, SLOT(setValue(int)));
  connect(MinDynQSD , SIGNAL(valueChanged(int )), this     , SLOT(MinDynQSDChangedSlot(int)));
  connect(MaxDynQSD , SIGNAL(valueChanged(int )), MaxDynQSB, SLOT(setValue(int)));
  connect(MaxDynQSD , SIGNAL(valueChanged(int )), this     , SLOT(MaxDynQSDChangedSlot(int)));
  connect(MinDynQSB , SIGNAL(valueChanged(int )), MinDynQSD, SLOT(setValue(int)));
  connect(MaxDynQSB , SIGNAL(valueChanged(int )), MaxDynQSD, SLOT(setValue(int)));
  connect(MinDynQCKB, SIGNAL(toggled     (bool)), MinDynQSD, SLOT(setEnabled(bool)));
  connect(MinDynQCKB, SIGNAL(toggled     (bool)), MinDynQSB, SLOT(setEnabled(bool)));
  connect(MinDynQCKB, SIGNAL(clicked     ()    ), MinDynQSB, SLOT(setFocus()      ));
  connect(MinDynQCKB, SIGNAL(pressed     ()    ), MinDynQSB, SLOT(clearFocus()    ));
  connect(MaxDynQCKB, SIGNAL(toggled     (bool)), MaxDynQSD, SLOT(setEnabled(bool)));
  connect(MaxDynQCKB, SIGNAL(toggled     (bool)), MaxDynQSB, SLOT(setEnabled(bool)));
  connect(MaxDynQCKB, SIGNAL(clicked     ()    ), MaxDynQSB, SLOT(setFocus()      ));
  connect(MaxDynQCKB, SIGNAL(pressed     ()    ), MaxDynQSB, SLOT(clearFocus()    ));
}

QTabDialogSettingsData::~QTabDialogSettingsData()
{
}

QLineEdit * QTabDialogSettingsData::GetDataRatioQLE()
{
  return DataRatioQLE;
}

QCheckBox * QTabDialogSettingsData::GetMinDynQCKB()
{
  return MinDynQCKB;
}

QCheckBox * QTabDialogSettingsData::GetMaxDynQCKB()
{
  return MaxDynQCKB;
}

QComboBox * QTabDialogSettingsData::GetDataTypeQCB()
{
  return DataTypeQCB;
}

QComboBox * QTabDialogSettingsData::GetPaletteQCB()
{
  return PaletteQCB;
}

QSlider * QTabDialogSettingsData::GetMinDynQSD()
{
  return MinDynQSD;
}

QSlider * QTabDialogSettingsData::GetMaxDynQSD()
{
  return MaxDynQSD;
}

QLineCombo * QTabDialogSettingsData::GetFunctionLineWidthQLC()
{
  return FunctionLineWidthQLC;
}

QLineCombo * QTabDialogSettingsData::GetHistogramLineWidthQLC()
{
  return HistogramLineWidthQLC;
}

QLineCombo * QTabDialogSettingsData::GetErrorsLineWidthQLC()
{
  return ErrorsLineWidthQLC;
}

QPushButtonColorSelect * QTabDialogSettingsData::GetFunctionLineColorQPBCS()
{
  return FunctionLineColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsData::GetHistogramLineColorQPBCS()
{
  return HistogramLineColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsData::GetHistogramFillColorQPBCS()
{
  return HistogramFillColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsData::GetDataColorQPBCS()
{
  return DataColorQPBCS;
}

QPushButtonColorSelect * QTabDialogSettingsData::GetErrorsLineColorQPBCS()
{
  return ErrorsLineColorQPBCS;
}

QPenCombo * QTabDialogSettingsData::GetFunctionLineStyleQPC()
{
  return FunctionLineStyleQPC;
}

QPenCombo * QTabDialogSettingsData::GetHistogramLineStyleQPC()
{
  return HistogramLineStyleQPC;
}

QPenCombo * QTabDialogSettingsData::GetErrorsLineStyleQPC()
{
  return ErrorsLineStyleQPC;
}

QBrushCombo * QTabDialogSettingsData::GetHistogramFillStyleQBC()
{
  return HistogramFillStyleQBC;
}

void QTabDialogSettingsData::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      DataRatioQLE->setAlignment(Qt::AlignRight);
      MinDynQSB->GetEditor()->setAlignment(Qt::AlignRight);
      MaxDynQSB->GetEditor()->setAlignment(Qt::AlignRight);
    }
  else
    {
      DataRatioQLE->setAlignment(Qt::AlignLeft);
      MinDynQSB->GetEditor()->setAlignment(Qt::AlignLeft);
      MaxDynQSB->GetEditor()->setAlignment(Qt::AlignLeft);
    }
}

void QTabDialogSettingsData::SetCurrentTab(int TabID)
{
  tabBar()->setCurrentTab(TabID);
}

int QTabDialogSettingsData::GetCurrentTabID()
{
  return tabBar()->currentTab();
}

void QTabDialogSettingsData::MinDynQSDChangedSlot(int Value)
{
  if(MaxDynQSD->value() <= Value)
    MaxDynQSD->setValue(Value+1);
}

void QTabDialogSettingsData::MaxDynQSDChangedSlot(int Value)
{
  if(MinDynQSD->value() >= Value)
    MinDynQSD->setValue(Value-1);
}
