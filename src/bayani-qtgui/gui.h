/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the GUI Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_GUI_H
#define BAYANI_GUI_H

using namespace std;

/* "Usual" headers */
#include <unistd.h>
//math.h ?
#include <vector>
/* QT headers */
#include <qevent.h>
#include <qnamespace.h>
#include <qmainwindow.h>
#include <qwidget.h>
#include <qwidgetlist.h>
#include <qapplication.h>
#include <qworkspace.h>
#include <qaction.h>
#include <qmenubar.h>/* Needed */
#include <qpopupmenu.h>
#include <qstatusbar.h>/* Needed */
#include <qtoolbar.h>
#include <qmessagebox.h>
#include <qfiledialog.h>
#include <qpixmap.h>
#include <qiconset.h>
#include <qrect.h>
#include <qsize.h>
#include <qdir.h>
#include <qfile.h>
#include <qfont.h>
#include <qcolor.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qtextstream.h>
#include <qsettings.h>
#include <qkeysequence.h>
#include <qlistview.h>
#include <qmemarray.h>
//#include <qdockwindow.h>
/* Bayani headers */
#include <histogram.h>
#include <dataarray.h>
#include <fitsimage.h>
#include <fit.h>
#include <bexpression.h>
#include <frame.h>
#include <graph.h>
#include <func1d.h>
#include <histo1d.h>
#include <graph2d.h>
#include <fitscontainer.h>
#include <qtablebayani.h>
#include <qtexteditbayani.h>
#include <qtabdialogframezone.h>
#include <qtabdialogframesetactiveplot.h>
#include <qtabdialogtablefillfromfile.h>
#include <qtabdialogtablewritetofile.h>
#include <qtabdialogtableswap.h>
#include <qtabdialogtablemanage.h>
#include <qtabdialogtableempty.h>
#include <qtabdialoggraphfunction.h>
#include <qtabdialoggraphhistogram.h>
#include <qtabdialoggraphgraph2d.h>
#include <qtabdialoggraphfits.h>
#include <qtabdialoggraphbounds.h>
#include <qtabdialoggraphaxes.h>
#include <qtabdialoggraphfit.h>
#include <qtabdialogcomputationvariables.h>
#include <qtabdialogcomputationcompute.h>
#include <qtabdialogtextsframe.h>
#include <qtabdialogtextsgraph.h>
#include <qtabdialogsettingsglobal.h>
#include <qtabdialogsettingsframe.h>
#include <qtabdialogsettingsgraph.h>
#include <qtabdialogsettingsdata.h>
#include <bhelpbrowser.h>
#include <qtabdialoghelpabout.h>

#define BAYANI_DIR     "/.bayani/"
#define BAYANI_EXT     ".bcf"
#define BAYANI_HISFILE "history"

#define MESSAGE_INFORMATION 0
#define MESSAGE_WARNING     1
#define MESSAGE_CRITICAL    2

/*!
  \file gui.h
  \brief Header file for the GUI class.
  
  The GUI class is defined in this file.
*/

static bool              TerminalSenderFlag;
static QTextEditBayani * ScriptingWindow;//rename
static QColor            OptionGlobalTerminalInformationColor;
static QColor            OptionGlobalTerminalCriticalColor;

//! Bayani's GUI.
/*!
  This class inherits QMainWindow. It defines the <b>core</b> of Bayani's GUI.
*/
class GUI: public QMainWindow
{
  Q_OBJECT
  
 protected:
  /* Global variables */
  
  bool    FirstShow;
  int     FramesCounter;
  int     SettingsDataCurrentTabID;
  Frame * TopFrame;
  vector  <Variable> VariablesList;
  
  int ViewDataTableMenuID;
  int ViewScriptingWindowMenuID;
  int GraphMenuID;
  int TextsMenuID;
  
  /* Global options */
  
  bool   OptionGlobalAppearanceR2LFlag;
  int    OptionGlobalTerminalHistorySize;
  QColor OptionGlobalTerminalTextColor;
  QColor OptionGlobalTerminalWarningColor;
  QColor OptionGlobalTerminalStatusColor;
  QColor OptionGlobalTerminalBGColor;
  QFont  OptionGlobalTerminalTextFont;
  //rename whats below
  bool OptionDataTableShowFlag;
  int OptionDataTableRowsNumber;
  int OptionDataTableColumnsNumber;
  bool OptionScriptingWindowShowFlag;
  int OptionFrameZoneHorNumber;
  int OptionFrameZoneVerNumber;
  bool OptionGraphHistogramColumnFlag;
  bool OptionGraphHistogramBinsNumberFlag;
  bool OptionGraphHistogramXMinFlag;
  bool OptionGraphHistogramXMaxFlag;
  bool OptionGraphHistogramCutFlag;
  bool GraphBoundsNewFlag;//rename
  bool GraphAxesNewFlag;//rename
  QColor OptionGlobalExternalErrorColor;//rename - should be settable - like everything else...
  
  /* Frame options */
  
  int      OptionFrameBoxLineWidth;
  int      OptionFrameHSepLineWidth;
  int      OptionFrameVSepLineWidth;
  double   OptionFrameXRatio;
  double   OptionFrameYRatio;
  double   OptionFrameWidthRatio;
  double   OptionFrameHeightRatio;
  double   OptionFrameBox2TitleRatio;
  QColor   OptionFrameBGColor;
  QColor   OptionFrameTitleColor;
  QColor   OptionFrameBoxColor;
  QColor   OptionFrameHSepColor;
  QColor   OptionFrameVSepColor;
  QFont    OptionFrameTitleFont;
  PenStyle OptionFrameBoxLineStyle;
  PenStyle OptionFrameHSepLineStyle;
  PenStyle OptionFrameVSepLineStyle;
  
  /* Graph options */
  
  int      OptionGraphBoxLineWidth;
  int      OptionGraphBigXTicksLineWidth;
  int      OptionGraphBigYTicksLineWidth;
  int      OptionGraphSmallXTicksLineWidth;
  int      OptionGraphSmallYTicksLineWidth;
  int      OptionGraphXGridLineWidth;
  int      OptionGraphYGridLineWidth;
  double   OptionGraphXRatio;
  double   OptionGraphYRatio;
  double   OptionGraphWidthRatio;
  double   OptionGraphHeightRatio;
  double   OptionGraphAxis2TitleRatio;
  double   OptionGraphBigXTicksRatio;
  double   OptionGraphBigYTicksRatio;
  double   OptionGraphSmallXTicksRatio;
  double   OptionGraphSmallYTicksRatio;
  double   OptionGraphXAxis2TicksLabelsRatio;
  double   OptionGraphYAxis2TicksLabelsRatio;
  double   OptionGraphXAxis2AxisLabelRatio;
  double   OptionGraphYAxis2AxisLabelRatio;
  QColor   OptionGraphBoxColor;
  QColor   OptionGraphBGColor;
  QColor   OptionGraphBigXTicksColor;
  QColor   OptionGraphBigYTicksColor;
  QColor   OptionGraphSmallXTicksColor;
  QColor   OptionGraphSmallYTicksColor;
  QColor   OptionGraphXTicksLabelsColor;
  QColor   OptionGraphYTicksLabelsColor;
  QColor   OptionGraphXAxisLabelColor;
  QColor   OptionGraphYAxisLabelColor;
  QColor   OptionGraphTitleColor;
  QColor   OptionGraphXGridColor;
  QColor   OptionGraphYGridColor;
  QFont    OptionGraphXAxisLabelFont;
  QFont    OptionGraphYAxisLabelFont;
  QFont    OptionGraphXTicksLabelsFont;
  QFont    OptionGraphYTicksLabelsFont;
  QFont    OptionGraphTitleFont;
  PenStyle OptionGraphBoxLineStyle;
  PenStyle OptionGraphBigXTicksLineStyle;
  PenStyle OptionGraphBigYTicksLineStyle;
  PenStyle OptionGraphSmallXTicksLineStyle;
  PenStyle OptionGraphSmallYTicksLineStyle;
  PenStyle OptionGraphXGridLineStyle;
  PenStyle OptionGraphYGridLineStyle;
  
  /* Data Options */
  
  int        OptionDataFunctionLineWidth;
  QColor     OptionDataFunctionLineColor;
  PenStyle   OptionDataFunctionLineStyle;
  int        OptionDataHistogramLineWidth;
  QColor     OptionDataHistogramLineColor;
  QColor     OptionDataHistogramFillColor;
  PenStyle   OptionDataHistogramLineStyle;
  BrushStyle OptionDataHistogramFillStyle;
  int        OptionDataGraphShapeType;
  int        OptionDataGraphErrorsLineWidth;
  double     OptionDataGraphShapeRatio;
  QColor     OptionDataGraphShapeColor;
  QColor     OptionDataGraphErrorsLineColor;
  PenStyle   OptionDataGraphErrorsLineStyle;
  int        OptionDataFITSPalette;
  bool       OptionDataFITSMinDynFlag;
  int        OptionDataFITSMinDynValue;
  bool       OptionDataFITSMaxDynFlag;
  int        OptionDataFITSMaxDynValue;
  
  QWorkspace * WorkSpaceQWS;
  
  QAction * FileQuitQA;
  QAction * FrameNewQA;
  QAction * FrameZoneQA;
  QAction * FrameSetActiveGraphQA;
  QAction * FrameSaveQA;
  QAction * FrameSaveAsQA;
  QAction * FrameCloseQA;
  QAction * TableFillQA;
  QAction * TableWriteQA;
  QAction * TableSwapQA;
  QAction * TableTransposeQA;
  QAction * TableManageQA;
  QAction * TableEmptyQA;
  QAction * GraphFunctionQA;
  QAction * GraphHistogramQA;
  QAction * GraphGraph2DQA;
  QAction * GraphFITSQA;
  QAction * GraphBoundsQA;
  QAction * GraphAxesQA;
  QAction * GraphFitQA;
  QAction * GraphCloseQA;
  QAction * ComputationVariablesQA;
  QAction * ComputationComputeQA;
  QAction * TextsFrameQA;
  QAction * TextsGraphQA;
  QAction * SettingsGlobalQA;
  QAction * SettingsFrameQA;
  QAction * SettingsGraphQA;
  QAction * SettingsDataQA;
  QAction * SettingsSaveQA;
  QAction * HelpContentsQA;
  QAction * HelpWhatsThisQA;
  QAction * HelpAboutQA;
  
  QPopupMenu * FileMenuQPM;
  QPopupMenu * ViewMenuQPM;
  QPopupMenu * ShowHideMenuQPM;
  QPopupMenu * FrameMenuQPM;
  QPopupMenu * TableMenuQPM;
  QPopupMenu * GraphMenuQPM;
  QPopupMenu * ComputationMenuQPM;
  QPopupMenu * TextsMenuQPM;
  QPopupMenu * SettingsMenuQPM;
  QPopupMenu * HelpMenuQPM;
  
  QToolBar * ToolBarQTB;
  
  //QDockWindow * ScriptingWindowDock;
  
  QTableBayani * DataTable;//rename
  
  QTabDialogFrameZone            * FrameZoneQTD;
  QTabDialogFrameSetActivePlot   * FrameSetActivePlotQTD;//rename
  QTabDialogTableFillFromFile    * TableFillFromFileQTD;
  QTabDialogTableWriteToFile     * TableWriteToFileQTD;
  QTabDialogTableSwap            * TableSwapQTD;
  QTabDialogTableManage          * TableManageQTD;
  QTabDialogTableEmpty           * TableEmptyQTD;
  QTabDialogGraphFunction        * GraphFunctionQTD;
  QTabDialogGraphHistogram       * GraphHistogramQTD;
  QTabDialogGraphGraph2D         * GraphGraph2DQTD;
  QTabDialogGraphFITS            * GraphFITSQTD;
  QTabDialogGraphBounds          * GraphBoundsQTD;
  QTabDialogGraphAxes            * GraphAxesQTD;
  QTabDialogGraphFit             * GraphFitQTD;
  QTabDialogComputationVariables * ComputationVariablesQTD;
  QTabDialogComputationCompute   * ComputationComputeQTD;
  QTabDialogTextsFrame           * TextsFrameQTD;
  QTabDialogTextsGraph           * TextsGraphQTD;
  QTabDialogSettingsGlobal       * SettingsGlobalQTD;
  QTabDialogSettingsFrame        * SettingsFrameQTD;
  QTabDialogSettingsGraph        * SettingsGraphQTD;
  QTabDialogSettingsData         * SettingsDataQTD;
  BHelpBrowser                   * HelpContentsBHB;
  QTabDialogHelpAbout            * HelpAboutQTD;
  
  void SetLayout                 ();
  void SetGlobalDefaultSettings  ();
  void ApplyGlobalDefaultSettings();
  void SaveGlobalDefaultSettings ();
  void SetFrameDefaultSettings   ();
  void ApplyFrameDefaultSettings ();
  void SaveFrameDefaultSettings  ();
  void SetGraphDefaultSettings   ();
  void ApplyGraphDefaultSettings ();
  void SaveGraphDefaultSettings  ();
  void SetDataDefaultSettings    ();
  void ApplyDataDefaultSettings  ();
  void SaveDataDefaultSettings   ();
  void EnableFrameRelatedMenus   ();
  void DisableFrameRelatedMenus  ();
  void UpdateTableRelatedItems   ();
  void EnableGraphRelatedMenus   ();
  void DisableGraphRelatedMenus  ();
  void CreateNewGraph            ();
  void GraphBoundsSettings       ();
  void GraphAxesSettings         ();
  void PrintStatus               (QString, int Duration = -1);
  
 public:
                      GUI         (QWidget * Parent = 0, const char * Name = 0);
  virtual            ~GUI         ();
                 void SetR2L      (bool);
  virtual        void showEvent   (QShowEvent *);
  virtual        void closeEvent  (QCloseEvent *);
          static int  PrintMessage(QWidget *, int, QString, QString Title = NULL, QTextEdit * OutputQTE = NULL);
	  
 protected slots:
  void FileQuitSlot                      ();
  void ViewDataTableSlot                 ();
  void ViewScriptingWindowSlot           ();
  void CheckViewDataTableMenuSlot        ();
  void CheckViewScriptingWindowMenuSlot  ();
  void UnCheckViewDataTableMenuSlot      ();
  void UnCheckViewScriptingWindowMenuSlot();
  void FrameNewSlot                      ();
  void FrameZoneSlot                     ();
  void FrameZoneApplySlot                ();
  void FrameSetActivePlotSlot            ();
  void FrameSetActivePlotApplySlot       ();
  void FrameSaveSlot                     ();
  void FrameSaveAsSlot                   ();
  void FrameCloseSlot                    ();
  void ChangeTopFrameSlot                (bool);
  void ChangeTopFrameSlot                (QWidget *);
  void TableFillFromFileSlot             ();
  void TableFillFromFileApplySlot        ();
  void TableWriteToFileSlot              ();
  void TableWriteToFileApplySlot         ();
  void TableSwapSlot                     ();
  void TableSwapApplySlot                ();
  void TableSwapRowsQRBSlot              (bool);
  void TableTransposeSlot                ();
  void TableManageSlot                   ();
  void TableManageApplySlot              ();
  void TableManageRowsQRBSlot            (bool);
  void TableEmptySlot                    ();
  void TableEmptyApplySlot               ();
  void GraphFunctionSlot                 ();
  void GraphFunctionApplySlot            ();
  void GraphHistogramSlot                ();
  void GraphHistogramApplySlot           ();
  void GraphGraph2DSlot                  ();
  void GraphGraph2DApplySlot             ();
  void GraphFITSSlot                     ();
  void GraphFITSApplySlot                ();
  void GraphBoundsSlot                   ();
  void GraphBoundsApplySlot              ();
  void GraphAxesSlot                     ();
  void GraphAxesApplySlot                ();
  void GraphFitSlot                      ();
  void GraphFitApplySlot                 ();
  void GraphCloseSlot                    ();
  void ComputationVariablesSlot          ();
  void ComputationVariablesApplySlot     ();
  void ComputationVariablesHelpSlot      ();
  void ComputationComputeSlot            ();
  void ComputationComputeApplySlot       ();
  void ComputationComputeHelpSlot        ();
  void TextsFrameSlot                    ();
  void TextsFrameApplySlot               ();
  void TextsGraphSlot                    ();
  void TextsGraphApplySlot               ();
  void SettingsGlobalSlot                ();
  void SettingsGlobalApplySlot           ();
  void SettingsGlobalDefaultsSlot        ();
  void SettingsGlobalCancelSlot          ();
  void SettingsFrameSlot                 ();
  void SettingsFrameApplySlot            ();
  void SettingsFrameDefaultsSlot         ();
  void SettingsFrameCancelSlot           ();
  void SettingsGraphSlot                 ();
  void SettingsGraphApplySlot            ();
  void SettingsGraphDefaultsSlot         ();
  void SettingsGraphCancelSlot           ();
  void SettingsDataSlot                  ();
  void SettingsDataApplySlot             ();
  void SettingsDataDefaultsSlot          ();
  void SettingsDataCancelSlot            ();
  void SettingsDataChangedSlot           (QWidget *);
  void SettingsDataFunctionSlot          ();
  void SettingsDataHistogramSlot         ();
  void SettingsDataGraph2DSlot           ();
  void SettingsDataFITSSlot              ();
  void SettingsSaveSlot                  ();
  void HelpContentsSlot                  ();
  void HelpAboutSlot                     ();
  void PrintStatusSlot                   (QString);
  void ParseCommandSlot                  (QString);
  void DrawingProblemSlot                (QString);
  
  void ShowCoordinates    (double, double);//tmp
  void ShowFITSCoordinates(double, double, double);//tmp
  void HideCoordinates    ();//tmp
};

#endif
