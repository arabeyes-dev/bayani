/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogGraphAxes Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGGRAPHAXES_H
#define BAYANI_QTABDIALOGGRAPHAXES_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>

/*!
  \file qtabdialoggraphaxes.h
  \brief Header file for the QTabDialogGraphAxes class.
  
  The QTabDialogGraphAxes class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set graphs' axes.
*/
class QTabDialogGraphAxes: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QCheckBox * XLogQCKB;
  QCheckBox * YLogQCKB;
  QCheckBox * XRevQCKB;
  QCheckBox * YRevQCKB;
  QCheckBox * NewQCKB;
  QGroupBox * General1QGB;
  QGroupBox * General2QGB;
  QVBox     * GeneralQVB;
  
 public:
                      QTabDialogGraphAxes(QWidget * Parent = 0, const char * Name = 0);
  virtual            ~QTabDialogGraphAxes();
          QCheckBox * GetXLogQCKB        ();
	  QCheckBox * GetYLogQCKB        ();
	  QCheckBox * GetXRevQCKB        ();
	  QCheckBox * GetYRevQCKB        ();
	  QCheckBox * GetNewQCKB         ();
	  void        SetR2L             (bool);
};

#endif
