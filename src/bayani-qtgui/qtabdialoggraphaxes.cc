/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogGraphAxes Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialoggraphaxes.h>

QTabDialogGraphAxes::QTabDialogGraphAxes(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB  = new QVBox(Parent);
  General1QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Logarithmic Scale"), GeneralQVB);
  General2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Reverse Direction"), GeneralQVB);
  
  XLogQCKB = new QCheckBox(trUtf8("&X axis"), General1QGB);
  
  YLogQCKB = new QCheckBox(trUtf8("&Y axis"), General1QGB);
  
  XRevQCKB = new QCheckBox(trUtf8("X ax&is"), General2QGB);
  
  YRevQCKB = new QCheckBox(trUtf8("Y axi&s"), General2QGB);
  
  NewQCKB = new QCheckBox(trUtf8("Also apply to &new graphs"), GeneralQVB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Graph - Set Axes"));
  
  XLogQCKB->setFocus();
}

QTabDialogGraphAxes::~QTabDialogGraphAxes()
{
}

QCheckBox * QTabDialogGraphAxes::GetXLogQCKB()
{
  return XLogQCKB;
}

QCheckBox * QTabDialogGraphAxes::GetYLogQCKB()
{
  return YLogQCKB;
}

QCheckBox * QTabDialogGraphAxes::GetXRevQCKB()
{
  return XRevQCKB;
}

QCheckBox * QTabDialogGraphAxes::GetYRevQCKB()
{
  return YRevQCKB;
}

QCheckBox * QTabDialogGraphAxes::GetNewQCKB()
{
  return NewQCKB;
}

void QTabDialogGraphAxes::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      
    }
  else
    {
      
    }
}
