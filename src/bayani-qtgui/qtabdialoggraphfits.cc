/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogGraphFITS Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialoggraphfits.h>

QTabDialogGraphFITS::QTabDialogGraphFITS(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB  = new QVBox(Parent);
  General1QGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Image"), GeneralQVB);
  General2QGB = new QGroupBox(1, Qt::Horizontal, trUtf8("Options"), GeneralQVB);
  
  FileNameQL   = new QLabel(trUtf8("&File name"), General1QGB);
  FileNameQFLS = new QFileSelector(General1QGB);
  FileNameQFLS->AddFileType(trUtf8("FITS images (*.fits)"));
  
  SameQCKB = new QCheckBox(trUtf8("&Superimpose on the active graph"), General2QGB);
  
  PlotSettingsQPB = new QPushButton(trUtf8("&Graphical settings..."), General2QGB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  
  FileNameQL->setBuddy(FileNameQFLS);
  
  PlotSettingsQPB->setFixedSize(PlotSettingsQPB->sizeHint());
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Graph - FITS Image"));
  
  FileNameQFLS->setFocus();
}

QTabDialogGraphFITS::~QTabDialogGraphFITS()
{
}

QFileSelector * QTabDialogGraphFITS::GetFileNameQFLS()
{
  return FileNameQFLS;
}

QPushButton * QTabDialogGraphFITS::GetPlotSettingsQPB()
{
  return PlotSettingsQPB;
}

QCheckBox * QTabDialogGraphFITS::GetSameQCKB()
{
  return SameQCKB;
}

void QTabDialogGraphFITS::SetR2L(bool R2LFlag)
{
  FileNameQFLS->SetR2L(R2LFlag);
  
  if(R2LFlag)
    {
    }
  else
    {
    }
}
