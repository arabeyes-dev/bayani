/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogComputationVariables Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGCOMPUTATIONVARIABLES_H
#define BAYANI_QTABDIALOGCOMPUTATIONVARIABLES_H

using namespace std;

/* "Usual" headers */
#include <vector>
/* QT headers */
#include <qobject.h>
#include <qnamespace.h>
#include <qwidget.h>
#include <qlistview.h>
#include <qgroupbox.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qvbox.h>
#include <qhbox.h>
#include <qvalidator.h>
#include <qregexp.h>
#include <qtabdialog.h>
#include <qmessagebox.h>
#include <qstring.h>
#include <qtabbar.h>
/* Bayani headers */
#include <bexpression.h>

/*!
  \file qtabdialogcomputationvariables.h
  \brief Header file for the QTabDialogComputationVariables class.
  
  The QTabDialogComputationVariables class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set the variables used in symbolic computation.
*/
class QTabDialogComputationVariables: public QTabDialog
{
  Q_OBJECT
  
 protected:
  vector        <Variable> Variables;
  QHBox       * VariablesQHB;
  QVBox       * VariablesQVB;
  QVBox       * Variables11QVB;
  QVBox       * Variables12QVB;
  QGroupBox   * Variables1QGB;
  QGroupBox   * Variables2QGB;
  QGroupBox   * Variables3QGB;
  QListView   * VariablesQLV;
  QLabel      * VariablesNameQL;
  QLabel      * VariablesValueQL;
  QLineEdit   * VariablesNameQLE;
  QLineEdit   * VariablesValueQLE;
  QPushButton * AddVariablesQPB;
  QPushButton * RemoveVariablesQPB;
  
 public:
                        QTabDialogComputationVariables(QWidget * Parent = 0, const char * Name = 0);
  virtual              ~QTabDialogComputationVariables();
          QListView   * GetVariablesQLV               ();
	  QLineEdit   * GetVariablesNameQLE           ();
	  QLineEdit   * GetVariablesValueQLE          ();
	  QPushButton * GetAddVariablesQPB            ();
	  QPushButton * GetRemoveVariablesQPB         ();
	  vector        <Variable> GetVariables       ();
	  void          SetR2L                        (bool);
	  
 public slots:
  void VariablesSelectionChangedSlot(               );
  void VariablesNameChangedSlot     (const QString &);
  void AddVariablesSlot             (               );
  void RemoveVariablesSlot          (               );
};

#endif
