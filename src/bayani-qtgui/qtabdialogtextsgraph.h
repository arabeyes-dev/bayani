/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the QTabDialogTextsGraph Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_QTABDIALOGTEXTSGRAPH_H
#define BAYANI_QTABDIALOGTEXTSGRAPH_H

/* QT headers */
#include <qnamespace.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qtabdialog.h>
#include <qtabbar.h>
#include <qcheckbox.h>

/*!
  \file qtabdialogtextsgraph.h
  \brief Header file for the QTabDialogTextsGraph class.
  
  The QTabDialogTextsGraph class is defined in this file.
*/

//! A dialog for Bayani's GUI.
/*!
  This class inherits QTabDialog. It defines a dialog which the user can use to set the graphs' texts.
*/
class QTabDialogTextsGraph: public QTabDialog
{
  Q_OBJECT
  
 protected:
  QLabel    * TitleQL;
  QLabel    * XAxisLabelQL;
  QLabel    * YAxisLabelQL;
  QLineEdit * TitleQLE;
  QLineEdit * XAxisLabelQLE;
  QLineEdit * YAxisLabelQLE;
  QCheckBox * XTicksLabelsFlagQCKB;
  QCheckBox * YTicksLabelsFlagQCKB;
  QGroupBox * General1QGB;
  QGroupBox * General2QGB;
  QVBox     * GeneralQVB;
  QVBox     * Sub1QVB;
  QVBox     * Sub2QVB;
  
 public:
                      QTabDialogTextsGraph   (QWidget * Parent = 0, const char * Name = 0);
  virtual            ~QTabDialogTextsGraph   ();
          QLineEdit * GetTitleQLE            ();
	  QLineEdit * GetXAxisLabelQLE       ();
	  QLineEdit * GetYAxisLabelQLE       ();
	  QCheckBox * GetXTicksLabelsFlagQCKB();
	  QCheckBox * GetYTicksLabelsFlagQCKB();
	  void        SetR2L                 (bool);
};

#endif
