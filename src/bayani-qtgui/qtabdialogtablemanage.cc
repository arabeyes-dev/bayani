/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the QTabDialogTableManage Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <qtabdialogtablemanage.h>

QTabDialogTableManage::QTabDialogTableManage(QWidget * Parent, const char * Name)
  : QTabDialog(Parent, Name)
{
  tabBar()->setHidden(true);
  
  GeneralQVB = new QVBox(Parent);
  SubQHB     = new QHBox(GeneralQVB);
  Sub1QBG    = new QButtonGroup(1, Qt::Horizontal, trUtf8("Action"), SubQHB);
  Sub2QBG    = new QButtonGroup(1, Qt::Horizontal, trUtf8("Type"  ), SubQHB);
  GeneralQGB = new QGroupBox(2, Qt::Horizontal, trUtf8("Options"), GeneralQVB);
  
  Sub1QVB = new QVBox(GeneralQGB);
  Sub2QVB = new QVBox(GeneralQGB);
  
  InsertQRB = new QRadioButton(trUtf8("&Insert"), Sub1QBG);
  RemoveQRB = new QRadioButton(trUtf8("&Remove"), Sub1QBG);
  
  RowsQRB    = new QRadioButton(trUtf8("Ro&ws"   ), Sub2QBG);
  ColumnsQRB = new QRadioButton(trUtf8("Co&lumns"), Sub2QBG);
  
  NumberQL  = new QLabel(trUtf8("&Elements number"), Sub1QVB);
  NumberQLE = new QLineEdit(Sub2QVB);
  
  StartQL  = new QLabel(trUtf8("&Starting element (included)"), Sub1QVB);
  StartQSB = new QSpinBoxBayani(1, 1, 1, Sub2QVB);
  
  GeneralQVB->setSpacing(5);
  GeneralQVB->setMargin(5);
  SubQHB->setSpacing(5);
  SubQHB->setMargin(5);
  
  Sub1QVB->setSpacing(5);
  Sub1QVB->setMargin(5);
  Sub2QVB->setSpacing(5);
  Sub2QVB->setMargin(5);
  
  NumberQL->setBuddy(NumberQLE);
  StartQL ->setBuddy(StartQSB );
  
  NumberQLE->setMaximumWidth(50);
  StartQSB->setMaximumWidth(50);
  NumberQLE->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{1,5}"), NumberQLE));
  
  addTab(GeneralQVB, "");
  
  setOKButton    (trUtf8("&OK")    );
  setApplyButton (trUtf8("&Apply") );
  setCancelButton(trUtf8("&Cancel"));
  
  setFixedSize(sizeHint());
  
  setCaption(trUtf8("Table - Manage Size"));
  
  NumberQLE->setFocus();
}

QTabDialogTableManage::~QTabDialogTableManage()
{
}

QRadioButton * QTabDialogTableManage::GetInsertQRB()
{
  return InsertQRB;
}

QRadioButton * QTabDialogTableManage::GetRemoveQRB()
{
  return RemoveQRB;
}

QRadioButton * QTabDialogTableManage::GetRowsQRB()
{
  return RowsQRB;
}

QRadioButton * QTabDialogTableManage::GetColumnsQRB()
{
  return ColumnsQRB;
}

QLineEdit * QTabDialogTableManage::GetNumberQLE()
{
  return NumberQLE;
}

QSpinBoxBayani * QTabDialogTableManage::GetStartQSB()
{
  return StartQSB;
}

void QTabDialogTableManage::SetR2L(bool R2LFlag)
{
  if(R2LFlag)
    {
      NumberQLE->setAlignment(Qt::AlignRight);
      StartQSB->GetEditor()->setAlignment(Qt::AlignRight);
    }
  else
    {
      NumberQLE->setAlignment(Qt::AlignLeft);
      StartQSB->GetEditor()->setAlignment(Qt::AlignLeft);
    }
}
