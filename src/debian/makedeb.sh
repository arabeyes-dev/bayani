#!/bin/bash

#--
# A small script to create the Bayani Debian package.
# Must be run on a Debian machine of course.
# $Id$
#--

if [ $# != 0 ]; then
    echo Usage:  ./makedeb.sh
    exit
fi

if [ ! -e control ]; then
    echo Error: This script needs to be called from its own directory
    exit
fi

echo Removing old package...
rm -rf bayanideb

echo Creating filesystem tree...
mkdir -p bayanideb/DEBIAN
mkdir -p bayanideb/usr/bin
mkdir -p bayanideb/usr/lib
mkdir -p bayanideb/usr/share/bayani/translations
mkdir -p bayanideb/usr/share/bayani/graphics
mkdir -p bayanideb/usr/share/bayani/doc
mkdir -p bayanideb/usr/share/bayani/demo

echo Copying files...
cp control bayanideb/DEBIAN
cp ../bayani bayanideb/usr/bin
cp ../*/*.so bayanideb/usr/lib
cp ../*qm bayanideb/usr/share/bayani/translations
cp ../graphics/splash.png bayanideb/usr/share/bayani/graphics
cp -r ../../doc/user/ ../../doc/developer/ ../../doc/about/ bayanideb/usr/share/bayani/doc
cp -r ../../demo/* bayanideb/usr/share/bayani/demo

echo Creating package...
dpkg --build bayanideb bayani.deb

echo Removing filesystem tree...
rm -rf bayanideb

echo done!
