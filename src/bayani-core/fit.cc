/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the Fit Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <fit.h>

/* Fit functions */

//return (exp(Params[0]*X+Params[1]));

//return (Params[0]*X*X+Params[1]);

double Polynomial3(double X, vector <double> P)
{
  return (P[0]*X*X*X+P[1]*X*X+P[2]*X+P[3]);
}

/* Fit class */

Fit::Fit()
{
  Clear();
}

Fit::~Fit()
{
}

void Fit::FIT()
{
  if(NDF < 0)
    return;
  
  int br = 1;
  while(1)
    {
      if(br >= 99)
	break;
      if(Step >= MaxStep)
	{
	  NoConvergenceFlag = true;
	  break;
	}
      double MinChi = Chi2();
      double MinChiInit = MinChi;
      
      int BestParam = -1;
      bool PlusFlag = false;
      bool MinusFlag = false;
      bool TimesFlag = false;
      bool DivFlag = false;
      
      for(int i = 0; i < Params.size(); i++)
	{
	  double ParChi;
	  Params[i] += pow(10., br);
	  ParChi = Chi2();
	  if(ParChi < MinChi)
	    {
	      MinChi = ParChi;
	      MinusFlag = false;
	      PlusFlag = true;
	      TimesFlag = true;
	      DivFlag = false;
	      BestParam = i;
	    }
	  Params[i] -= 2*pow(10., br);
	  ParChi = Chi2();
	  if(ParChi < MinChi)
	    {
	      MinChi = ParChi;
	      MinusFlag = true;
	      PlusFlag = false;
	      TimesFlag = true;
	      DivFlag = false;
	      BestParam = i;
	    }
	  Params[i] += pow(10., br);
	  Params[i] += pow(10., -br);
	  ParChi = Chi2();
	  if(ParChi < MinChi)
	    {
	      MinChi = ParChi;
	      MinusFlag = false;
	      PlusFlag = true;
	      TimesFlag = false;
	      DivFlag = true;
	      BestParam = i;
	    }
	  Params[i] -= 2*pow(10., -br);
	  ParChi = Chi2();
	  if(ParChi < MinChi)
	    {
	      MinChi = ParChi;
	      MinusFlag = true;
	      PlusFlag = false;
	      TimesFlag = false;
	      DivFlag = true;
	      BestParam = i;
	    }
	  Params[i] += pow(10., -br);
	}
      
      if(!PlusFlag && !MinusFlag)
	{
	  br++;
	  continue;
	}
      
      int brr = br+1;
      double parin = Params[BestParam];
      Params[BestParam] += PlusFlag*TimesFlag*pow(10., brr)+PlusFlag*DivFlag*pow(10., -brr)-MinusFlag*TimesFlag*pow(10., brr)-MinusFlag*DivFlag*pow(10., -brr);
      double chmi = Chi2(); 
      while(chmi < MinChi)
	{
	  MinChi = chmi;
	  brr++;
	  Params[BestParam] = parin;
	  Params[BestParam] += PlusFlag*TimesFlag*pow(10., brr)+PlusFlag*DivFlag*pow(10., -brr)-MinusFlag*TimesFlag*pow(10., brr)-MinusFlag*DivFlag*pow(10., -brr);
	  chmi = Chi2();
	}
      
      /* If there's an exit from the loop then (brr-1) was better than brr */
      br = brr-1;
      Params[BestParam] = parin;
      
      Params[BestParam] += PlusFlag*TimesFlag*pow(10., br)+PlusFlag*DivFlag*pow(10., -br)-MinusFlag*TimesFlag*pow(10., br)-MinusFlag*DivFlag*pow(10., -br);
      
      ErrParams[BestParam] = fabs(Params[BestParam]-parin);
      
      DivFlag = false;
      TimesFlag = false;
      MinusFlag = false;
      PlusFlag = false;
      if(fabs(MinChi - MinChiInit) < ChiTwoLim)
	break;
      
      Step ++;
      br = 1;
    }
}

void Fit::PolynomialFit(int Degree)
{
  if(Degree < 0 || Degree > 2)
    return;
  
  SetNbParams(Degree+1);
  
  if(NDF < 0)
    return;
  
  if(Degree == 0)
    {
      if(Data.GetSize() < 1)
	return;
      
      double S00 = Data.GetSize();
      double S01 = 0.;
      
      for(int i = 0; i < Data.GetSize(); i++)
	S01 += Data.GetY(i);
      
      double Delta = S00;
      
      if(Delta == 0.)
	{
	  NoSolutionFlag = true;
	  
	  return;
	}
      
      Params[0] = S01/Delta;
    }
  
  if(Degree == 1)
    {
      if(Data.GetSize() < 2)
	return;
      
      double S00 = Data.GetSize();
      double S10 = 0.;
      double S20 = 0.;
      double S01 = 0.;
      double S11 = 0.;
      
      for(int i = 0; i < Data.GetSize(); i++)
	{
	  S10 += Data.GetX(i);
	  S20 += Data.GetX(i)*Data.GetX(i);
	  
	  S01 += Data.GetY(i);
	  S11 += Data.GetX(i)*Data.GetY(i);
	}
      
      double Delta = S00*S20-S10*S10;
      
      if(Delta == 0.)
	{
	  NoSolutionFlag = true;
	  
	  return;
	}
      
      Params[0] = (S00*S11-S10*S01)/Delta;
      Params[1] = (S20*S01-S10*S11)/Delta;
    }
  
  if(Degree == 2)
    {
      if(Data.GetSize() < 3)
	return;
      
      double S00 = Data.GetSize();
      double S10 = 0.;
      double S20 = 0.;
      double S30 = 0.;
      double S40 = 0.;
      double S01 = 0.;
      double S11 = 0.;
      double S21 = 0.;
      
      for(int i = 0; i < Data.GetSize(); i++)
	{
	  S10 += Data.GetX(i);
	  S20 += Data.GetX(i)*Data.GetX(i);
	  S30 += Data.GetX(i)*Data.GetX(i)*Data.GetX(i);
	  S40 += Data.GetX(i)*Data.GetX(i)*Data.GetX(i)*Data.GetX(i);
	  
	  S01 += Data.GetY(i);
	  S11 += Data.GetX(i)*Data.GetY(i);
	  S21 += Data.GetX(i)*Data.GetX(i)*Data.GetY(i);
	}
      
      double Delta = S00*S20*S40-S00*S30*S30-S10*S10*S40+2.*S10*S20*S30-S20*S20*S20;
      
      if(Delta == 0.)
	{
	  NoSolutionFlag = true;
	  
	  return;
	}
      
      Params[0] = (S00*S20*S21-S00*S30*S11-S10*S10*S21+S10*S20*S11+S10*S30*S01-S20*S20*S01)/Delta;
      Params[1] = (S00*S40*S11-S00*S30*S21-S10*S40*S01+S20*S30*S01+S10*S20*S21-S20*S20*S11)/Delta;
      Params[2] = (S20*S40*S01-S30*S30*S01-S10*S40*S11+S10*S30*S21+S20*S30*S11-S20*S20*S21)/Delta;
    }
}

double Fit::Chi2()
{
  ChiTwo = 0.;
  
  for (int i = 0; i < Data.GetSize(); i++)
    {
      if(PointerFunctionFlag)
	ChiTwo += pow(Data.GetY(i)-PointerFunction(Data.GetX(i), Params), 2.);
      else
	ChiTwo += pow(Data.GetY(i)-StringFunction(Data.GetX(i), Params), 2.);
    }
  
  return ChiTwo;
}

double Fit::GetChi2()
{
  return ChiTwo;
}

int Fit::GetNDF()
{
  return NDF;
}

void Fit::SetParams(vector <double> NewParams)
{
  NDF += Params.size();
  
  Params = NewParams;
  ErrParams.assign(Params.size(), 0.);
  
  NDF -= Params.size();
  Chi2();
}

void Fit::SetParam(int i, double NewParam)
{
  if(i >= Params.size() || i < 0)
    return;
  
  Params[i] = NewParam;
  Chi2();
}

void Fit::GetParams(vector <double> & NewParams)
{
  NewParams = Params;
}

void Fit::GetErrParams(vector <double> & NewErrParams)
{
  NewErrParams = ErrParams;
}

double Fit::GetParam(int i)
{
  return Params[i];
}

double Fit::GetErrParam(int i)
{
  return ErrParams[i];
}

void Fit::SetData(DataArray NewData)
{
  NDF -= Data.GetSize();
  
  Data = NewData;
  
  NDF += Data.GetSize();
}

void Fit::Print()
{
  /*
    if(NoConvergenceFlag)
    cout << "No Convergence !" << endl;
    cout << "Step=" << Step << " Chi2=" << ChiTwo << " DataPoints=" << XY[0].size() << " NDF=" << NDF << " Chi2Lim=" << ChiTwoLim << " MaxStep=" << MaxStep << endl;
    for(int i = 0; i < Params.size(); i++)
    cout << "Params[" << i << "]=" << Params[i] << " +/- ErrParam[" << i << "]=" << ErrParams[i] << endl;
  */
}

void Fit::SetChi2Lim(double NewChiTwoLim)
{
  ChiTwoLim = NewChiTwoLim;
}

void Fit::SetMaxStep(int NewMaxStep)
{
  MaxStep = NewMaxStep;
}

double Fit::GetChi2Lim()
{
  return ChiTwoLim;
}

int Fit::GetMaxStep()
{
  return MaxStep;
}

int Fit::GetStep()
{
  return Step;
}

void Fit::SetStringFunction(QString NewString)
{
  strStringFunction = NewString;
  
  PointerFunctionFlag = false;
  
  QStringFunctionEvaluator.clearVariables(                 );
  QStringFunctionEvaluator.setExpression (strStringFunction);
}

double Fit::StringFunction(double X, vector <double> P)
{
  for(int i = 0; i < P.size(); i++)
    QStringFunctionEvaluator.setVariable(QObject::trUtf8("p%1", "Parameter") .arg(i), P[i]);
  
  QStringFunctionEvaluator.setVariable(QObject::trUtf8("x", "Variable"), X);
  
  QStringFunctionEvaluator.evaluate();
  
  return QStringFunctionEvaluator.value();
}

double Fit::Value(double X)
{
  if(PointerFunctionFlag)
    return PointerFunction(X, Params);
  else
    return StringFunction(X, Params);
}

void Fit::SetFunction(double (*NewPointerFunction)(double, vector <double>))
{
  PointerFunction = NewPointerFunction;
  
  PointerFunctionFlag = true;
}

bool Fit::NoSolution()
{
  return NoSolutionFlag;
}

bool Fit::NoConvergence()
{
  return NoConvergenceFlag;
}

void Fit::Clear()
{
  NoSolutionFlag      = false;
  NoConvergenceFlag   = false;
  PointerFunctionFlag = true;
  NDF = 0;
  Step = 0;
  MaxStep = 1000;
  ChiTwoLim = 1.e-8;
  strStringFunction = "";
  PointerFunction = Polynomial3;
  Params.clear();
  SetNbParams(2);
  Data.Clear();
}

void Fit::SetNbParams(int NewNbParams)
{
  if(NewNbParams <= 0)
    return;
  
  NDF += Params.size();
  
  Params.resize(NewNbParams);
  ErrParams.resize(NewNbParams);
  
  Params.assign(Params.size(), 1.);
  ErrParams.assign(Params.size(), 0.);
  
  NDF -= Params.size();
  Chi2();
}

int Fit::GetNbParams()
{
  return Params.size();
}
