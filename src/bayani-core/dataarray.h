/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the DataArray Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_DATAARRAY_H
#define BAYANI_DATAARRAY_H

/* "Usual" headers */
#include <math.h>
#include <vector>

/*!
  \file dataarray.h
  \brief Header file for the DataArray class.
  
  The DataArray class is defined in this file.
*/

//! Class to manipulate multi-dimensional data arrays.
/*!
  This class defines a data array with asymmetric errors. Once the data are added, it is possible to have access to some useful properties such as the minimum and maximum values of the data and the errors for example.
*/
class DataArray
{
 protected:
  struct Data{double X; double Y; double XErrorDown; double YErrorDown; double XErrorUp; double YErrorUp;};
  double XMin;
  double XMax;
  double YMin;
  double YMax;
  double XValuePlusErrorMin;
  double XValuePlusErrorMax;
  double YValuePlusErrorMin;
  double YValuePlusErrorMax;
  double MeanX             ;
  double MeanX2            ;
  double SigmaX            ;
  double MeanY             ;
  double MeanY2            ;
  double MeanXY            ;
  double SigmaY            ;
  double Cov               ;
  double Rho               ;
  vector <Data> InternalData;
  
  void Initialize();
  
 public:
         DataArray            ();
        ~DataArray            ();
  void   Clear                ();
  void   AddData              (double, double, double NewXErrorDown = 0., double NewYErrorDown = 0., double NewXErrorUp = 0., double NewYErrorUp = 0.);
  void   Compute              ();
  double GetXMin              ();
  double GetXMax              ();
  double GetYMin              ();
  double GetYMax              ();
  double GetXValuePlusErrorMin();
  double GetXValuePlusErrorMax();
  double GetYValuePlusErrorMin();
  double GetYValuePlusErrorMax();
  int    GetSize              ();
  double GetMeanX             ();
  double GetSigmaX            ();
  double GetMeanY             ();
  double GetSigmaY            ();
  double GetCov               ();
  double GetRho               ();
  double GetX                 (int);
  double GetY                 (int);
  double GetXErrorDown        (int);
  double GetYErrorDown        (int);
  double GetXErrorUp          (int);
  double GetYErrorUp          (int);
};

#endif
