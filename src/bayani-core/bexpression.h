/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the BExpression Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_BEXPRESSION_H
#define BAYANI_BEXPRESSION_H

/* "Usual" headers */
#include <stdio.h>
#include <math.h>
#include <vector>
/* QT headers */
#include <qobject.h>
#include <qstring.h>

#define OBracketType 0
#define CBracketType 1
#define OperatorType 2
#define NumberType   3
#define VariableType 4
#define FunctionType 5

#define OpeningBracket QString("(")
#define ClosingBracket QString(")")

#define OperatorExc    QObject::trUtf8("!" , "Operator")
#define OperatorExp    QObject::trUtf8("^" , "Operator")
#define OperatorMul    QObject::trUtf8("*" , "Operator")
#define OperatorDiv    QObject::trUtf8("/" , "Operator")
#define OperatorAdd    QObject::trUtf8("+" , "Operator")
#define OperatorSub    QObject::trUtf8("-" , "Operator")
#define OperatorInf    QObject::trUtf8("<" , "Operator")
#define OperatorInfEqu QObject::trUtf8("<=", "Operator")
#define OperatorSup    QObject::trUtf8(">" , "Operator")
#define OperatorSupEqu QObject::trUtf8(">=", "Operator")
#define OperatorEquEqu QObject::trUtf8("==", "Operator")
#define OperatorExcEqu QObject::trUtf8("!=", "Operator")
#define OperatorAndAnd QObject::trUtf8("&&", "Operator")
#define OperatorPipPip QObject::trUtf8("||", "Operator")

#define NumberZero "0"
#define NumberOne  "1"
#define NumberTwo  "2"
#define NumberTen  "10"

#define FunctionSqrt  QObject::trUtf8("sqrt" , "Function")
#define FunctionFabs  QObject::trUtf8("fabs" , "Function")
#define FunctionSign  QObject::trUtf8("sign" , "Function")
#define FunctionSin   QObject::trUtf8("sin"  , "Function")
#define FunctionAsin  QObject::trUtf8("asin" , "Function")
#define FunctionCos   QObject::trUtf8("cos"  , "Function")
#define FunctionAcos  QObject::trUtf8("acos" , "Function")
#define FunctionTan   QObject::trUtf8("tan"  , "Function")
#define FunctionAtan  QObject::trUtf8("atan" , "Function")
#define FunctionLog   QObject::trUtf8("log"  , "Function")
#define FunctionLog10 QObject::trUtf8("log10", "Function")
#define FunctionExp   QObject::trUtf8("exp"  , "Function")

/*!
  \file bexpression.h
  \brief Header file for the Element and BExpression classes.
  
  The Element and BExpression classes are defined in this file.
*/

//! Structure that defines a mathematical variable.
/*!
  Useful when used in a vector to handle multiple variables in BExpression.
  
  \param Name  Name of the variable.
  \param Value Value of the variable.
*/
struct Variable
{
  QString Name;
  double Value;
};

//! Class defining a basic mathematical element.
/*!
  This class is used by the BExpression class in order to build a tree representing a mathematical expression.
  \sa BExpression.
*/
class Element
{
 protected:
  void initialize    ();
  bool defineOperator();
  bool defineNumber  ();
  bool defineVariable(vector <Variable> *);
  bool defineFunction();
  
 public:
  QString Name;
  int Type;
  int Index;
  int Depth;
  Element * Parent;
  vector <Element *> Children;
  
  double (* Operator)(double, double);
  int    Priority;
  int    OperandsMin;
  int    OperandsMax;
  bool   (* CheckOperator)(double, double);
  bool   ZeroElement0Flag;
  double ZeroElement0;
  bool   ZeroElement1Flag;
  double ZeroElement1;
  bool   NeutralElement0Flag;
  double NeutralElement0;
  bool   NeutralElement1Flag;
  double NeutralElement1;
  
  double NumberValue;
  
  int VariableIndex;
  
  double (* Function)(double);
  int ArgumentsMin;
  int ArgumentsMax;
  bool (* CheckFunction)(double);
  Element * (* DerivativeFunction)(Element *);
  QString Derivative;
  
            Element();
           ~Element();
  bool      define (vector <Variable> * Variables = NULL);
  Element * copy   ();
  void      adopt  (Element *);
  void      detach ();
};

//! Class to handle mathematical expressions.
/*!
  Mathematical expressions are considered as trees made of elements. This make them easy to manipulate: simplify, evaluate, derive, etc.
  \sa Element.
*/
class BExpression
{
 protected:
  bool                ErrorFlag;
  bool                EvaluateErrorFlag;
  double              Value;
  Element     *       TreeRoot;
  QString             Expression;
  QString             ErrorMessage;
  vector  <Variable > Variables;
  vector  <Element *> Elements;
  
  bool      lex            ();
  bool      parse          ();
  void      setError       (QString, bool NewEvaluateErrorFlag = false);  
  bool      check          ();
  void      write          ();
  void      print          ();
  void      del            ();
  bool      checkElement   (Element *);
  void      writeElement   (Element *);
  void      printElement   (Element *, QString Init = "");
  Element * simplifyElement(Element *);
  double    evaluateElement(Element *);
  Element * deriveElement  (Element *);
  
 public:
                   BExpression   ();
                  ~BExpression   ();
  static bool      isInt         (double);
  static bool      isOperator    (QChar);
  static bool      isDigit       (QChar);
  static bool      isLetter      (QChar);
  static Element * OperElements  (QString, Element *, Element * E2 = NULL);
  static Element * NumElement    (QString);
  static Element * FuncElement   (QString, Element *);
         QString   expression    ();
	 bool      isCorrect     ();
	 QString   error         ();
	 double    value         ();
	 bool      setExpression (QString);
	 void      setVariable   (QString, double Value = 0.);
	 void      setVariables  (vector <Variable>);
	 void      clearVariables();
	 bool      simplify      ();
	 bool      evaluate      ();
	 bool      resolve       ();
	 bool      derive        ();
	 bool      integrate     ();
};

#endif
