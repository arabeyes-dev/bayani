/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the Histogram Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_HISTOGRAM_H
#define BAYANI_HISTOGRAM_H

/* "Usual" headers */
#include <stdio.h>
#include <math.h>
#include <vector>
/* Bayani headers */
#include <dataarray.h>

/*!
  \file histogram.h
  \brief Header file for the Histogram class.
  
  The Histogram class is defined in this file.
*/

//! Class defining a histogram.
/*!
  This class defines a histogram. Once data have been added, it is possible to have access to useful properties such as underflows and overflows, mean, sigma, etc.
*/
class Histogram
{
 protected:
  bool     FixBinsNumberFlag;
  bool     FixXMinFlag;
  bool     FixXMaxFlag;
  int      BinsNumber;
  int      HistoEntries;
  int      UnderFlows;
  int      OverFlows;
  int      Min;
  int      Max;
  int      BinMin;
  int      BinMax;
  double   XMin;
  double   XMax;
  double   DataMin;
  double   DataMax;
  double   BinWidth;
  double   Mean;
  double   Mean2;
  double   Sigma;
  vector   <int>    Bins;
  vector   <double> Data;
  DataArray InternalDataArray;
  
  void Initialize();
  
 public:
            Histogram       ();
           ~Histogram       ();
  void      SetXMin         (double);
  void      SetXMax         (double);
  double    GetXMin         ();
  double    GetXMax         ();
  void      SetBinsNumber   (int);
  int       GetBinsNumber   ();
  void      SetData         (vector <double>);
  vector    <double> GetData();
  void      AddValue        (double);
  void      Compute         ();
  double    GetBinWidth     ();
  int       GetUnderFlows   ();
  int       GetOverFlows    ();
  int       GetMin          ();
  int       GetMax          ();
  double    GetMean         ();
  double    GetSigma        ();
  int       GetBinEntries   (int);
  DataArray GetDataArray    ();
};

#endif
