/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the DataArray Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <dataarray.h>

DataArray::DataArray()
{
  Initialize();
}

DataArray::~DataArray()
{
}

void DataArray::Clear()
{
  InternalData.clear();
  
  Initialize();
}

void DataArray::AddData(double NewX, double NewY, double NewXErrorDown, double NewYErrorDown, double NewXErrorUp, double NewYErrorUp)
{
  Data NewData;
  
  NewData.X          = NewX;
  NewData.Y          = NewY;
  NewData.XErrorDown = NewXErrorDown;
  NewData.YErrorDown = NewYErrorDown;
  NewData.XErrorUp   = NewXErrorUp;
  NewData.YErrorUp   = NewYErrorUp;
  
  InternalData.push_back(NewData);
}

void DataArray::Compute()
{
  if(InternalData.empty())
    return;
  
  XMin = InternalData[0].X;
  XMax = InternalData[0].X;
  YMin = InternalData[0].Y;
  YMax = InternalData[0].Y;
  
  double XValuePlusErrorMinTmp = (InternalData[0].XErrorDown > 0.) ? (InternalData[0].X-InternalData[0].XErrorDown) : InternalData[0].X;
  double XValuePlusErrorMaxTmp = (InternalData[0].XErrorUp   > 0.) ? (InternalData[0].X+InternalData[0].XErrorUp)   : InternalData[0].X;
  double YValuePlusErrorMinTmp = (InternalData[0].YErrorDown > 0.) ? (InternalData[0].Y-InternalData[0].YErrorDown) : InternalData[0].Y;
  double YValuePlusErrorMaxTmp = (InternalData[0].YErrorUp   > 0.) ? (InternalData[0].Y+InternalData[0].YErrorUp)   : InternalData[0].Y;
  
  XValuePlusErrorMin = XValuePlusErrorMinTmp;
  XValuePlusErrorMax = XValuePlusErrorMaxTmp;
  YValuePlusErrorMin = YValuePlusErrorMinTmp;
  YValuePlusErrorMax = YValuePlusErrorMaxTmp;
  
  for(int i = 0; i < InternalData.size(); i++)
    {
      if(InternalData[i].X < XMin)
	XMin = InternalData[i].X;
      
      if(InternalData[i].X > XMax)
	XMax = InternalData[i].X;
      
      if(InternalData[i].Y < YMin)
	YMin = InternalData[i].Y;
      
      if(InternalData[i].Y > YMax)
	YMax = InternalData[i].Y;
      
      XValuePlusErrorMinTmp = (InternalData[i].XErrorDown > 0.) ? (InternalData[i].X-InternalData[i].XErrorDown) : InternalData[i].X;
      XValuePlusErrorMaxTmp = (InternalData[i].XErrorUp   > 0.) ? (InternalData[i].X+InternalData[i].XErrorUp)   : InternalData[i].X;
      YValuePlusErrorMinTmp = (InternalData[i].YErrorDown > 0.) ? (InternalData[i].Y-InternalData[i].YErrorDown) : InternalData[i].Y;
      YValuePlusErrorMaxTmp = (InternalData[i].YErrorUp   > 0.) ? (InternalData[i].Y+InternalData[i].YErrorUp)   : InternalData[i].Y;
      
      if(XValuePlusErrorMinTmp < XValuePlusErrorMin)
	XValuePlusErrorMin = XValuePlusErrorMinTmp;
      
      if(XValuePlusErrorMaxTmp > XValuePlusErrorMax)
	XValuePlusErrorMax = XValuePlusErrorMaxTmp;
      
      if(YValuePlusErrorMinTmp < YValuePlusErrorMin)
	YValuePlusErrorMin = YValuePlusErrorMinTmp;
      
      if(YValuePlusErrorMaxTmp > YValuePlusErrorMax)
	YValuePlusErrorMax = YValuePlusErrorMaxTmp;
      
      MeanX  += InternalData[i].X;
      MeanX2 += InternalData[i].X*InternalData[i].X;
      
      MeanY  += InternalData[i].Y;
      MeanY2 += InternalData[i].Y*InternalData[i].Y;
      
      MeanXY += InternalData[i].X*InternalData[i].Y;
    }
  
  MeanX  /= InternalData.size()     ;
  MeanX2 /= InternalData.size()     ;
  SigmaX  = sqrt(MeanX2-MeanX*MeanX);
  
  MeanY  /= InternalData.size()     ;
  MeanY2 /= InternalData.size()     ;
  SigmaY  = sqrt(MeanY2-MeanY*MeanY);
  
  MeanXY /= InternalData.size();
  
  Cov = MeanXY - MeanX * MeanY;
  
  if(SigmaX != 0. && SigmaY != 0.)
    Rho = Cov / (SigmaX * SigmaY);
}

double DataArray::GetXMin()
{
  return XMin;
}

double DataArray::GetXMax()
{
  return XMax;
}

double DataArray::GetYMin()
{
  return YMin;
}

double DataArray::GetYMax()
{
  return YMax;
}

double DataArray::GetXValuePlusErrorMin()
{
  return XValuePlusErrorMin;
}

double DataArray::GetXValuePlusErrorMax()
{
  return XValuePlusErrorMax;
}

double DataArray::GetYValuePlusErrorMin()
{
  return YValuePlusErrorMin;
}

double DataArray::GetYValuePlusErrorMax()
{
  return YValuePlusErrorMax;
}

int DataArray::GetSize()
{
  return InternalData.size();
}

double DataArray::GetMeanX()
{
  return MeanX;
}

double DataArray::GetSigmaX()
{
  return SigmaX;
}

double DataArray::GetMeanY()
{
  return MeanY;
}

double DataArray::GetSigmaY()
{
  return SigmaY;
}

double DataArray::GetCov()
{
  return Cov;
}

double DataArray::GetRho()
{
  return Rho;
}

double DataArray::GetX(int I)
{
  if(I < 0 || I >= InternalData.size())
    return -1.;
  
  return InternalData[I].X;
}

double DataArray::GetY(int I)
{
  if(I < 0 || I >= InternalData.size())
    return -1.;
  
  return InternalData[I].Y;
}

double DataArray::GetXErrorDown(int I)
{
  if(I < 0 || I >= InternalData.size())
    return -1.;
  
  return InternalData[I].XErrorDown;
}

double DataArray::GetYErrorDown(int I)
{
  if(I < 0 || I >= InternalData.size())
    return -1.;
  
  return InternalData[I].YErrorDown;
}

double DataArray::GetXErrorUp(int I)
{
  if(I < 0 || I >= InternalData.size())
    return -1.;
  
  return InternalData[I].XErrorUp;
}

double DataArray::GetYErrorUp(int I)
{
  if(I < 0 || I >= InternalData.size())
    return -1.;
  
  return InternalData[I].YErrorUp;
}

void DataArray::Initialize()
{
  XMin               = -1.e99;
  XMax               =  1.e99;
  YMin               = -1.e99;
  YMax               =  1.e99;
  XValuePlusErrorMin = -1.e99;
  XValuePlusErrorMax =  1.e99;
  YValuePlusErrorMin = -1.e99;
  YValuePlusErrorMax =  1.e99;
  MeanX              =  0.   ;
  MeanX2             =  0.   ;
  SigmaX             = -1.   ;
  MeanY              =  0.   ;
  MeanY2             =  0.   ;
  SigmaY             = -1.   ;
  MeanXY             =  0.   ;
  Cov                = -1.   ;
  Rho                = -1.e99;
}
