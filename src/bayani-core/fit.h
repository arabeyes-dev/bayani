/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the Fit Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_FIT_H
#define BAYANI_FIT_H

/* "Usual" headers */
#include <math.h>
#include <vector>
/* QT headers */
#include <qobject.h>
#include <qstring.h>
/* Bayani headers */
#include <dataarray.h>
#include <bexpression.h>

/*!
  \file fit.h
  \brief Header file for the Fit class.
  
  The Fit class is defined in this file.
*/

//! Class for fitting data.
/*!  
  This class fits a one variable function on data analytically or numerically, using a built-in algorithm.
*/
class Fit
{
 protected:
  bool        NoSolutionFlag;
  bool        NoConvergenceFlag;
  bool        PointerFunctionFlag;
  int         NDF;
  int         MaxStep;
  int         Step;
  double      ChiTwoLim;
  double      ChiTwo;
  QString     strStringFunction;
  vector      <double> Params;
  vector      <double> ErrParams;
  DataArray   Data;
  BExpression QStringFunctionEvaluator;
  
  double Chi2              ();
  double (*PointerFunction)(double, vector <double>);
  
 public:
         Fit              ();
        ~Fit              ();
  double StringFunction   (double, vector <double>);
  void   FIT              ();
  void   PolynomialFit    (int);
  void   SetData          (DataArray);
  void   SetParams        (vector <double>);
  void   SetParam         (int, double);
  void   FixParam         (int, double);
  void   FixParam         (int);
  void   SetChi2Lim       (double);
  void   SetMaxStep       (int);
  double GetChi2Lim       ();
  int    GetMaxStep       ();
  int    GetStep          ();
  void   GetParams        (vector <double> &);
  void   GetErrParams     (vector <double> &);
  double GetParam         (int);
  double GetErrParam      (int);
  double GetChi2          ();
  int    GetNDF           ();
  void   Print            ();
  void   SetStringFunction(QString);
  void   SetFunction      (double (*NewPointerFunction)(double, vector <double>));
  double Value            (double);
  bool   NoSolution       ();
  bool   NoConvergence    ();
  void   Clear            ();
  void   SetNbParams      (int);
  int    GetNbParams      ();
};

#endif
