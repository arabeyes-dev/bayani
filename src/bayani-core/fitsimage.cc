/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the FITSImage Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <fitsimage.h>

//! Default constructor. Needed especially by FITSImage.
/*!
  Nothing special is done in this constructor.
*/
FITSHeader::FITSHeader()
{
  HeaderSize = 0;
  Header = "";
}

//! Default destructor.
/*!
  Nothing special is done in this destructor.
*/
FITSHeader::~FITSHeader()
{
}

//! Gets the header of a FITS file.
/*!
  This method is used to get the all header block as a string (usually from a FITSImage object).
  
  \param NewHeader The whole header as a string.
*/
void FITSHeader::ReadHeader(string NewHeader, int NewHeaderSize)
{
  HeaderSize = NewHeaderSize;
  Header = NewHeader;
  
  ParseHeader();
}

//! Parses the header.
/*!
  Parses the header and explodes the lines as keys, values and comments.
*/
void FITSHeader::ParseHeader()
{
  Keys.clear();
  Values.clear();
  Comments.clear();

  int Index = 0;
  
  while(Index + FITSKEYLENGTH <= Header.length())
    {
      string Line = Header.substr(Index, FITSKEYLENGTH);
      
      int Index1 = Line.find(FITSVALSEP, 0);
      int Index2 = Line.find(FITSCOMSEP, 0);
      
      if(Index1 == string::npos && Line.substr(0, 3) == "END")
	break;
      
      Keys.push_back(Line.substr(0, Index1));
      Values.push_back(Line.substr(Index1+1, Index2-Index1));
      Comments.push_back(Line.substr(Index2+1, Line.length()-Index2));
      
      Index += FITSKEYLENGTH;
    }
}

//! The number of keys in the header.
/*!
  The number gives back the number of keys that are contained in the object.
  
  \returns Number of read keys.
*/
int FITSHeader::GetKeysNumber()
{
  return Keys.size();
}

//! The value of a key as a string.
/*!
  This function searches and returns the value of the key given in argument.
  
  \param Key The intersting key.
  \returns The parsed value of the key.
*/
string FITSHeader::GetValue(string Key)
{
  for(int i = 0; i < Keys.size(); i++)
    if(Keys[i] == Key)
      return Values[i];
  
  return "";
}

//! Returns the header.
/*!
  This function returns the header so it can be written by FITSImage::WriteFile() for example.
  
  \returns The header as a std::string.
*/
string FITSHeader::GetHeader()
{
  return Header;
}

//! Returns the header's size.
/*!
  This function returns the number of characters composing the header so it can be used by FITSImage::WriteFile() for example and avoid buffer overflows.
  
  \returns The header's size as an integer.
*/
int FITSHeader::GetHeaderSize()
{
  return HeaderSize;
}


//! Default constructor.
/*!
  Nothing special is done in this constructor, except testing the endianness.
*/
FITSImage::FITSImage()
{
  BitPix = 0;
  NAxis = 0;
  XSize = 0;
  YSize = 0;
  DataSize = 0;
  Min = 0;
  Max = 0;
  MinDyn = 0.;
  MaxDyn = 0.;
  Mean = 0.;
  Mean2 = 0.;
  Sigma = 0.;
  Data = NULL;
  BigEndian = IsBigEndian();
}

//! Default destructor.
/*!
  Memory freeing.
*/
FITSImage::~FITSImage()
{
  /*
    if(Data != NULL && XSize > 0 && YSize > 0)
    {
    for(int i = 0; i < XSize; i++)
    delete[] Data[i];
    delete[] Data;
    
    Data = NULL;
    }
  */
}

//! Reads header and data from a FITS file.
/*!
  This method is used to read the header and the data from a file after creating the object (re-reading from the same file or reading a new file without being obliged to create a new object).
  
  \param FileName The FITS image file name.
*/
void FITSImage::ReadFile(QString FileName)
{
  char charKeyLine[FITSKEYLENGTH];
  string strKeyLine = "";
  int Counter = 0;
  
  ifstream FITSFile;
  
  FITSFile.open(FileName, fstream::binary | fstream::in);
  
  if(!FITSFile.is_open())
    return;
  
  FITSFile.read(charKeyLine, sizeof(char)*FITSKEYLENGTH);
  
  strKeyLine = string(charKeyLine);
  Counter = FITSKEYLENGTH;
  
  while(strKeyLine.substr(0,3) != "END")
    {
      if(FITSFile.eof())
	return;
      
      FITSFile.read(charKeyLine, sizeof(char)*FITSKEYLENGTH);
      strKeyLine = string(charKeyLine);
      Counter += FITSKEYLENGTH;
    }
  
  Counter += FITSBLKLENGTH-FITSFile.tellg()%FITSBLKLENGTH;
  
  FITSFile.seekg(0, ios::beg);
  
  char * AddBuff = new char[Counter];
  FITSFile.read(AddBuff, sizeof(char)*Counter);
  Header.ReadHeader(string(AddBuff), Counter);
  delete[] AddBuff;
  
  BitPix = atoi(Header.GetValue("BITPIX  ").c_str());
  NAxis  = atoi(Header.GetValue("NAXIS   ").c_str());
  XSize  = atoi(Header.GetValue("NAXIS1  ").c_str());
  YSize  = atoi(Header.GetValue("NAXIS2  ").c_str());
  DataSize = XSize * YSize;
  
  Data = new ushort*[XSize];
  for(int i = 0; i < XSize; i++)
    Data[i] = new ushort[YSize];
  
  for(int j = 0; j < YSize; j++)
    for(int i = 0; i < XSize; i++)
      {
	FITSFile.read((char*)&Data[i][j], sizeof(ushort));
	if(!BigEndian)
	  Data[i][j] = ConvertEndian(Data[i][j]);
      }
  
  Min = Data[0][0];
  Max = Data[0][0];
  for(int i = 0; i < XSize; i++)
    for(int j = 0; j < YSize; j++)
      {
	if(Data[i][j] > Max)
	  Max = Data[i][j];
	if(Data[i][j] < Min)
	  Min = Data[i][j];

	Mean  += Data[i][j];
	Mean2 += double(Data[i][j])*Data[i][j];
      }
  
  Mean  /= DataSize;
  Mean2 /= DataSize;
  Sigma = sqrt(Mean2-Mean*Mean);
  
  MinDyn = Mean-Sigma;
  MaxDyn = Mean+Sigma;
  
  if(MinDyn < 0.)
    {
      printf("FITSImage::ReadFile(): MinDyn=%f. I fix it to 0.\n", MinDyn);
      
      MinDyn = 0.;
    }
  
  if(MaxDyn > 65535.)
    {
      printf("FITSImage::ReadFile(): MaxDyn=%f. I fix it to 65535.\n", MaxDyn);
      
      MaxDyn = 65535.;
    }
  
  FITSFile.close();
}

//! Writes header and data to a FITS file.
/*!
  This method is used to write the header and the data to a file after creating and manipulating the object.
  
  \param FileName The output FITS image file name.
*/
void FITSImage::WriteFile(QString FileName)
{
  ofstream FITSFile;
  
  FITSFile.open(FileName, fstream::binary | fstream::out | fstream::trunc);
  FITSFile.write(Header.GetHeader().c_str(), Header.GetHeaderSize()*sizeof(char));
  
  for(int j = 0; j < YSize; j++)
    for(int i = 0; i < XSize; i++)
      {
	ushort Value = Data[i][j];
	if(!BigEndian)
	  Value = ConvertEndian(Value);
	FITSFile.write((char*)&Value, sizeof(ushort));
      }
  
  int AddBuffSize = FITSBLKLENGTH-(XSize*YSize*sizeof(ushort))%FITSBLKLENGTH;
  
  for(int i = 0; i < AddBuffSize; i++)
    FITSFile.write(" ", sizeof(char));
  
  FITSFile.close();
}

//! Converts Big Endian <-> Little Endian.
/*!
  This function swaps the bytes of un unsigned short so it changes its state from Big Endian to Little Endian and vice versa.
  
  \param Num ushort to convert.
  \returns Converted ushort.
*/
ushort FITSImage::ConvertEndian(ushort Num)
{
  ushort Num1 = Num << 8;
  ushort Num2 = Num >> 8;
  
  return(Num1+ Num2);
}

//! Tests the hardware Endianness.
/*!
  Since the FITS convention is to write data in Big Endian, it is necessary to test if the hardware has that convention. If not (PC for example), it is necessary to call FITSImage::ConvertEndian() when doing I/O operations.
  
  \returns Returns true if the system is Big Endian, false otherwise.
*/
bool FITSImage::IsBigEndian()
{
  int X = 1;
  
  if (*(char *) &X == 1)
    return false;

  return true;
}

//! To get the XSize of the image.
/*!
  In order to ease image manipulation and drawing, it is necessary to have the images dimensions.
  
  \returns Image X Size.
*/
int FITSImage::GetXSize()
{
  return XSize;
}

//! To get the YSize of the image.
/*!
  In order to ease image manipulation and drawing, it is necessary to have the images dimensions.
  
  \returns Image Y Size.
*/
int FITSImage::GetYSize()
{
  return YSize;
}

//! Gives access to image data.
/*!
  This function returns the pixel value at (I,J)
  
  \param I Pixel along x axis
  \param J Pixel along y axis
  \returns Pixel value
*/
ushort FITSImage::GetPixel(int I, int J)
{
  if((XSize <= 0) || (YSize <= 0))
    {
      printf("FITSImage::GetPixel(): XSize=%d YSize=%d.\n", XSize, YSize);
      
      return 0;
    }
  
  if(
     ((I < 0) || (I >= XSize))
     ||
     ((J < 0) || (J >= YSize))
     )
    {
      printf("FITSImage::GetPixel(): XSize=%d YSize=%d I=%d J=%d.\n", XSize, YSize, I, J);
      
      return 0;
    }
  
  if(Data == NULL)
    {
      printf("FITSImage::GetPixel(): Data=NULL.\n");
      
      return 0;
    }
  
  return Data[I][J];
}

//! Changes the pixel's value.
/*!
  This function changes a pixel's value.
  
  \param I Pixel along x axis
  \param J Pixel along y axis
  \param PixelValue
*/
void FITSImage::PutPixel(int I, int J, ushort PixelValue)
{
  if((XSize <= 0) || (YSize <= 0))
    {
      printf("FITSImage::PutPixel(): XSize=%d YSize=%d.\n", XSize, YSize);
      
      return;
    }
  
  if(
     ((I < 0) || (I > XSize))
     ||
     ((J < 0) || (J > YSize))
     )
    {
      printf("FITSImage::PutPixel(): XSize=%d YSize=%d I=%d J=%d.\n", XSize, YSize, I, J);
      
      return;
    }
  
  if(Data == NULL)
    {
      printf("FITSImage::PutPixel(): Data=NULL.\n");
      
      return;
    }
  
  Data[I][J] = PixelValue;
}

//! Returns the image's minimum.
/*!
  Use this function to access the image's minimal value.
  
  \returns Image's minimum value.
*/
ushort FITSImage::GetMin()
{
  return Min;
}

//! Returns the image's maximum.
/*!
  Use this function to access the image's maximal value.
  
  \returns Image's maximum value.
*/
ushort FITSImage::GetMax()
{
  return Max;
}

//! Returns the image's dynamical minimum.
/*!
  Use this function to access the image's minimal dynamical value. This value is computed via MinDyn = Mean-Sigma, where Mean is the pixels' mean value and Sigma their standard deviation. This value is convenient to use when displaying the image and setting a palette, so the latter can go between (the useful) minimal and maximal dynamical values.
  
  \returns Image's minimum dynamical value.
*/
double FITSImage::GetMinDyn()
{
  return MinDyn;
}

//! Returns the image's dynamical maximum.
/*!
  Use this function to access the image's maximal dynamical value. This value is computed via MaxDyn = Mean+Sigma, where Mean is the pixels' mean value and Sigma their standard deviation. This value is convenient to use when displaying the image and setting a palette, so the latter can go between (the useful) minimal and maximal dynamical values.
  
  \returns Image's maximum dynamical value.
*/
double FITSImage::GetMaxDyn()
{
  return MaxDyn;
}
