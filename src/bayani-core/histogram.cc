/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the Histogram Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <histogram.h>

//! Basic constructor.
/*!
  This constructor is used by default. Inside we intitialize the different variables used in the histograms, using the Initialize() method.
  The default histogram is a 100-bin one, defined between 0. and 1.
*/
Histogram::Histogram()
{
  BinsNumber = 100;
  XMin       = 0.;
  XMax       = 1.;
  BinWidth   = 0.01;
  
  FixBinsNumberFlag = false;
  FixXMinFlag       = false;
  FixXMaxFlag       = false;
  
  Initialize();
}

//! Basic destructor.
/*!
  This destructor is used by default.
*/
Histogram::~Histogram()
{
}

//! Initialization function.
/*!
  This is a protected method. It is called by the constructor and inside the Compute() method in order to re-initialize all the histogram variables.
*/
void Histogram::Initialize()
{
  HistoEntries = 0;
  UnderFlows   = 0;
  OverFlows    = 0;
  Min          = 0;
  Max          = 0;
  BinMin       = 0;
  BinMax       = 0;
  DataMin      = 0.;
  DataMax      = 0.;
  Mean         = 0.;
  Mean2        = 0.;
  Sigma        = 0.;
  Bins.resize(BinsNumber);
  Bins.assign(BinsNumber, 0);
  InternalDataArray.Clear();
}

//! Sets the histogram's minimal bound.
/*!
  Use this function to set the minimal (left) histogram's bound.
  
  \param NewXMin The new minimal value.
*/
void Histogram::SetXMin(double NewXMin)
{
  XMin = NewXMin;
  
  FixXMinFlag = true;
}

//! Sets the histogram's maximal bound.
/*!
  Use this function to set the maximal (right) histogram's bound.
  
  \param NewXMax The new maximal value.
*/
void Histogram::SetXMax(double NewXMax)
{
  XMax = NewXMax;
  
  FixXMaxFlag = true;
}

//! Returns the histogram's minimal bound.
/*!
  Use this function to get the minimal (left) histogram's bound.
  
  \returns The minimal value.
*/
double Histogram::GetXMin()
{
  return XMin;
}

//! Returns the histogram's maximal bound.
/*!
  Use this function to get the maximal (right) histogram's bound.
  
  \returns The maximal value.
*/
double Histogram::GetXMax()
{
  return XMax;
}

//! Sets the histogram's bins' number.
/*!
  Use this function to set the number of bins that are contained in the histogram.
  
  \param NewBinsNumber The new number of bins.
*/
void Histogram::SetBinsNumber(int NewBinsNumber)
{
  if(NewBinsNumber > 0)
    {
      BinsNumber = NewBinsNumber;
      
      FixBinsNumberFlag = true;
    }
}

//! Returns the histogram's bins' number.
/*!
  Use this function to get the number of bins that are contained in the histogram.
  
  \returns The number of bins.
*/
int Histogram::GetBinsNumber()
{
  return BinsNumber;
}

//! Sets the histogram's data.
/*!
  Use this function to set the data of the histogram.
  
  \param NewData The new data vector.
*/
void Histogram::SetData(vector <double> NewData)
{
  Data = NewData;
}

//! Returns the histogram's data.
/*!
  Use this function to get the whole hitogram's data.
  
  \returns The data vector.
*/
vector <double> Histogram::GetData()
{
  return Data;
}

//! Adds a new value to the histogram.
/*!
  Use this function to add a new value to the histogram.
  
  \param NewValue The new value.
*/
void Histogram::AddValue(double NewValue)
{
  Data.push_back(NewValue);
}

//! Computes the histogram's relevant quantities.
/*!
  Use this function <b>after</b> filling the histogram in order to compute the mean, the sigma, etc.
*/
void Histogram::Compute()
{
  if(Data.empty())
    return;
  
  /* Automatic computation of the number of bins depending on the data size */
  int    LocalBinsNumber = int(sqrt(Data.size()));
  double LocalXMin       = 0.;
  double LocalXMax       = 1.;
  
  DataMin = Data[0];
  DataMax = Data[0];
  
  for(int i = 0; i < Data.size(); i++)
    {
      if(Data[i] < DataMin)
	DataMin = Data[i];
      
      if(Data[i] > DataMax)
	DataMax = Data[i];
    }
  
  if(DataMin < DataMax)
    {
      LocalXMin = DataMin;
      LocalXMax = DataMax;
      
      /*
	Artificial augmentation of LocalXMax by 1% of the total width.
	Otherwise, DataMax will be out of the histogram.
      */
      LocalXMax += 0.01*(LocalXMax-LocalXMin);
    }
  else
    /* DataMin == DataMax */
    {
      LocalXMin = DataMin-0.5;
      LocalXMax = DataMax+0.5;
    }
  
  if(!FixBinsNumberFlag)
    BinsNumber = LocalBinsNumber;
  if(!FixXMinFlag)
    XMin = LocalXMin;
  if(!FixXMaxFlag)
    XMax = LocalXMax;
  
  if(XMin > XMax)
    {
      if(FixXMinFlag == FixXMaxFlag)
	{
	  printf("Histogram::Compute(): FixXMinFlag=FixXMaxFlag=%d and (XMin=%g)>(XMax=%g).\n", FixXMinFlag, XMin, XMax);
	  
	  double TmpVal = XMin;
	  
	  XMin = XMax;
	  XMax = TmpVal;
	}
      else
	{
	  if(FixXMinFlag)
	    XMax = XMin + 0.5;
	  else
	    XMin = XMax - 0.5;
	}
    }
  
  if(XMin == XMax)
    {
      if(FixXMinFlag == FixXMaxFlag)
	{
	  printf("Histogram::Compute(): FixXMinFlag=FixXMaxFlag=%d and XMin=XMax=%g.\n", FixXMinFlag, XMin);
	  
	  XMin -= 0.5;
	  XMax += 0.5;
	}
      else
	{
	  if(FixXMinFlag)
	    XMax = XMin + 0.5;
	  else
	    XMin = XMax - 0.5;
	}
    }
  
  BinWidth = (XMax-XMin)/BinsNumber;
  
  Initialize();
  
  for(int i = 0; i < Data.size(); i++)
    {
      int Test = int((Data[i]-XMin)/BinWidth);
      
      if(Test < 0)
	UnderFlows++;
      else if(Test >= BinsNumber)
	OverFlows++;
      else
	{
	  HistoEntries++;
	  Bins[Test]++;
	  Mean += Data[i];
	  Mean2 += Data[i]*Data[i];
	}
    }
  
  Min = Bins[0];
  Max = Bins[0];
  
  for(int i = 0; i < BinsNumber; i++)
    {
      InternalDataArray.AddData(XMin+0.5*BinWidth+i*BinWidth, double(Bins[i]));
      
      if(Bins[i] < Min)
	{
	  Min = Bins[i];
	  BinMin = i;
	}
      
      if(Bins[i] > Max)
	{
	  Max = Bins[i];
	  BinMax = i;
	}
    }
    
  Mean  /= HistoEntries;
  Mean2 /= HistoEntries;
  Sigma =  sqrt(Mean2-Mean*Mean);
}

//! Returns the histogram's bin width.
/*!
  Use this function to get the width of each bin in the histogram.
  
  \returns The bin width.
*/
double Histogram::GetBinWidth()
{
  return BinWidth;
}

//! Returns the histogram's underflows.
/*!
  Use this function to get the number of underflows of the histogram, ie. the number of entered data, which are less than the XMin value and thus do not count in the histogram.
  
  \returns The number underflows.
*/
int Histogram::GetUnderFlows()
{
  return UnderFlows;
}

//! Returns the histogram's overflows.
/*!
  Use this function to get the number of overflows of the histogram, ie. the number of entered data, which are greater than the XMax value and thus do not count in the histogram.
  
  \returns The number of overflows.
*/
int Histogram::GetOverFlows()
{
  return OverFlows;
}

//! Returns the histogram's minimal value.
/*!
  Use this function to get the minimal value of the histogram (the number of entries in the bin that contains the smallest number of entries).
  
  \returns The minimal value.
*/
int Histogram::GetMin()
{
  return Min;
}

//! Returns the histogram's maximal value.
/*!
  Use this function to get the maximal value of the histogram (the number of entries in the bin that contains the greatest number of entries).
  
  \returns The maximal value.
*/
int Histogram::GetMax()
{
  return Max;
}

//! Returns the histogram's mean value.
/*!
  Use this function to get the mean value of the histogram (the data's mean value).
  
  \returns The mean value.
*/
double Histogram::GetMean()
{
  return Mean;
}

//! Returns the histogram's standard deviation.
/*!
  Use this function to get the sigma of the histogram (the data's sigma).
  
  \returns The standard deviation.
*/
double Histogram::GetSigma()
{
  return Sigma;
}

//! Returns the content of a bin.
/*!
  Use this function to get the number of entries that a bin contains.
  
  \param BinNumber The number of the bin.
  \returns The Entries number;
*/
int Histogram::GetBinEntries(int BinNumber)
{
  if(BinNumber < 0 || BinNumber >= Bins.size())
    {
      printf("Histogram::GetBinEntries(): BinNumber=%d is outside the permitted range [0, %d].\n", BinNumber, Bins.size()-1);
      
      return -1;
    }
  
  return Bins[BinNumber];
}

DataArray Histogram::GetDataArray()
{
  return InternalDataArray;
}
