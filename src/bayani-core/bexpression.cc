/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani implementation of the BExpression Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

using namespace std;

/* Bayani headers */
#include <bexpression.h>

/* Operators */

double notf(double A, double B){return   !B;}
double mul (double A, double B){return A *B;}
double div (double A, double B){return A /B;}
double add (double A, double B){return A +B;}
double sub (double A, double B){return A -B;}
double lt  (double A, double B){return A <B;}
double le  (double A, double B){return A<=B;}
double gt  (double A, double B){return A >B;}
double ge  (double A, double B){return A>=B;}
double eq  (double A, double B){return A==B;}
double ne  (double A, double B){return A!=B;}
double andf(double A, double B){return A&&B;}
double orf (double A, double B){return A||B;}

/* Functions */

double sign(double A)
{
  if(A >= 0.)
    return 1.;
  else
    return -1.;
}

/* Operators check */

bool checkDiv(double A, double B)
{
  if(!B)
    return false;
  
  return true;
}

bool checkPow(double A, double B)
{
  if(
     (!A && B < 0.)
     ||
     (A < 0. && !BExpression::isInt(B))
     )
    return false;
  
  return true;
}

/* Functions check */

bool checkSqrt(double A)
{
  if(A < 0.)
    return false;
  
  return true;
}

bool checkAsin(double A)
{
  if(A < -1. || A > 1.)
    return false;
  
  return true;
}

bool checkTan(double A)
{
  if(cos(A) == 0.)
    return false;
  
  return true;
}

bool checkLog(double A)
{
  if(A <= 0.)
    return false;
  
  return true;
}

/* Functions derivatives */

Element * deriveSqrt(Element * E)
{
  Element * E1 = BExpression::OperElements(OperatorMul, BExpression::NumElement(NumberTwo), BExpression::FuncElement(FunctionSqrt, E));
  
  return BExpression::OperElements(OperatorDiv, BExpression::NumElement(NumberOne), E1);
}

Element * deriveFabs(Element * E)
{
  return BExpression::FuncElement(FunctionSign, E);
}

Element * deriveSign(Element * E)
{
  return BExpression::NumElement(NumberZero);
}

Element * deriveAsin(Element * E)
{
  Element * E1 = BExpression::OperElements(OperatorExp, E                                 , BExpression::NumElement(NumberTwo));
  Element * E2 = BExpression::OperElements(OperatorSub, BExpression::NumElement(NumberOne), E1                                );
  Element * E3 = BExpression::FuncElement(FunctionSqrt, E2);
  
  return BExpression::OperElements(OperatorDiv, BExpression::NumElement(NumberOne), E3);
}

Element * deriveCos(Element * E)
{
  return BExpression::OperElements(OperatorSub, BExpression::FuncElement(FunctionSin, E));
}

Element * deriveAcos(Element * E)
{
  Element * E1 = BExpression::OperElements(OperatorExp, E                                 , BExpression::NumElement(NumberTwo));
  Element * E2 = BExpression::OperElements(OperatorSub, BExpression::NumElement(NumberOne), E1                                );
  Element * E3 = BExpression::FuncElement(FunctionSqrt, E2);
  Element * E4 = BExpression::OperElements(OperatorDiv, BExpression::NumElement(NumberOne), E3);
  
  return BExpression::OperElements(OperatorSub, E4);
}

Element * deriveTan(Element * E)
{
  Element * E1 = BExpression::OperElements(OperatorExp, BExpression::FuncElement(FunctionCos, E), BExpression::NumElement(NumberTwo));
  
  return BExpression::OperElements(OperatorDiv, BExpression::NumElement(NumberOne), E1);
}

Element * deriveAtan(Element * E)
{
  Element * E1 = BExpression::OperElements(OperatorExp, E                                 , BExpression::NumElement(NumberTwo));
  Element * E2 = BExpression::OperElements(OperatorAdd, BExpression::NumElement(NumberOne), E1                                );
  
  return BExpression::OperElements(OperatorDiv, BExpression::NumElement(NumberOne), E2);
}

Element * deriveLog(Element * E)
{
  return BExpression::OperElements(OperatorDiv, BExpression::NumElement(NumberOne), E);
}

Element * deriveLog10(Element * E)
{
  Element * E1 = BExpression::OperElements(OperatorDiv, BExpression::NumElement(NumberOne), BExpression::FuncElement(FunctionLog, BExpression::NumElement(NumberTen)));
  Element * E2 = BExpression::OperElements(OperatorDiv, BExpression::NumElement(NumberOne), E);
  
  return BExpression::OperElements(OperatorMul, E1, E2);
}

/* Element class */

Element::Element()
{
  initialize();
}

Element::~Element()
{
  for(int i = 0; i < Children.size(); i++)
    delete Children[i];
}

//! Defines the element's attributes.
/*!
  Defines <b>only</b> the attributes corresponding to the object's type. Call this method <b>without</b> argument when the object is an operator, a number or a function. However, be sure that the argument is <b>valid</b> when the object is a variable.
  \param Variables A pointer to a Variable vector.
  \returns Returns false if the attributes could not be set (usually happens when the element's Name is not recognized). Returns true otherwise.
  \sa defineOperator, defineNumber, defineVariable and defineFunction.
*/
bool Element::define(vector <Variable> * Variables)
{
  bool Value = false;
  
  switch(Type)
    {
    case OperatorType:
      Value = defineOperator();
      break;
    case NumberType:
      Value = defineNumber();
      break;
    case VariableType:
      Value = defineVariable(Variables);
      break;
    case FunctionType:
      Value = defineFunction();
      break;
    default:
      Value = false;
      break;
    }
  
  return Value;
}

//! Copies the element.
/*!
  Creates an exact copy of the current object, along with its children. The copy has no parent however (Parent = NULL).
  \returns Returns a new copy of the element.
*/
Element * Element::copy()
{
  Element * NewElement = new Element;
  
  *NewElement = *this;
  
  for(int i = 0; i < Children.size(); i++)
    {
      NewElement->Children[i] = Children[i]->copy();
      
      NewElement->Children[i]->Parent = NewElement;
    }
  
  NewElement->Parent = NULL;
  
  return NewElement;
}

void Element::adopt(Element * NewChild)
{
  NewChild->detach();
  
  NewChild->Parent = this;
  
  Children.push_back(NewChild);
}

//! Detaches the element from its parent.
/*!
  The object is removed for its parent's children list, and the Parent attribute is set to NULL. Does nothing if the object has no parent initially . This method assumes that the relationship parent-child is well defined on both sides. 
*/
void Element::detach()
{
  if(!Parent)
    return;
  
  int j;
  
  for(int i = 0; i < Parent->Children.size(); i++)
    if(Parent->Children[i] == this)
      {
	j = i;
	
	break;
      }
  
  for(int i = j; i < Parent->Children.size()-1; i++)
    Parent->Children[i] = Parent->Children[i+1];
  
  Parent->Children.pop_back();
  
  Parent = NULL;
}

void Element::initialize()
{
  Name   = NumberZero;
  Type   = NumberType;
  Index  = 0;
  Depth  = 0;
  Parent = NULL;
  
  define();
}

//! Defines operators' attributes.
/*!
  Operators' priorities are set according to the table in http://www.cplusplus.com/doc/tutorial/tut1-3.html
  \returns \returns Returns false if the attributes could not be set (usually happens when the element's Name is not recognized). Returns true otherwise.
  \sa define, defineNumber, defineVariable and defineFunction.
*/
bool Element::defineOperator()
{
  OperandsMin         = 2    ;
  OperandsMax         = 2    ;
  CheckOperator       = NULL ;
  ZeroElement0Flag    = false;
  ZeroElement0        = 0.   ;
  ZeroElement1Flag    = false;
  ZeroElement1        = 0.   ;
  NeutralElement0Flag = false;
  NeutralElement0     = 0.   ;
  NeutralElement1Flag = false;
  NeutralElement1     = 0.   ;
  
  if(Name == OperatorExc)
    {
      Priority    = 3;
      OperandsMin = 1;
      OperandsMax = 1;
      Operator    = notf;
    }
  
  else if(Name == OperatorExp)
    {
      Priority            = 0;/* ? */
      Operator            = pow;
      CheckOperator       = checkPow;
      ZeroElement0Flag    = true;
      ZeroElement0        = 1.  ;//0 too ! (with a restriction on the other element) [vector?]
      //ZeroElement1Flag    = true;
      //ZeroElement1        = 0.   ;//0 but the result is 1 (with a restriction on the other element)
      NeutralElement1Flag = true;
      NeutralElement1     = 1.  ;
    }
  
  else if(Name == OperatorMul)
    {
      Priority            = 4   ;
      Operator            = mul ;
      ZeroElement0Flag    = true;
      ZeroElement0        = 0.  ;
      ZeroElement1Flag    = true;
      ZeroElement1        = 0.   ;
      NeutralElement0Flag = true;
      NeutralElement0     = 1.  ;
      NeutralElement1Flag = true;
      NeutralElement1     = 1.  ;
    }
  
  else if(Name == OperatorDiv)
    {
      Priority            = 4       ;
      Operator            = div     ;
      CheckOperator       = checkDiv;
      ZeroElement0Flag    = true    ;
      ZeroElement0        = 0.      ;
      NeutralElement1Flag = true    ;
      NeutralElement1     = 1.      ;
    }
  
  else if(Name == OperatorAdd)
    {
      Priority            = 5;
      OperandsMin         = 1;
      Operator            = add;
      NeutralElement0Flag = true;
      NeutralElement0     = 0.  ;
      NeutralElement1Flag = true;
      NeutralElement1     = 0.  ;
    }
  
  else if(Name == OperatorSub)
    {
      Priority            = 5;
      OperandsMin         = 1;
      Operator            = sub;
      NeutralElement1Flag = true;
      NeutralElement1     = 0.  ;
    }
  
  else if(Name == OperatorInf)
    {
      Priority = 7;
      Operator = lt;
    }
  
  else if(Name == OperatorInfEqu)
    {
      Priority = 7;
      Operator = le;
    }
  
  else if(Name == OperatorSup)
    {
      Priority = 7;
      Operator = gt;
    }
  
  else if(Name == OperatorSupEqu)
    {
      Priority = 7;
      Operator = ge;
    }
  
  else if(Name == OperatorEquEqu)
    {
      Priority = 8;
      Operator = eq;
    }
  
  else if(Name == OperatorExcEqu)
    {
      Priority = 8;
      Operator = ne;
    }
  
  else if(Name == OperatorAndAnd)
    {
      Priority = 10;
      Operator = andf;
    }
  
  else if(Name == OperatorPipPip)
    {
      Priority = 10;
      Operator = orf;
    }
  
  else
    return false;
  
  return true;
}

bool Element::defineNumber()
{
  NumberValue = Name.toDouble();
  
  return true;
}

bool Element::defineVariable(vector <Variable> * Variables)
{
  for (int i = 0; i < Variables->size(); i++)
    if((*Variables)[i].Name == Name)
      {
	VariableIndex = i;
	
	return true;
      }
  
  return false;
}

bool Element::defineFunction()
{
  ArgumentsMin       = 1;
  ArgumentsMax       = 1;
  CheckFunction      = NULL;
  DerivativeFunction = NULL;
  Derivative         = "";
  
  if(Name == FunctionSqrt)
    {
      Function           = sqrt;
      CheckFunction      = checkSqrt;
      DerivativeFunction = deriveSqrt;
    }
  
  else if(Name == FunctionFabs)
    {
      Function           = fabs;
      DerivativeFunction = deriveFabs;
    }
  
  else if(Name == FunctionSign)
    {
      Function           = sign;
      DerivativeFunction = deriveSign;
    }
  
  else if(Name == FunctionSin)
    {
      Function   = sin;
      Derivative = FunctionCos;
    }
  
  else if(Name == FunctionAsin)
    {
      Function           = asin;
      CheckFunction      = checkAsin;
      DerivativeFunction = deriveAsin;
    }
  
  else if(Name == FunctionCos)
    {
      Function           = cos;
      DerivativeFunction = deriveCos;
    }
  
  else if(Name == FunctionAcos)
    {
      Function           = acos;
      CheckFunction      = checkAsin;
      DerivativeFunction = deriveAcos;
    }
  
  else if(Name == FunctionTan)
    {
      Function           = tan;
      CheckFunction      = checkTan;
      DerivativeFunction = deriveTan;
    }
  
  else if(Name == FunctionAtan)
    {
      Function           = atan;
      DerivativeFunction = deriveAtan;
    }
  
  else if(Name == FunctionLog)
    {
      Function           = log;
      CheckFunction      = checkLog;
      DerivativeFunction = deriveLog;
    }
  
  else if(Name == FunctionLog10)
    {
      Function           = log10;
      CheckFunction      = checkLog;
      DerivativeFunction = deriveLog10;
    }
  
  else if(Name == FunctionExp)
    {
      Function   = exp;
      Derivative = FunctionExp;
    }
  
  else
    return false;
  
  return true;
}

/* BExpression class */

BExpression::BExpression()
{
  TreeRoot = NULL;
  
  setExpression("");
}

BExpression::~BExpression()
{
  if(TreeRoot)
    delete TreeRoot;
}

bool BExpression::isInt(double A)
{
  if(A-int(A))
    return false;
  
  return true;  
}

bool BExpression::isOperator(QChar Character)
{
  return(
	 Character == '>' ||
	 Character == '<' ||
	 Character == '=' ||
	 Character == '!' ||
	 Character == '&' ||
	 Character == '|' ||
	 Character == '^' ||
	 Character == '%' ||
	 Character == "*" ||
	 Character == "/" ||
	 Character == '+' ||
	 Character == '-'
	 );
}

bool BExpression::isDigit(QChar Character)
{
  return (Character.isDigit() || Character == QObject::trUtf8(".", "Digits separator (comma)"));
}

bool BExpression::isLetter(QChar Character)
{
  return (Character.isLetter() || Character == '_');
}

Element * BExpression::OperElements(QString OperatorName, Element * E1, Element * E2)
{
  Element * Operator = new Element;
  
  Operator->Name = OperatorName;
  Operator->Type = OperatorType;
  
  Operator->define();
  
  Operator->adopt(E1);
  
  if(E2)
    Operator->adopt(E2);
  
  return Operator;
}

Element * BExpression::NumElement(QString NumberStr)
{
  Element * Number = new Element;
  
  Number->Name = NumberStr;
  Number->Type = NumberType;
  
  Number->define();
  
  return Number;
}

Element * BExpression::FuncElement(QString FunctionName, Element * E)
{
  Element * Function = new Element;
  
  Function->Name = FunctionName;
  Function->Type = FunctionType;
  
  Function->define();
  
  Function->adopt(E);
  
  return Function;
}

QString BExpression::expression()
{
  return Expression;
}

bool BExpression::isCorrect()
{
  return !ErrorFlag;
}

QString BExpression::error()
{
  return ErrorMessage;
}

double BExpression::value()
{
  return Value;
}

bool BExpression::setExpression(QString NewExpression)
{
  Expression = NewExpression;
  
  if(TreeRoot)
    {
      delete TreeRoot;
      
      TreeRoot = NULL;
    }
  
  ErrorFlag         = false;
  EvaluateErrorFlag = false;
  ErrorMessage      = "";
  
  Value = 0.;
  
  if(!lex())
    {
      del();
      
      return false;
    }
  
  if(!parse())
    {
      del();
      
      return false;
    }
  
  return check();
}

//! Sets a new variable or updates an existing one.
/*!
  Call this function <b>before</b> setExpression() in order to define a variable, otherwise there will be a syntax error when setExpression() is called. After a call to setExpression(), all you can do is update the values of the already set variables (i.e. you can't define a new variable) until setExpression() is called again.
  \param Name Name of the variable.
  \param Value Its value (defaulted to 0).
  \sa setVariables and clearVariables.
*/
void BExpression::setVariable(QString Name, double Value)
{
  Variable NewVariable;
  
  NewVariable.Name  = Name;
  NewVariable.Value = Value;
  
  for (int i = 0; i < Variables.size(); i++)
    if(Variables[i].Name == NewVariable.Name)
      {
        Variables[i].Value = NewVariable.Value;
	
        return;
      }
  
  Variables.push_back(NewVariable);
}

//! Sets a new variables list (and erases the previous one if any).
/*!
  Call this function <b>before</b> setExpression() in order to define a new list of variables, otherwise there may be evaluation errors and/or segfaults.
  \param NewVariables New list of variables.
  \sa setVariable and clearVariables.
*/
void BExpression::setVariables(vector <Variable> NewVariables)
{
  Variables = NewVariables;
}

//! Clears the variables list.
/*!
  Call this function <b>before</b> setExpression() in order to clear the list of variables (and before defining a new one, if any), otherwise there may be segfaults.
  \sa setVariable and setVariables.
*/
void BExpression::clearVariables()
{
  Variables.clear();
}

bool BExpression::simplify()
{
  if(isCorrect())
    {
      TreeRoot = simplifyElement(TreeRoot);
      
      write();
      
      if(EvaluateErrorFlag)
	{
	  EvaluateErrorFlag = false;
	  
	  return false;
	}
      else
	return true;
    }
  
  return false;
}

bool BExpression::evaluate()
{
  if(isCorrect())
    {
      Value = evaluateElement(TreeRoot);
      
      if(EvaluateErrorFlag)
	{
	  EvaluateErrorFlag = false;
	  
	  Value = 0.;
	  
	  return false;
	}
      else
	return true;
    }
  
  return false;
}

bool BExpression::resolve()
{
  return isCorrect();
}

bool BExpression::derive()
{
  if(isCorrect())
    {
      if(!simplify())
	return false;
      
      TreeRoot = deriveElement(TreeRoot);
      
      if(!simplify())
	return false;
      else
	return true;
    }
  
  return false;
}

bool BExpression::integrate()
{
  return isCorrect();
}

bool BExpression::lex()
{
  Elements.clear();
  
  if(Expression.remove(" ").isEmpty())
    {
      setError(QObject::trUtf8("Empty expression"));
      
      return false;
    }
  
  bool InsertFlag = false;
  
  bool DecodingOperator = false;
  bool DecodingNumber   = false;
  bool DecodingVariable = false;
  
  int Depth = 0;
  
  int OperatorStartIndex = -1;
  int NumberStartIndex   = -1;
  int VariableStartIndex = -1;
  
  int OperatorLength = 1;
  int NumberLength   = 1;
  int VariableLength = 1;
  
  Element * NewElement1 = NULL;
  Element * NewElement2 = NULL;
  Element * NewElement3 = NULL;
  Element * NewElement4 = NULL;
  
  for(int i = 0; i < Expression.length(); i++)
    {
      if(Expression.mid(i, 1) == OpeningBracket)
	{
	  DecodingOperator = false;
	  DecodingNumber   = false;
	  DecodingVariable = false;
	  
	  NewElement1 = new Element;
	  
	  NewElement1->Name  = OpeningBracket;
	  NewElement1->Type  = OBracketType;
	  NewElement1->Index = i;
	  NewElement1->Depth = Depth;
	  
	  InsertFlag = true;
	  
	  Depth ++;
	}
      
      else if(Expression.mid(i, 1) == ClosingBracket)
	{
	  Depth --;
	  
	  DecodingOperator = false;
	  DecodingNumber   = false;
	  DecodingVariable = false;
	  
	  NewElement1 = new Element;
	  
	  NewElement1->Name  = ClosingBracket;
	  NewElement1->Type  = CBracketType;
	  NewElement1->Index = i;
	  NewElement1->Depth = Depth;
	  
	  InsertFlag = true;
	}
      
      else if(isOperator(Expression[i]))
	{
	  if(OperatorStartIndex == -1)
	    {
	      DecodingOperator = true;
	      DecodingNumber   = false;
	      DecodingVariable = false;
	      
	      OperatorStartIndex = i;
	      
	      NewElement2 = new Element;
	      
	      NewElement2->Depth = Depth;
	    }
	  else
	    OperatorLength = i - OperatorStartIndex + 1;
	}
      
      else if(isDigit(Expression[i]))
	{
	  if(!DecodingVariable)
	    {
	      if(NumberStartIndex == -1)
		{
		  DecodingOperator = false;
		  DecodingNumber   = true;
		  DecodingVariable = false;
		  
		  NumberStartIndex = i;
		  
		  NewElement3 = new Element;
		  
		  NewElement3->Depth = Depth;
		}
	      else
		NumberLength = i - NumberStartIndex + 1;
	    }
	  else
	    /* We allow variables names such as x0 */
	    VariableLength = i - VariableStartIndex + 1;
	}
      
      else if(isLetter(Expression[i]))
	{
	  if(VariableStartIndex == -1)
	    {
	      DecodingOperator = false;
	      DecodingNumber   = false;
	      DecodingVariable = true;
	      
	      VariableStartIndex = i;
	      
	      NewElement4 = new Element;
	      
	      NewElement4->Depth = Depth;
	    }
	  else
	    VariableLength = i - VariableStartIndex + 1;
	}
      
      else
	{
	  setError(QObject::trUtf8("Undefined symbol '%1' (index %2)") .arg(Expression.mid(i, 1)) .arg(i));
	  
	  if(NewElement1) delete NewElement1;
	  if(NewElement2) delete NewElement2;
	  if(NewElement3) delete NewElement3;
	  if(NewElement4) delete NewElement4;
	  
	  return false;
	}
      
      if(OperatorStartIndex > -1 && !DecodingOperator)
	{
	  NewElement2->Name  = Expression.mid(OperatorStartIndex, OperatorLength);
	  NewElement2->Type  = OperatorType;
	  NewElement2->Index = OperatorStartIndex;
	  
	  Elements.push_back(NewElement2);
	  
	  NewElement2 = NULL;
	  
	  OperatorStartIndex = -1;
	  
	  OperatorLength = 1;
	}
      
      if(NumberStartIndex > -1 && !DecodingNumber)
	{
	  NewElement3->Name  = Expression.mid(NumberStartIndex, NumberLength);
	  NewElement3->Type  = NumberType;
	  NewElement3->Index = NumberStartIndex;
	  
	  Elements.push_back(NewElement3);
	  
	  NewElement3 = NULL;
	  
	  NumberStartIndex = -1;
	  
	  NumberLength = 1;
	}
      
      if(VariableStartIndex > -1 && !DecodingVariable)
	{
	  NewElement4->Name  = Expression.mid(VariableStartIndex, VariableLength);
	  NewElement4->Type  = VariableType;
	  NewElement4->Index = VariableStartIndex;
	  
	  /* If the variable preceeds an opening bracket then it should be interpreted as a function */
	  if(InsertFlag && NewElement1->Type == OBracketType)
	    NewElement4->Type = FunctionType;
	  
	  Elements.push_back(NewElement4);
	  
	  NewElement4 = NULL;
	  
	  VariableStartIndex = -1;
	  
	  VariableLength = 1;
	}
      
      if(InsertFlag)
	{
	  Elements.push_back(NewElement1);
	  
	  NewElement1 = NULL;
	  
	  InsertFlag = false;
	}
    }
  
  if(OperatorStartIndex > -1)
    {
      NewElement2->Name  = Expression.mid(OperatorStartIndex, OperatorLength);
      NewElement2->Type  = OperatorType;
      NewElement2->Index = OperatorStartIndex;
      
      Elements.push_back(NewElement2);
    }
  
  if(NumberStartIndex > -1)
    {
      NewElement3->Name  = Expression.mid(NumberStartIndex, NumberLength);
      NewElement3->Type  = NumberType;
      NewElement3->Index = NumberStartIndex;
      
      Elements.push_back(NewElement3);
    }
  
  if(VariableStartIndex > -1)
    {
      NewElement4->Name  = Expression.mid(VariableStartIndex, VariableLength);
      NewElement4->Type  = VariableType;
      NewElement4->Index = VariableStartIndex;
      
      Elements.push_back(NewElement4);
    }
  
  return true;
}

bool BExpression::parse()
{
  vector <Element *> ContextRoot;
  ContextRoot.push_back(NULL);
  
  vector <bool> PriorityContext;
  vector <int > ParentPriority;//in Element?
  
  PriorityContext.push_back(false);
  ParentPriority.push_back(0);
  
  for(int i = 0; i < Elements.size(); i++)
    {
      if(Elements[i]->Type == OBracketType)
	{
	  ContextRoot.push_back(NULL);
	  PriorityContext.push_back(false);
	  ParentPriority.push_back(0);
	  
	  delete Elements[i];
	  
	  Elements[i] = NULL;
	}
      
      else if(Elements[i]->Type == CBracketType)
	{
	  if(ContextRoot.size() == 1)
	    {
	      setError(QObject::trUtf8("Extra closing bracket (index %1)") .arg(Elements[i]->Index));
	      
	      return false;
	    }
	  
	  if(i < Elements.size()-1 && Elements[i+1]->Type != CBracketType && Elements[i+1]->Type != OperatorType)
	    {
	      setError(QObject::trUtf8("Element '%1' (index %2) not allowed after a closing bracket. Expecting an operator or a closing bracket") .arg(Elements[i+1]->Name) .arg(Elements[i+1]->Index));
	      
	      return false;
	    }
	  
	  Element * CurrentRoot = ContextRoot.back();
	  
	  if(PriorityContext.back())
	    {
	      ContextRoot.pop_back();
	      PriorityContext.pop_back();
	      ParentPriority.pop_back();
	      
	      CurrentRoot->Parent = ContextRoot.back();//?
	      //parent?
	      ContextRoot.back()->Children.push_back(CurrentRoot);//safe?
	      
	      CurrentRoot = ContextRoot.back();
	    }
	  
	  ContextRoot.pop_back();
	  PriorityContext.pop_back();
	  ParentPriority.pop_back();
	  
	  if(ContextRoot.back() && ContextRoot.back()->Type == FunctionType && ContextRoot.back()->Parent)
	    {
	      if(CurrentRoot)
		{
		  CurrentRoot->Parent = ContextRoot.back();//?
		  //parent?
		  ContextRoot.back()->Children.push_back(CurrentRoot);
		  
		  CurrentRoot = NULL;
		}
	      
	      ContextRoot.back() = ContextRoot.back()->Parent;
	    }
	  
	  if(CurrentRoot)
	    {
	      if(!ContextRoot.back())
		ContextRoot.back() = CurrentRoot;
	      else
		{
		  CurrentRoot->Parent = ContextRoot.back();//?
		  //parent?
		  ContextRoot.back()->Children.push_back(CurrentRoot);
		}
	    }
	  
	  delete Elements[i];
	  
	  Elements[i] = NULL;
	}
      
      else if(Elements[i]->Type == OperatorType)
	{
	  if(!Elements[i]->define())
	    {
	      setError(QObject::trUtf8("Undefined operator '%1' (index %2)") .arg(Elements[i]->Name) .arg(Elements[i]->Index));
	      
	      return false;
	    }
	  
	  if(!ContextRoot.back())
	    ContextRoot.back() = Elements[i];
	  else
	    {
	      if(ContextRoot.back()->Type == OperatorType && ContextRoot.back()->Priority > Elements[i]->Priority && ContextRoot.back()->Depth <= Elements[i]->Depth)
		{
		  ContextRoot.back()->Children.back()->Parent = Elements[i];
		  //parent?
		  Elements[i]->Children.push_back(ContextRoot.back()->Children.back());//is this justified? and safe?
		  ContextRoot.back()->Children.pop_back();
		  
		  ParentPriority.push_back(ContextRoot.back()->Priority);
		  ContextRoot.push_back(Elements[i]);
		  PriorityContext.push_back(true);
		}
	      else
		{
		  if(PriorityContext.back() && ParentPriority.back() <= Elements[i]->Priority)
		    {
		      Element * CurrentRoot = ContextRoot.back();
		      
		      ContextRoot.pop_back();
		      PriorityContext.pop_back();
		      ParentPriority.pop_back();
		      
		      CurrentRoot->Parent = ContextRoot.back();//?
		      //parent?
		      ContextRoot.back()->Children.push_back(CurrentRoot);//safe?
		    }
		  
		  ContextRoot.back()->Parent = Elements[i];//?
		  
		  Elements[i]->Children.push_back(ContextRoot.back());
		  
		  ContextRoot.back() = Elements[i];
		}
	    }
	}
      
      else
	/* Number, variable, function */
	{
	  if(Elements[i]->Type == NumberType)
	    Elements[i]->define();
	  
	  if(Elements[i]->Type == VariableType && !Elements[i]->define(&Variables))
	    {
	      setError(QObject::trUtf8("Undefined variable '%1' (index %2)") .arg(Elements[i]->Name) .arg(Elements[i]->Index));
	      
	      return false;
	    }
	  
	  if(Elements[i]->Type == FunctionType && !Elements[i]->define())
	    {
	      setError(QObject::trUtf8("Undefined function '%1' (index %2)") .arg(Elements[i]->Name) .arg(Elements[i]->Index));
	      
	      return false;
	    }
	  
	  if(!ContextRoot.back())
	    ContextRoot.back() = Elements[i];
	  else
	    {
	      if(i < Elements.size()-1 && Elements[i+1]->Type != CBracketType && Elements[i+1]->Type != OperatorType && Elements[i]->Type != FunctionType)
		{
		  setError(QObject::trUtf8("Expecting a closing bracket or an operator after element '%1' (index %2)") .arg(Elements[i]->Name) .arg(Elements[i]->Index));
		  
		  return false;
		}
	      
	      Elements[i]->Parent = ContextRoot.back();//?
	      
	      ContextRoot.back()->Children.push_back(Elements[i]);
	      
	      if(Elements[i]->Type == FunctionType)
		ContextRoot.back() = Elements[i];
	    }
	}
    }
  
  if(PriorityContext.back())
    {
      Element * CurrentRoot = ContextRoot.back();
      
      ContextRoot.pop_back();
      PriorityContext.pop_back();
      ParentPriority.pop_back();
      
      CurrentRoot->Parent = ContextRoot.back();//?
      //parent?
      ContextRoot.back()->Children.push_back(CurrentRoot);//safe?
    }
  
  if(ContextRoot.size() > 1)
    {
      setError(QObject::trUtf8("%1 closing brackets are missing") .arg(ContextRoot.size()-1));
      
      return false;
    }
  
  TreeRoot = ContextRoot.back();
  
  return true;
}

void BExpression::setError(QString NewErrorMessage, bool NewEvaluateErrorFlag)
{
  ErrorFlag         = !NewEvaluateErrorFlag;
  EvaluateErrorFlag =  NewEvaluateErrorFlag;
  
  ErrorMessage = NewErrorMessage;
}

bool BExpression::check()
{
  return checkElement(TreeRoot);
}

void BExpression::write()
{
  Expression = "";
  
  writeElement(TreeRoot);
}

void BExpression::print()
{
  printf("#########################################\n");
  
  printf("Expression: %s\n", (const char *)(Expression));
  
  /*
    printf("\nBExpression::lex()\nNumber of elements detected: %d\n", Elements.size());
    
    for(int i = 0; i < Elements.size(); i++)
    printf("Element %3d Type=%3d Depth=%3d: %s\n", i, Elements[i]->Type, Elements[i]->Depth, (const char *)(Elements[i]->Name));
  */
  
  if(TreeRoot)
    {
      printf("\nBExpression::parse()\nTreeRoot = %d\n", TreeRoot->Index);
      
      printElement(TreeRoot);
    }
  
  printf("#########################################\n");
}

void BExpression::del()
{
  for(int i = 0; i < Elements.size(); i++)
    if(Elements[i])
      {
	Elements[i]->Children.clear();
	
	delete Elements[i];
      }
  
  Elements.clear();
}

bool BExpression::checkElement(Element * E)
{
  if(E->Type == OperatorType)
    {
      if(E->Children.size() < E->OperandsMin || E->Children.size() > E->OperandsMax)
	{
	  setError(QObject::trUtf8("Wrong operands number for operator '%1' (index %2)") .arg(E->Name) .arg(E->Index));
	  
	  return false;
	}
      
      if(E->OperandsMin == 1 && E->Children.size() == 1 && E->Index > E->Children[0]->Index)
	{
	  setError(QObject::trUtf8("Operand cannot preceed unary operator '%1' (index %2)") .arg(E->Name) .arg(E->Index));
	  
	  return false;
	}
    }
  
  if(E->Type == FunctionType)
    {
      if(E->Children.size() < E->ArgumentsMin || E->Children.size() > E->ArgumentsMax)
	{
	  setError(QObject::trUtf8("Wrong arguments number for function '%1' (index %2)") .arg(E->Name) .arg(E->Index));
	  
	  return false;
	}
    }
  
  for(int i = 0; i < E->Children.size(); i++)
    if(!checkElement(E->Children[i]))
      return false;
  
  return true;
}

void BExpression::writeElement(Element * E)
{
  if(E->Type == OperatorType)
    {
      if(E->Children.size() == 1)
	{
	  Expression += E->Name;
	  
	  if(E->Children[0]->Type == OperatorType)
	    Expression += OpeningBracket;
	  
	  writeElement(E->Children[0]);
	  
	  if(E->Children[0]->Type == OperatorType)
	    Expression += ClosingBracket;
	}
      else
	/* 2 */
	{
	  bool Flag0 = false;
	  bool Flag1 = false;
	  
	  if(
	     E->Children[0]->Type == OperatorType
	     &&
	     (
	      (E->Children[0]->Children.size() == 1 && !Expression.isEmpty() && Expression.right(1) != OpeningBracket)
	      ||
	      E->Priority < E->Children[0]->Priority
	      )
	     )
	    {
	      Expression += OpeningBracket;
	      
	      Flag0 = true;
	    }
	  
	  writeElement(E->Children[0]);
	  
	  if(Flag0)
	    Expression += ClosingBracket;
	  
	  Expression += E->Name;
	  
	  if(
	     E->Children[1]->Type == OperatorType
	     &&
	     (
	      E->Children[1]->Children.size() == 1
	      ||
	      (E->Children[1]->Children.size() == 2 && E->Priority == E->Children[1]->Priority)
	      ||
	      E->Priority < E->Children[1]->Priority
	      )
	     )
	    {
	      Expression += OpeningBracket;
	      
	      Flag1 = true;
	    }
	  
	  writeElement(E->Children[1]);
	  
	  if(Flag1)
	    Expression += ClosingBracket;
	}
    }
  
  else if(E->Type == NumberType || E->Type == VariableType)
    Expression += E->Name;
  
  else
    /* Function */
    {
      Expression += E->Name;
      
      Expression += OpeningBracket;
      
      writeElement(E->Children[0]);
      
      Expression += ClosingBracket;
    }
}

void BExpression::printElement(Element * E, QString Prefix)
{
  printf("%s%s\n", (const char *)(Prefix), (const char *)(E->Name));
  
  for(int i = 0; i < E->Children.size(); i++)
    printElement(E->Children[i], Prefix + "\t");
}

Element * BExpression::simplifyElement(Element * E)
{
  /*
    if(EvaluateErrorFlag)
    return;
  */
  
  for(int i = 0; i < E->Children.size(); i++)
    E->Children[i] = simplifyElement(E->Children[i]);
  
  Element * NewElement = E;
  
  if(E->Type == OperatorType)
    {
      /*
	if(E->Children.size() == 1 && E->Name == OperatorAdd)
	{
	Element * Tmp = E->Children[0];
	
	E->Name     = E->Children[0]->Name;
	E->Type     = E->Children[0]->Type;
	E->Children = E->Children[0]->Children;
	
	delete Tmp;
	}
      */
      if(E->Children.size() == 2)
	{
	  if(E->Children[0]->Type == NumberType && E->Children[1]->Type == NumberType)
	    {
	      double A = evaluateElement(E);
	      
	      if(!EvaluateErrorFlag && (isInt(A) || !isInt(E->Children[0]->NumberValue) || !isInt(E->Children[1]->NumberValue)))
		{
		  NewElement = NumElement(QString("%1") .arg(A));
		  
		  NewElement->Parent = E->Parent;
		  
		  delete E;
		}
	    }
	  else
	    {
	      if(E->ZeroElement0Flag && E->Children[0]->Type == NumberType && E->Children[0]->NumberValue == E->ZeroElement0)
		{
		  NewElement = E->Children[0]->copy();
		  
		  NewElement->Parent = E->Parent;
		  
		  delete E;
		}
	      
	      else if(E->ZeroElement1Flag && E->Children[1]->Type == NumberType && E->Children[1]->NumberValue == E->ZeroElement1)
		{
		  NewElement = E->Children[1]->copy();
		  
		  NewElement->Parent = E->Parent;
		  
		  delete E;
		}
	      
	      else if(E->NeutralElement0Flag && E->Children[0]->Type == NumberType && E->Children[0]->NumberValue == E->NeutralElement0)
		{
		  NewElement = E->Children[1]->copy();
		  
		  NewElement->Parent = E->Parent;
		  
		  delete E;
		}
	      
	      else if(E->NeutralElement1Flag && E->Children[1]->Type == NumberType && E->Children[1]->NumberValue == E->NeutralElement1)
		{
		  NewElement = E->Children[0]->copy();
		  
		  NewElement->Parent = E->Parent;
		  
		  delete E;
		}
	    }
	  
	  /*
	    if(
	    (E->Name == OperatorAdd || E->Name == OperatorSub)
	    &&
	    (E->Children[1]->Name == OperatorAdd || E->Children[1]->Name == OperatorSub)
	    && E->Children[1]->Children.size() == 1
	    )
	    {
	    Element * Tmp = E->Children[1];
	    
	    if(E->Name == E->Children[1]->Name)
	    E->Name = OperatorAdd;
	    else
	    E->Name = OperatorSub;
	    
	    E->Children[1] = E->Children[1]->Children[0];
	    
	    delete Tmp;
	    }
	  */
	}
    }
  
  else if(E->Type == FunctionType)
    {
      if(E->Children[0]->Type == NumberType)
	{
	  double A = evaluateElement(E);
	  
	  if(!EvaluateErrorFlag && isInt(A))
	    {
	      NewElement = NumElement(QString("%1") .arg(A));
	      
	      NewElement->Parent = E->Parent;
	      
	      delete E;
	    }
	}
    }
  
  return NewElement;
}

double BExpression::evaluateElement(Element * E)
{
  if(EvaluateErrorFlag)
    return 0.;
  
  if(E->Type == OperatorType)
    {
      double A = 0.;
      double B = 0.;
      
      if(E->Children.size() == 1)
	B = evaluateElement(E->Children[0]);
      else
	/* 2 */
	{
	  A = evaluateElement(E->Children[0]);
	  B = evaluateElement(E->Children[1]);
	}
      
      if(E->CheckOperator && !E->CheckOperator(A, B))
	{
	  setError(QObject::trUtf8("Bad operands for operator '%1' (index %2)") .arg(E->Name) .arg(E->Index), true);
	  
	  return 0.;
	}
      
      return E->Operator(A, B);
    }
  
  else if(E->Type == NumberType)
    return E->NumberValue;
  
  else if(E->Type == VariableType)
    return Variables[E->VariableIndex].Value;
  
  else
    /* Function */
    {
      double A = evaluateElement(E->Children[0]);
      
      if(E->CheckFunction && !E->CheckFunction(A))
	{
	  setError(QObject::trUtf8("Bad argument for function '%1' (index %2)") .arg(E->Name) .arg(E->Index), true);
	  
	  return 0.;
	}
      
      return E->Function(A);
    }
}

Element * BExpression::deriveElement(Element * E)
{
  Element * NewElement;
  
  if(E->Type == OperatorType)
    {
      if(E->Children.size() == 1)
	NewElement = OperElements(E->Name, deriveElement(E->Children[0]));/* E->Children[0] is detached and deleted here! */
      else
	/* 2 */
	{
	  if(E->Name == OperatorMul)
	    {
	      Element * Child0 = E->Children[0]->copy();
	      Element * Child1 = E->Children[1]->copy();
	      
	      Element * Element1 = OperElements(OperatorMul, deriveElement(E->Children[0]), Child1                       );/* E->Children[0] is detached and deleted here! */
	      Element * Element2 = OperElements(OperatorMul, Child0                       , deriveElement(E->Children[1]));/* E->Children[1] is detached and deleted here! */
	      
	      NewElement = OperElements(OperatorAdd, Element1, Element2);
	    }
	  
	  else if(E->Name == OperatorDiv)
	    {
	      Element * Child0  = E->Children[0]->copy();
	      Element * Child10 = E->Children[1]->copy();
	      Element * Child11 = E->Children[1]->copy();
	      
	      Element * Element1 = OperElements(OperatorMul, deriveElement(E->Children[0]), Child10                      );/* E->Children[0] is detached and deleted here! */
	      Element * Element2 = OperElements(OperatorMul, Child0                       , deriveElement(E->Children[1]));/* E->Children[1] is detached and deleted here! */
	      Element * Element3 = OperElements(OperatorSub, Element1                     , Element2                     );
	      Element * Element4 = OperElements(OperatorExp, Child11                      , NumElement(NumberTwo)        );
	      
	      NewElement = OperElements(OperatorDiv, Element3, Element4);
	    }
	  
	  else if(E->Name == OperatorExp)
	    {
	      Element * Elem    = E->copy();
	      Element * Child00 = E->Children[0]->copy();
	      Element * Child01 = E->Children[0]->copy();
	      Element * Child10 = E->Children[1]->copy();
	      Element * Child11 = E->Children[1]->copy();
	      
	      Element * E1 = OperElements(OperatorSub, Child10                          , NumElement(NumberOne));
	      Element * E2 = OperElements(OperatorExp, Child00                          , E1                   );
	      Element * E3 = OperElements(OperatorMul, deriveElement(E->Children[0])    , E2                   );/* E->Children[0] is detached and deleted here! */
	      Element * E4 = OperElements(OperatorMul, Child11                          , E3                   );
	      Element * E5 = OperElements(OperatorMul, deriveElement(E->Children[1])    , Elem                 );/* E->Children[1] is detached and deleted here! */
	      Element * E6 = OperElements(OperatorMul, FuncElement(FunctionLog, Child01), E5                   );
	      
	      NewElement = OperElements(OperatorAdd, E4, E6);
	    }
	  
	  else
	    NewElement = OperElements(E->Name, deriveElement(E->Children[0]), deriveElement(E->Children[1]));
	}
    }
  
  else if(E->Type == NumberType || (E->Type == VariableType && E->Name != Variables[0].Name))
    NewElement = NumElement(NumberZero);
  
  else if(E->Type == VariableType)
    NewElement = NumElement(NumberOne);
  
  else
    /* Function */
    {
      Element * Child = E->Children[0]->copy();
      
      if(E->DerivativeFunction)
	NewElement = OperElements(OperatorMul, deriveElement(E->Children[0]), E->DerivativeFunction(Child)     );/* E->Children[0] is detached and deleted here! */
      else
	NewElement = OperElements(OperatorMul, deriveElement(E->Children[0]), FuncElement(E->Derivative, Child));/* E->Children[0] is detached and deleted here! */
    }
  
  E->detach();
  
  delete E;
  
  return NewElement;
}
