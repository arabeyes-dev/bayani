/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Bayani definition of the FITSImage Class.
 *
 * (c) Copyright 2002-2004, Arabeyes, Youcef Rabah Rahal.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Bayani is licensed under the GPL license. See COPYING
 *  file.
 *
 ************************************************************************/

#ifndef BAYANI_FITSIMAGE_H
#define BAYANI_FITSIMAGE_H

/* "Usual" headers */
#include <stdio.h>
#include <math.h>
#include <fstream>
#include <vector>
/* QT headers */
#include <qstring.h>

#define FITSKEYLENGTH 80
#define FITSBLKLENGTH 2880
#define FITSVALSEP    "="
#define FITSCOMSEP    "/"

/*!
  \file fitsimage.h
  \brief Header file for the FITSHeader and FITSImage classes.
  
  The FITSHeader and FITSImage classes are defined in this file.
*/

//! Class that defines a FITS image's header.
/*!
  Basic class to manipulate FITS images' headers. It stocks headers and perform basic operations on them.
  \sa FITSImage.
*/
class FITSHeader
{
 protected:
  int    HeaderSize;
  string Header;
  vector <string> Keys;
  vector <string> Values;
  vector <string> Comments;
  
  void ParseHeader();
  
 public:
         FITSHeader   ();
        ~FITSHeader   ();
  void   ReadHeader   (string, int);
  int    GetKeysNumber();
  string GetValue     (string);
  string GetHeader    ();
  int    GetHeaderSize();
};

//! Class that defines a FITS image's data.
/*!
  Basic class to manipulate FITS images' data. It can manipulate headers (via a FITSHeader object member) and read/write FITS data.
*/
class FITSImage
{
 protected:
  bool       BigEndian;
  int        BitPix;
  int        NAxis;
  int        XSize;
  int        YSize;
  int        DataSize;
  ushort     Min;
  ushort     Max;
  double     MinDyn;
  double     MaxDyn;
  double     Mean;
  double     Mean2;
  double     Sigma;
  ushort **  Data;
  FITSHeader Header;
  
  bool   IsBigEndian  ();
  ushort ConvertEndian(ushort);
  
 public:
         FITSImage();
        ~FITSImage();
  void   ReadFile (QString);
  void   WriteFile(QString);
  int    GetXSize ();
  int    GetYSize ();
  ushort GetPixel (int, int);
  void   PutPixel (int, int, ushort);
  ushort GetMin   ();
  ushort GetMax   ();
  double GetMinDyn();
  double GetMaxDyn();
};

#endif
