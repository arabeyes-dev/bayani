#!/bin/bash

#--
# A small script to create the Bayani documentation.
# Make sure ldp.dsl is present in the corresponding directory.
# $Id$
#--

if [ $# != 0 ]; then
    echo Usage:  ./makedoc.sh
    exit
fi

if [ ! -e i18n.pro ]; then
    echo Error: This script needs to be called from its own directory
    exit
fi

echo Creating user documentation
cd ../doc/user/
rm -f *html
jade -E 30000000 -i html -t xml -d /usr/share/sgml/docbook/stylesheet/dsssl/ldp/ldp.dsl#html bayani.xml
cd -
echo Creating user documentation: done

echo Creating developer documentation
cd ../doc/developer/
rm -rf html/ latex/
doxygen doxyfile
cd -
echo Creating developer documentation: done
