#--
# Bayani ChangeLog file.
# $Id$
#--

- The Arabic and French QT translations are now included.
- The technical Arabic terms are now in accordance with the ALECSO
  recommendations.
- Added demo files.
- Possible default options reading/writing from/to '~/.bayani/settingsrc'.
- MDI is supported.
- Implemented a script window in order to execute command line actions and ease
  repetitive tasks.
  Possibility to execute a macro from a file.
  The last entered commands are saved into a '~/.bayani/history.bcf' file
  at exit and reloaded at start.
- Implemented SymbComp class: possibility to derive expressions such as
  '(x+1)*sin(x)'.
- The fit is possible now with interpreted expressions (still invisible fot the
  user).
- It is possible now to set graph's bounds.
- Nicer graph's layout
- It is possible to view coordinates on the status bar.
- Added new shapes in the drawn points (squares).
- Added a feature to display 2D FITS images (astronomical format).
- Skeleton for a more generic Graph class + Histo1D and FITSContainer classes.
- Use of QT 3.2.3 (removed QT 3.0 support ?).
- Introduced a histogram and a dataarray class.
- Possibility to plot histograms (with adjustable graphical settings).
- Functions are now plotted as 'continuous' lines and not a serie of data.
- Possibility to plot data with errors (even asymmetric).
- Possibility to choose logarithmic axes (for positive values).
- Possibility to reverse axes direction.
- Added a splash screen.
- Reststructed the code (class hierarchy)
- Code optimization.
- New tools to manage precisely the data table.
- Possibility to save the table in a text file.
- Possibility to impose cuts on drawn data (from the table)
- Fitting:
  + Analytical fits up to 2-degree polynomials.
  + Numerical fits with a 3-degree polynomial.
  + The fit result is considered as a function (and not as a serie of points).
  + Possibility to fit histograms too.
- Possibility to use expressions (combinations) of table columns to plot data.
- Bug fixes:
  a- Segfault when intiliazing parametres one by one in the Fit class.
  b- Bug that caused useless refresh at every paintevent.
  c- Bug that caused a segfault when the frame was too small.


JAN 26, 2003
------------
0.1a
----
- Possibility to plot graphs from data.
  A table can be filled either by hand or directly from a text file.
- Possibility to plot complex function expressions thanks to an internal
  interpreter.
- Possibilty to fit a linear function on the data.
- Total Arabic interface with UTF-8 strings handling.
- Possibilty to save into a PNG image.
- Total English and French translations. Everything works with the same
  efficiency in all the versions, even the internal interpreter (in the
  appropriate language).
